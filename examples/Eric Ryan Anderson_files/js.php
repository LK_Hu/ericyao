function DesignX() {

var ACCOUNT_MODEL = {"ownerName":"Eric Ryan Anderson","businessName":"Eric Ryan Anderson Photography","phone":"214.288.3130","mobilePhone":"","streetAddress":"243 St Marks Ave #8 Brooklyn NY 11238","email":"studio@ericryananderson.com","pageStatistics":"<script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','\/\/www.google-analytics.com\/analytics.js','ga');  ga('create', 'UA-46897400-1', 'ericryananderson.com');  ga('send', 'pageview');<\/script>","signUpStatus":"","signUpLink":"","signUpToken":"","fotoMotoId":""};

var LAYOUT_MODEL = {"captionBackgroundAlpha":0.9,"captionBackgroundRectColor":"#000000","captionFont":"Varela","captionFontColor":"#ffffff","captionFontSize":12,"colorCorrection":false,"contactFont":"Varela","contactFontColor":"#F5C131","contactFieldRectColor":"#919191","contactFontSize":16,"contactTitleShow":true,"contactTitleFont":"Signika Negative Light","contactTitleFontSize":26,"containerColor":"#191919","containerHeight":"FLUID","containerWidth":"FLUID","copyrightAlignHorizontal":"left","copyrightAlignVertical":"bottom","copyrightOffsetX":15,"copyrightColor":"#989898","copyrightFont":"Varela","copyrightFontSize":"12px","copyrightOffsetY":-10,"cursorBlendMode":"difference","cursorColor":"#F5C131","cursorIconStyle":"plus2","defaultVideoScale":"no stretch","imageAlignHorizontal":"center","imageScaleType":"fit","introFont":"Georgia","introFontColor":"#CC0000","introFontSize":20,"introFile":"","introText":"","introType":"none","landingMediaScaleType":"fillSite","landingMediaSpeed":2,"landingMediaTransitionDuration":0.5,"logoAlignHorizontal":"left","logoAlignVertical":"top","logoFile":"ERALOGO.png","logoFont":"Raleway-Thin","logoFontColor":"#A8A8A8","logoFontSize":42,"logoOffsetX":20,"logoOffsetY":10,"logoText":"","logoType":"file","menuAlignHorizontal":"left","menuAlignVertical":"top","menuBgAlpha":0,"menuBgColor":"#191919","menuBulletType":"arrow","menuBulletOffsetHorizontal":-2,"menuBulletOffsetVertical":-3,"menuSubBulletOffsetHorizontal":-2,"menuSubBulletOffsetVertical":-2,"menuExpandAll":false,"menuFont":"Questrial","menuFontColor":"#bababa","menuFontColorHover":"#ff9933","menuFontColorSelected":"#ffffff","menuFontSize":14,"menuOffsetX":0,"menuOffsetY":0,"menuScrollbarAlign":"left","menuScrollbarColor":"#262626","menuScrollbarHover":"#343434","menuSubFontSize":13,"menuSubGapLeading":8,"menuSubGapTrailing":11,"menuSubIndent":0,"menuSubTextGap":5,"menuTextAlignHorizontal":"left","menuTextAlignVertical":"top","menuTextGap":0,"menuTextPaddingHorizontal":33,"menuTextPaddingVertical":100,"menuWidth":210,"navbarAlignHorizontal":"right","navbarAlignVertical":"bottom","navbarColor":"#C7C7C7","navbarColorDeselected":"#556699","navbarColorHover":"#F5C131","navbarFont":"Signika Negative Light","navbarFontSize":13,"navbarGap":10,"navbarIconOffsetX":-3,"navbarIconOffsetY":1,"navbarOffsetX":-30,"navbarOffsetY":-10,"overlayAlpha":1,"overlayColor":"#191919","pageHorizontalMargin":70,"pageMarginTop":"70px","pageMarginRight":"30px","pageMarginBottom":"20px","pageMarginLeft":"10%","pageVerticalMargin":50,"pageScrollbarAlignment":"right","pageScrollbarColor":"#545454","pageScrollbarHorizontalOffset":30,"pageScrollbarHover":"#A9A9A9","pageScrollbarVerticalOffset":0,"pageTextFont":"Questrial","pageTextFontColor":"#6b6b6b","pageTextFontSize":14,"pageTitle":false,"pageTitleFont":"Source Sans Pro ExtraLight","pageTitleFontColor":"#A8A8A8","pageTitleFontSize":40,"sitePaddingBottom":30,"sitePaddingLeft":0,"sitePaddingRight":0,"sitePaddingTop":50,"siteBackgroundColor":"#191919","thumbnailGap":20,"thumbnailGridHorizontalMargin":50,"thumbnailGridVerticalMargin":50,"thumbnailGridTransparency":0.85,"thumbnailHoverColor":"#d9d9d9","thumbnailScrollbarAlignment":"right","thumbnailScrollbarColor":"#989898","thumbnailScrollbarHorizontalOffset":26,"thumbnailScrollbarHover":"#bababa","thumbnailScrollbarVerticalOffset":0,"thumbnailScrollType":"scrollbar","thumbnailSelectedColor":"#ffffff","thumbnailSize":"large","thumbnailTitleFont":"Arial","thumbnailTitleFontColor":"#989898","thumbnailTitleFontSize":16,"transitionDuration":1,"transitionType":"fade","videoAutoPlay":false,"captionDefault":false,"imageVideoMarginTop":"0px","imageVideoMarginRight":"0px","imageVideoMarginBottom":"0px","imageVideoMarginLeft":"0px","thumbnailHoverAlpha":0.05,"imageAlignVertical":"center","tabletLogoFile":"","mobileLogoFile":"","fotomotoToolbarAlignHorizontal":"right","fotomotoToolbarAlignVertical":"top","fotomotoToolbarOffsetX":0,"fotomotoToolbarOffsetY":0,"landingMediaAlignHorizontal":"center","landingMediaAlignVertical":"top","landingMediaRandomize":false,"tabletLogoOffsetX":0,"tabletLogoOffsetY":0};

var MEDIA_MODEL = {"10000":{"id":10000,"size":0,"label":"contact","type":"contactForm","content":"","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"10001":{"id":10001,"size":0,"label":"APF_logo_nostripe.jpg","type":"image","content":"APF_logo_nostripe.jpg","thumb":"APF_logo_nostripe.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10002":{"id":10002,"size":0,"label":"ABOUT","type":"html","content":"<p>Eric Ryan Anderson hails from the State of Texas and currently lives in Brooklyn with his lovely wife.<\/p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 14px;\">\u00c2\u00a0<a href=\"mailto:studio@ericryananderson.com\" target=\"_blank\">studio@ericryananderson.com<\/a>\u00c2\u00a0- <a href=\"http:\/\/www.twitter.com\/anderson_eric\" target=\"_blank\">@anderson_eric<\/a>\u00c2\u00a0-\u00c2\u00a0\u00c2\u00a0<a href=\"http:\/\/www.facebook.com\/ericryanandersonphoto\" target=\"_blank\" style=\"text-align: -webkit-auto;\">Facebook<\/a><\/span><\/p>\r\n<p>Clients include Draft FCB, AKQA, Viacom\/MTV, Atlantic Records, Ernest Alexander New York, Desiron|Frank Carfaro, Fueled by Ramen, Target, Tamara Magel Design, EMI Records, Capitol Virgin, Razor & Tie, Paste Magazine, Harwood International, Trivate Music, Alternative Press, Hanover, Duke Realty, Integrity Music & Churchmedia.<span style=\"color: #000000;\">\u00c2\u00a0<\/span><\/p>\r\n<p>His work has been published in Italian Vanity Fair, Vogue Hommes, Computer Arts, Sherman's Travel, T:New York Times Style, Pollstar, D Magazine, Dallas Observer, Conde Nast Traveler, Taschen Hotels Now, Businessweek and National Geographic Traveler.\u00c2\u00a0<br \/><span style=\"color: #000000;\">\u00c2\u00a0<\/span><\/p>\r\n\r\n<p><span style=\"color: #000000;\"><br \/><\/span><\/p>\r\n<p><span style=\"color: #000000;\"><br \/><\/span><\/p>","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"10003":{"id":10003,"size":0,"label":"BLOG","type":"link","content":"REDIRECT:http:\/\/ivegotfriends.com","thumb":"","linkTarget":"_blank","caption":"","featuredImage":"","filters":""},"10004":{"id":10004,"size":0,"label":"MISC","type":"link","content":"REDIRECT:http:\/\/ericryananderson.tumblr.com","thumb":"","linkTarget":"_blank","caption":"","featuredImage":"","filters":""},"11561":{"id":11561,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA140616_Interpol_098B","type":"image","content":"ERA140616_Interpol_098B.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"10006":{"id":10006,"size":0,"label":"PDF","type":"link","content":"REDIRECT:http:\/\/ericryananderson.com\/pdf","thumb":"","linkTarget":"_blank","caption":"","featuredImage":"","filters":""},"10007":{"id":10007,"size":0,"label":"WORK","type":"html","content":"AUGUST INTERN\/ASSISTANT\r\n\r\nWe have a busy August coming up, and are seeking 1-2 paid intern\/assistants to help navigate a wide range of duties over the coming weeks.  You'll be helping with production and execution on several upcoming projects, including a lifestyle campaign shoot, a travel shoot for a furniture designer, a music video project and on-going documentary film project.  In addition to actual on set assisting, looking for someone who can help with production around the studio.\r\n\r\nLooking for one or two people to work 4-5 days\/week.  Ideally, you already live in NYC, but open to someone looking to come up for the month. Position is paid based on experience and availability, though travel and lodging are not provided.\r\n\r\nWe need someone who is familiar with Adobe Creative Suite, Final Cut Pro, Canon 5D\/7D workflow and interested in the production side of the business, as well as growing into a studio manager\/first assistant position.  Please tell us who you are and why you're interested!  Looking to fill positions and get started by August 2.\r\n\r\n<a href=\"mailto:studio@ericryananderson.com\">studio@ericryananderson.com<\/a>\r\n","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"10008":{"id":10008,"size":0,"label":"rein.jpg","type":"image","content":"rein.jpg","thumb":"rein.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"BOOK ONE"},"10009":{"id":10009,"size":0,"label":"IMG_7956.jpg","type":"image","content":"IMG_7956.jpg","thumb":"IMG_7956.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10010":{"id":10010,"size":0,"label":"LASTROYALS-12.jpg","type":"image","content":"LASTROYALS-12.jpg","thumb":"LASTROYALS-12.jpg","linkTarget":"","caption":"Last Royals","featuredImage":"","filters":""},"10011":{"id":10011,"size":0,"label":"march_017.jpg","type":"image","content":"march_017.jpg","thumb":"march_017.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10012":{"id":10012,"size":0,"label":"ALEXANDER-6260b.jpg","type":"image","content":"ALEXANDER-6260b.jpg","thumb":"ALEXANDER-6260b.jpg","linkTarget":"","caption":"ERNEST ALEXANDER FW 2010","featuredImage":"","filters":"Ernest Alexander"},"10013":{"id":10013,"size":0,"label":"untitled-9632.jpg","type":"image","content":"untitled-9632.jpg","thumb":"untitled-9632.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10014":{"id":10014,"size":0,"label":"730EA_NYLON-1901-2.jpg","type":"image","content":"730EA_NYLON-1901-2.jpg","thumb":"730EA_NYLON-1901-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10015":{"id":10015,"size":0,"label":"ernestalexander_003.jpg","type":"image","content":"ernestalexander_003.jpg","thumb":"ernestalexander_003.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10016":{"id":10016,"size":0,"label":"FIORE_069.jpg","type":"image","content":"FIORE_069.jpg","thumb":"FIORE_069.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10017":{"id":10017,"size":0,"label":"march_016.jpg","type":"image","content":"march_016.jpg","thumb":"march_016.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10018":{"id":10018,"size":0,"label":"JACKIE_001B.jpg","type":"image","content":"JACKIE_001B.jpg","thumb":"JACKIE_001B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10019":{"id":10019,"size":0,"label":"Untitled-1.jpg","type":"image","content":"Untitled-1.jpg","thumb":"Untitled-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10020":{"id":10020,"size":0,"label":"IMG_9871-Edit.jpg","type":"image","content":"IMG_9871-Edit.jpg","thumb":"IMG_9871-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10021":{"id":10021,"size":0,"label":"IMG_6154-Edit.jpg","type":"image","content":"IMG_6154-Edit.jpg","thumb":"IMG_6154-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10022":{"id":10022,"size":0,"label":"IMG_6198-Edit.jpg","type":"image","content":"IMG_6198-Edit.jpg","thumb":"IMG_6198-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10023":{"id":10023,"size":0,"label":"IMG_6476.jpg","type":"image","content":"IMG_6476.jpg","thumb":"IMG_6476.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10024":{"id":10024,"size":0,"label":"IMG_7956.jpg","type":"image","content":"IMG_7956.jpg","thumb":"IMG_7956.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10025":{"id":10025,"size":0,"label":"IMG_9871-Edit.jpg","type":"image","content":"IMG_9871-Edit.jpg","thumb":"IMG_9871-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10026":{"id":10026,"size":0,"label":"LASTROYALS-12.jpg","type":"image","content":"LASTROYALS-12.jpg","thumb":"LASTROYALS-12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10027":{"id":10027,"size":0,"label":"untitled-9632.jpg","type":"image","content":"untitled-9632.jpg","thumb":"untitled-9632.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10028":{"id":10028,"size":0,"label":"EAFALL-1191.jpg","type":"image","content":"EAFALL-1191.jpg","thumb":"EAFALL-1191.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10029":{"id":10029,"size":0,"label":"EAFALL-0062-1.jpg","type":"image","content":"EAFALL-0062-1.jpg","thumb":"EAFALL-0062-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10030":{"id":10030,"size":0,"label":"EAFALL-0451bb.jpg","type":"image","content":"EAFALL-0451bb.jpg","thumb":"EAFALL-0451bb.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10031":{"id":10031,"size":0,"label":"EAFALL-0876-1.jpg","type":"image","content":"EAFALL-0876-1.jpg","thumb":"EAFALL-0876-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10032":{"id":10032,"size":0,"label":"ea-401.jpg","type":"image","content":"ea-401.jpg","thumb":"ea-401.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10033":{"id":10033,"size":0,"label":"EA_FALL_ROID_04b.jpg","type":"image","content":"EA_FALL_ROID_04b.jpg","thumb":"EA_FALL_ROID_04b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10034":{"id":10034,"size":0,"label":"_MG_6306.jpg","type":"image","content":"_MG_6306.jpg","thumb":"_MG_6306.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10035":{"id":10035,"size":0,"label":"_MG_6248.jpg","type":"image","content":"_MG_6248.jpg","thumb":"_MG_6248.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10036":{"id":10036,"size":0,"label":"Marsalis_03d.jpg","type":"image","content":"Marsalis_03d.jpg","thumb":"Marsalis_03d.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10037":{"id":10037,"size":0,"label":"Marsalis_03d.jpg","type":"image","content":"Marsalis_03d.jpg","thumb":"Marsalis_03d.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10038":{"id":10038,"size":0,"label":"HEIGHTS_01c.jpg","type":"image","content":"HEIGHTS_01c.jpg","thumb":"HEIGHTS_01c.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10039":{"id":10039,"size":0,"label":"_MG_1974.jpg","type":"image","content":"_MG_1974.jpg","thumb":"_MG_1974.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10040":{"id":10040,"size":0,"label":"_MG_3013.jpg","type":"image","content":"_MG_3013.jpg","thumb":"_MG_3013.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10041":{"id":10041,"size":0,"label":"FALCHI-0379.jpg","type":"image","content":"FALCHI-0379.jpg","thumb":"FALCHI-0379.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10042":{"id":10042,"size":0,"label":"GRO_01.jpg","type":"image","content":"GRO_01.jpg","thumb":"GRO_01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10043":{"id":10043,"size":0,"label":"JENNY.jpg","type":"image","content":"JENNY.jpg","thumb":"JENNY.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10044":{"id":10044,"size":0,"label":"IMG_7764.jpg","type":"image","content":"IMG_7764.jpg","thumb":"IMG_7764.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10045":{"id":10045,"size":0,"label":"ERICANDERSON_GRO_DPRO_IMG_2188-copy.jpg","type":"image","content":"ERICANDERSON_GRO_DPRO_IMG_2188-copy.jpg","thumb":"ERICANDERSON_GRO_DPRO_IMG_2188-copy.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10046":{"id":10046,"size":0,"label":"_MG_7834.jpg","type":"image","content":"_MG_7834.jpg","thumb":"_MG_7834.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10047":{"id":10047,"size":0,"label":"avett.jpg","type":"image","content":"avett.jpg","thumb":"avett.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10048":{"id":10048,"size":0,"label":"_MG_3155.jpg","type":"image","content":"_MG_3155.jpg","thumb":"_MG_3155.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10049":{"id":10049,"size":0,"label":"_MG_3155.jpg","type":"image","content":"_MG_3155.jpg","thumb":"_MG_3155.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10050":{"id":10050,"size":0,"label":"_MG_7834.jpg","type":"image","content":"_MG_7834.jpg","thumb":"_MG_7834.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10051":{"id":10051,"size":0,"label":"_MG_3550.jpg","type":"image","content":"_MG_3550.jpg","thumb":"_MG_3550.jpg","linkTarget":"","caption":"Langhorne Slim","featuredImage":"","filters":""},"10052":{"id":10052,"size":0,"label":"EASS12_04A.jpg","type":"image","content":"EASS12_04A.jpg","thumb":"EASS12_04A.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10053":{"id":10053,"size":0,"label":"_MG_6546.jpg","type":"image","content":"_MG_6546.jpg","thumb":"_MG_6546.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10054":{"id":10054,"size":0,"label":"_MG_6546.jpg","type":"image","content":"_MG_6546.jpg","thumb":"_MG_6546.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10055":{"id":10055,"size":0,"label":"EASS12_04A.jpg","type":"image","content":"EASS12_04A.jpg","thumb":"EASS12_04A.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10056":{"id":10056,"size":0,"label":"fortlean.jpg","type":"image","content":"fortlean.jpg","thumb":"fortlean.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10057":{"id":10057,"size":0,"label":"lonestar_006.jpg","type":"image","content":"lonestar_006.jpg","thumb":"lonestar_006.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10058":{"id":10058,"size":0,"label":"DESIRON_9198-2.jpg","type":"image","content":"DESIRON_9198-2.jpg","thumb":"DESIRON_9198-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10059":{"id":10059,"size":0,"label":"DESIRON_2436b.jpg","type":"image","content":"DESIRON_2436b.jpg","thumb":"DESIRON_2436b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10060":{"id":10060,"size":0,"label":"DESIRON_7693-3.jpg","type":"image","content":"DESIRON_7693-3.jpg","thumb":"DESIRON_7693-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10061":{"id":10061,"size":0,"label":"FIORE_POLA_05.jpg","type":"image","content":"FIORE_POLA_05.jpg","thumb":"FIORE_POLA_05.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10062":{"id":10062,"size":0,"label":"LENKA.jpg","type":"image","content":"LENKA.jpg","thumb":"LENKA.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10063":{"id":10063,"size":0,"label":"Andrew_Belle011d.jpg","type":"image","content":"Andrew_Belle011d.jpg","thumb":"Andrew_Belle011d.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10064":{"id":10064,"size":0,"label":"AA_03.jpg","type":"image","content":"AA_03.jpg","thumb":"AA_03.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10065":{"id":10065,"size":0,"label":"LENKA.jpg","type":"image","content":"LENKA.jpg","thumb":"LENKA.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10066":{"id":10066,"size":0,"label":"untitled-9326.jpg","type":"image","content":"untitled-9326.jpg","thumb":"untitled-9326.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10067":{"id":10067,"size":0,"label":"MOTH_BRIDGES_01.jpg","type":"image","content":"MOTH_BRIDGES_01.jpg","thumb":"MOTH_BRIDGES_01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10068":{"id":10068,"size":0,"label":"IMG_5360.jpg","type":"image","content":"IMG_5360.jpg","thumb":"IMG_5360.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10069":{"id":10069,"size":0,"label":"IMG_6476.jpg","type":"image","content":"IMG_6476.jpg","thumb":"IMG_6476.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10070":{"id":10070,"size":0,"label":"IMG_6440-Edit.jpg","type":"image","content":"IMG_6440-Edit.jpg","thumb":"IMG_6440-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10071":{"id":10071,"size":0,"label":"IMG_6383.jpg","type":"image","content":"IMG_6383.jpg","thumb":"IMG_6383.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10072":{"id":10072,"size":0,"label":"IMG_6217.jpg","type":"image","content":"IMG_6217.jpg","thumb":"IMG_6217.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10073":{"id":10073,"size":0,"label":"IMG_6154-Edit.jpg","type":"image","content":"IMG_6154-Edit.jpg","thumb":"IMG_6154-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10074":{"id":10074,"size":0,"label":"qIMG_6629.jpg","type":"image","content":"qIMG_6629.jpg","thumb":"qIMG_6629.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10075":{"id":10075,"size":0,"label":"IMG_6079-Edit.jpg","type":"image","content":"IMG_6079-Edit.jpg","thumb":"IMG_6079-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10076":{"id":10076,"size":0,"label":"IMG_6071.jpg","type":"image","content":"IMG_6071.jpg","thumb":"IMG_6071.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10077":{"id":10077,"size":0,"label":"IMG_6065.jpg","type":"image","content":"IMG_6065.jpg","thumb":"IMG_6065.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10078":{"id":10078,"size":0,"label":"IMG_6055.jpg","type":"image","content":"IMG_6055.jpg","thumb":"IMG_6055.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10079":{"id":10079,"size":0,"label":"IMG_6165-Edit.jpg","type":"image","content":"IMG_6165-Edit.jpg","thumb":"IMG_6165-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10080":{"id":10080,"size":0,"label":"IMG_6198-Edit.jpg","type":"image","content":"IMG_6198-Edit.jpg","thumb":"IMG_6198-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10081":{"id":10081,"size":0,"label":"IMG_3246.jpg","type":"image","content":"IMG_3246.jpg","thumb":"IMG_3246.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10082":{"id":10082,"size":0,"label":"BELLOW_03C.jpg","type":"image","content":"BELLOW_03C.jpg","thumb":"BELLOW_03C.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10083":{"id":10083,"size":0,"label":"_MG_2629.jpg","type":"image","content":"_MG_2629.jpg","thumb":"_MG_2629.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10084":{"id":10084,"size":0,"label":"_MG_2061.jpg","type":"image","content":"_MG_2061.jpg","thumb":"_MG_2061.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10085":{"id":10085,"size":0,"label":"APRIL_POLAb.jpg","type":"image","content":"APRIL_POLAb.jpg","thumb":"APRIL_POLAb.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10086":{"id":10086,"size":0,"label":"EA_HASS_04.jpg","type":"image","content":"EA_HASS_04.jpg","thumb":"EA_HASS_04.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10087":{"id":10087,"size":0,"label":"IMG_2585b.jpg","type":"image","content":"IMG_2585b.jpg","thumb":"IMG_2585b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10088":{"id":10088,"size":0,"label":"IMG_8530-Edit.jpg","type":"image","content":"IMG_8530-Edit.jpg","thumb":"IMG_8530-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10089":{"id":10089,"size":0,"label":"PAGUIA-8639.jpg","type":"image","content":"PAGUIA-8639.jpg","thumb":"PAGUIA-8639.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10090":{"id":10090,"size":0,"label":"TARGET_15.jpg","type":"image","content":"TARGET_15.jpg","thumb":"TARGET_15.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10091":{"id":10091,"size":0,"label":"TARGET_16.jpg","type":"image","content":"TARGET_16.jpg","thumb":"TARGET_16.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10092":{"id":10092,"size":0,"label":"TARGET_17.jpg","type":"image","content":"TARGET_17.jpg","thumb":"TARGET_17.jpg","linkTarget":"","caption":"June Ambrose for Target","featuredImage":"","filters":""},"10093":{"id":10093,"size":0,"label":"ANDERSON_JACKIE__MG_6462.jpg","type":"image","content":"ANDERSON_JACKIE__MG_6462.jpg","thumb":"ANDERSON_JACKIE__MG_6462.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10094":{"id":10094,"size":0,"label":"ANDERSON_JACKIE_JACKIE_001B.jpg","type":"image","content":"ANDERSON_JACKIE_JACKIE_001B.jpg","thumb":"ANDERSON_JACKIE_JACKIE_001B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10095":{"id":10095,"size":0,"label":"EASS12_04Ab.jpg","type":"image","content":"EASS12_04Ab.jpg","thumb":"EASS12_04Ab.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10096":{"id":10096,"size":0,"label":"EASS12-6628b.jpg","type":"image","content":"EASS12-6628b.jpg","thumb":"EASS12-6628b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10097":{"id":10097,"size":0,"label":"EASS12-6615.jpg","type":"image","content":"EASS12-6615.jpg","thumb":"EASS12-6615.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10098":{"id":10098,"size":0,"label":"EASS12-6546.jpg","type":"image","content":"EASS12-6546.jpg","thumb":"EASS12-6546.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10099":{"id":10099,"size":0,"label":"EA_HASS_04.jpg","type":"image","content":"EA_HASS_04.jpg","thumb":"EA_HASS_04.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10100":{"id":10100,"size":0,"label":"IMG_9890.jpg","type":"image","content":"IMG_9890.jpg","thumb":"IMG_9890.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10101":{"id":10101,"size":0,"label":"GRO_12.jpg","type":"image","content":"GRO_12.jpg","thumb":"GRO_12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10102":{"id":10102,"size":0,"label":"rector.jpg","type":"image","content":"rector.jpg","thumb":"rector.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10103":{"id":10103,"size":0,"label":"rector3.jpg","type":"image","content":"rector3.jpg","thumb":"rector3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10104":{"id":10104,"size":0,"label":"rectorIMG_8123.jpg","type":"image","content":"rectorIMG_8123.jpg","thumb":"rectorIMG_8123.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10105":{"id":10105,"size":0,"label":"ALEXANDER-6642.jpg","type":"image","content":"ALEXANDER-6642.jpg","thumb":"ALEXANDER-6642.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10106":{"id":10106,"size":0,"label":"ianwalsh.jpg","type":"image","content":"ianwalsh.jpg","thumb":"ianwalsh.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10107":{"id":10107,"size":0,"label":"LASTROYALS-49.jpg","type":"image","content":"LASTROYALS-49.jpg","thumb":"LASTROYALS-49.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10108":{"id":10108,"size":0,"label":"nathaniel.jpg","type":"image","content":"nathaniel.jpg","thumb":"nathaniel.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10109":{"id":10109,"size":0,"label":"rein.jpg","type":"image","content":"rein.jpg","thumb":"rein.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10110":{"id":10110,"size":0,"label":"AA_03.jpg","type":"image","content":"AA_03.jpg","thumb":"AA_03.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10111":{"id":10111,"size":0,"label":"Andrew_Belle011d.jpg","type":"image","content":"Andrew_Belle011d.jpg","thumb":"Andrew_Belle011d.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10112":{"id":10112,"size":0,"label":"IMG_9890.jpg","type":"image","content":"IMG_9890.jpg","thumb":"IMG_9890.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10113":{"id":10113,"size":0,"label":"_MG_8460b.jpg","type":"image","content":"_MG_8460b.jpg","thumb":"_MG_8460b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10114":{"id":10114,"size":0,"label":"_MG_7807-Edit.jpg","type":"image","content":"_MG_7807-Edit.jpg","thumb":"_MG_7807-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10115":{"id":10115,"size":0,"label":"MERMAID_APF_02.jpg","type":"image","content":"MERMAID_APF_02.jpg","thumb":"MERMAID_APF_02.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10116":{"id":10116,"size":0,"label":"MERMAID_APF_01.jpg","type":"image","content":"MERMAID_APF_01.jpg","thumb":"MERMAID_APF_01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10117":{"id":10117,"size":0,"label":"MERMAID_APF_12.jpg","type":"image","content":"MERMAID_APF_12.jpg","thumb":"MERMAID_APF_12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10118":{"id":10118,"size":0,"label":"MERMAID_APF_11.jpg","type":"image","content":"MERMAID_APF_11.jpg","thumb":"MERMAID_APF_11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10119":{"id":10119,"size":0,"label":"MERMAID_APF_10.jpg","type":"image","content":"MERMAID_APF_10.jpg","thumb":"MERMAID_APF_10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10120":{"id":10120,"size":0,"label":"MERMAID_APF_09.jpg","type":"image","content":"MERMAID_APF_09.jpg","thumb":"MERMAID_APF_09.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10121":{"id":10121,"size":0,"label":"MERMAID_APF_08.jpg","type":"image","content":"MERMAID_APF_08.jpg","thumb":"MERMAID_APF_08.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10122":{"id":10122,"size":0,"label":"MERMAID_APF_07.jpg","type":"image","content":"MERMAID_APF_07.jpg","thumb":"MERMAID_APF_07.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10123":{"id":10123,"size":0,"label":"MERMAID_APF_06.jpg","type":"image","content":"MERMAID_APF_06.jpg","thumb":"MERMAID_APF_06.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10124":{"id":10124,"size":0,"label":"MERMAID_APF_05.jpg","type":"image","content":"MERMAID_APF_05.jpg","thumb":"MERMAID_APF_05.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10125":{"id":10125,"size":0,"label":"MERMAID_APF_04.jpg","type":"image","content":"MERMAID_APF_04.jpg","thumb":"MERMAID_APF_04.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10126":{"id":10126,"size":0,"label":"MERMAID_APF_03.jpg","type":"image","content":"MERMAID_APF_03.jpg","thumb":"MERMAID_APF_03.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10127":{"id":10127,"size":0,"label":"f_MG_7973.jpg","type":"image","content":"f_MG_7973.jpg","thumb":"f_MG_7973.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10128":{"id":10128,"size":0,"label":"TEXAS_APF_12.jpg","type":"image","content":"TEXAS_APF_12.jpg","thumb":"TEXAS_APF_12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10129":{"id":10129,"size":0,"label":"TEXAS_APF_11.jpg","type":"image","content":"TEXAS_APF_11.jpg","thumb":"TEXAS_APF_11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10130":{"id":10130,"size":0,"label":"TEXAS_APF_10.jpg","type":"image","content":"TEXAS_APF_10.jpg","thumb":"TEXAS_APF_10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10131":{"id":10131,"size":0,"label":"TEXAS_APF_09.jpg","type":"image","content":"TEXAS_APF_09.jpg","thumb":"TEXAS_APF_09.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10132":{"id":10132,"size":0,"label":"TEXAS_APF_08.jpg","type":"image","content":"TEXAS_APF_08.jpg","thumb":"TEXAS_APF_08.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10133":{"id":10133,"size":0,"label":"TEXAS_APF_07.jpg","type":"image","content":"TEXAS_APF_07.jpg","thumb":"TEXAS_APF_07.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10134":{"id":10134,"size":0,"label":"TEXAS_APF_06.jpg","type":"image","content":"TEXAS_APF_06.jpg","thumb":"TEXAS_APF_06.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10135":{"id":10135,"size":0,"label":"TEXAS_APF_05.jpg","type":"image","content":"TEXAS_APF_05.jpg","thumb":"TEXAS_APF_05.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10136":{"id":10136,"size":0,"label":"TEXAS_APF_04.jpg","type":"image","content":"TEXAS_APF_04.jpg","thumb":"TEXAS_APF_04.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10137":{"id":10137,"size":0,"label":"TEXAS_APF_03.jpg","type":"image","content":"TEXAS_APF_03.jpg","thumb":"TEXAS_APF_03.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10138":{"id":10138,"size":0,"label":"TEXAS_APF_02.jpg","type":"image","content":"TEXAS_APF_02.jpg","thumb":"TEXAS_APF_02.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10139":{"id":10139,"size":0,"label":"TEXAS_APF_01.jpg","type":"image","content":"TEXAS_APF_01.jpg","thumb":"TEXAS_APF_01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10140":{"id":10140,"size":0,"label":"RACHELROY.jpg","type":"image","content":"RACHELROY.jpg","thumb":"RACHELROY.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10141":{"id":10141,"size":0,"label":"rein.jpg","type":"image","content":"rein.jpg","thumb":"rein.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10142":{"id":10142,"size":0,"label":"EAFALL-0917-1.jpg","type":"image","content":"EAFALL-0917-1.jpg","thumb":"EAFALL-0917-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10143":{"id":10143,"size":0,"label":"EAFALL-0876-1.jpg","type":"image","content":"EAFALL-0876-1.jpg","thumb":"EAFALL-0876-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10144":{"id":10144,"size":0,"label":"EAFALL-0819.jpg","type":"image","content":"EAFALL-0819.jpg","thumb":"EAFALL-0819.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10145":{"id":10145,"size":0,"label":"EAFALL-0655.jpg","type":"image","content":"EAFALL-0655.jpg","thumb":"EAFALL-0655.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10146":{"id":10146,"size":0,"label":"EAFALL-0585.jpg","type":"image","content":"EAFALL-0585.jpg","thumb":"EAFALL-0585.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10147":{"id":10147,"size":0,"label":"eaaa.jpg","type":"image","content":"eaaa.jpg","thumb":"eaaa.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10148":{"id":10148,"size":0,"label":"EAFALL-0475.jpg","type":"image","content":"EAFALL-0475.jpg","thumb":"EAFALL-0475.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10149":{"id":10149,"size":0,"label":"EAFALL-0401-1.jpg","type":"image","content":"EAFALL-0401-1.jpg","thumb":"EAFALL-0401-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10150":{"id":10150,"size":0,"label":"eaa.jpg","type":"image","content":"eaa.jpg","thumb":"eaa.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10151":{"id":10151,"size":0,"label":"Untitled-1.jpg","type":"image","content":"Untitled-1.jpg","thumb":"Untitled-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10152":{"id":10152,"size":0,"label":"EAFALL-0252-1.jpg","type":"image","content":"EAFALL-0252-1.jpg","thumb":"EAFALL-0252-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10153":{"id":10153,"size":0,"label":"EAFALL-0067-1.jpg","type":"image","content":"EAFALL-0067-1.jpg","thumb":"EAFALL-0067-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10154":{"id":10154,"size":0,"label":"ea-390.jpg","type":"image","content":"ea-390.jpg","thumb":"ea-390.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10155":{"id":10155,"size":0,"label":"ea-298.jpg","type":"image","content":"ea-298.jpg","thumb":"ea-298.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10156":{"id":10156,"size":0,"label":"ea-272.jpg","type":"image","content":"ea-272.jpg","thumb":"ea-272.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10157":{"id":10157,"size":0,"label":"ea-194.jpg","type":"image","content":"ea-194.jpg","thumb":"ea-194.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10158":{"id":10158,"size":0,"label":"EAFALL-1141-2.jpg","type":"image","content":"EAFALL-1141-2.jpg","thumb":"EAFALL-1141-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10159":{"id":10159,"size":0,"label":"EAFALL-1022.jpg","type":"image","content":"EAFALL-1022.jpg","thumb":"EAFALL-1022.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10160":{"id":10160,"size":0,"label":"EAFALL-0949-1.jpg","type":"image","content":"EAFALL-0949-1.jpg","thumb":"EAFALL-0949-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10161":{"id":10161,"size":0,"label":"ea-21.jpg","type":"image","content":"ea-21.jpg","thumb":"ea-21.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10162":{"id":10162,"size":0,"label":"EASS12_04Ab.jpg","type":"image","content":"EASS12_04Ab.jpg","thumb":"EASS12_04Ab.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10163":{"id":10163,"size":0,"label":"IMG_7893.jpg","type":"image","content":"IMG_7893.jpg","thumb":"IMG_7893.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10164":{"id":10164,"size":0,"label":"mclendon.jpg","type":"image","content":"mclendon.jpg","thumb":"mclendon.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10165":{"id":10165,"size":0,"label":"IMG_1799.jpg","type":"image","content":"IMG_1799.jpg","thumb":"IMG_1799.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10166":{"id":10166,"size":0,"label":"IMG_1837.jpg","type":"image","content":"IMG_1837.jpg","thumb":"IMG_1837.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10167":{"id":10167,"size":0,"label":"brand.jpg","type":"image","content":"brand.jpg","thumb":"brand.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10168":{"id":10168,"size":0,"label":"IMG_2199.jpg","type":"image","content":"IMG_2199.jpg","thumb":"IMG_2199.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10169":{"id":10169,"size":0,"label":"IMG_2241.jpg","type":"image","content":"IMG_2241.jpg","thumb":"IMG_2241.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10170":{"id":10170,"size":0,"label":"IMG_2229.jpg","type":"image","content":"IMG_2229.jpg","thumb":"IMG_2229.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10171":{"id":10171,"size":0,"label":"IMG_2276.jpg","type":"image","content":"IMG_2276.jpg","thumb":"IMG_2276.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10172":{"id":10172,"size":0,"label":"IMG_2280.jpg","type":"image","content":"IMG_2280.jpg","thumb":"IMG_2280.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10173":{"id":10173,"size":0,"label":"Untitled-1.jpg","type":"image","content":"Untitled-1.jpg","thumb":"Untitled-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10174":{"id":10174,"size":0,"label":"IMG_2395.jpg","type":"image","content":"IMG_2395.jpg","thumb":"IMG_2395.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10175":{"id":10175,"size":0,"label":"IMG_2417.jpg","type":"image","content":"IMG_2417.jpg","thumb":"IMG_2417.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10176":{"id":10176,"size":0,"label":"IMG_2535.jpg","type":"image","content":"IMG_2535.jpg","thumb":"IMG_2535.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10177":{"id":10177,"size":0,"label":"IMG_2625.jpg","type":"image","content":"IMG_2625.jpg","thumb":"IMG_2625.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10178":{"id":10178,"size":0,"label":"IMG_2725.jpg","type":"image","content":"IMG_2725.jpg","thumb":"IMG_2725.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10179":{"id":10179,"size":0,"label":"IMG_2796.jpg","type":"image","content":"IMG_2796.jpg","thumb":"IMG_2796.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10180":{"id":10180,"size":0,"label":"IMG_2893.jpg","type":"image","content":"IMG_2893.jpg","thumb":"IMG_2893.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10181":{"id":10181,"size":0,"label":"IMG_2885.jpg","type":"image","content":"IMG_2885.jpg","thumb":"IMG_2885.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10182":{"id":10182,"size":0,"label":"IMG_2888.jpg","type":"image","content":"IMG_2888.jpg","thumb":"IMG_2888.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10183":{"id":10183,"size":0,"label":"IMG_2949.jpg","type":"image","content":"IMG_2949.jpg","thumb":"IMG_2949.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10184":{"id":10184,"size":0,"label":"IMG_1865.jpg","type":"image","content":"IMG_1865.jpg","thumb":"IMG_1865.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10185":{"id":10185,"size":0,"label":"IMG_1920.jpg","type":"image","content":"IMG_1920.jpg","thumb":"IMG_1920.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10186":{"id":10186,"size":0,"label":"IMG_1964.jpg","type":"image","content":"IMG_1964.jpg","thumb":"IMG_1964.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10187":{"id":10187,"size":0,"label":"merica.jpg","type":"image","content":"merica.jpg","thumb":"merica.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10188":{"id":10188,"size":0,"label":"IMG_5851.jpg","type":"image","content":"IMG_5851.jpg","thumb":"IMG_5851.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10189":{"id":10189,"size":0,"label":"IMG_5945.jpg","type":"image","content":"IMG_5945.jpg","thumb":"IMG_5945.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10190":{"id":10190,"size":0,"label":"f_IMG_5359.jpg","type":"image","content":"f_IMG_5359.jpg","thumb":"f_IMG_5359.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10191":{"id":10191,"size":0,"label":"_MG_2992.jpg","type":"image","content":"_MG_2992.jpg","thumb":"_MG_2992.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10192":{"id":10192,"size":0,"label":"RAHR_009.jpg","type":"image","content":"RAHR_009.jpg","thumb":"RAHR_009.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10193":{"id":10193,"size":0,"label":"navar.jpg","type":"image","content":"navar.jpg","thumb":"navar.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10194":{"id":10194,"size":0,"label":"IMG_2515.jpg","type":"image","content":"IMG_2515.jpg","thumb":"IMG_2515.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10195":{"id":10195,"size":0,"label":"IMG_2534.jpg","type":"image","content":"IMG_2534.jpg","thumb":"IMG_2534.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10196":{"id":10196,"size":0,"label":"IMG_2673.jpg","type":"image","content":"IMG_2673.jpg","thumb":"IMG_2673.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10197":{"id":10197,"size":0,"label":"MATTDODGE_IMG_5166.jpg","type":"image","content":"MATTDODGE_IMG_5166.jpg","thumb":"MATTDODGE_IMG_5166.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10198":{"id":10198,"size":0,"label":"IMG_0888.jpg","type":"image","content":"IMG_0888.jpg","thumb":"IMG_0888.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10199":{"id":10199,"size":0,"label":"IMG_8241.jpg","type":"image","content":"IMG_8241.jpg","thumb":"IMG_8241.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10200":{"id":10200,"size":0,"label":"ARTTM_3536.jpg","type":"image","content":"ARTTM_3536.jpg","thumb":"ARTTM_3536.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10201":{"id":10201,"size":0,"label":"ARTTM_3468.jpg","type":"image","content":"ARTTM_3468.jpg","thumb":"ARTTM_3468.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10202":{"id":10202,"size":0,"label":"ARTTM_3332.jpg","type":"image","content":"ARTTM_3332.jpg","thumb":"ARTTM_3332.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10203":{"id":10203,"size":0,"label":"SpiritFamilyReunion_004d.jpg","type":"image","content":"SpiritFamilyReunion_004d.jpg","thumb":"SpiritFamilyReunion_004d.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10204":{"id":10204,"size":0,"label":"smilesmile.jpg","type":"image","content":"smilesmile.jpg","thumb":"smilesmile.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10205":{"id":10205,"size":0,"label":"_MG_0899.jpg","type":"image","content":"_MG_0899.jpg","thumb":"_MG_0899.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10206":{"id":10206,"size":0,"label":"2GALLANTS-0094.jpg","type":"image","content":"2GALLANTS-0094.jpg","thumb":"2GALLANTS-0094.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10207":{"id":10207,"size":0,"label":"2GALLANTS-POLA01.jpg","type":"image","content":"2GALLANTS-POLA01.jpg","thumb":"2GALLANTS-POLA01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10208":{"id":10208,"size":0,"label":"Untitled-1-DUP.jpg","type":"image","content":"Untitled-1-DUP.jpg","thumb":"Untitled-1-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10209":{"id":10209,"size":0,"label":"_MG_6349.jpg","type":"image","content":"_MG_6349.jpg","thumb":"_MG_6349.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10210":{"id":10210,"size":0,"label":"_MG_5310b.jpg","type":"image","content":"_MG_5310b.jpg","thumb":"_MG_5310b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10211":{"id":10211,"size":0,"label":"OLIBERTE-9615.jpg","type":"image","content":"OLIBERTE-9615.jpg","thumb":"OLIBERTE-9615.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10212":{"id":10212,"size":0,"label":"OLIBERTE-0959.jpg","type":"image","content":"OLIBERTE-0959.jpg","thumb":"OLIBERTE-0959.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10213":{"id":10213,"size":0,"label":"OLIBERTE-1342.jpg","type":"image","content":"OLIBERTE-1342.jpg","thumb":"OLIBERTE-1342.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10214":{"id":10214,"size":0,"label":"OLIBERTE-1715.jpg","type":"image","content":"OLIBERTE-1715.jpg","thumb":"OLIBERTE-1715.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10215":{"id":10215,"size":0,"label":"OLIBERTE-1699.jpg","type":"image","content":"OLIBERTE-1699.jpg","thumb":"OLIBERTE-1699.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10216":{"id":10216,"size":0,"label":"OLIBERTE-0202.jpg","type":"image","content":"OLIBERTE-0202.jpg","thumb":"OLIBERTE-0202.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10217":{"id":10217,"size":0,"label":"bellowpola_012BB.jpg","type":"image","content":"bellowpola_012BB.jpg","thumb":"bellowpola_012BB.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Music I"},"10218":{"id":10218,"size":0,"label":"bellowpola_09B.jpg","type":"image","content":"bellowpola_09B.jpg","thumb":"bellowpola_09B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10219":{"id":10219,"size":0,"label":"granger_POLA_34D.jpg","type":"image","content":"granger_POLA_34D.jpg","thumb":"granger_POLA_34D.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10220":{"id":10220,"size":0,"label":"granger-4137.jpg","type":"image","content":"granger-4137.jpg","thumb":"granger-4137.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10221":{"id":10221,"size":0,"label":"SEANLEE_POLA_001A.jpg","type":"image","content":"SEANLEE_POLA_001A.jpg","thumb":"SEANLEE_POLA_001A.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10222":{"id":10222,"size":0,"label":"SEANLEE_POLA_006A-Edit.jpg","type":"image","content":"SEANLEE_POLA_006A-Edit.jpg","thumb":"SEANLEE_POLA_006A-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10223":{"id":10223,"size":0,"label":"_MG_6207.jpg","type":"image","content":"_MG_6207.jpg","thumb":"_MG_6207.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10224":{"id":10224,"size":0,"label":"anderson_script_2013_300.jpg","type":"image","content":"anderson_script_2013_300.jpg","thumb":"anderson_script_2013_300.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10225":{"id":10225,"size":0,"label":"logo_holder_01.jpg","type":"image","content":"logo_holder_01.jpg","thumb":"logo_holder_01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10226":{"id":10226,"size":0,"label":"TESTSTSTS","type":"link","content":"www.ivegotfriends.com","thumb":"","linkTarget":"_blank","caption":"null","featuredImage":" ","filters":""},"10227":{"id":10227,"size":0,"label":"| BLOG ","type":"link","content":"http:\/\/www.ivegotfriends.com","thumb":"","linkTarget":"_blank","caption":"null","featuredImage":" ","filters":""},"10228":{"id":10228,"size":0,"label":"SantinoPOLA_001C.jpg","type":"image","content":"SantinoPOLA_001C.jpg","thumb":"SantinoPOLA_001C.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"BOOK ONE"},"10229":{"id":10229,"size":0,"label":"063A2570.jpg","type":"image","content":"063A2570.jpg","thumb":"063A2570.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10230":{"id":10230,"size":0,"label":"063A6532.jpg","type":"image","content":"063A6532.jpg","thumb":"063A6532.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10231":{"id":10231,"size":0,"label":"063A8105.jpg","type":"image","content":"063A8105.jpg","thumb":"063A8105.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10232":{"id":10232,"size":0,"label":"063A1186.jpg","type":"image","content":"063A1186.jpg","thumb":"063A1186.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10233":{"id":10233,"size":0,"label":"063A4734.jpg","type":"image","content":"063A4734.jpg","thumb":"063A4734.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10234":{"id":10234,"size":0,"label":"063A2812.jpg","type":"image","content":"063A2812.jpg","thumb":"063A2812.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10235":{"id":10235,"size":0,"label":".jpg","type":"image","content":".jpg","thumb":".jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10236":{"id":10236,"size":0,"label":"willy.jpg","type":"image","content":"willy.jpg","thumb":"willy.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10237":{"id":10237,"size":0,"label":"ERA_STYLESPY_OCT-7.jpg","type":"image","content":"ERA_STYLESPY_OCT-7.jpg","thumb":"ERA_STYLESPY_OCT-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10238":{"id":10238,"size":0,"label":"bellow-6776-2.jpg","type":"image","content":"bellow-6776-2.jpg","thumb":"bellow-6776-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10239":{"id":10239,"size":0,"label":"anderson_script_2013_black250.jpg","type":"image","content":"anderson_script_2013_black250.jpg","thumb":"anderson_script_2013_black250.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10240":{"id":10240,"size":0,"label":"Overview","type":"link","content":"\/overview","thumb":"","linkTarget":"_self","caption":"null","featuredImage":" ","filters":""},"10241":{"id":10241,"size":0,"label":"OVERVIEW","type":"link","content":"\/designx\/overview","thumb":"","linkTarget":"_blank","caption":"null","featuredImage":" ","filters":""},"10242":{"id":10242,"size":0,"label":"anderson_script_2013BLACK.jpg","type":"image","content":"anderson_script_2013BLACK.jpg","thumb":"anderson_script_2013BLACK.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10243":{"id":10243,"size":0,"label":"063A3955.jpg","type":"image","content":"063A3955.jpg","thumb":"063A3955.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Ernest Alexander"},"10244":{"id":10244,"size":0,"label":"063A4531-2.jpg","type":"image","content":"063A4531-2.jpg","thumb":"063A4531-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10245":{"id":10245,"size":0,"label":"Journal","type":"link","content":"http:\/\/www.ivegotfriends.com","thumb":"","linkTarget":"_blank","caption":"null","featuredImage":" ","filters":""},"10246":{"id":10246,"size":0,"label":"ANDERSON_POLA_519.jpg","type":"image","content":"ANDERSON_POLA_519.jpg","thumb":"ANDERSON_POLA_519.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10247":{"id":10247,"size":0,"label":"ANDERSON_POLA_545.jpg","type":"image","content":"ANDERSON_POLA_545.jpg","thumb":"ANDERSON_POLA_545.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10248":{"id":10248,"size":0,"label":"ANDERSON_POLA_460.jpg","type":"image","content":"ANDERSON_POLA_460.jpg","thumb":"ANDERSON_POLA_460.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10249":{"id":10249,"size":0,"label":"ANDERSON_POLA_507B.jpg","type":"image","content":"ANDERSON_POLA_507B.jpg","thumb":"ANDERSON_POLA_507B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10250":{"id":10250,"size":0,"label":"ANDERSON_POLA_508B.jpg","type":"image","content":"ANDERSON_POLA_508B.jpg","thumb":"ANDERSON_POLA_508B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10251":{"id":10251,"size":0,"label":"ANDERSON_POLA_510B.jpg","type":"image","content":"ANDERSON_POLA_510B.jpg","thumb":"ANDERSON_POLA_510B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10252":{"id":10252,"size":0,"label":"ANDERSON_POLA_321.jpg","type":"image","content":"ANDERSON_POLA_321.jpg","thumb":"ANDERSON_POLA_321.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10253":{"id":10253,"size":0,"label":"ANDERSON_POLA_326B.jpg","type":"image","content":"ANDERSON_POLA_326B.jpg","thumb":"ANDERSON_POLA_326B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10254":{"id":10254,"size":0,"label":"ANDERSON_POLA_405B.jpg","type":"image","content":"ANDERSON_POLA_405B.jpg","thumb":"ANDERSON_POLA_405B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10255":{"id":10255,"size":0,"label":"ANDERSON_POLA_271.jpg","type":"image","content":"ANDERSON_POLA_271.jpg","thumb":"ANDERSON_POLA_271.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10256":{"id":10256,"size":0,"label":"ANDERSON_POLA_302.jpg","type":"image","content":"ANDERSON_POLA_302.jpg","thumb":"ANDERSON_POLA_302.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10257":{"id":10257,"size":0,"label":"ANDERSON_POLA_312.jpg","type":"image","content":"ANDERSON_POLA_312.jpg","thumb":"ANDERSON_POLA_312.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10258":{"id":10258,"size":0,"label":"ANDERSON_POLA_275.jpg","type":"image","content":"ANDERSON_POLA_275.jpg","thumb":"ANDERSON_POLA_275.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10259":{"id":10259,"size":0,"label":"ANDERSON_POLA_427.jpg","type":"image","content":"ANDERSON_POLA_427.jpg","thumb":"ANDERSON_POLA_427.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10260":{"id":10260,"size":0,"label":"ANDERSON_POLA_379B.jpg","type":"image","content":"ANDERSON_POLA_379B.jpg","thumb":"ANDERSON_POLA_379B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10261":{"id":10261,"size":0,"label":"ANDERSON_POLA_381.jpg","type":"image","content":"ANDERSON_POLA_381.jpg","thumb":"ANDERSON_POLA_381.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10262":{"id":10262,"size":0,"label":"ANDERSON_POLA_397B.jpg","type":"image","content":"ANDERSON_POLA_397B.jpg","thumb":"ANDERSON_POLA_397B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10263":{"id":10263,"size":0,"label":"ANDERSON_POLA_309.jpg","type":"image","content":"ANDERSON_POLA_309.jpg","thumb":"ANDERSON_POLA_309.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10264":{"id":10264,"size":0,"label":"ANDERSON_POLA_328.jpg","type":"image","content":"ANDERSON_POLA_328.jpg","thumb":"ANDERSON_POLA_328.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10265":{"id":10265,"size":0,"label":"ANDERSON_POLA_367.jpg","type":"image","content":"ANDERSON_POLA_367.jpg","thumb":"ANDERSON_POLA_367.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10266":{"id":10266,"size":0,"label":"ANDERSON_POLA_304.jpg","type":"image","content":"ANDERSON_POLA_304.jpg","thumb":"ANDERSON_POLA_304.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10267":{"id":10267,"size":0,"label":"ANDERSON_POLA_306.jpg","type":"image","content":"ANDERSON_POLA_306.jpg","thumb":"ANDERSON_POLA_306.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10268":{"id":10268,"size":0,"label":"ANDERSON_POLA_318.jpg","type":"image","content":"ANDERSON_POLA_318.jpg","thumb":"ANDERSON_POLA_318.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10269":{"id":10269,"size":0,"label":"ANDERSON_POLA_290.jpg","type":"image","content":"ANDERSON_POLA_290.jpg","thumb":"ANDERSON_POLA_290.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10270":{"id":10270,"size":0,"label":"ANDERSON_POLA_291.jpg","type":"image","content":"ANDERSON_POLA_291.jpg","thumb":"ANDERSON_POLA_291.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10271":{"id":10271,"size":0,"label":"ANDERSON_POLA_303.jpg","type":"image","content":"ANDERSON_POLA_303.jpg","thumb":"ANDERSON_POLA_303.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10272":{"id":10272,"size":0,"label":"ANDERSON_POLA_269.jpg","type":"image","content":"ANDERSON_POLA_269.jpg","thumb":"ANDERSON_POLA_269.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10273":{"id":10273,"size":0,"label":"ANDERSON_POLA_277.jpg","type":"image","content":"ANDERSON_POLA_277.jpg","thumb":"ANDERSON_POLA_277.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10274":{"id":10274,"size":0,"label":"ANDERSON_POLA_278.jpg","type":"image","content":"ANDERSON_POLA_278.jpg","thumb":"ANDERSON_POLA_278.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10275":{"id":10275,"size":0,"label":"ANDERSON_POLA_251.jpg","type":"image","content":"ANDERSON_POLA_251.jpg","thumb":"ANDERSON_POLA_251.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10276":{"id":10276,"size":0,"label":"ANDERSON_POLA_253.jpg","type":"image","content":"ANDERSON_POLA_253.jpg","thumb":"ANDERSON_POLA_253.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10277":{"id":10277,"size":0,"label":"ANDERSON_POLA_255.jpg","type":"image","content":"ANDERSON_POLA_255.jpg","thumb":"ANDERSON_POLA_255.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10278":{"id":10278,"size":0,"label":"ANDERSON_POLA_246.jpg","type":"image","content":"ANDERSON_POLA_246.jpg","thumb":"ANDERSON_POLA_246.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10279":{"id":10279,"size":0,"label":"ANDERSON_POLA_247.jpg","type":"image","content":"ANDERSON_POLA_247.jpg","thumb":"ANDERSON_POLA_247.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10280":{"id":10280,"size":0,"label":"ANDERSON_POLA_234.jpg","type":"image","content":"ANDERSON_POLA_234.jpg","thumb":"ANDERSON_POLA_234.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10281":{"id":10281,"size":0,"label":"ANDERSON_POLA_237.jpg","type":"image","content":"ANDERSON_POLA_237.jpg","thumb":"ANDERSON_POLA_237.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10282":{"id":10282,"size":0,"label":"ANDERSON_POLA_238.jpg","type":"image","content":"ANDERSON_POLA_238.jpg","thumb":"ANDERSON_POLA_238.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10283":{"id":10283,"size":0,"label":"ANDERSON_POLA_239.jpg","type":"image","content":"ANDERSON_POLA_239.jpg","thumb":"ANDERSON_POLA_239.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10284":{"id":10284,"size":0,"label":"ANDERSON_POLA_221.jpg","type":"image","content":"ANDERSON_POLA_221.jpg","thumb":"ANDERSON_POLA_221.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10285":{"id":10285,"size":0,"label":"ANDERSON_POLA_227.jpg","type":"image","content":"ANDERSON_POLA_227.jpg","thumb":"ANDERSON_POLA_227.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10286":{"id":10286,"size":0,"label":"ANDERSON_POLA_206.jpg","type":"image","content":"ANDERSON_POLA_206.jpg","thumb":"ANDERSON_POLA_206.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10287":{"id":10287,"size":0,"label":"ANDERSON_POLA_215.jpg","type":"image","content":"ANDERSON_POLA_215.jpg","thumb":"ANDERSON_POLA_215.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10288":{"id":10288,"size":0,"label":"ANDERSON_POLA_217 (1).jpg","type":"image","content":"ANDERSON_POLA_217 (1).jpg","thumb":"ANDERSON_POLA_217 (1).jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10289":{"id":10289,"size":0,"label":"ANDERSON_POLA_217.jpg","type":"image","content":"ANDERSON_POLA_217.jpg","thumb":"ANDERSON_POLA_217.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10290":{"id":10290,"size":0,"label":"ANDERSON_POLA_224.jpg","type":"image","content":"ANDERSON_POLA_224.jpg","thumb":"ANDERSON_POLA_224.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10291":{"id":10291,"size":0,"label":"ANDERSON_POLA_256.jpg","type":"image","content":"ANDERSON_POLA_256.jpg","thumb":"ANDERSON_POLA_256.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10292":{"id":10292,"size":0,"label":"ANDERSON_POLA_205.jpg","type":"image","content":"ANDERSON_POLA_205.jpg","thumb":"ANDERSON_POLA_205.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10293":{"id":10293,"size":0,"label":"ANDERSON_POLA_219.jpg","type":"image","content":"ANDERSON_POLA_219.jpg","thumb":"ANDERSON_POLA_219.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10294":{"id":10294,"size":0,"label":"ANDERSON_POLA_222.jpg","type":"image","content":"ANDERSON_POLA_222.jpg","thumb":"ANDERSON_POLA_222.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10295":{"id":10295,"size":0,"label":"ANDERSON_POLA_142.jpg","type":"image","content":"ANDERSON_POLA_142.jpg","thumb":"ANDERSON_POLA_142.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10296":{"id":10296,"size":0,"label":"ANDERSON_POLA_164.jpg","type":"image","content":"ANDERSON_POLA_164.jpg","thumb":"ANDERSON_POLA_164.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10297":{"id":10297,"size":0,"label":"ANDERSON_POLA_181.jpg","type":"image","content":"ANDERSON_POLA_181.jpg","thumb":"ANDERSON_POLA_181.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10298":{"id":10298,"size":0,"label":"ANDERSON_POLA_184.jpg","type":"image","content":"ANDERSON_POLA_184.jpg","thumb":"ANDERSON_POLA_184.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10299":{"id":10299,"size":0,"label":"ANDERSON_POLA_103.jpg","type":"image","content":"ANDERSON_POLA_103.jpg","thumb":"ANDERSON_POLA_103.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10300":{"id":10300,"size":0,"label":"ANDERSON_POLA_107.jpg","type":"image","content":"ANDERSON_POLA_107.jpg","thumb":"ANDERSON_POLA_107.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10301":{"id":10301,"size":0,"label":"ANDERSON_POLA_125.jpg","type":"image","content":"ANDERSON_POLA_125.jpg","thumb":"ANDERSON_POLA_125.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10302":{"id":10302,"size":0,"label":"ANDERSON_POLA_095B.jpg","type":"image","content":"ANDERSON_POLA_095B.jpg","thumb":"ANDERSON_POLA_095B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10303":{"id":10303,"size":0,"label":"ANDERSON_POLA_075.jpg","type":"image","content":"ANDERSON_POLA_075.jpg","thumb":"ANDERSON_POLA_075.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10304":{"id":10304,"size":0,"label":"ANDERSON_POLA_076.jpg","type":"image","content":"ANDERSON_POLA_076.jpg","thumb":"ANDERSON_POLA_076.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10305":{"id":10305,"size":0,"label":"ANDERSON_POLA_272.jpg","type":"image","content":"ANDERSON_POLA_272.jpg","thumb":"ANDERSON_POLA_272.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10306":{"id":10306,"size":0,"label":"ANDERSON_POLA_177.jpg","type":"image","content":"ANDERSON_POLA_177.jpg","thumb":"ANDERSON_POLA_177.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10307":{"id":10307,"size":0,"label":"ANDERSON_POLA_211.jpg","type":"image","content":"ANDERSON_POLA_211.jpg","thumb":"ANDERSON_POLA_211.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10308":{"id":10308,"size":0,"label":"ANDERSON_POLA_220.jpg","type":"image","content":"ANDERSON_POLA_220.jpg","thumb":"ANDERSON_POLA_220.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10309":{"id":10309,"size":0,"label":"ANDERSON_POLA_078.jpg","type":"image","content":"ANDERSON_POLA_078.jpg","thumb":"ANDERSON_POLA_078.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10310":{"id":10310,"size":0,"label":"ANDERSON_POLA_118.jpg","type":"image","content":"ANDERSON_POLA_118.jpg","thumb":"ANDERSON_POLA_118.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10311":{"id":10311,"size":0,"label":"ANDERSON_POLA_133.jpg","type":"image","content":"ANDERSON_POLA_133.jpg","thumb":"ANDERSON_POLA_133.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10312":{"id":10312,"size":0,"label":"ANDERSON_POLA_043.jpg","type":"image","content":"ANDERSON_POLA_043.jpg","thumb":"ANDERSON_POLA_043.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10313":{"id":10313,"size":0,"label":"ANDERSON_POLA_059.jpg","type":"image","content":"ANDERSON_POLA_059.jpg","thumb":"ANDERSON_POLA_059.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10314":{"id":10314,"size":0,"label":"ANDERSON_POLA_062.jpg","type":"image","content":"ANDERSON_POLA_062.jpg","thumb":"ANDERSON_POLA_062.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10315":{"id":10315,"size":0,"label":"ANDERSON_POLA_001.jpg","type":"image","content":"ANDERSON_POLA_001.jpg","thumb":"ANDERSON_POLA_001.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10316":{"id":10316,"size":0,"label":"ANDERSON_POLA_012.jpg","type":"image","content":"ANDERSON_POLA_012.jpg","thumb":"ANDERSON_POLA_012.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10317":{"id":10317,"size":0,"label":"ANDERSON_POLA_016.jpg","type":"image","content":"ANDERSON_POLA_016.jpg","thumb":"ANDERSON_POLA_016.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10318":{"id":10318,"size":0,"label":"063A0487.jpg","type":"image","content":"063A0487.jpg","thumb":"063A0487.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10319":{"id":10319,"size":0,"label":"Anderson_Montauk_0001.jpg","type":"image","content":"Anderson_Montauk_0001.jpg","thumb":"Anderson_Montauk_0001.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10320":{"id":10320,"size":0,"label":"Anderson_Montauk_0002.jpg","type":"image","content":"Anderson_Montauk_0002.jpg","thumb":"Anderson_Montauk_0002.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10321":{"id":10321,"size":0,"label":"Anderson_Montauk_0003.jpg","type":"image","content":"Anderson_Montauk_0003.jpg","thumb":"Anderson_Montauk_0003.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10322":{"id":10322,"size":0,"label":"Anderson_Montauk_0004.jpg","type":"image","content":"Anderson_Montauk_0004.jpg","thumb":"Anderson_Montauk_0004.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10323":{"id":10323,"size":0,"label":"Anderson_Montauk_0005.jpg","type":"image","content":"Anderson_Montauk_0005.jpg","thumb":"Anderson_Montauk_0005.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10324":{"id":10324,"size":0,"label":"Anderson_Montauk_0006.jpg","type":"image","content":"Anderson_Montauk_0006.jpg","thumb":"Anderson_Montauk_0006.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10325":{"id":10325,"size":0,"label":"Anderson_Montauk_0007.jpg","type":"image","content":"Anderson_Montauk_0007.jpg","thumb":"Anderson_Montauk_0007.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10326":{"id":10326,"size":0,"label":"Anderson_Montauk_0008.jpg","type":"image","content":"Anderson_Montauk_0008.jpg","thumb":"Anderson_Montauk_0008.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10327":{"id":10327,"size":0,"label":"Anderson_Montauk_0009.jpg","type":"image","content":"Anderson_Montauk_0009.jpg","thumb":"Anderson_Montauk_0009.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10328":{"id":10328,"size":0,"label":"Anderson_Montauk_0010.jpg","type":"image","content":"Anderson_Montauk_0010.jpg","thumb":"Anderson_Montauk_0010.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10329":{"id":10329,"size":0,"label":"Anderson_Montauk_0011.jpg","type":"image","content":"Anderson_Montauk_0011.jpg","thumb":"Anderson_Montauk_0011.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10330":{"id":10330,"size":0,"label":"Anderson_Montauk_0012.jpg","type":"image","content":"Anderson_Montauk_0012.jpg","thumb":"Anderson_Montauk_0012.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10331":{"id":10331,"size":0,"label":"Anderson_Montauk_0013.jpg","type":"image","content":"Anderson_Montauk_0013.jpg","thumb":"Anderson_Montauk_0013.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10332":{"id":10332,"size":0,"label":"Anderson_Montauk_0014.jpg","type":"image","content":"Anderson_Montauk_0014.jpg","thumb":"Anderson_Montauk_0014.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10333":{"id":10333,"size":0,"label":"Anderson_Montauk_0015.jpg","type":"image","content":"Anderson_Montauk_0015.jpg","thumb":"Anderson_Montauk_0015.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10334":{"id":10334,"size":0,"label":"Anderson_Montauk_0016.jpg","type":"image","content":"Anderson_Montauk_0016.jpg","thumb":"Anderson_Montauk_0016.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10335":{"id":10335,"size":0,"label":"Anderson_Montauk_0017.jpg","type":"image","content":"Anderson_Montauk_0017.jpg","thumb":"Anderson_Montauk_0017.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10336":{"id":10336,"size":0,"label":"Anderson_Montauk_0018.jpg","type":"image","content":"Anderson_Montauk_0018.jpg","thumb":"Anderson_Montauk_0018.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10337":{"id":10337,"size":0,"label":"Anderson_Montauk_0019.jpg","type":"image","content":"Anderson_Montauk_0019.jpg","thumb":"Anderson_Montauk_0019.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10338":{"id":10338,"size":0,"label":"Anderson_Montauk_0020.jpg","type":"image","content":"Anderson_Montauk_0020.jpg","thumb":"Anderson_Montauk_0020.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10339":{"id":10339,"size":0,"label":"Anderson_Montauk_0021.jpg","type":"image","content":"Anderson_Montauk_0021.jpg","thumb":"Anderson_Montauk_0021.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10340":{"id":10340,"size":0,"label":"Anderson_Montauk_0022.jpg","type":"image","content":"Anderson_Montauk_0022.jpg","thumb":"Anderson_Montauk_0022.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10341":{"id":10341,"size":0,"label":"Anderson_Montauk_0023.jpg","type":"image","content":"Anderson_Montauk_0023.jpg","thumb":"Anderson_Montauk_0023.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10342":{"id":10342,"size":0,"label":"Anderson_Montauk_0024.jpg","type":"image","content":"Anderson_Montauk_0024.jpg","thumb":"Anderson_Montauk_0024.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10343":{"id":10343,"size":0,"label":"Anderson_Montauk_0025.jpg","type":"image","content":"Anderson_Montauk_0025.jpg","thumb":"Anderson_Montauk_0025.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10344":{"id":10344,"size":0,"label":"Anderson_Montauk_0026.jpg","type":"image","content":"Anderson_Montauk_0026.jpg","thumb":"Anderson_Montauk_0026.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10345":{"id":10345,"size":0,"label":"Anderson_Montauk_0027.jpg","type":"image","content":"Anderson_Montauk_0027.jpg","thumb":"Anderson_Montauk_0027.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10346":{"id":10346,"size":0,"label":"Anderson_Montauk_0028.jpg","type":"image","content":"Anderson_Montauk_0028.jpg","thumb":"Anderson_Montauk_0028.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10347":{"id":10347,"size":0,"label":"Anderson_Montauk_0029.jpg","type":"image","content":"Anderson_Montauk_0029.jpg","thumb":"Anderson_Montauk_0029.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10348":{"id":10348,"size":0,"label":"Anderson_Montauk_0030.jpg","type":"image","content":"Anderson_Montauk_0030.jpg","thumb":"Anderson_Montauk_0030.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10349":{"id":10349,"size":0,"label":"Anderson_Montauk_0031.jpg","type":"image","content":"Anderson_Montauk_0031.jpg","thumb":"Anderson_Montauk_0031.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10350":{"id":10350,"size":0,"label":"Anderson_Montauk_0032.jpg","type":"image","content":"Anderson_Montauk_0032.jpg","thumb":"Anderson_Montauk_0032.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10351":{"id":10351,"size":0,"label":"Anderson_Montauk_0033.jpg","type":"image","content":"Anderson_Montauk_0033.jpg","thumb":"Anderson_Montauk_0033.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10352":{"id":10352,"size":0,"label":"Anderson_Montauk_0034.jpg","type":"image","content":"Anderson_Montauk_0034.jpg","thumb":"Anderson_Montauk_0034.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10353":{"id":10353,"size":0,"label":"Anderson_Montauk_0035.jpg","type":"image","content":"Anderson_Montauk_0035.jpg","thumb":"Anderson_Montauk_0035.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10354":{"id":10354,"size":0,"label":"Anderson_Montauk_0036.jpg","type":"image","content":"Anderson_Montauk_0036.jpg","thumb":"Anderson_Montauk_0036.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10355":{"id":10355,"size":0,"label":"Anderson_Montauk_0037.jpg","type":"image","content":"Anderson_Montauk_0037.jpg","thumb":"Anderson_Montauk_0037.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10356":{"id":10356,"size":0,"label":"Anderson_Montauk_0038.jpg","type":"image","content":"Anderson_Montauk_0038.jpg","thumb":"Anderson_Montauk_0038.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10357":{"id":10357,"size":0,"label":"Anderson_Montauk_0039.jpg","type":"image","content":"Anderson_Montauk_0039.jpg","thumb":"Anderson_Montauk_0039.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10358":{"id":10358,"size":0,"label":"Anderson_Montauk_0040.jpg","type":"image","content":"Anderson_Montauk_0040.jpg","thumb":"Anderson_Montauk_0040.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10359":{"id":10359,"size":0,"label":"Anderson_Montauk_0041.jpg","type":"image","content":"Anderson_Montauk_0041.jpg","thumb":"Anderson_Montauk_0041.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10360":{"id":10360,"size":0,"label":"Anderson_Montauk_0042.jpg","type":"image","content":"Anderson_Montauk_0042.jpg","thumb":"Anderson_Montauk_0042.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10361":{"id":10361,"size":0,"label":"Anderson_Montauk_0043.jpg","type":"image","content":"Anderson_Montauk_0043.jpg","thumb":"Anderson_Montauk_0043.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10362":{"id":10362,"size":0,"label":"Anderson_ParkHall_0001.jpg","type":"image","content":"Anderson_ParkHall_0001.jpg","thumb":"Anderson_ParkHall_0001.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10363":{"id":10363,"size":0,"label":"Anderson_ParkHall_0002.jpg","type":"image","content":"Anderson_ParkHall_0002.jpg","thumb":"Anderson_ParkHall_0002.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10364":{"id":10364,"size":0,"label":"Anderson_ParkHall_0003.jpg","type":"image","content":"Anderson_ParkHall_0003.jpg","thumb":"Anderson_ParkHall_0003.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10365":{"id":10365,"size":0,"label":"Anderson_ParkHall_0004.jpg","type":"image","content":"Anderson_ParkHall_0004.jpg","thumb":"Anderson_ParkHall_0004.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10366":{"id":10366,"size":0,"label":"Anderson_ParkHall_0005.jpg","type":"image","content":"Anderson_ParkHall_0005.jpg","thumb":"Anderson_ParkHall_0005.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10367":{"id":10367,"size":0,"label":"Anderson_ParkHall_0006.jpg","type":"image","content":"Anderson_ParkHall_0006.jpg","thumb":"Anderson_ParkHall_0006.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10368":{"id":10368,"size":0,"label":"Anderson_ParkHall_0007.jpg","type":"image","content":"Anderson_ParkHall_0007.jpg","thumb":"Anderson_ParkHall_0007.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10369":{"id":10369,"size":0,"label":"Anderson_ParkHall_0008.jpg","type":"image","content":"Anderson_ParkHall_0008.jpg","thumb":"Anderson_ParkHall_0008.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10370":{"id":10370,"size":0,"label":"Anderson_ParkHall_0009.jpg","type":"image","content":"Anderson_ParkHall_0009.jpg","thumb":"Anderson_ParkHall_0009.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10371":{"id":10371,"size":0,"label":"Anderson_ParkHall_0010.jpg","type":"image","content":"Anderson_ParkHall_0010.jpg","thumb":"Anderson_ParkHall_0010.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10372":{"id":10372,"size":0,"label":"Anderson_ParkHall_0011.jpg","type":"image","content":"Anderson_ParkHall_0011.jpg","thumb":"Anderson_ParkHall_0011.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10373":{"id":10373,"size":0,"label":"Anderson_ParkHall_0012.jpg","type":"image","content":"Anderson_ParkHall_0012.jpg","thumb":"Anderson_ParkHall_0012.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10374":{"id":10374,"size":0,"label":"Anderson_ParkHall_0013.jpg","type":"image","content":"Anderson_ParkHall_0013.jpg","thumb":"Anderson_ParkHall_0013.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10375":{"id":10375,"size":0,"label":"Anderson_ParkHall_0014.jpg","type":"image","content":"Anderson_ParkHall_0014.jpg","thumb":"Anderson_ParkHall_0014.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10376":{"id":10376,"size":0,"label":"Anderson_ParkHall_0015.jpg","type":"image","content":"Anderson_ParkHall_0015.jpg","thumb":"Anderson_ParkHall_0015.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10377":{"id":10377,"size":0,"label":"Anderson_ParkHall_0016.jpg","type":"image","content":"Anderson_ParkHall_0016.jpg","thumb":"Anderson_ParkHall_0016.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10378":{"id":10378,"size":0,"label":"Anderson_ParkHall_0017.jpg","type":"image","content":"Anderson_ParkHall_0017.jpg","thumb":"Anderson_ParkHall_0017.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10379":{"id":10379,"size":0,"label":"Anderson_ParkHall_0018.jpg","type":"image","content":"Anderson_ParkHall_0018.jpg","thumb":"Anderson_ParkHall_0018.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10380":{"id":10380,"size":0,"label":"Anderson_ParkHall_0019.jpg","type":"image","content":"Anderson_ParkHall_0019.jpg","thumb":"Anderson_ParkHall_0019.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10381":{"id":10381,"size":0,"label":"Anderson_ParkHall_0020.jpg","type":"image","content":"Anderson_ParkHall_0020.jpg","thumb":"Anderson_ParkHall_0020.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10382":{"id":10382,"size":0,"label":"Anderson_ParkHall_0021.jpg","type":"image","content":"Anderson_ParkHall_0021.jpg","thumb":"Anderson_ParkHall_0021.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10383":{"id":10383,"size":0,"label":"Anderson_ParkHall_0022.jpg","type":"image","content":"Anderson_ParkHall_0022.jpg","thumb":"Anderson_ParkHall_0022.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10384":{"id":10384,"size":0,"label":"Anderson_ParkHall_0023.jpg","type":"image","content":"Anderson_ParkHall_0023.jpg","thumb":"Anderson_ParkHall_0023.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10385":{"id":10385,"size":0,"label":"Anderson_ParkHall_0024.jpg","type":"image","content":"Anderson_ParkHall_0024.jpg","thumb":"Anderson_ParkHall_0024.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10386":{"id":10386,"size":0,"label":"Anderson_ParkHall_0025.jpg","type":"image","content":"Anderson_ParkHall_0025.jpg","thumb":"Anderson_ParkHall_0025.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10387":{"id":10387,"size":0,"label":"Anderson_ParkHall_0026.jpg","type":"image","content":"Anderson_ParkHall_0026.jpg","thumb":"Anderson_ParkHall_0026.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10388":{"id":10388,"size":0,"label":"Anderson_ParkHall_0027.jpg","type":"image","content":"Anderson_ParkHall_0027.jpg","thumb":"Anderson_ParkHall_0027.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10389":{"id":10389,"size":0,"label":"Anderson_ParkHall_0028.jpg","type":"image","content":"Anderson_ParkHall_0028.jpg","thumb":"Anderson_ParkHall_0028.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10390":{"id":10390,"size":0,"label":"Anderson_ParkHall_0029.jpg","type":"image","content":"Anderson_ParkHall_0029.jpg","thumb":"Anderson_ParkHall_0029.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10391":{"id":10391,"size":0,"label":"Anderson_ParkHall_0030.jpg","type":"image","content":"Anderson_ParkHall_0030.jpg","thumb":"Anderson_ParkHall_0030.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10392":{"id":10392,"size":0,"label":"Anderson_ParkHall_0031.jpg","type":"image","content":"Anderson_ParkHall_0031.jpg","thumb":"Anderson_ParkHall_0031.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10393":{"id":10393,"size":0,"label":"Anderson_ParkHall_0032.jpg","type":"image","content":"Anderson_ParkHall_0032.jpg","thumb":"Anderson_ParkHall_0032.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10394":{"id":10394,"size":0,"label":"Anderson_ParkHall_0033.jpg","type":"image","content":"Anderson_ParkHall_0033.jpg","thumb":"Anderson_ParkHall_0033.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10395":{"id":10395,"size":0,"label":"Anderson_ParkHall_0034.jpg","type":"image","content":"Anderson_ParkHall_0034.jpg","thumb":"Anderson_ParkHall_0034.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10396":{"id":10396,"size":0,"label":"Anderson_ParkHall_0035.jpg","type":"image","content":"Anderson_ParkHall_0035.jpg","thumb":"Anderson_ParkHall_0035.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10397":{"id":10397,"size":0,"label":"Anderson_ParkHall_0036.jpg","type":"image","content":"Anderson_ParkHall_0036.jpg","thumb":"Anderson_ParkHall_0036.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10398":{"id":10398,"size":0,"label":"Anderson_ParkHall_0037.jpg","type":"image","content":"Anderson_ParkHall_0037.jpg","thumb":"Anderson_ParkHall_0037.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10399":{"id":10399,"size":0,"label":"Anderson_ParkHall_0038.jpg","type":"image","content":"Anderson_ParkHall_0038.jpg","thumb":"Anderson_ParkHall_0038.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10400":{"id":10400,"size":0,"label":"Anderson_ParkHall_0039.jpg","type":"image","content":"Anderson_ParkHall_0039.jpg","thumb":"Anderson_ParkHall_0039.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10401":{"id":10401,"size":0,"label":"Anderson_ParkHall_0040.jpg","type":"image","content":"Anderson_ParkHall_0040.jpg","thumb":"Anderson_ParkHall_0040.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10402":{"id":10402,"size":0,"label":"Anderson_ParkHall_0041.jpg","type":"image","content":"Anderson_ParkHall_0041.jpg","thumb":"Anderson_ParkHall_0041.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10403":{"id":10403,"size":0,"label":"Anderson_ParkHall_0042.jpg","type":"image","content":"Anderson_ParkHall_0042.jpg","thumb":"Anderson_ParkHall_0042.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10404":{"id":10404,"size":0,"label":"Anderson_ParkHall_0043.jpg","type":"image","content":"Anderson_ParkHall_0043.jpg","thumb":"Anderson_ParkHall_0043.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10405":{"id":10405,"size":0,"label":"Anderson_ParkHall_0044.jpg","type":"image","content":"Anderson_ParkHall_0044.jpg","thumb":"Anderson_ParkHall_0044.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10406":{"id":10406,"size":0,"label":"Anderson_ParkHall_0045.jpg","type":"image","content":"Anderson_ParkHall_0045.jpg","thumb":"Anderson_ParkHall_0045.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10407":{"id":10407,"size":0,"label":"Anderson_ParkHall_0046.jpg","type":"image","content":"Anderson_ParkHall_0046.jpg","thumb":"Anderson_ParkHall_0046.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10408":{"id":10408,"size":0,"label":"Anderson_ParkHall_0047.jpg","type":"image","content":"Anderson_ParkHall_0047.jpg","thumb":"Anderson_ParkHall_0047.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10409":{"id":10409,"size":0,"label":"Anderson_ParkHall_0048.jpg","type":"image","content":"Anderson_ParkHall_0048.jpg","thumb":"Anderson_ParkHall_0048.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10410":{"id":10410,"size":0,"label":"Anderson_ParkHall_0049.jpg","type":"image","content":"Anderson_ParkHall_0049.jpg","thumb":"Anderson_ParkHall_0049.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10411":{"id":10411,"size":0,"label":"Anderson_ParkHall_0050.jpg","type":"image","content":"Anderson_ParkHall_0050.jpg","thumb":"Anderson_ParkHall_0050.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10412":{"id":10412,"size":0,"label":"Anderson_ParkHall_0051.jpg","type":"image","content":"Anderson_ParkHall_0051.jpg","thumb":"Anderson_ParkHall_0051.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10413":{"id":10413,"size":0,"label":"Anderson_ParkHall_0052.jpg","type":"image","content":"Anderson_ParkHall_0052.jpg","thumb":"Anderson_ParkHall_0052.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10414":{"id":10414,"size":0,"label":"Anderson_ParkHall_0053.jpg","type":"image","content":"Anderson_ParkHall_0053.jpg","thumb":"Anderson_ParkHall_0053.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10415":{"id":10415,"size":0,"label":"shoptiques-1.jpg","type":"image","content":"shoptiques-1.jpg","thumb":"shoptiques-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10416":{"id":10416,"size":0,"label":"shoptiques-2.jpg","type":"image","content":"shoptiques-2.jpg","thumb":"shoptiques-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10417":{"id":10417,"size":0,"label":"shoptiques-3.jpg","type":"image","content":"shoptiques-3.jpg","thumb":"shoptiques-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10418":{"id":10418,"size":0,"label":"shoptiques-4.jpg","type":"image","content":"shoptiques-4.jpg","thumb":"shoptiques-4.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10419":{"id":10419,"size":0,"label":"shoptiques-5.jpg","type":"image","content":"shoptiques-5.jpg","thumb":"shoptiques-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10420":{"id":10420,"size":0,"label":"shoptiques-6.jpg","type":"image","content":"shoptiques-6.jpg","thumb":"shoptiques-6.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10421":{"id":10421,"size":0,"label":"shoptiques-7.jpg","type":"image","content":"shoptiques-7.jpg","thumb":"shoptiques-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10422":{"id":10422,"size":0,"label":"shoptiques-8.jpg","type":"image","content":"shoptiques-8.jpg","thumb":"shoptiques-8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10423":{"id":10423,"size":0,"label":"shoptiques-9.jpg","type":"image","content":"shoptiques-9.jpg","thumb":"shoptiques-9.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10424":{"id":10424,"size":0,"label":"shoptiques-10.jpg","type":"image","content":"shoptiques-10.jpg","thumb":"shoptiques-10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10425":{"id":10425,"size":0,"label":"shoptiques-11.jpg","type":"image","content":"shoptiques-11.jpg","thumb":"shoptiques-11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10426":{"id":10426,"size":0,"label":"shoptiques-12.jpg","type":"image","content":"shoptiques-12.jpg","thumb":"shoptiques-12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10427":{"id":10427,"size":0,"label":"shoptiques-13.jpg","type":"image","content":"shoptiques-13.jpg","thumb":"shoptiques-13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10428":{"id":10428,"size":0,"label":"shoptiques-14.jpg","type":"image","content":"shoptiques-14.jpg","thumb":"shoptiques-14.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10429":{"id":10429,"size":0,"label":"shoptiques-15.jpg","type":"image","content":"shoptiques-15.jpg","thumb":"shoptiques-15.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10430":{"id":10430,"size":0,"label":"shoptiques-16.jpg","type":"image","content":"shoptiques-16.jpg","thumb":"shoptiques-16.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10431":{"id":10431,"size":0,"label":"shoptiques-17.jpg","type":"image","content":"shoptiques-17.jpg","thumb":"shoptiques-17.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10432":{"id":10432,"size":0,"label":"shoptiques-18.jpg","type":"image","content":"shoptiques-18.jpg","thumb":"shoptiques-18.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10433":{"id":10433,"size":0,"label":"shoptiques-19.jpg","type":"image","content":"shoptiques-19.jpg","thumb":"shoptiques-19.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10434":{"id":10434,"size":0,"label":"shoptiques-20.jpg","type":"image","content":"shoptiques-20.jpg","thumb":"shoptiques-20.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10435":{"id":10435,"size":0,"label":"shoptiques-21.jpg","type":"image","content":"shoptiques-21.jpg","thumb":"shoptiques-21.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10436":{"id":10436,"size":0,"label":"shoptiques-22.jpg","type":"image","content":"shoptiques-22.jpg","thumb":"shoptiques-22.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10437":{"id":10437,"size":0,"label":"shoptiques-23.jpg","type":"image","content":"shoptiques-23.jpg","thumb":"shoptiques-23.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10438":{"id":10438,"size":0,"label":"shoptiques-24.jpg","type":"image","content":"shoptiques-24.jpg","thumb":"shoptiques-24.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10439":{"id":10439,"size":0,"label":"shoptiques-25.jpg","type":"image","content":"shoptiques-25.jpg","thumb":"shoptiques-25.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10440":{"id":10440,"size":0,"label":"shoptiques-26.jpg","type":"image","content":"shoptiques-26.jpg","thumb":"shoptiques-26.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10441":{"id":10441,"size":0,"label":"shoptiques-27.jpg","type":"image","content":"shoptiques-27.jpg","thumb":"shoptiques-27.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10442":{"id":10442,"size":0,"label":"shoptiques-28.jpg","type":"image","content":"shoptiques-28.jpg","thumb":"shoptiques-28.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10443":{"id":10443,"size":0,"label":"shoptiques-29.jpg","type":"image","content":"shoptiques-29.jpg","thumb":"shoptiques-29.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10444":{"id":10444,"size":0,"label":"shoptiques-30.jpg","type":"image","content":"shoptiques-30.jpg","thumb":"shoptiques-30.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10445":{"id":10445,"size":0,"label":"shoptiques-31.jpg","type":"image","content":"shoptiques-31.jpg","thumb":"shoptiques-31.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10446":{"id":10446,"size":0,"label":"shoptiques-32.jpg","type":"image","content":"shoptiques-32.jpg","thumb":"shoptiques-32.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10447":{"id":10447,"size":0,"label":"shoptiques-33.jpg","type":"image","content":"shoptiques-33.jpg","thumb":"shoptiques-33.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10448":{"id":10448,"size":0,"label":"shoptiques-34.jpg","type":"image","content":"shoptiques-34.jpg","thumb":"shoptiques-34.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10449":{"id":10449,"size":0,"label":"shoptiques-35.jpg","type":"image","content":"shoptiques-35.jpg","thumb":"shoptiques-35.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10450":{"id":10450,"size":0,"label":"shoptiques-36.jpg","type":"image","content":"shoptiques-36.jpg","thumb":"shoptiques-36.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10451":{"id":10451,"size":0,"label":"shoptiques-37.jpg","type":"image","content":"shoptiques-37.jpg","thumb":"shoptiques-37.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10452":{"id":10452,"size":0,"label":"shoptiques-38.jpg","type":"image","content":"shoptiques-38.jpg","thumb":"shoptiques-38.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10453":{"id":10453,"size":0,"label":"shoptiques-39.jpg","type":"image","content":"shoptiques-39.jpg","thumb":"shoptiques-39.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10454":{"id":10454,"size":0,"label":"shoptiques-40.jpg","type":"image","content":"shoptiques-40.jpg","thumb":"shoptiques-40.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10455":{"id":10455,"size":0,"label":"shoptiques-41.jpg","type":"image","content":"shoptiques-41.jpg","thumb":"shoptiques-41.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10456":{"id":10456,"size":0,"label":"shoptiques-42.jpg","type":"image","content":"shoptiques-42.jpg","thumb":"shoptiques-42.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10457":{"id":10457,"size":0,"label":"shoptiques-43.jpg","type":"image","content":"shoptiques-43.jpg","thumb":"shoptiques-43.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10458":{"id":10458,"size":0,"label":"shoptiques-44.jpg","type":"image","content":"shoptiques-44.jpg","thumb":"shoptiques-44.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10459":{"id":10459,"size":0,"label":"shoptiques-45.jpg","type":"image","content":"shoptiques-45.jpg","thumb":"shoptiques-45.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10460":{"id":10460,"size":0,"label":"shoptiques-46.jpg","type":"image","content":"shoptiques-46.jpg","thumb":"shoptiques-46.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10461":{"id":10461,"size":0,"label":"shoptiques-47.jpg","type":"image","content":"shoptiques-47.jpg","thumb":"shoptiques-47.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10462":{"id":10462,"size":0,"label":"shoptiques-48.jpg","type":"image","content":"shoptiques-48.jpg","thumb":"shoptiques-48.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10463":{"id":10463,"size":0,"label":"shoptiques-49.jpg","type":"image","content":"shoptiques-49.jpg","thumb":"shoptiques-49.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10464":{"id":10464,"size":0,"label":"shoptiques-50.jpg","type":"image","content":"shoptiques-50.jpg","thumb":"shoptiques-50.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10465":{"id":10465,"size":0,"label":"shoptiques-51.jpg","type":"image","content":"shoptiques-51.jpg","thumb":"shoptiques-51.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10466":{"id":10466,"size":0,"label":"shoptiques-52.jpg","type":"image","content":"shoptiques-52.jpg","thumb":"shoptiques-52.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10467":{"id":10467,"size":0,"label":"shoptiques-53.jpg","type":"image","content":"shoptiques-53.jpg","thumb":"shoptiques-53.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10468":{"id":10468,"size":0,"label":"shoptiques-54.jpg","type":"image","content":"shoptiques-54.jpg","thumb":"shoptiques-54.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10469":{"id":10469,"size":0,"label":"shoptiques-55.jpg","type":"image","content":"shoptiques-55.jpg","thumb":"shoptiques-55.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10470":{"id":10470,"size":0,"label":"shoptiques-56.jpg","type":"image","content":"shoptiques-56.jpg","thumb":"shoptiques-56.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10471":{"id":10471,"size":0,"label":"shoptiques-57.jpg","type":"image","content":"shoptiques-57.jpg","thumb":"shoptiques-57.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10472":{"id":10472,"size":0,"label":"shoptiques-58.jpg","type":"image","content":"shoptiques-58.jpg","thumb":"shoptiques-58.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10473":{"id":10473,"size":0,"label":"shoptiques-59.jpg","type":"image","content":"shoptiques-59.jpg","thumb":"shoptiques-59.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10474":{"id":10474,"size":0,"label":"shoptiques-60.jpg","type":"image","content":"shoptiques-60.jpg","thumb":"shoptiques-60.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10475":{"id":10475,"size":0,"label":"shoptiques-61.jpg","type":"image","content":"shoptiques-61.jpg","thumb":"shoptiques-61.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10476":{"id":10476,"size":0,"label":"shoptiques-62.jpg","type":"image","content":"shoptiques-62.jpg","thumb":"shoptiques-62.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10477":{"id":10477,"size":0,"label":"shoptiques-63.jpg","type":"image","content":"shoptiques-63.jpg","thumb":"shoptiques-63.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10478":{"id":10478,"size":0,"label":"shoptiques-64.jpg","type":"image","content":"shoptiques-64.jpg","thumb":"shoptiques-64.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10479":{"id":10479,"size":0,"label":"shoptiques-65.jpg","type":"image","content":"shoptiques-65.jpg","thumb":"shoptiques-65.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10480":{"id":10480,"size":0,"label":"shoptiques-66.jpg","type":"image","content":"shoptiques-66.jpg","thumb":"shoptiques-66.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10481":{"id":10481,"size":0,"label":"Oliberte2013-1.jpg","type":"image","content":"Oliberte2013-1.jpg","thumb":"Oliberte2013-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10482":{"id":10482,"size":0,"label":"Oliberte2013-2.jpg","type":"image","content":"Oliberte2013-2.jpg","thumb":"Oliberte2013-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10483":{"id":10483,"size":0,"label":"Oliberte2013-3.jpg","type":"image","content":"Oliberte2013-3.jpg","thumb":"Oliberte2013-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10484":{"id":10484,"size":0,"label":"Oliberte2013-4.jpg","type":"image","content":"Oliberte2013-4.jpg","thumb":"Oliberte2013-4.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10485":{"id":10485,"size":0,"label":"Oliberte2013-5.jpg","type":"image","content":"Oliberte2013-5.jpg","thumb":"Oliberte2013-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10486":{"id":10486,"size":0,"label":"Oliberte2013-6.jpg","type":"image","content":"Oliberte2013-6.jpg","thumb":"Oliberte2013-6.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10487":{"id":10487,"size":0,"label":"Oliberte2013-7.jpg","type":"image","content":"Oliberte2013-7.jpg","thumb":"Oliberte2013-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10488":{"id":10488,"size":0,"label":"Oliberte2013-8.jpg","type":"image","content":"Oliberte2013-8.jpg","thumb":"Oliberte2013-8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10489":{"id":10489,"size":0,"label":"Oliberte2013-9.jpg","type":"image","content":"Oliberte2013-9.jpg","thumb":"Oliberte2013-9.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10490":{"id":10490,"size":0,"label":"Oliberte2013-10.jpg","type":"image","content":"Oliberte2013-10.jpg","thumb":"Oliberte2013-10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10491":{"id":10491,"size":0,"label":"Oliberte2013-11.jpg","type":"image","content":"Oliberte2013-11.jpg","thumb":"Oliberte2013-11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10492":{"id":10492,"size":0,"label":"Oliberte2013-12.jpg","type":"image","content":"Oliberte2013-12.jpg","thumb":"Oliberte2013-12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10493":{"id":10493,"size":0,"label":"Oliberte2013-13.jpg","type":"image","content":"Oliberte2013-13.jpg","thumb":"Oliberte2013-13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10494":{"id":10494,"size":0,"label":"Oliberte2013-14.jpg","type":"image","content":"Oliberte2013-14.jpg","thumb":"Oliberte2013-14.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10495":{"id":10495,"size":0,"label":"Oliberte2013-15.jpg","type":"image","content":"Oliberte2013-15.jpg","thumb":"Oliberte2013-15.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10496":{"id":10496,"size":0,"label":"Oliberte2013-16.jpg","type":"image","content":"Oliberte2013-16.jpg","thumb":"Oliberte2013-16.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10497":{"id":10497,"size":0,"label":"Oliberte2013-17.jpg","type":"image","content":"Oliberte2013-17.jpg","thumb":"Oliberte2013-17.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10498":{"id":10498,"size":0,"label":"Oliberte2013-18.jpg","type":"image","content":"Oliberte2013-18.jpg","thumb":"Oliberte2013-18.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10499":{"id":10499,"size":0,"label":"Oliberte2013-19.jpg","type":"image","content":"Oliberte2013-19.jpg","thumb":"Oliberte2013-19.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10500":{"id":10500,"size":0,"label":"Oliberte2013-20.jpg","type":"image","content":"Oliberte2013-20.jpg","thumb":"Oliberte2013-20.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10501":{"id":10501,"size":0,"label":"Oliberte2013-21.jpg","type":"image","content":"Oliberte2013-21.jpg","thumb":"Oliberte2013-21.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10502":{"id":10502,"size":0,"label":"Oliberte2013-22.jpg","type":"image","content":"Oliberte2013-22.jpg","thumb":"Oliberte2013-22.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10503":{"id":10503,"size":0,"label":"Oliberte2013-23.jpg","type":"image","content":"Oliberte2013-23.jpg","thumb":"Oliberte2013-23.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10504":{"id":10504,"size":0,"label":"Oliberte2013-24.jpg","type":"image","content":"Oliberte2013-24.jpg","thumb":"Oliberte2013-24.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10505":{"id":10505,"size":0,"label":"Oliberte2013-25.jpg","type":"image","content":"Oliberte2013-25.jpg","thumb":"Oliberte2013-25.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10506":{"id":10506,"size":0,"label":"Oliberte2013-26.jpg","type":"image","content":"Oliberte2013-26.jpg","thumb":"Oliberte2013-26.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10507":{"id":10507,"size":0,"label":"Oliberte2013-27.jpg","type":"image","content":"Oliberte2013-27.jpg","thumb":"Oliberte2013-27.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10508":{"id":10508,"size":0,"label":"Oliberte2013-28.jpg","type":"image","content":"Oliberte2013-28.jpg","thumb":"Oliberte2013-28.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10509":{"id":10509,"size":0,"label":"Oliberte2013-29.jpg","type":"image","content":"Oliberte2013-29.jpg","thumb":"Oliberte2013-29.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10510":{"id":10510,"size":0,"label":"Oliberte2013-30.jpg","type":"image","content":"Oliberte2013-30.jpg","thumb":"Oliberte2013-30.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10511":{"id":10511,"size":0,"label":"Oliberte2013-31.jpg","type":"image","content":"Oliberte2013-31.jpg","thumb":"Oliberte2013-31.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10512":{"id":10512,"size":0,"label":"Oliberte2013-32.jpg","type":"image","content":"Oliberte2013-32.jpg","thumb":"Oliberte2013-32.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10513":{"id":10513,"size":0,"label":"Oliberte2013-33.jpg","type":"image","content":"Oliberte2013-33.jpg","thumb":"Oliberte2013-33.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10514":{"id":10514,"size":0,"label":"Oliberte2013-34.jpg","type":"image","content":"Oliberte2013-34.jpg","thumb":"Oliberte2013-34.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10515":{"id":10515,"size":0,"label":"Oliberte2013-35.jpg","type":"image","content":"Oliberte2013-35.jpg","thumb":"Oliberte2013-35.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10516":{"id":10516,"size":0,"label":"Oliberte2013-36.jpg","type":"image","content":"Oliberte2013-36.jpg","thumb":"Oliberte2013-36.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10517":{"id":10517,"size":0,"label":"Oliberte2013-37.jpg","type":"image","content":"Oliberte2013-37.jpg","thumb":"Oliberte2013-37.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10518":{"id":10518,"size":0,"label":"WERTZ_HASS_006b.jpg","type":"image","content":"WERTZ_HASS_006b.jpg","thumb":"WERTZ_HASS_006b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10519":{"id":10519,"size":0,"label":"wertz_pola_color_013B.jpg","type":"image","content":"wertz_pola_color_013B.jpg","thumb":"wertz_pola_color_013B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10520":{"id":10520,"size":0,"label":"wertz_pola_neg_14.jpg","type":"image","content":"wertz_pola_neg_14.jpg","thumb":"wertz_pola_neg_14.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10521":{"id":10521,"size":0,"label":"WERTZ-8291.jpg","type":"image","content":"WERTZ-8291.jpg","thumb":"WERTZ-8291.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10522":{"id":10522,"size":0,"label":"WERTZ-8340.jpg","type":"image","content":"WERTZ-8340.jpg","thumb":"WERTZ-8340.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10523":{"id":10523,"size":0,"label":"WERTZ-8356.jpg","type":"image","content":"WERTZ-8356.jpg","thumb":"WERTZ-8356.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10524":{"id":10524,"size":0,"label":"WERTZ-9511.jpg","type":"image","content":"WERTZ-9511.jpg","thumb":"WERTZ-9511.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10525":{"id":10525,"size":0,"label":"IMG_8578.jpg","type":"image","content":"IMG_8578.jpg","thumb":"IMG_8578.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10526":{"id":10526,"size":0,"label":"LASTROYALS-49-DUP.jpg","type":"image","content":"LASTROYALS-49-DUP.jpg","thumb":"LASTROYALS-49-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10527":{"id":10527,"size":0,"label":"LASTROYALS-12-DUP.jpg","type":"image","content":"LASTROYALS-12-DUP.jpg","thumb":"LASTROYALS-12-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10528":{"id":10528,"size":0,"label":"IMG_9817b.jpg","type":"image","content":"IMG_9817b.jpg","thumb":"IMG_9817b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10529":{"id":10529,"size":0,"label":"IMG_9013b.jpg","type":"image","content":"IMG_9013b.jpg","thumb":"IMG_9013b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10530":{"id":10530,"size":0,"label":"IMG_7956-DUP.jpg","type":"image","content":"IMG_7956-DUP.jpg","thumb":"IMG_7956-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10531":{"id":10531,"size":0,"label":"nataliejohn_0939.jpg","type":"image","content":"nataliejohn_0939.jpg","thumb":"nataliejohn_0939.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10532":{"id":10532,"size":0,"label":"DAYLIGHTS_5846.jpg","type":"image","content":"DAYLIGHTS_5846.jpg","thumb":"DAYLIGHTS_5846.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10533":{"id":10533,"size":0,"label":"ERICANDERSON_GRO_DPRO_IMG_2188 copy.jpg","type":"image","content":"ERICANDERSON_GRO_DPRO_IMG_2188 copy.jpg","thumb":"ERICANDERSON_GRO_DPRO_IMG_2188 copy.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10534":{"id":10534,"size":0,"label":"IMG_6231.jpg","type":"image","content":"IMG_6231.jpg","thumb":"IMG_6231.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10535":{"id":10535,"size":0,"label":"_MG_7433.jpg","type":"image","content":"_MG_7433.jpg","thumb":"_MG_7433.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10536":{"id":10536,"size":0,"label":"RRNPOLA-14.jpg","type":"image","content":"RRNPOLA-14.jpg","thumb":"RRNPOLA-14.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10537":{"id":10537,"size":0,"label":"RRNPOLA-16.jpg","type":"image","content":"RRNPOLA-16.jpg","thumb":"RRNPOLA-16.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10538":{"id":10538,"size":0,"label":"063A2549.jpg","type":"image","content":"063A2549.jpg","thumb":"063A2549.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10539":{"id":10539,"size":0,"label":"Anderson_LongleyPOLA-5.jpg","type":"image","content":"Anderson_LongleyPOLA-5.jpg","thumb":"Anderson_LongleyPOLA-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10540":{"id":10540,"size":0,"label":"Anderson_LongleyPOLA-7.jpg","type":"image","content":"Anderson_LongleyPOLA-7.jpg","thumb":"Anderson_LongleyPOLA-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10541":{"id":10541,"size":0,"label":"Anderson_Longley-79.jpg","type":"image","content":"Anderson_Longley-79.jpg","thumb":"Anderson_Longley-79.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10542":{"id":10542,"size":0,"label":"Anderson_Longley-185.jpg","type":"image","content":"Anderson_Longley-185.jpg","thumb":"Anderson_Longley-185.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10543":{"id":10543,"size":0,"label":"Anderson_Longley-51.jpg","type":"image","content":"Anderson_Longley-51.jpg","thumb":"Anderson_Longley-51.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10544":{"id":10544,"size":0,"label":"Anderson_Longley-22.jpg","type":"image","content":"Anderson_Longley-22.jpg","thumb":"Anderson_Longley-22.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10545":{"id":10545,"size":0,"label":"BOBBYLONG_ANDERSON-198.jpg","type":"image","content":"BOBBYLONG_ANDERSON-198.jpg","thumb":"BOBBYLONG_ANDERSON-198.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10546":{"id":10546,"size":0,"label":"BOBBYLONG_ANDERSON-188.jpg","type":"image","content":"BOBBYLONG_ANDERSON-188.jpg","thumb":"BOBBYLONG_ANDERSON-188.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10547":{"id":10547,"size":0,"label":"BOBBYLONG_ANDERSON-189.jpg","type":"image","content":"BOBBYLONG_ANDERSON-189.jpg","thumb":"BOBBYLONG_ANDERSON-189.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10548":{"id":10548,"size":0,"label":"BOBBYLONG_ANDERSON-187.jpg","type":"image","content":"BOBBYLONG_ANDERSON-187.jpg","thumb":"BOBBYLONG_ANDERSON-187.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10549":{"id":10549,"size":0,"label":"BOBBYLONG_ANDERSON-176.jpg","type":"image","content":"BOBBYLONG_ANDERSON-176.jpg","thumb":"BOBBYLONG_ANDERSON-176.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10550":{"id":10550,"size":0,"label":"BOBBYLONG_ANDERSON-163.jpg","type":"image","content":"BOBBYLONG_ANDERSON-163.jpg","thumb":"BOBBYLONG_ANDERSON-163.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10551":{"id":10551,"size":0,"label":"BOBBYLONG_ANDERSON-148.jpg","type":"image","content":"BOBBYLONG_ANDERSON-148.jpg","thumb":"BOBBYLONG_ANDERSON-148.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10552":{"id":10552,"size":0,"label":"BOBBYLONG_ANDERSON-154.jpg","type":"image","content":"BOBBYLONG_ANDERSON-154.jpg","thumb":"BOBBYLONG_ANDERSON-154.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10553":{"id":10553,"size":0,"label":"BOBBYLONG_ANDERSON-95.jpg","type":"image","content":"BOBBYLONG_ANDERSON-95.jpg","thumb":"BOBBYLONG_ANDERSON-95.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10554":{"id":10554,"size":0,"label":"granger_POLA_30B.jpg","type":"image","content":"granger_POLA_30B.jpg","thumb":"granger_POLA_30B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10555":{"id":10555,"size":0,"label":"granger_POLA_01.jpg","type":"image","content":"granger_POLA_01.jpg","thumb":"granger_POLA_01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10556":{"id":10556,"size":0,"label":"ANDERSON_GRANGER-3947.jpg","type":"image","content":"ANDERSON_GRANGER-3947.jpg","thumb":"ANDERSON_GRANGER-3947.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10557":{"id":10557,"size":0,"label":"ANDERSON_GRANGER-3373.jpg","type":"image","content":"ANDERSON_GRANGER-3373.jpg","thumb":"ANDERSON_GRANGER-3373.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10558":{"id":10558,"size":0,"label":"ANDERSON_GRANGER-.jpg","type":"image","content":"ANDERSON_GRANGER-.jpg","thumb":"ANDERSON_GRANGER-.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10559":{"id":10559,"size":0,"label":"bellow-7068-EditC.jpg","type":"image","content":"bellow-7068-EditC.jpg","thumb":"bellow-7068-EditC.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10560":{"id":10560,"size":0,"label":"bellowpola_09A.jpg","type":"image","content":"bellowpola_09A.jpg","thumb":"bellowpola_09A.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10561":{"id":10561,"size":0,"label":"bellowpola_010B.jpg","type":"image","content":"bellowpola_010B.jpg","thumb":"bellowpola_010B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10562":{"id":10562,"size":0,"label":"bellowpola_02B.jpg","type":"image","content":"bellowpola_02B.jpg","thumb":"bellowpola_02B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10563":{"id":10563,"size":0,"label":"bellowpola_07D.jpg","type":"image","content":"bellowpola_07D.jpg","thumb":"bellowpola_07D.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10564":{"id":10564,"size":0,"label":"bellow-6996.jpg","type":"image","content":"bellow-6996.jpg","thumb":"bellow-6996.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10565":{"id":10565,"size":0,"label":"bellowpola_012B-Edit-2.jpg","type":"image","content":"bellowpola_012B-Edit-2.jpg","thumb":"bellowpola_012B-Edit-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10566":{"id":10566,"size":0,"label":"bellow-6665.jpg","type":"image","content":"bellow-6665.jpg","thumb":"bellow-6665.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10567":{"id":10567,"size":0,"label":"_MG_9975.jpg","type":"image","content":"_MG_9975.jpg","thumb":"_MG_9975.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10568":{"id":10568,"size":0,"label":"COUR_POLA_39b.jpg","type":"image","content":"COUR_POLA_39b.jpg","thumb":"COUR_POLA_39b.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10569":{"id":10569,"size":0,"label":"IMG_4158.jpg","type":"image","content":"IMG_4158.jpg","thumb":"IMG_4158.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10570":{"id":10570,"size":0,"label":"_MG_8008.jpg","type":"image","content":"_MG_8008.jpg","thumb":"_MG_8008.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10571":{"id":10571,"size":0,"label":"_MG_3907.jpg","type":"image","content":"_MG_3907.jpg","thumb":"_MG_3907.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10572":{"id":10572,"size":0,"label":"IMG_6055-DUP.jpg","type":"image","content":"IMG_6055-DUP.jpg","thumb":"IMG_6055-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10573":{"id":10573,"size":0,"label":"IMG_6071-DUP.jpg","type":"image","content":"IMG_6071-DUP.jpg","thumb":"IMG_6071-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10574":{"id":10574,"size":0,"label":"IMG_6079-Edit-DUP.jpg","type":"image","content":"IMG_6079-Edit-DUP.jpg","thumb":"IMG_6079-Edit-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10575":{"id":10575,"size":0,"label":"IMG_6148-Edit.jpg","type":"image","content":"IMG_6148-Edit.jpg","thumb":"IMG_6148-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10576":{"id":10576,"size":0,"label":"IMG_6154-Edit-DUP.jpg","type":"image","content":"IMG_6154-Edit-DUP.jpg","thumb":"IMG_6154-Edit-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10577":{"id":10577,"size":0,"label":"IMG_6165-Edit-DUP.jpg","type":"image","content":"IMG_6165-Edit-DUP.jpg","thumb":"IMG_6165-Edit-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10578":{"id":10578,"size":0,"label":"IMG_6176.jpg","type":"image","content":"IMG_6176.jpg","thumb":"IMG_6176.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10579":{"id":10579,"size":0,"label":"IMG_6198-Edit-DUP.jpg","type":"image","content":"IMG_6198-Edit-DUP.jpg","thumb":"IMG_6198-Edit-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10580":{"id":10580,"size":0,"label":"IMG_6217-DUP.jpg","type":"image","content":"IMG_6217-DUP.jpg","thumb":"IMG_6217-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10581":{"id":10581,"size":0,"label":"IMG_6231-DUP.jpg","type":"image","content":"IMG_6231-DUP.jpg","thumb":"IMG_6231-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"BOOK ONE"},"10582":{"id":10582,"size":0,"label":"IMG_6265.jpg","type":"image","content":"IMG_6265.jpg","thumb":"IMG_6265.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10583":{"id":10583,"size":0,"label":"IMG_6383-DUP.jpg","type":"image","content":"IMG_6383-DUP.jpg","thumb":"IMG_6383-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10584":{"id":10584,"size":0,"label":"IMG_6440-Edit-DUP.jpg","type":"image","content":"IMG_6440-Edit-DUP.jpg","thumb":"IMG_6440-Edit-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10585":{"id":10585,"size":0,"label":"IMG_6476-DUP.jpg","type":"image","content":"IMG_6476-DUP.jpg","thumb":"IMG_6476-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10586":{"id":10586,"size":0,"label":"IMG_6569.jpg","type":"image","content":"IMG_6569.jpg","thumb":"IMG_6569.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10587":{"id":10587,"size":0,"label":"IMG_6625.jpg","type":"image","content":"IMG_6625.jpg","thumb":"IMG_6625.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10588":{"id":10588,"size":0,"label":"IMG_6629.jpg","type":"image","content":"IMG_6629.jpg","thumb":"IMG_6629.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10589":{"id":10589,"size":0,"label":"063A1114.jpg","type":"image","content":"063A1114.jpg","thumb":"063A1114.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10590":{"id":10590,"size":0,"label":"063A1204.jpg","type":"image","content":"063A1204.jpg","thumb":"063A1204.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10591":{"id":10591,"size":0,"label":"063A1385.jpg","type":"image","content":"063A1385.jpg","thumb":"063A1385.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10592":{"id":10592,"size":0,"label":"063A1417.jpg","type":"image","content":"063A1417.jpg","thumb":"063A1417.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10593":{"id":10593,"size":0,"label":"063A1548-2.jpg","type":"image","content":"063A1548-2.jpg","thumb":"063A1548-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10594":{"id":10594,"size":0,"label":"063A2520.jpg","type":"image","content":"063A2520.jpg","thumb":"063A2520.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10595":{"id":10595,"size":0,"label":"063A2651.jpg","type":"image","content":"063A2651.jpg","thumb":"063A2651.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10596":{"id":10596,"size":0,"label":"063A2749.jpg","type":"image","content":"063A2749.jpg","thumb":"063A2749.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10597":{"id":10597,"size":0,"label":"063A2966.jpg","type":"image","content":"063A2966.jpg","thumb":"063A2966.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10598":{"id":10598,"size":0,"label":"063A3129.jpg","type":"image","content":"063A3129.jpg","thumb":"063A3129.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10599":{"id":10599,"size":0,"label":"Untitled2.jpg","type":"image","content":"Untitled2.jpg","thumb":"Untitled2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10600":{"id":10600,"size":0,"label":"WillyMason_POLA_013C.jpg","type":"image","content":"WillyMason_POLA_013C.jpg","thumb":"WillyMason_POLA_013C.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10601":{"id":10601,"size":0,"label":"_MG_2613.jpg","type":"image","content":"_MG_2613.jpg","thumb":"_MG_2613.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10602":{"id":10602,"size":0,"label":"albertacross.jpg","type":"image","content":"albertacross.jpg","thumb":"albertacross.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10603":{"id":10603,"size":0,"label":"EA_branch_002.jpg","type":"image","content":"EA_branch_002.jpg","thumb":"EA_branch_002.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10604":{"id":10604,"size":0,"label":"EA_MARCH_001.jpg","type":"image","content":"EA_MARCH_001.jpg","thumb":"EA_MARCH_001.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10605":{"id":10605,"size":0,"label":"Holcomb_pola_B01.jpg","type":"image","content":"Holcomb_pola_B01.jpg","thumb":"Holcomb_pola_B01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10606":{"id":10606,"size":0,"label":"063A2262.jpg","type":"image","content":"063A2262.jpg","thumb":"063A2262.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10607":{"id":10607,"size":0,"label":"063A3456.jpg","type":"image","content":"063A3456.jpg","thumb":"063A3456.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10608":{"id":10608,"size":0,"label":"063A1719.jpg","type":"image","content":"063A1719.jpg","thumb":"063A1719.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10609":{"id":10609,"size":0,"label":"SEANLEE_POLA_001D-Edit.jpg","type":"image","content":"SEANLEE_POLA_001D-Edit.jpg","thumb":"SEANLEE_POLA_001D-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10610":{"id":10610,"size":0,"label":"SEANLEE_POLA_002B.jpg","type":"image","content":"SEANLEE_POLA_002B.jpg","thumb":"SEANLEE_POLA_002B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10611":{"id":10611,"size":0,"label":"SEANLEE_POLA_007A-Edit.jpg","type":"image","content":"SEANLEE_POLA_007A-Edit.jpg","thumb":"SEANLEE_POLA_007A-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10612":{"id":10612,"size":0,"label":"2GALLANTS-0076.jpg","type":"image","content":"2GALLANTS-0076.jpg","thumb":"2GALLANTS-0076.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10613":{"id":10613,"size":0,"label":"BELLOW-3054.jpg","type":"image","content":"BELLOW-3054.jpg","thumb":"BELLOW-3054.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10614":{"id":10614,"size":0,"label":"ARTTM_214_HR.jpg","type":"image","content":"ARTTM_214_HR.jpg","thumb":"ARTTM_214_HR.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10615":{"id":10615,"size":0,"label":"ARTTM_232_HR.jpg","type":"image","content":"ARTTM_232_HR.jpg","thumb":"ARTTM_232_HR.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10616":{"id":10616,"size":0,"label":"ARTTM_193_HR.jpg","type":"image","content":"ARTTM_193_HR.jpg","thumb":"ARTTM_193_HR.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10617":{"id":10617,"size":0,"label":"ARTTM_015_HR.jpg","type":"image","content":"ARTTM_015_HR.jpg","thumb":"ARTTM_015_HR.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10618":{"id":10618,"size":0,"label":"FORTLEAN_POLA_02Ab.jpg","type":"image","content":"FORTLEAN_POLA_02Ab.jpg","thumb":"FORTLEAN_POLA_02Ab.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10619":{"id":10619,"size":0,"label":"NTBFILM_FILLMORE.jpg","type":"image","content":"NTBFILM_FILLMORE.jpg","thumb":"NTBFILM_FILLMORE.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10620":{"id":10620,"size":0,"label":"_MG_1284.jpg","type":"image","content":"_MG_1284.jpg","thumb":"_MG_1284.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10621":{"id":10621,"size":0,"label":"_MG_7238B.jpg","type":"image","content":"_MG_7238B.jpg","thumb":"_MG_7238B.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10622":{"id":10622,"size":0,"label":"_MG_0032 (1).jpg","type":"image","content":"_MG_0032 (1).jpg","thumb":"_MG_0032 (1).jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10623":{"id":10623,"size":0,"label":"_MG_5033-2.jpg","type":"image","content":"_MG_5033-2.jpg","thumb":"_MG_5033-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10624":{"id":10624,"size":0,"label":"_MG_9647.jpg","type":"image","content":"_MG_9647.jpg","thumb":"_MG_9647.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10625":{"id":10625,"size":0,"label":"_MG_4988.jpg","type":"image","content":"_MG_4988.jpg","thumb":"_MG_4988.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10626":{"id":10626,"size":0,"label":"AA_03-DUP.jpg","type":"image","content":"AA_03-DUP.jpg","thumb":"AA_03-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10627":{"id":10627,"size":0,"label":"_MG_8460.jpg","type":"image","content":"_MG_8460.jpg","thumb":"_MG_8460.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10628":{"id":10628,"size":0,"label":"IMG_8380.jpg","type":"image","content":"IMG_8380.jpg","thumb":"IMG_8380.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10629":{"id":10629,"size":0,"label":"IMG_0249-Edit.jpg","type":"image","content":"IMG_0249-Edit.jpg","thumb":"IMG_0249-Edit.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10630":{"id":10630,"size":0,"label":"IMG_9871-Edit-DUP.jpg","type":"image","content":"IMG_9871-Edit-DUP.jpg","thumb":"IMG_9871-Edit-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10631":{"id":10631,"size":0,"label":"APRIL_POLA.jpg","type":"image","content":"APRIL_POLA.jpg","thumb":"APRIL_POLA.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10632":{"id":10632,"size":0,"label":"GreatLakeSwimmers_01.jpg","type":"image","content":"GreatLakeSwimmers_01.jpg","thumb":"GreatLakeSwimmers_01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10633":{"id":10633,"size":0,"label":"IMG_6561.jpg","type":"image","content":"IMG_6561.jpg","thumb":"IMG_6561.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10634":{"id":10634,"size":0,"label":"IMG_5150.jpg","type":"image","content":"IMG_5150.jpg","thumb":"IMG_5150.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10635":{"id":10635,"size":0,"label":"IMG_4927.jpg","type":"image","content":"IMG_4927.jpg","thumb":"IMG_4927.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10636":{"id":10636,"size":0,"label":"IMG_9106.jpg","type":"image","content":"IMG_9106.jpg","thumb":"IMG_9106.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10637":{"id":10637,"size":0,"label":"PANIC-4965.jpg","type":"image","content":"PANIC-4965.jpg","thumb":"PANIC-4965.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10638":{"id":10638,"size":0,"label":"EARLYHOURS-2320.jpg","type":"image","content":"EARLYHOURS-2320.jpg","thumb":"EARLYHOURS-2320.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10639":{"id":10639,"size":0,"label":"EARLYHOURS-2571.jpg","type":"image","content":"EARLYHOURS-2571.jpg","thumb":"EARLYHOURS-2571.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10640":{"id":10640,"size":0,"label":"GRO_13.jpg","type":"image","content":"GRO_13.jpg","thumb":"GRO_13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10641":{"id":10641,"size":0,"label":"IMG_7764-DUP.jpg","type":"image","content":"IMG_7764-DUP.jpg","thumb":"IMG_7764-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10642":{"id":10642,"size":0,"label":"IMG_1492.jpg","type":"image","content":"IMG_1492.jpg","thumb":"IMG_1492.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10643":{"id":10643,"size":0,"label":"IMG_9777.jpg","type":"image","content":"IMG_9777.jpg","thumb":"IMG_9777.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10644":{"id":10644,"size":0,"label":"IMG_5945-DUP.jpg","type":"image","content":"IMG_5945-DUP.jpg","thumb":"IMG_5945-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10645":{"id":10645,"size":0,"label":"IMG_5955.jpg","type":"image","content":"IMG_5955.jpg","thumb":"IMG_5955.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10646":{"id":10646,"size":0,"label":"IMG_5870.jpg","type":"image","content":"IMG_5870.jpg","thumb":"IMG_5870.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10647":{"id":10647,"size":0,"label":"DAYLIGHTS_5846-DUP.jpg","type":"image","content":"DAYLIGHTS_5846-DUP.jpg","thumb":"DAYLIGHTS_5846-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10648":{"id":10648,"size":0,"label":"IMG_0961.jpg","type":"image","content":"IMG_0961.jpg","thumb":"IMG_0961.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10649":{"id":10649,"size":0,"label":"IMG_0920.jpg","type":"image","content":"IMG_0920.jpg","thumb":"IMG_0920.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10650":{"id":10650,"size":0,"label":"_MG_5171.jpg","type":"image","content":"_MG_5171.jpg","thumb":"_MG_5171.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10651":{"id":10651,"size":0,"label":"_MG_3634.jpg","type":"image","content":"_MG_3634.jpg","thumb":"_MG_3634.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10652":{"id":10652,"size":0,"label":"ANDERSON_STY-1.jpg","type":"image","content":"ANDERSON_STY-1.jpg","thumb":"ANDERSON_STY-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10653":{"id":10653,"size":0,"label":"ANDERSON_STY-2.jpg","type":"image","content":"ANDERSON_STY-2.jpg","thumb":"ANDERSON_STY-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10654":{"id":10654,"size":0,"label":"ANDERSON_STY-3.jpg","type":"image","content":"ANDERSON_STY-3.jpg","thumb":"ANDERSON_STY-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10655":{"id":10655,"size":0,"label":"ANDERSON_STY-4.jpg","type":"image","content":"ANDERSON_STY-4.jpg","thumb":"ANDERSON_STY-4.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10656":{"id":10656,"size":0,"label":"ANDERSON_STY-5.jpg","type":"image","content":"ANDERSON_STY-5.jpg","thumb":"ANDERSON_STY-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10657":{"id":10657,"size":0,"label":"ANDERSON_STY-6.jpg","type":"image","content":"ANDERSON_STY-6.jpg","thumb":"ANDERSON_STY-6.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10658":{"id":10658,"size":0,"label":"ANDERSON_STY-7.jpg","type":"image","content":"ANDERSON_STY-7.jpg","thumb":"ANDERSON_STY-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10659":{"id":10659,"size":0,"label":"ANDERSON_STY-8.jpg","type":"image","content":"ANDERSON_STY-8.jpg","thumb":"ANDERSON_STY-8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10660":{"id":10660,"size":0,"label":"ANDERSON_STY-9.jpg","type":"image","content":"ANDERSON_STY-9.jpg","thumb":"ANDERSON_STY-9.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10661":{"id":10661,"size":0,"label":"ANDERSON_STY-10.jpg","type":"image","content":"ANDERSON_STY-10.jpg","thumb":"ANDERSON_STY-10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10662":{"id":10662,"size":0,"label":"ANDERSON_STY-11.jpg","type":"image","content":"ANDERSON_STY-11.jpg","thumb":"ANDERSON_STY-11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10663":{"id":10663,"size":0,"label":"ANDERSON_STY-12.jpg","type":"image","content":"ANDERSON_STY-12.jpg","thumb":"ANDERSON_STY-12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10664":{"id":10664,"size":0,"label":"ANDERSON_STY-13.jpg","type":"image","content":"ANDERSON_STY-13.jpg","thumb":"ANDERSON_STY-13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10665":{"id":10665,"size":0,"label":"ANDERSON_STY-14.jpg","type":"image","content":"ANDERSON_STY-14.jpg","thumb":"ANDERSON_STY-14.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10666":{"id":10666,"size":0,"label":"ANDERSON_STY-15.jpg","type":"image","content":"ANDERSON_STY-15.jpg","thumb":"ANDERSON_STY-15.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10667":{"id":10667,"size":0,"label":"ANDERSON_STY-16.jpg","type":"image","content":"ANDERSON_STY-16.jpg","thumb":"ANDERSON_STY-16.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10668":{"id":10668,"size":0,"label":"ANDERSON_STY-17.jpg","type":"image","content":"ANDERSON_STY-17.jpg","thumb":"ANDERSON_STY-17.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10669":{"id":10669,"size":0,"label":"ANDERSON_STY-18.jpg","type":"image","content":"ANDERSON_STY-18.jpg","thumb":"ANDERSON_STY-18.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10670":{"id":10670,"size":0,"label":"ANDERSON_STY-19.jpg","type":"image","content":"ANDERSON_STY-19.jpg","thumb":"ANDERSON_STY-19.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10671":{"id":10671,"size":0,"label":"ANDERSON_STY-20.jpg","type":"image","content":"ANDERSON_STY-20.jpg","thumb":"ANDERSON_STY-20.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10672":{"id":10672,"size":0,"label":"ANDERSON_STY-21.jpg","type":"image","content":"ANDERSON_STY-21.jpg","thumb":"ANDERSON_STY-21.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10673":{"id":10673,"size":0,"label":"ANDERSON_STY-22.jpg","type":"image","content":"ANDERSON_STY-22.jpg","thumb":"ANDERSON_STY-22.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10674":{"id":10674,"size":0,"label":"ANDERSON_STY-23.jpg","type":"image","content":"ANDERSON_STY-23.jpg","thumb":"ANDERSON_STY-23.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10675":{"id":10675,"size":0,"label":"ANDERSON_STY-24.jpg","type":"image","content":"ANDERSON_STY-24.jpg","thumb":"ANDERSON_STY-24.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10676":{"id":10676,"size":0,"label":"ANDERSON_STY-25.jpg","type":"image","content":"ANDERSON_STY-25.jpg","thumb":"ANDERSON_STY-25.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10677":{"id":10677,"size":0,"label":"ANDERSON_STY-26.jpg","type":"image","content":"ANDERSON_STY-26.jpg","thumb":"ANDERSON_STY-26.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10678":{"id":10678,"size":0,"label":"ANDERSON_STY-27.jpg","type":"image","content":"ANDERSON_STY-27.jpg","thumb":"ANDERSON_STY-27.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10679":{"id":10679,"size":0,"label":"ANDERSON_STY-28.jpg","type":"image","content":"ANDERSON_STY-28.jpg","thumb":"ANDERSON_STY-28.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10680":{"id":10680,"size":0,"label":"ANDERSON_STY-29.jpg","type":"image","content":"ANDERSON_STY-29.jpg","thumb":"ANDERSON_STY-29.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10681":{"id":10681,"size":0,"label":"ANDERSON_STY-30.jpg","type":"image","content":"ANDERSON_STY-30.jpg","thumb":"ANDERSON_STY-30.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10682":{"id":10682,"size":0,"label":"ANDERSON_STY-31.jpg","type":"image","content":"ANDERSON_STY-31.jpg","thumb":"ANDERSON_STY-31.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10683":{"id":10683,"size":0,"label":"ANDERSON_STY-32.jpg","type":"image","content":"ANDERSON_STY-32.jpg","thumb":"ANDERSON_STY-32.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10684":{"id":10684,"size":0,"label":"ANDERSON_STY-33.jpg","type":"image","content":"ANDERSON_STY-33.jpg","thumb":"ANDERSON_STY-33.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10685":{"id":10685,"size":0,"label":"ANDERSON_STY-34.jpg","type":"image","content":"ANDERSON_STY-34.jpg","thumb":"ANDERSON_STY-34.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10686":{"id":10686,"size":0,"label":"ANDERSON_STY-35.jpg","type":"image","content":"ANDERSON_STY-35.jpg","thumb":"ANDERSON_STY-35.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10687":{"id":10687,"size":0,"label":"ANDERSON_STY-36.jpg","type":"image","content":"ANDERSON_STY-36.jpg","thumb":"ANDERSON_STY-36.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10688":{"id":10688,"size":0,"label":"ANDERSON_STY-37.jpg","type":"image","content":"ANDERSON_STY-37.jpg","thumb":"ANDERSON_STY-37.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10689":{"id":10689,"size":0,"label":"ANDERSON_STY-38.jpg","type":"image","content":"ANDERSON_STY-38.jpg","thumb":"ANDERSON_STY-38.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10690":{"id":10690,"size":0,"label":"ANDERSON_STY-39.jpg","type":"image","content":"ANDERSON_STY-39.jpg","thumb":"ANDERSON_STY-39.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10691":{"id":10691,"size":0,"label":"ANDERSON_STY-40.jpg","type":"image","content":"ANDERSON_STY-40.jpg","thumb":"ANDERSON_STY-40.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10692":{"id":10692,"size":0,"label":"ANDERSON_STY-41.jpg","type":"image","content":"ANDERSON_STY-41.jpg","thumb":"ANDERSON_STY-41.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10693":{"id":10693,"size":0,"label":"ANDERSON_STY-42.jpg","type":"image","content":"ANDERSON_STY-42.jpg","thumb":"ANDERSON_STY-42.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10694":{"id":10694,"size":0,"label":"ANDERSON_STY-43.jpg","type":"image","content":"ANDERSON_STY-43.jpg","thumb":"ANDERSON_STY-43.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10695":{"id":10695,"size":0,"label":"ANDERSON_STY-44.jpg","type":"image","content":"ANDERSON_STY-44.jpg","thumb":"ANDERSON_STY-44.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10696":{"id":10696,"size":0,"label":"ANDERSON_STY-45.jpg","type":"image","content":"ANDERSON_STY-45.jpg","thumb":"ANDERSON_STY-45.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10697":{"id":10697,"size":0,"label":"ANDERSON_STY-46.jpg","type":"image","content":"ANDERSON_STY-46.jpg","thumb":"ANDERSON_STY-46.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10698":{"id":10698,"size":0,"label":"ANDERSON_STY-47.jpg","type":"image","content":"ANDERSON_STY-47.jpg","thumb":"ANDERSON_STY-47.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10699":{"id":10699,"size":0,"label":"ANDERSON_STY-48.jpg","type":"image","content":"ANDERSON_STY-48.jpg","thumb":"ANDERSON_STY-48.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10700":{"id":10700,"size":0,"label":"ANDERSON_STY-49.jpg","type":"image","content":"ANDERSON_STY-49.jpg","thumb":"ANDERSON_STY-49.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10701":{"id":10701,"size":0,"label":"ANDERSON_STY-50.jpg","type":"image","content":"ANDERSON_STY-50.jpg","thumb":"ANDERSON_STY-50.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10702":{"id":10702,"size":0,"label":"ANDERSON_STY-51.jpg","type":"image","content":"ANDERSON_STY-51.jpg","thumb":"ANDERSON_STY-51.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10703":{"id":10703,"size":0,"label":"ANDERSON_STY-52.jpg","type":"image","content":"ANDERSON_STY-52.jpg","thumb":"ANDERSON_STY-52.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10704":{"id":10704,"size":0,"label":"ANDERSON_STY-53.jpg","type":"image","content":"ANDERSON_STY-53.jpg","thumb":"ANDERSON_STY-53.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10705":{"id":10705,"size":0,"label":"ANDERSON_STY-54.jpg","type":"image","content":"ANDERSON_STY-54.jpg","thumb":"ANDERSON_STY-54.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10706":{"id":10706,"size":0,"label":"ANDERSON_STY-55.jpg","type":"image","content":"ANDERSON_STY-55.jpg","thumb":"ANDERSON_STY-55.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10707":{"id":10707,"size":0,"label":"ANDERSON_STY-56.jpg","type":"image","content":"ANDERSON_STY-56.jpg","thumb":"ANDERSON_STY-56.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10708":{"id":10708,"size":0,"label":"ANDERSON_STY-57.jpg","type":"image","content":"ANDERSON_STY-57.jpg","thumb":"ANDERSON_STY-57.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10709":{"id":10709,"size":0,"label":"ANDERSON_STY-58.jpg","type":"image","content":"ANDERSON_STY-58.jpg","thumb":"ANDERSON_STY-58.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10710":{"id":10710,"size":0,"label":"ANDERSON_STY-59.jpg","type":"image","content":"ANDERSON_STY-59.jpg","thumb":"ANDERSON_STY-59.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10711":{"id":10711,"size":0,"label":"ANDERSON_STY-60.jpg","type":"image","content":"ANDERSON_STY-60.jpg","thumb":"ANDERSON_STY-60.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10712":{"id":10712,"size":0,"label":"ANDERSON_STY-61.jpg","type":"image","content":"ANDERSON_STY-61.jpg","thumb":"ANDERSON_STY-61.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10713":{"id":10713,"size":0,"label":"ANDERSON_STY-62.jpg","type":"image","content":"ANDERSON_STY-62.jpg","thumb":"ANDERSON_STY-62.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10714":{"id":10714,"size":0,"label":"ANDERSON_STY-63.jpg","type":"image","content":"ANDERSON_STY-63.jpg","thumb":"ANDERSON_STY-63.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10715":{"id":10715,"size":0,"label":"ANDERSON_STY-64.jpg","type":"image","content":"ANDERSON_STY-64.jpg","thumb":"ANDERSON_STY-64.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10716":{"id":10716,"size":0,"label":"ANDERSON_STY-65.jpg","type":"image","content":"ANDERSON_STY-65.jpg","thumb":"ANDERSON_STY-65.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10717":{"id":10717,"size":0,"label":"ANDERSON_STY-66.jpg","type":"image","content":"ANDERSON_STY-66.jpg","thumb":"ANDERSON_STY-66.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10718":{"id":10718,"size":0,"label":"ANDERSON_STY-67.jpg","type":"image","content":"ANDERSON_STY-67.jpg","thumb":"ANDERSON_STY-67.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10719":{"id":10719,"size":0,"label":"ANDERSON_STY-68.jpg","type":"image","content":"ANDERSON_STY-68.jpg","thumb":"ANDERSON_STY-68.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10720":{"id":10720,"size":0,"label":"ANDERSON_STY-69.jpg","type":"image","content":"ANDERSON_STY-69.jpg","thumb":"ANDERSON_STY-69.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10721":{"id":10721,"size":0,"label":"ANDERSON_STY-70.jpg","type":"image","content":"ANDERSON_STY-70.jpg","thumb":"ANDERSON_STY-70.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10722":{"id":10722,"size":0,"label":"ANDERSON_STY-71.jpg","type":"image","content":"ANDERSON_STY-71.jpg","thumb":"ANDERSON_STY-71.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10723":{"id":10723,"size":0,"label":"ANDERSON_STY-72.jpg","type":"image","content":"ANDERSON_STY-72.jpg","thumb":"ANDERSON_STY-72.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview"},"10724":{"id":10724,"size":0,"label":"ANDERSON_STY-73.jpg","type":"image","content":"ANDERSON_STY-73.jpg","thumb":"ANDERSON_STY-73.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10725":{"id":10725,"size":0,"label":"ANDERSON_STY-74.jpg","type":"image","content":"ANDERSON_STY-74.jpg","thumb":"ANDERSON_STY-74.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10726":{"id":10726,"size":0,"label":"ANDERSON_STY-75.jpg","type":"image","content":"ANDERSON_STY-75.jpg","thumb":"ANDERSON_STY-75.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10727":{"id":10727,"size":0,"label":"Vimeo ID: 63942611","type":"video","content":"vimeo:63942611","thumb":"prt_320x212_1368047795.jpg","linkTarget":"","caption":" The Lone Bellow - Bleeding Out","featuredImage":" ","filters":""},"10728":{"id":10728,"size":0,"label":"Vimeo ID: 40035313","type":"video","content":"vimeo:40035313","thumb":"prt_320x212_1368047714.jpg","linkTarget":"","caption":" ","featuredImage":" ","filters":""},"10729":{"id":10729,"size":0,"label":"Vimeo ID: 38129453","type":"video","content":"vimeo:38129453","thumb":"prt_320x212_1368045913.jpg","linkTarget":"","caption":" ","featuredImage":" ","filters":""},"10730":{"id":10730,"size":0,"label":"Anderson_KateSpade_Sept-1.jpg","type":"image","content":"Anderson_KateSpade_Sept-1.jpg","thumb":"Anderson_KateSpade_Sept-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10731":{"id":10731,"size":0,"label":"Anderson_KateSpade_Sept-2.jpg","type":"image","content":"Anderson_KateSpade_Sept-2.jpg","thumb":"Anderson_KateSpade_Sept-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10732":{"id":10732,"size":0,"label":"Anderson_KateSpade_Sept-3.jpg","type":"image","content":"Anderson_KateSpade_Sept-3.jpg","thumb":"Anderson_KateSpade_Sept-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10733":{"id":10733,"size":0,"label":"Anderson_KateSpade_Sept-4.jpg","type":"image","content":"Anderson_KateSpade_Sept-4.jpg","thumb":"Anderson_KateSpade_Sept-4.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10734":{"id":10734,"size":0,"label":"Anderson_KateSpade_Sept-5.jpg","type":"image","content":"Anderson_KateSpade_Sept-5.jpg","thumb":"Anderson_KateSpade_Sept-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10735":{"id":10735,"size":0,"label":"Anderson_KateSpade_Sept-6.jpg","type":"image","content":"Anderson_KateSpade_Sept-6.jpg","thumb":"Anderson_KateSpade_Sept-6.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10736":{"id":10736,"size":0,"label":"Anderson_KateSpade_Sept-7.jpg","type":"image","content":"Anderson_KateSpade_Sept-7.jpg","thumb":"Anderson_KateSpade_Sept-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10737":{"id":10737,"size":0,"label":"Anderson_KateSpade_Sept-8.jpg","type":"image","content":"Anderson_KateSpade_Sept-8.jpg","thumb":"Anderson_KateSpade_Sept-8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10738":{"id":10738,"size":0,"label":"Anderson_KateSpade_Sept-9.jpg","type":"image","content":"Anderson_KateSpade_Sept-9.jpg","thumb":"Anderson_KateSpade_Sept-9.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10739":{"id":10739,"size":0,"label":"Anderson_KateSpade_Sept-10.jpg","type":"image","content":"Anderson_KateSpade_Sept-10.jpg","thumb":"Anderson_KateSpade_Sept-10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10740":{"id":10740,"size":0,"label":"Anderson_KateSpade_Sept-11.jpg","type":"image","content":"Anderson_KateSpade_Sept-11.jpg","thumb":"Anderson_KateSpade_Sept-11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10741":{"id":10741,"size":0,"label":"Anderson_KateSpade_Sept-12.jpg","type":"image","content":"Anderson_KateSpade_Sept-12.jpg","thumb":"Anderson_KateSpade_Sept-12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10742":{"id":10742,"size":0,"label":"Anderson_KateSpade_Sept-13.jpg","type":"image","content":"Anderson_KateSpade_Sept-13.jpg","thumb":"Anderson_KateSpade_Sept-13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10743":{"id":10743,"size":0,"label":"Anderson_KateSpade_Sept-14.jpg","type":"image","content":"Anderson_KateSpade_Sept-14.jpg","thumb":"Anderson_KateSpade_Sept-14.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10744":{"id":10744,"size":0,"label":"Anderson_KateSpade_Sept-15.jpg","type":"image","content":"Anderson_KateSpade_Sept-15.jpg","thumb":"Anderson_KateSpade_Sept-15.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10745":{"id":10745,"size":0,"label":"Anderson_KateSpade_Sept-16.jpg","type":"image","content":"Anderson_KateSpade_Sept-16.jpg","thumb":"Anderson_KateSpade_Sept-16.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10746":{"id":10746,"size":0,"label":"Anderson_KateSpade_Sept-17.jpg","type":"image","content":"Anderson_KateSpade_Sept-17.jpg","thumb":"Anderson_KateSpade_Sept-17.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10747":{"id":10747,"size":0,"label":"Anderson_KateSpade_Sept-18.jpg","type":"image","content":"Anderson_KateSpade_Sept-18.jpg","thumb":"Anderson_KateSpade_Sept-18.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10748":{"id":10748,"size":0,"label":"Anderson_KateSpade_Sept-19.jpg","type":"image","content":"Anderson_KateSpade_Sept-19.jpg","thumb":"Anderson_KateSpade_Sept-19.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10749":{"id":10749,"size":0,"label":"Anderson_KateSpade_Sept-20.jpg","type":"image","content":"Anderson_KateSpade_Sept-20.jpg","thumb":"Anderson_KateSpade_Sept-20.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10750":{"id":10750,"size":0,"label":"Anderson_KateSpade_Sept-21.jpg","type":"image","content":"Anderson_KateSpade_Sept-21.jpg","thumb":"Anderson_KateSpade_Sept-21.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10751":{"id":10751,"size":0,"label":"Anderson_KateSpade_Sept-22.jpg","type":"image","content":"Anderson_KateSpade_Sept-22.jpg","thumb":"Anderson_KateSpade_Sept-22.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10752":{"id":10752,"size":0,"label":"Anderson_KateSpade_Sept-23.jpg","type":"image","content":"Anderson_KateSpade_Sept-23.jpg","thumb":"Anderson_KateSpade_Sept-23.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10753":{"id":10753,"size":0,"label":"11_dec_street_style1.jpg","type":"image","content":"11_dec_street_style1.jpg","thumb":"11_dec_street_style1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10754":{"id":10754,"size":0,"label":"11_dec_street_style2.jpg","type":"image","content":"11_dec_street_style2.jpg","thumb":"11_dec_street_style2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10755":{"id":10755,"size":0,"label":"11_dec_street_style3.jpg","type":"image","content":"11_dec_street_style3.jpg","thumb":"11_dec_street_style3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10756":{"id":10756,"size":0,"label":"11_dec_street_style4.jpg","type":"image","content":"11_dec_street_style4.jpg","thumb":"11_dec_street_style4.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10757":{"id":10757,"size":0,"label":"11_dec_street_style5.jpg","type":"image","content":"11_dec_street_style5.jpg","thumb":"11_dec_street_style5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10758":{"id":10758,"size":0,"label":"11_dec_street_style6.jpg","type":"image","content":"11_dec_street_style6.jpg","thumb":"11_dec_street_style6.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10759":{"id":10759,"size":0,"label":"11_dec_street_style7.jpg","type":"image","content":"11_dec_street_style7.jpg","thumb":"11_dec_street_style7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10760":{"id":10760,"size":0,"label":"11_dec_street_style8.jpg","type":"image","content":"11_dec_street_style8.jpg","thumb":"11_dec_street_style8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10761":{"id":10761,"size":0,"label":"11_dec_street_style9.jpg","type":"image","content":"11_dec_street_style9.jpg","thumb":"11_dec_street_style9.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10762":{"id":10762,"size":0,"label":"Anderson_KateSpade_Oct-1.jpg","type":"image","content":"Anderson_KateSpade_Oct-1.jpg","thumb":"Anderson_KateSpade_Oct-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10763":{"id":10763,"size":0,"label":"Anderson_KateSpade_Oct-2.jpg","type":"image","content":"Anderson_KateSpade_Oct-2.jpg","thumb":"Anderson_KateSpade_Oct-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10764":{"id":10764,"size":0,"label":"Anderson_KateSpade_Oct-3.jpg","type":"image","content":"Anderson_KateSpade_Oct-3.jpg","thumb":"Anderson_KateSpade_Oct-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10765":{"id":10765,"size":0,"label":"Anderson_KateSpade_Oct-4.jpg","type":"image","content":"Anderson_KateSpade_Oct-4.jpg","thumb":"Anderson_KateSpade_Oct-4.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10766":{"id":10766,"size":0,"label":"Anderson_KateSpade_Oct-5.jpg","type":"image","content":"Anderson_KateSpade_Oct-5.jpg","thumb":"Anderson_KateSpade_Oct-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10767":{"id":10767,"size":0,"label":"Anderson_KateSpade_Oct-6.jpg","type":"image","content":"Anderson_KateSpade_Oct-6.jpg","thumb":"Anderson_KateSpade_Oct-6.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10768":{"id":10768,"size":0,"label":"Anderson_KateSpade_Oct-7.jpg","type":"image","content":"Anderson_KateSpade_Oct-7.jpg","thumb":"Anderson_KateSpade_Oct-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10769":{"id":10769,"size":0,"label":"Anderson_KateSpade_Oct-8.jpg","type":"image","content":"Anderson_KateSpade_Oct-8.jpg","thumb":"Anderson_KateSpade_Oct-8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10770":{"id":10770,"size":0,"label":"Anderson_KateSpade_Oct-9.jpg","type":"image","content":"Anderson_KateSpade_Oct-9.jpg","thumb":"Anderson_KateSpade_Oct-9.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10771":{"id":10771,"size":0,"label":"Anderson_KateSpade_Oct-10.jpg","type":"image","content":"Anderson_KateSpade_Oct-10.jpg","thumb":"Anderson_KateSpade_Oct-10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10772":{"id":10772,"size":0,"label":"Anderson_KateSpade_Oct-11.jpg","type":"image","content":"Anderson_KateSpade_Oct-11.jpg","thumb":"Anderson_KateSpade_Oct-11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10773":{"id":10773,"size":0,"label":"Anderson_KateSpade_Oct-12.jpg","type":"image","content":"Anderson_KateSpade_Oct-12.jpg","thumb":"Anderson_KateSpade_Oct-12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10774":{"id":10774,"size":0,"label":"Anderson_KateSpade_Oct-13.jpg","type":"image","content":"Anderson_KateSpade_Oct-13.jpg","thumb":"Anderson_KateSpade_Oct-13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10775":{"id":10775,"size":0,"label":"Anderson_Portrait-1.jpg","type":"image","content":"Anderson_Portrait-1.jpg","thumb":"Anderson_Portrait-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10776":{"id":10776,"size":0,"label":"Anderson_Portrait-2.jpg","type":"image","content":"Anderson_Portrait-2.jpg","thumb":"Anderson_Portrait-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10777":{"id":10777,"size":0,"label":"Anderson_Portrait-3.jpg","type":"image","content":"Anderson_Portrait-3.jpg","thumb":"Anderson_Portrait-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10778":{"id":10778,"size":0,"label":"Anderson_Portrait-4.jpg","type":"image","content":"Anderson_Portrait-4.jpg","thumb":"Anderson_Portrait-4.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10779":{"id":10779,"size":0,"label":"Anderson_Portrait-5.jpg","type":"image","content":"Anderson_Portrait-5.jpg","thumb":"Anderson_Portrait-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10780":{"id":10780,"size":0,"label":"Anderson_Portrait-6.jpg","type":"image","content":"Anderson_Portrait-6.jpg","thumb":"Anderson_Portrait-6.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10781":{"id":10781,"size":0,"label":"Anderson_Portrait-7.jpg","type":"image","content":"Anderson_Portrait-7.jpg","thumb":"Anderson_Portrait-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10782":{"id":10782,"size":0,"label":"Anderson_Portrait-8.jpg","type":"image","content":"Anderson_Portrait-8.jpg","thumb":"Anderson_Portrait-8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10783":{"id":10783,"size":0,"label":"Anderson_Portrait-9.jpg","type":"image","content":"Anderson_Portrait-9.jpg","thumb":"Anderson_Portrait-9.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10784":{"id":10784,"size":0,"label":"Anderson_Portrait-10.jpg","type":"image","content":"Anderson_Portrait-10.jpg","thumb":"Anderson_Portrait-10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10785":{"id":10785,"size":0,"label":"Anderson_Portrait-11.jpg","type":"image","content":"Anderson_Portrait-11.jpg","thumb":"Anderson_Portrait-11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10786":{"id":10786,"size":0,"label":"Anderson_Portrait-12.jpg","type":"image","content":"Anderson_Portrait-12.jpg","thumb":"Anderson_Portrait-12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10787":{"id":10787,"size":0,"label":"Anderson_Portrait-13.jpg","type":"image","content":"Anderson_Portrait-13.jpg","thumb":"Anderson_Portrait-13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10788":{"id":10788,"size":0,"label":"Anderson_Portrait-14.jpg","type":"image","content":"Anderson_Portrait-14.jpg","thumb":"Anderson_Portrait-14.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10789":{"id":10789,"size":0,"label":"Anderson_Portrait-15.jpg","type":"image","content":"Anderson_Portrait-15.jpg","thumb":"Anderson_Portrait-15.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10790":{"id":10790,"size":0,"label":"Anderson_Portrait-16.jpg","type":"image","content":"Anderson_Portrait-16.jpg","thumb":"Anderson_Portrait-16.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10791":{"id":10791,"size":0,"label":"Anderson_Portrait-17.jpg","type":"image","content":"Anderson_Portrait-17.jpg","thumb":"Anderson_Portrait-17.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10792":{"id":10792,"size":0,"label":"Anderson_Portrait-18.jpg","type":"image","content":"Anderson_Portrait-18.jpg","thumb":"Anderson_Portrait-18.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10793":{"id":10793,"size":0,"label":"Anderson_Portrait-19.jpg","type":"image","content":"Anderson_Portrait-19.jpg","thumb":"Anderson_Portrait-19.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10794":{"id":10794,"size":0,"label":"Anderson_Portrait-20.jpg","type":"image","content":"Anderson_Portrait-20.jpg","thumb":"Anderson_Portrait-20.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10795":{"id":10795,"size":0,"label":"Anderson_Portrait-21.jpg","type":"image","content":"Anderson_Portrait-21.jpg","thumb":"Anderson_Portrait-21.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10796":{"id":10796,"size":0,"label":"Anderson_Portrait-22.jpg","type":"image","content":"Anderson_Portrait-22.jpg","thumb":"Anderson_Portrait-22.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10797":{"id":10797,"size":0,"label":"Anderson_Portrait-23.jpg","type":"image","content":"Anderson_Portrait-23.jpg","thumb":"Anderson_Portrait-23.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10798":{"id":10798,"size":0,"label":"Anderson_Portrait-24.jpg","type":"image","content":"Anderson_Portrait-24.jpg","thumb":"Anderson_Portrait-24.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10799":{"id":10799,"size":0,"label":"Anderson_Portrait-25.jpg","type":"image","content":"Anderson_Portrait-25.jpg","thumb":"Anderson_Portrait-25.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10800":{"id":10800,"size":0,"label":"Anderson_Portrait-26.jpg","type":"image","content":"Anderson_Portrait-26.jpg","thumb":"Anderson_Portrait-26.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10801":{"id":10801,"size":0,"label":"Anderson_Portrait-27.jpg","type":"image","content":"Anderson_Portrait-27.jpg","thumb":"Anderson_Portrait-27.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10802":{"id":10802,"size":0,"label":"Anderson_Portrait-28.jpg","type":"image","content":"Anderson_Portrait-28.jpg","thumb":"Anderson_Portrait-28.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10803":{"id":10803,"size":0,"label":"Anderson_Portrait-29.jpg","type":"image","content":"Anderson_Portrait-29.jpg","thumb":"Anderson_Portrait-29.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10804":{"id":10804,"size":0,"label":"Anderson_Portrait-30.jpg","type":"image","content":"Anderson_Portrait-30.jpg","thumb":"Anderson_Portrait-30.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10805":{"id":10805,"size":0,"label":"Anderson_Portrait-31.jpg","type":"image","content":"Anderson_Portrait-31.jpg","thumb":"Anderson_Portrait-31.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10806":{"id":10806,"size":0,"label":"Anderson_Portrait-32.jpg","type":"image","content":"Anderson_Portrait-32.jpg","thumb":"Anderson_Portrait-32.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10807":{"id":10807,"size":0,"label":"ALBUM_104-2.jpg","type":"image","content":"ALBUM_104-2.jpg","thumb":"ALBUM_104-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10808":{"id":10808,"size":0,"label":"ALBUM_103-2.jpg","type":"image","content":"ALBUM_103-2.jpg","thumb":"ALBUM_103-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10809":{"id":10809,"size":0,"label":"ALBUM_104.jpg","type":"image","content":"ALBUM_104.jpg","thumb":"ALBUM_104.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10810":{"id":10810,"size":0,"label":"ALBUM_102-2.jpg","type":"image","content":"ALBUM_102-2.jpg","thumb":"ALBUM_102-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10811":{"id":10811,"size":0,"label":"ALBUM_102.jpg","type":"image","content":"ALBUM_102.jpg","thumb":"ALBUM_102.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10812":{"id":10812,"size":0,"label":"ALBUM_103.jpg","type":"image","content":"ALBUM_103.jpg","thumb":"ALBUM_103.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10813":{"id":10813,"size":0,"label":"ALBUM_101-2.jpg","type":"image","content":"ALBUM_101-2.jpg","thumb":"ALBUM_101-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10814":{"id":10814,"size":0,"label":"ALBUM_101.jpg","type":"image","content":"ALBUM_101.jpg","thumb":"ALBUM_101.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10815":{"id":10815,"size":0,"label":"ALBUM_100-2.jpg","type":"image","content":"ALBUM_100-2.jpg","thumb":"ALBUM_100-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10816":{"id":10816,"size":0,"label":"ALBUM_100.jpg","type":"image","content":"ALBUM_100.jpg","thumb":"ALBUM_100.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10817":{"id":10817,"size":0,"label":"a0315758608_10.jpg","type":"image","content":"a0315758608_10.jpg","thumb":"a0315758608_10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10818":{"id":10818,"size":0,"label":"Anderson_LONESTAR_0001.jpg","type":"image","content":"Anderson_LONESTAR_0001.jpg","thumb":"Anderson_LONESTAR_0001.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10819":{"id":10819,"size":0,"label":"Anderson_LONESTAR_0002.jpg","type":"image","content":"Anderson_LONESTAR_0002.jpg","thumb":"Anderson_LONESTAR_0002.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10820":{"id":10820,"size":0,"label":"Anderson_LONESTAR_0003.jpg","type":"image","content":"Anderson_LONESTAR_0003.jpg","thumb":"Anderson_LONESTAR_0003.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10821":{"id":10821,"size":0,"label":"Anderson_LONESTAR_0004.jpg","type":"image","content":"Anderson_LONESTAR_0004.jpg","thumb":"Anderson_LONESTAR_0004.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10822":{"id":10822,"size":0,"label":"Anderson_LONESTAR_0005.jpg","type":"image","content":"Anderson_LONESTAR_0005.jpg","thumb":"Anderson_LONESTAR_0005.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10823":{"id":10823,"size":0,"label":"Anderson_LONESTAR_0006.jpg","type":"image","content":"Anderson_LONESTAR_0006.jpg","thumb":"Anderson_LONESTAR_0006.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10824":{"id":10824,"size":0,"label":"Anderson_LONESTAR_0007.jpg","type":"image","content":"Anderson_LONESTAR_0007.jpg","thumb":"Anderson_LONESTAR_0007.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10825":{"id":10825,"size":0,"label":"Anderson_LONESTAR_0008.jpg","type":"image","content":"Anderson_LONESTAR_0008.jpg","thumb":"Anderson_LONESTAR_0008.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10826":{"id":10826,"size":0,"label":"Anderson_LONESTAR_0009.jpg","type":"image","content":"Anderson_LONESTAR_0009.jpg","thumb":"Anderson_LONESTAR_0009.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10827":{"id":10827,"size":0,"label":"Anderson_LONESTAR_0010.jpg","type":"image","content":"Anderson_LONESTAR_0010.jpg","thumb":"Anderson_LONESTAR_0010.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10828":{"id":10828,"size":0,"label":"Anderson_LONESTAR_0011.jpg","type":"image","content":"Anderson_LONESTAR_0011.jpg","thumb":"Anderson_LONESTAR_0011.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10829":{"id":10829,"size":0,"label":"Anderson_LONESTAR_0012.jpg","type":"image","content":"Anderson_LONESTAR_0012.jpg","thumb":"Anderson_LONESTAR_0012.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10830":{"id":10830,"size":0,"label":"Anderson_LONESTAR_0013.jpg","type":"image","content":"Anderson_LONESTAR_0013.jpg","thumb":"Anderson_LONESTAR_0013.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10831":{"id":10831,"size":0,"label":"Anderson_LONESTAR_0014.jpg","type":"image","content":"Anderson_LONESTAR_0014.jpg","thumb":"Anderson_LONESTAR_0014.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10832":{"id":10832,"size":0,"label":"Anderson_LONESTAR_0015.jpg","type":"image","content":"Anderson_LONESTAR_0015.jpg","thumb":"Anderson_LONESTAR_0015.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10833":{"id":10833,"size":0,"label":"Anderson_LONESTAR_0016.jpg","type":"image","content":"Anderson_LONESTAR_0016.jpg","thumb":"Anderson_LONESTAR_0016.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10834":{"id":10834,"size":0,"label":"Anderson_LONESTAR_0017.jpg","type":"image","content":"Anderson_LONESTAR_0017.jpg","thumb":"Anderson_LONESTAR_0017.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10835":{"id":10835,"size":0,"label":"Anderson_LONESTAR_0018.jpg","type":"image","content":"Anderson_LONESTAR_0018.jpg","thumb":"Anderson_LONESTAR_0018.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10836":{"id":10836,"size":0,"label":"Anderson_LONESTAR_0019.jpg","type":"image","content":"Anderson_LONESTAR_0019.jpg","thumb":"Anderson_LONESTAR_0019.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10837":{"id":10837,"size":0,"label":"Anderson_LONESTAR_0020.jpg","type":"image","content":"Anderson_LONESTAR_0020.jpg","thumb":"Anderson_LONESTAR_0020.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10838":{"id":10838,"size":0,"label":"Anderson_LONESTAR_0021.jpg","type":"image","content":"Anderson_LONESTAR_0021.jpg","thumb":"Anderson_LONESTAR_0021.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10839":{"id":10839,"size":0,"label":"Anderson_LONESTAR_0022.jpg","type":"image","content":"Anderson_LONESTAR_0022.jpg","thumb":"Anderson_LONESTAR_0022.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10840":{"id":10840,"size":0,"label":"Anderson_LONESTAR_0023.jpg","type":"image","content":"Anderson_LONESTAR_0023.jpg","thumb":"Anderson_LONESTAR_0023.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10841":{"id":10841,"size":0,"label":"Anderson_LONESTAR_0024.jpg","type":"image","content":"Anderson_LONESTAR_0024.jpg","thumb":"Anderson_LONESTAR_0024.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10842":{"id":10842,"size":0,"label":"Anderson_LONESTAR_0025.jpg","type":"image","content":"Anderson_LONESTAR_0025.jpg","thumb":"Anderson_LONESTAR_0025.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10843":{"id":10843,"size":0,"label":"Anderson_LONESTAR_0026.jpg","type":"image","content":"Anderson_LONESTAR_0026.jpg","thumb":"Anderson_LONESTAR_0026.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10844":{"id":10844,"size":0,"label":"Anderson_LONESTAR_0027.jpg","type":"image","content":"Anderson_LONESTAR_0027.jpg","thumb":"Anderson_LONESTAR_0027.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10845":{"id":10845,"size":0,"label":"Anderson_LONESTAR_0028.jpg","type":"image","content":"Anderson_LONESTAR_0028.jpg","thumb":"Anderson_LONESTAR_0028.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10846":{"id":10846,"size":0,"label":"Anderson_LONESTAR_0029.jpg","type":"image","content":"Anderson_LONESTAR_0029.jpg","thumb":"Anderson_LONESTAR_0029.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10847":{"id":10847,"size":0,"label":"Anderson_LONESTAR_0030.jpg","type":"image","content":"Anderson_LONESTAR_0030.jpg","thumb":"Anderson_LONESTAR_0030.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10848":{"id":10848,"size":0,"label":"Anderson_LONESTAR_0031.jpg","type":"image","content":"Anderson_LONESTAR_0031.jpg","thumb":"Anderson_LONESTAR_0031.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10849":{"id":10849,"size":0,"label":"Anderson_LONESTAR_0032.jpg","type":"image","content":"Anderson_LONESTAR_0032.jpg","thumb":"Anderson_LONESTAR_0032.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10850":{"id":10850,"size":0,"label":"Anderson_LONESTAR_0033.jpg","type":"image","content":"Anderson_LONESTAR_0033.jpg","thumb":"Anderson_LONESTAR_0033.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10851":{"id":10851,"size":0,"label":"Anderson_LONESTAR_0034.jpg","type":"image","content":"Anderson_LONESTAR_0034.jpg","thumb":"Anderson_LONESTAR_0034.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10852":{"id":10852,"size":0,"label":"Anderson_LONESTAR_0035.jpg","type":"image","content":"Anderson_LONESTAR_0035.jpg","thumb":"Anderson_LONESTAR_0035.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10853":{"id":10853,"size":0,"label":"Anderson_LONESTAR_0036.jpg","type":"image","content":"Anderson_LONESTAR_0036.jpg","thumb":"Anderson_LONESTAR_0036.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10854":{"id":10854,"size":0,"label":"Anderson_LONESTAR_0037.jpg","type":"image","content":"Anderson_LONESTAR_0037.jpg","thumb":"Anderson_LONESTAR_0037.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10855":{"id":10855,"size":0,"label":"Anderson_LONESTAR_0038.jpg","type":"image","content":"Anderson_LONESTAR_0038.jpg","thumb":"Anderson_LONESTAR_0038.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10856":{"id":10856,"size":0,"label":"Anderson_LONESTAR_0039.jpg","type":"image","content":"Anderson_LONESTAR_0039.jpg","thumb":"Anderson_LONESTAR_0039.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10857":{"id":10857,"size":0,"label":"Anderson_LONESTAR_0040.jpg","type":"image","content":"Anderson_LONESTAR_0040.jpg","thumb":"Anderson_LONESTAR_0040.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10858":{"id":10858,"size":0,"label":"Anderson_LONESTAR_0041.jpg","type":"image","content":"Anderson_LONESTAR_0041.jpg","thumb":"Anderson_LONESTAR_0041.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10859":{"id":10859,"size":0,"label":"Anderson_LONESTAR_0042.jpg","type":"image","content":"Anderson_LONESTAR_0042.jpg","thumb":"Anderson_LONESTAR_0042.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10860":{"id":10860,"size":0,"label":"Anderson_LONESTAR_0043.jpg","type":"image","content":"Anderson_LONESTAR_0043.jpg","thumb":"Anderson_LONESTAR_0043.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10861":{"id":10861,"size":0,"label":"Anderson_LONESTAR_0044.jpg","type":"image","content":"Anderson_LONESTAR_0044.jpg","thumb":"Anderson_LONESTAR_0044.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10862":{"id":10862,"size":0,"label":"Anderson_LONESTAR_0045.jpg","type":"image","content":"Anderson_LONESTAR_0045.jpg","thumb":"Anderson_LONESTAR_0045.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10863":{"id":10863,"size":0,"label":"Anderson_LONESTAR_0046.jpg","type":"image","content":"Anderson_LONESTAR_0046.jpg","thumb":"Anderson_LONESTAR_0046.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10864":{"id":10864,"size":0,"label":"Anderson_LONESTAR_0047.jpg","type":"image","content":"Anderson_LONESTAR_0047.jpg","thumb":"Anderson_LONESTAR_0047.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10865":{"id":10865,"size":0,"label":"Anderson_LONESTAR_0048.jpg","type":"image","content":"Anderson_LONESTAR_0048.jpg","thumb":"Anderson_LONESTAR_0048.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10866":{"id":10866,"size":0,"label":"Anderson_LONESTAR_0049.jpg","type":"image","content":"Anderson_LONESTAR_0049.jpg","thumb":"Anderson_LONESTAR_0049.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10867":{"id":10867,"size":0,"label":"Anderson_LONESTAR_0050.jpg","type":"image","content":"Anderson_LONESTAR_0050.jpg","thumb":"Anderson_LONESTAR_0050.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10868":{"id":10868,"size":0,"label":"Anderson_LONESTAR_0051.jpg","type":"image","content":"Anderson_LONESTAR_0051.jpg","thumb":"Anderson_LONESTAR_0051.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10869":{"id":10869,"size":0,"label":"Anderson_LONESTAR_0052.jpg","type":"image","content":"Anderson_LONESTAR_0052.jpg","thumb":"Anderson_LONESTAR_0052.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10870":{"id":10870,"size":0,"label":"Anderson_LONESTAR_0053.jpg","type":"image","content":"Anderson_LONESTAR_0053.jpg","thumb":"Anderson_LONESTAR_0053.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10871":{"id":10871,"size":0,"label":"Anderson_LONESTAR_0054.jpg","type":"image","content":"Anderson_LONESTAR_0054.jpg","thumb":"Anderson_LONESTAR_0054.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10872":{"id":10872,"size":0,"label":"Anderson_LONESTAR_0055.jpg","type":"image","content":"Anderson_LONESTAR_0055.jpg","thumb":"Anderson_LONESTAR_0055.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10873":{"id":10873,"size":0,"label":"Anderson_LONESTAR_0056.jpg","type":"image","content":"Anderson_LONESTAR_0056.jpg","thumb":"Anderson_LONESTAR_0056.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10874":{"id":10874,"size":0,"label":"Anderson_LONESTAR_0057.jpg","type":"image","content":"Anderson_LONESTAR_0057.jpg","thumb":"Anderson_LONESTAR_0057.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10875":{"id":10875,"size":0,"label":"Anderson_LONESTAR_0058.jpg","type":"image","content":"Anderson_LONESTAR_0058.jpg","thumb":"Anderson_LONESTAR_0058.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10876":{"id":10876,"size":0,"label":"Anderson_LONESTAR_0059.jpg","type":"image","content":"Anderson_LONESTAR_0059.jpg","thumb":"Anderson_LONESTAR_0059.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10877":{"id":10877,"size":0,"label":"Anderson_LONESTAR_0060.jpg","type":"image","content":"Anderson_LONESTAR_0060.jpg","thumb":"Anderson_LONESTAR_0060.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10878":{"id":10878,"size":0,"label":"Anderson_LONESTAR_0061.jpg","type":"image","content":"Anderson_LONESTAR_0061.jpg","thumb":"Anderson_LONESTAR_0061.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10879":{"id":10879,"size":0,"label":"Anderson_LONESTAR_0062.jpg","type":"image","content":"Anderson_LONESTAR_0062.jpg","thumb":"Anderson_LONESTAR_0062.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10880":{"id":10880,"size":0,"label":"Anderson_LONESTAR_0063.jpg","type":"image","content":"Anderson_LONESTAR_0063.jpg","thumb":"Anderson_LONESTAR_0063.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10881":{"id":10881,"size":0,"label":"Anderson_LONESTAR_0064.jpg","type":"image","content":"Anderson_LONESTAR_0064.jpg","thumb":"Anderson_LONESTAR_0064.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10882":{"id":10882,"size":0,"label":"Anderson_LONESTAR_0065.jpg","type":"image","content":"Anderson_LONESTAR_0065.jpg","thumb":"Anderson_LONESTAR_0065.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10883":{"id":10883,"size":0,"label":"Anderson_LONESTAR_0066.jpg","type":"image","content":"Anderson_LONESTAR_0066.jpg","thumb":"Anderson_LONESTAR_0066.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10884":{"id":10884,"size":0,"label":"Anderson_LONESTAR_0067.jpg","type":"image","content":"Anderson_LONESTAR_0067.jpg","thumb":"Anderson_LONESTAR_0067.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10885":{"id":10885,"size":0,"label":"Anderson_LONESTAR_0068.jpg","type":"image","content":"Anderson_LONESTAR_0068.jpg","thumb":"Anderson_LONESTAR_0068.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10886":{"id":10886,"size":0,"label":"Anderson_LONESTAR_0069.jpg","type":"image","content":"Anderson_LONESTAR_0069.jpg","thumb":"Anderson_LONESTAR_0069.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10887":{"id":10887,"size":0,"label":"Anderson_LONESTAR_0070.jpg","type":"image","content":"Anderson_LONESTAR_0070.jpg","thumb":"Anderson_LONESTAR_0070.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10888":{"id":10888,"size":0,"label":"SantaFE_POLA_001.jpg","type":"image","content":"SantaFE_POLA_001.jpg","thumb":"SantaFE_POLA_001.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10889":{"id":10889,"size":0,"label":"SantaFE_POLA_003.jpg","type":"image","content":"SantaFE_POLA_003.jpg","thumb":"SantaFE_POLA_003.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10890":{"id":10890,"size":0,"label":"SantaFE_POLA_004.jpg","type":"image","content":"SantaFE_POLA_004.jpg","thumb":"SantaFE_POLA_004.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10891":{"id":10891,"size":0,"label":"Anderson_PassLee_0001.jpg","type":"image","content":"Anderson_PassLee_0001.jpg","thumb":"Anderson_PassLee_0001.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10892":{"id":10892,"size":0,"label":"Anderson_PassLee_0002.jpg","type":"image","content":"Anderson_PassLee_0002.jpg","thumb":"Anderson_PassLee_0002.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10893":{"id":10893,"size":0,"label":"Anderson_PassLee_0003.jpg","type":"image","content":"Anderson_PassLee_0003.jpg","thumb":"Anderson_PassLee_0003.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10894":{"id":10894,"size":0,"label":"Anderson_PassLee_0004.jpg","type":"image","content":"Anderson_PassLee_0004.jpg","thumb":"Anderson_PassLee_0004.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10895":{"id":10895,"size":0,"label":"Anderson_PassLee_0005.jpg","type":"image","content":"Anderson_PassLee_0005.jpg","thumb":"Anderson_PassLee_0005.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10896":{"id":10896,"size":0,"label":"Anderson_PassLee_0006.jpg","type":"image","content":"Anderson_PassLee_0006.jpg","thumb":"Anderson_PassLee_0006.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10897":{"id":10897,"size":0,"label":"Anderson_PassLee_0007.jpg","type":"image","content":"Anderson_PassLee_0007.jpg","thumb":"Anderson_PassLee_0007.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10898":{"id":10898,"size":0,"label":"Anderson_PassLee_0008.jpg","type":"image","content":"Anderson_PassLee_0008.jpg","thumb":"Anderson_PassLee_0008.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10899":{"id":10899,"size":0,"label":"Anderson_PassLee_0009.jpg","type":"image","content":"Anderson_PassLee_0009.jpg","thumb":"Anderson_PassLee_0009.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10900":{"id":10900,"size":0,"label":"Anderson_PassLee_0010.jpg","type":"image","content":"Anderson_PassLee_0010.jpg","thumb":"Anderson_PassLee_0010.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10901":{"id":10901,"size":0,"label":"Anderson_PassLee_0011.jpg","type":"image","content":"Anderson_PassLee_0011.jpg","thumb":"Anderson_PassLee_0011.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10902":{"id":10902,"size":0,"label":"Anderson_PassLee_0012.jpg","type":"image","content":"Anderson_PassLee_0012.jpg","thumb":"Anderson_PassLee_0012.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10903":{"id":10903,"size":0,"label":"Anderson_PassLee_0013.jpg","type":"image","content":"Anderson_PassLee_0013.jpg","thumb":"Anderson_PassLee_0013.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10904":{"id":10904,"size":0,"label":"Anderson_PassLee_0014.jpg","type":"image","content":"Anderson_PassLee_0014.jpg","thumb":"Anderson_PassLee_0014.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10905":{"id":10905,"size":0,"label":"Anderson_PassLee_0015.jpg","type":"image","content":"Anderson_PassLee_0015.jpg","thumb":"Anderson_PassLee_0015.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10906":{"id":10906,"size":0,"label":"Anderson_PassLee_0016.jpg","type":"image","content":"Anderson_PassLee_0016.jpg","thumb":"Anderson_PassLee_0016.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10907":{"id":10907,"size":0,"label":"anderson_script_2013BLACK.gif","type":"image","content":"anderson_script_2013BLACK.gif","thumb":"anderson_script_2013BLACK.gif","linkTarget":"","caption":"","featuredImage":"","filters":""},"10908":{"id":10908,"size":0,"label":"ABOUT ME","type":"html","content":"<br> <br>Howdy.  <br> <br> I was born and raised in Texas.  <br> <br> Studied business at Texas A&M.  <br> <br>Moved to New York. <br> <br>Left business world to do what I wanted.  <br> <br>Moved to Brooklyn.   <br> <br>Married the love of my life.  <br> <br>Grew a beard. <br> <br>Traveled.  <br> <br>Explored.  <br> <br>Worked.  <br> <br>Had the second love of my life. <br> <br>Living life in Brooklyn.  <br> <br>Eager to travel and meet and make new things together.  <br> <br>\rEric","thumb":"","linkTarget":"","caption":"","featuredImage":"EA_BIO.jpg","filters":""},"10909":{"id":10909,"size":0,"label":"25798_10150169501750604_5747241_n.jpg","type":"image","content":"25798_10150169501750604_5747241_n.jpg","thumb":"25798_10150169501750604_5747241_n.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10910":{"id":10910,"size":0,"label":"25798_10150169501750604_5747241_n-DUP.jpg","type":"image","content":"25798_10150169501750604_5747241_n-DUP.jpg","thumb":"25798_10150169501750604_5747241_n-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Overview,BOOK ONE"},"10911":{"id":10911,"size":0,"label":"EA_BIO.jpg","type":"image","content":"EA_BIO.jpg","thumb":"EA_BIO.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"BOOK ONE"},"10912":{"id":10912,"size":0,"label":"| INSTAGRAM","type":"link","content":"http:\/\/www.instagram.com\/ericryananderson","thumb":"","linkTarget":"_blank","caption":"null","featuredImage":" ","filters":""},"10913":{"id":10913,"size":0,"label":"CONTACT","type":"html","content":"<br>\r\rFor <i><b>Photography<\/i><\/b> inquiries:<br>\rPlease email us at <br><a href=\"mailto:studio@ericryananderson.com?Subject=Website Inquiry\" target=\"_top\">\rstudio@ericryananderson.com<\/a> \r\r<br>\r\rFor <i><b>Video<\/i><\/b> inquiries: <br>\rPlease contact Greg Beauchamp at The Bindery <br><a href=\"mailto:greg@binderynyc.com?Subject=Video Inquiry - Eric Ryan Anderson\" target=\"_top\">\rgreg@binderynyc.com<\/a>\r<br>\rLook forward to hearing from you!","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"10914":{"id":10914,"size":0,"label":"11_dec_street_style6-DUP.jpg","type":"image","content":"11_dec_street_style6-DUP.jpg","thumb":"11_dec_street_style6-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10915":{"id":10915,"size":0,"label":"11_dec_street_style5-DUP.jpg","type":"image","content":"11_dec_street_style5-DUP.jpg","thumb":"11_dec_street_style5-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10916":{"id":10916,"size":0,"label":"11_dec_street_style4-DUP.jpg","type":"image","content":"11_dec_street_style4-DUP.jpg","thumb":"11_dec_street_style4-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10917":{"id":10917,"size":0,"label":"11_dec_street_style2-DUP.jpg","type":"image","content":"11_dec_street_style2-DUP.jpg","thumb":"11_dec_street_style2-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10918":{"id":10918,"size":0,"label":"anderson_stirling_0001.jpg","type":"image","content":"anderson_stirling_0001.jpg","thumb":"anderson_stirling_0001.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10919":{"id":10919,"size":0,"label":"anderson_stirling_0002.jpg","type":"image","content":"anderson_stirling_0002.jpg","thumb":"anderson_stirling_0002.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10920":{"id":10920,"size":0,"label":"anderson_stirling_0003.jpg","type":"image","content":"anderson_stirling_0003.jpg","thumb":"anderson_stirling_0003.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10921":{"id":10921,"size":0,"label":"anderson_stirling_0004.jpg","type":"image","content":"anderson_stirling_0004.jpg","thumb":"anderson_stirling_0004.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10922":{"id":10922,"size":0,"label":"anderson_stirling_0005.jpg","type":"image","content":"anderson_stirling_0005.jpg","thumb":"anderson_stirling_0005.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10923":{"id":10923,"size":0,"label":"anderson_stirling_0006.jpg","type":"image","content":"anderson_stirling_0006.jpg","thumb":"anderson_stirling_0006.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10924":{"id":10924,"size":0,"label":"anderson_stirling_0007.jpg","type":"image","content":"anderson_stirling_0007.jpg","thumb":"anderson_stirling_0007.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10925":{"id":10925,"size":0,"label":"anderson_stirling_0008.jpg","type":"image","content":"anderson_stirling_0008.jpg","thumb":"anderson_stirling_0008.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10926":{"id":10926,"size":0,"label":"anderson_stirling_0009.jpg","type":"image","content":"anderson_stirling_0009.jpg","thumb":"anderson_stirling_0009.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10927":{"id":10927,"size":0,"label":"anderson_stirling_0010.jpg","type":"image","content":"anderson_stirling_0010.jpg","thumb":"anderson_stirling_0010.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10928":{"id":10928,"size":0,"label":"anderson_stirling_0011.jpg","type":"image","content":"anderson_stirling_0011.jpg","thumb":"anderson_stirling_0011.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10929":{"id":10929,"size":0,"label":"anderson_stirling_0012.jpg","type":"image","content":"anderson_stirling_0012.jpg","thumb":"anderson_stirling_0012.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10930":{"id":10930,"size":0,"label":"anderson_stirling_0013.jpg","type":"image","content":"anderson_stirling_0013.jpg","thumb":"anderson_stirling_0013.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10931":{"id":10931,"size":0,"label":"anderson_stirling_0014.jpg","type":"image","content":"anderson_stirling_0014.jpg","thumb":"anderson_stirling_0014.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10932":{"id":10932,"size":0,"label":"anderson_stirling_0015.jpg","type":"image","content":"anderson_stirling_0015.jpg","thumb":"anderson_stirling_0015.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10933":{"id":10933,"size":0,"label":"anderson_stirling_0016.jpg","type":"image","content":"anderson_stirling_0016.jpg","thumb":"anderson_stirling_0016.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10934":{"id":10934,"size":0,"label":"anderson_stirling_0017.jpg","type":"image","content":"anderson_stirling_0017.jpg","thumb":"anderson_stirling_0017.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10935":{"id":10935,"size":0,"label":"anderson_stirling_0018.jpg","type":"image","content":"anderson_stirling_0018.jpg","thumb":"anderson_stirling_0018.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10936":{"id":10936,"size":0,"label":"anderson_stirling_0019.jpg","type":"image","content":"anderson_stirling_0019.jpg","thumb":"anderson_stirling_0019.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10937":{"id":10937,"size":0,"label":"anderson_stirling_0020.jpg","type":"image","content":"anderson_stirling_0020.jpg","thumb":"anderson_stirling_0020.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10938":{"id":10938,"size":0,"label":"anderson_stirling_0021.jpg","type":"image","content":"anderson_stirling_0021.jpg","thumb":"anderson_stirling_0021.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10939":{"id":10939,"size":0,"label":"anderson_stirling_0022.jpg","type":"image","content":"anderson_stirling_0022.jpg","thumb":"anderson_stirling_0022.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10940":{"id":10940,"size":0,"label":"anderson_stirling_0023.jpg","type":"image","content":"anderson_stirling_0023.jpg","thumb":"anderson_stirling_0023.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10941":{"id":10941,"size":0,"label":"anderson_stirling_0024.jpg","type":"image","content":"anderson_stirling_0024.jpg","thumb":"anderson_stirling_0024.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10942":{"id":10942,"size":0,"label":"anderson_stirling_0025.jpg","type":"image","content":"anderson_stirling_0025.jpg","thumb":"anderson_stirling_0025.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10943":{"id":10943,"size":0,"label":"anderson_stirling_0026.jpg","type":"image","content":"anderson_stirling_0026.jpg","thumb":"anderson_stirling_0026.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10944":{"id":10944,"size":0,"label":"anderson_stirling_0027.jpg","type":"image","content":"anderson_stirling_0027.jpg","thumb":"anderson_stirling_0027.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10945":{"id":10945,"size":0,"label":"anderson_stirling_0028.jpg","type":"image","content":"anderson_stirling_0028.jpg","thumb":"anderson_stirling_0028.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10946":{"id":10946,"size":0,"label":"anderson_stirling_0029.jpg","type":"image","content":"anderson_stirling_0029.jpg","thumb":"anderson_stirling_0029.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10947":{"id":10947,"size":0,"label":"anderson_stirling_0030.jpg","type":"image","content":"anderson_stirling_0030.jpg","thumb":"anderson_stirling_0030.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10948":{"id":10948,"size":0,"label":"anderson_stirling_0031.jpg","type":"image","content":"anderson_stirling_0031.jpg","thumb":"anderson_stirling_0031.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10949":{"id":10949,"size":0,"label":"anderson_stirling_0032.jpg","type":"image","content":"anderson_stirling_0032.jpg","thumb":"anderson_stirling_0032.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10950":{"id":10950,"size":0,"label":"anderson_stirling_0033.jpg","type":"image","content":"anderson_stirling_0033.jpg","thumb":"anderson_stirling_0033.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10951":{"id":10951,"size":0,"label":"anderson_stirling_0034.jpg","type":"image","content":"anderson_stirling_0034.jpg","thumb":"anderson_stirling_0034.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10952":{"id":10952,"size":0,"label":"anderson_stirling_0035.jpg","type":"image","content":"anderson_stirling_0035.jpg","thumb":"anderson_stirling_0035.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10953":{"id":10953,"size":0,"label":"anderson_stirling_0036.jpg","type":"image","content":"anderson_stirling_0036.jpg","thumb":"anderson_stirling_0036.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10954":{"id":10954,"size":0,"label":"anderson_stirling_0037.jpg","type":"image","content":"anderson_stirling_0037.jpg","thumb":"anderson_stirling_0037.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10955":{"id":10955,"size":0,"label":"anderson_stirling_0038.jpg","type":"image","content":"anderson_stirling_0038.jpg","thumb":"anderson_stirling_0038.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10956":{"id":10956,"size":0,"label":"anderson_stirling_0039.jpg","type":"image","content":"anderson_stirling_0039.jpg","thumb":"anderson_stirling_0039.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10957":{"id":10957,"size":0,"label":"anderson_stirling_0040.jpg","type":"image","content":"anderson_stirling_0040.jpg","thumb":"anderson_stirling_0040.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10958":{"id":10958,"size":0,"label":"anderson_stirling_0041.jpg","type":"image","content":"anderson_stirling_0041.jpg","thumb":"anderson_stirling_0041.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10959":{"id":10959,"size":0,"label":"anderson_stirling_0042.jpg","type":"image","content":"anderson_stirling_0042.jpg","thumb":"anderson_stirling_0042.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10960":{"id":10960,"size":0,"label":"ADDISWEDDING_PREVIEW-1-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-1-2.jpg","thumb":"ADDISWEDDING_PREVIEW-1-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10961":{"id":10961,"size":0,"label":"ADDISWEDDING_PREVIEW-1.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-1.jpg","thumb":"ADDISWEDDING_PREVIEW-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10962":{"id":10962,"size":0,"label":"ADDISWEDDING_PREVIEW-2-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-2-2.jpg","thumb":"ADDISWEDDING_PREVIEW-2-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10963":{"id":10963,"size":0,"label":"ADDISWEDDING_PREVIEW-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-2.jpg","thumb":"ADDISWEDDING_PREVIEW-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10964":{"id":10964,"size":0,"label":"ADDISWEDDING_PREVIEW-3-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-3-2.jpg","thumb":"ADDISWEDDING_PREVIEW-3-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10965":{"id":10965,"size":0,"label":"ADDISWEDDING_PREVIEW-3.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-3.jpg","thumb":"ADDISWEDDING_PREVIEW-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10966":{"id":10966,"size":0,"label":"ADDISWEDDING_PREVIEW-4-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-4-2.jpg","thumb":"ADDISWEDDING_PREVIEW-4-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10967":{"id":10967,"size":0,"label":"ADDISWEDDING_PREVIEW-4.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-4.jpg","thumb":"ADDISWEDDING_PREVIEW-4.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10968":{"id":10968,"size":0,"label":"ADDISWEDDING_PREVIEW-5-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-5-2.jpg","thumb":"ADDISWEDDING_PREVIEW-5-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10969":{"id":10969,"size":0,"label":"ADDISWEDDING_PREVIEW-5.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-5.jpg","thumb":"ADDISWEDDING_PREVIEW-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10970":{"id":10970,"size":0,"label":"ADDISWEDDING_PREVIEW-6-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-6-2.jpg","thumb":"ADDISWEDDING_PREVIEW-6-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10971":{"id":10971,"size":0,"label":"ADDISWEDDING_PREVIEW-6.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-6.jpg","thumb":"ADDISWEDDING_PREVIEW-6.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10972":{"id":10972,"size":0,"label":"ADDISWEDDING_PREVIEW-7-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-7-2.jpg","thumb":"ADDISWEDDING_PREVIEW-7-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10973":{"id":10973,"size":0,"label":"ADDISWEDDING_PREVIEW-7.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-7.jpg","thumb":"ADDISWEDDING_PREVIEW-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10974":{"id":10974,"size":0,"label":"ADDISWEDDING_PREVIEW-8-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-8-2.jpg","thumb":"ADDISWEDDING_PREVIEW-8-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10975":{"id":10975,"size":0,"label":"ADDISWEDDING_PREVIEW-8.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-8.jpg","thumb":"ADDISWEDDING_PREVIEW-8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10976":{"id":10976,"size":0,"label":"ADDISWEDDING_PREVIEW-9-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-9-2.jpg","thumb":"ADDISWEDDING_PREVIEW-9-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10977":{"id":10977,"size":0,"label":"ADDISWEDDING_PREVIEW-9.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-9.jpg","thumb":"ADDISWEDDING_PREVIEW-9.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10978":{"id":10978,"size":0,"label":"ADDISWEDDING_PREVIEW-10-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-10-2.jpg","thumb":"ADDISWEDDING_PREVIEW-10-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10979":{"id":10979,"size":0,"label":"ADDISWEDDING_PREVIEW-10.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-10.jpg","thumb":"ADDISWEDDING_PREVIEW-10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10980":{"id":10980,"size":0,"label":"ADDISWEDDING_PREVIEW-11-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-11-2.jpg","thumb":"ADDISWEDDING_PREVIEW-11-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10981":{"id":10981,"size":0,"label":"ADDISWEDDING_PREVIEW-11.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-11.jpg","thumb":"ADDISWEDDING_PREVIEW-11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10982":{"id":10982,"size":0,"label":"ADDISWEDDING_PREVIEW-12-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-12-2.jpg","thumb":"ADDISWEDDING_PREVIEW-12-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10983":{"id":10983,"size":0,"label":"ADDISWEDDING_PREVIEW-12.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-12.jpg","thumb":"ADDISWEDDING_PREVIEW-12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10984":{"id":10984,"size":0,"label":"ADDISWEDDING_PREVIEW-13-2.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-13-2.jpg","thumb":"ADDISWEDDING_PREVIEW-13-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10985":{"id":10985,"size":0,"label":"ADDISWEDDING_PREVIEW-13.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-13.jpg","thumb":"ADDISWEDDING_PREVIEW-13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10986":{"id":10986,"size":0,"label":"ADDISWEDDING_PREVIEW-14.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-14.jpg","thumb":"ADDISWEDDING_PREVIEW-14.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10987":{"id":10987,"size":0,"label":"ADDISWEDDING_PREVIEW-15.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-15.jpg","thumb":"ADDISWEDDING_PREVIEW-15.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10988":{"id":10988,"size":0,"label":"ADDISWEDDING_PREVIEW-16.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-16.jpg","thumb":"ADDISWEDDING_PREVIEW-16.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10989":{"id":10989,"size":0,"label":"ADDISWEDDING_PREVIEW-17.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-17.jpg","thumb":"ADDISWEDDING_PREVIEW-17.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10990":{"id":10990,"size":0,"label":"ADDISWEDDING_PREVIEW-18.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-18.jpg","thumb":"ADDISWEDDING_PREVIEW-18.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10991":{"id":10991,"size":0,"label":"ADDISWEDDING_PREVIEW-19.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-19.jpg","thumb":"ADDISWEDDING_PREVIEW-19.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10992":{"id":10992,"size":0,"label":"ADDISWEDDING_PREVIEW-20.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-20.jpg","thumb":"ADDISWEDDING_PREVIEW-20.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10993":{"id":10993,"size":0,"label":"ADDISWEDDING_PREVIEW-21.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-21.jpg","thumb":"ADDISWEDDING_PREVIEW-21.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10994":{"id":10994,"size":0,"label":"ADDISWEDDING_PREVIEW-22.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-22.jpg","thumb":"ADDISWEDDING_PREVIEW-22.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10995":{"id":10995,"size":0,"label":"ADDISWEDDING_PREVIEW-23.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-23.jpg","thumb":"ADDISWEDDING_PREVIEW-23.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10996":{"id":10996,"size":0,"label":"ADDISWEDDING_PREVIEW-24.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-24.jpg","thumb":"ADDISWEDDING_PREVIEW-24.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10997":{"id":10997,"size":0,"label":"ADDISWEDDING_PREVIEW-25.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-25.jpg","thumb":"ADDISWEDDING_PREVIEW-25.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10998":{"id":10998,"size":0,"label":"ADDISWEDDING_PREVIEW-26.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-26.jpg","thumb":"ADDISWEDDING_PREVIEW-26.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"10999":{"id":10999,"size":0,"label":"ADDISWEDDING_PREVIEW-27.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-27.jpg","thumb":"ADDISWEDDING_PREVIEW-27.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11000":{"id":11000,"size":0,"label":"ADDISWEDDING_PREVIEW-28.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-28.jpg","thumb":"ADDISWEDDING_PREVIEW-28.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11001":{"id":11001,"size":0,"label":"ADDISWEDDING_PREVIEW-29.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-29.jpg","thumb":"ADDISWEDDING_PREVIEW-29.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11002":{"id":11002,"size":0,"label":"ADDISWEDDING_PREVIEW-30.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-30.jpg","thumb":"ADDISWEDDING_PREVIEW-30.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11003":{"id":11003,"size":0,"label":"ADDISWEDDING_PREVIEW-31.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-31.jpg","thumb":"ADDISWEDDING_PREVIEW-31.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11004":{"id":11004,"size":0,"label":"ADDISWEDDING_PREVIEW-32.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-32.jpg","thumb":"ADDISWEDDING_PREVIEW-32.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11005":{"id":11005,"size":0,"label":"ADDISWEDDING_PREVIEW-33.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-33.jpg","thumb":"ADDISWEDDING_PREVIEW-33.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11006":{"id":11006,"size":0,"label":"ADDISWEDDING_PREVIEW-34.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-34.jpg","thumb":"ADDISWEDDING_PREVIEW-34.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11007":{"id":11007,"size":0,"label":"ADDISWEDDING_PREVIEW-35.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-35.jpg","thumb":"ADDISWEDDING_PREVIEW-35.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11008":{"id":11008,"size":0,"label":"ADDISWEDDING_PREVIEW-36.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-36.jpg","thumb":"ADDISWEDDING_PREVIEW-36.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11009":{"id":11009,"size":0,"label":"ADDISWEDDING_PREVIEW-37.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-37.jpg","thumb":"ADDISWEDDING_PREVIEW-37.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11010":{"id":11010,"size":0,"label":"ADDISWEDDING_PREVIEW-38.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-38.jpg","thumb":"ADDISWEDDING_PREVIEW-38.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11011":{"id":11011,"size":0,"label":"ADDISWEDDING_PREVIEW-39.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-39.jpg","thumb":"ADDISWEDDING_PREVIEW-39.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11012":{"id":11012,"size":0,"label":"ADDISWEDDING_PREVIEW-40.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-40.jpg","thumb":"ADDISWEDDING_PREVIEW-40.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11013":{"id":11013,"size":0,"label":"ADDISWEDDING_PREVIEW-41.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-41.jpg","thumb":"ADDISWEDDING_PREVIEW-41.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11014":{"id":11014,"size":0,"label":"ADDISWEDDING_PREVIEW-42.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-42.jpg","thumb":"ADDISWEDDING_PREVIEW-42.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11015":{"id":11015,"size":0,"label":"ADDISWEDDING_PREVIEW-43.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-43.jpg","thumb":"ADDISWEDDING_PREVIEW-43.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11016":{"id":11016,"size":0,"label":"ADDISWEDDING_PREVIEW-44.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-44.jpg","thumb":"ADDISWEDDING_PREVIEW-44.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11017":{"id":11017,"size":0,"label":"ADDISWEDDING_PREVIEW-45.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-45.jpg","thumb":"ADDISWEDDING_PREVIEW-45.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11018":{"id":11018,"size":0,"label":"ADDISWEDDING_PREVIEW-46.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-46.jpg","thumb":"ADDISWEDDING_PREVIEW-46.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11019":{"id":11019,"size":0,"label":"ADDISWEDDING_PREVIEW-47.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-47.jpg","thumb":"ADDISWEDDING_PREVIEW-47.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11020":{"id":11020,"size":0,"label":"ADDISWEDDING_PREVIEW-48.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-48.jpg","thumb":"ADDISWEDDING_PREVIEW-48.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11021":{"id":11021,"size":0,"label":"ADDISWEDDING_PREVIEW-49.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-49.jpg","thumb":"ADDISWEDDING_PREVIEW-49.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11022":{"id":11022,"size":0,"label":"ADDISWEDDING_PREVIEW-50.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-50.jpg","thumb":"ADDISWEDDING_PREVIEW-50.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11023":{"id":11023,"size":0,"label":"ADDISWEDDING_PREVIEW-51.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-51.jpg","thumb":"ADDISWEDDING_PREVIEW-51.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11024":{"id":11024,"size":0,"label":"ADDISWEDDING_PREVIEW-52.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-52.jpg","thumb":"ADDISWEDDING_PREVIEW-52.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11025":{"id":11025,"size":0,"label":"ADDISWEDDING_PREVIEW-53.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-53.jpg","thumb":"ADDISWEDDING_PREVIEW-53.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11026":{"id":11026,"size":0,"label":"ADDISWEDDING_PREVIEW-54.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-54.jpg","thumb":"ADDISWEDDING_PREVIEW-54.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11027":{"id":11027,"size":0,"label":"ADDISWEDDING_PREVIEW-55.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-55.jpg","thumb":"ADDISWEDDING_PREVIEW-55.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11028":{"id":11028,"size":0,"label":"ADDISWEDDING_PREVIEW-56.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-56.jpg","thumb":"ADDISWEDDING_PREVIEW-56.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11029":{"id":11029,"size":0,"label":"ADDISWEDDING_PREVIEW-57.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-57.jpg","thumb":"ADDISWEDDING_PREVIEW-57.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11030":{"id":11030,"size":0,"label":"ADDISWEDDING_PREVIEW-58.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-58.jpg","thumb":"ADDISWEDDING_PREVIEW-58.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11031":{"id":11031,"size":0,"label":"ADDISWEDDING_PREVIEW-59.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-59.jpg","thumb":"ADDISWEDDING_PREVIEW-59.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11032":{"id":11032,"size":0,"label":"ADDISWEDDING_PREVIEW-60.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-60.jpg","thumb":"ADDISWEDDING_PREVIEW-60.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11033":{"id":11033,"size":0,"label":"ADDISWEDDING_PREVIEW-61.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-61.jpg","thumb":"ADDISWEDDING_PREVIEW-61.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11034":{"id":11034,"size":0,"label":"ADDISWEDDING_PREVIEW-62.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-62.jpg","thumb":"ADDISWEDDING_PREVIEW-62.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11035":{"id":11035,"size":0,"label":"ADDISWEDDING_PREVIEW-63.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-63.jpg","thumb":"ADDISWEDDING_PREVIEW-63.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11036":{"id":11036,"size":0,"label":"ADDISWEDDING_PREVIEW-64.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-64.jpg","thumb":"ADDISWEDDING_PREVIEW-64.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11037":{"id":11037,"size":0,"label":"ADDISWEDDING_PREVIEW-65.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-65.jpg","thumb":"ADDISWEDDING_PREVIEW-65.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11038":{"id":11038,"size":0,"label":"ADDISWEDDING_PREVIEW-66.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-66.jpg","thumb":"ADDISWEDDING_PREVIEW-66.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11039":{"id":11039,"size":0,"label":"ADDISWEDDING_PREVIEW-67.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-67.jpg","thumb":"ADDISWEDDING_PREVIEW-67.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11040":{"id":11040,"size":0,"label":"ADDISWEDDING_PREVIEW-68.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-68.jpg","thumb":"ADDISWEDDING_PREVIEW-68.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11041":{"id":11041,"size":0,"label":"ADDISWEDDING_PREVIEW-69.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-69.jpg","thumb":"ADDISWEDDING_PREVIEW-69.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11042":{"id":11042,"size":0,"label":"ADDISWEDDING_PREVIEW-70.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-70.jpg","thumb":"ADDISWEDDING_PREVIEW-70.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11043":{"id":11043,"size":0,"label":"ADDISWEDDING_PREVIEW-71.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-71.jpg","thumb":"ADDISWEDDING_PREVIEW-71.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11044":{"id":11044,"size":0,"label":"ADDISWEDDING_PREVIEW-72.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-72.jpg","thumb":"ADDISWEDDING_PREVIEW-72.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11045":{"id":11045,"size":0,"label":"ADDISWEDDING_PREVIEW-73.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-73.jpg","thumb":"ADDISWEDDING_PREVIEW-73.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11046":{"id":11046,"size":0,"label":"ADDISWEDDING_PREVIEW-74.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-74.jpg","thumb":"ADDISWEDDING_PREVIEW-74.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11047":{"id":11047,"size":0,"label":"ADDISWEDDING_PREVIEW-75.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-75.jpg","thumb":"ADDISWEDDING_PREVIEW-75.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11048":{"id":11048,"size":0,"label":"ADDISWEDDING_PREVIEW-76.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-76.jpg","thumb":"ADDISWEDDING_PREVIEW-76.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11049":{"id":11049,"size":0,"label":"ADDISWEDDING_PREVIEW-77.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-77.jpg","thumb":"ADDISWEDDING_PREVIEW-77.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11050":{"id":11050,"size":0,"label":"ADDISWEDDING_PREVIEW-78.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-78.jpg","thumb":"ADDISWEDDING_PREVIEW-78.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11051":{"id":11051,"size":0,"label":"ADDISWEDDING_PREVIEW-79.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-79.jpg","thumb":"ADDISWEDDING_PREVIEW-79.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11052":{"id":11052,"size":0,"label":"ADDISWEDDING_PREVIEW-80.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-80.jpg","thumb":"ADDISWEDDING_PREVIEW-80.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11053":{"id":11053,"size":0,"label":"ADDISWEDDING_PREVIEW-81.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-81.jpg","thumb":"ADDISWEDDING_PREVIEW-81.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11054":{"id":11054,"size":0,"label":"ADDISWEDDING_PREVIEW-82.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-82.jpg","thumb":"ADDISWEDDING_PREVIEW-82.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11055":{"id":11055,"size":0,"label":"ADDISWEDDING_PREVIEW-83.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-83.jpg","thumb":"ADDISWEDDING_PREVIEW-83.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11056":{"id":11056,"size":0,"label":"ADDISWEDDING_PREVIEW-84.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-84.jpg","thumb":"ADDISWEDDING_PREVIEW-84.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11057":{"id":11057,"size":0,"label":"ADDISWEDDING_PREVIEW-85.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-85.jpg","thumb":"ADDISWEDDING_PREVIEW-85.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11058":{"id":11058,"size":0,"label":"ADDISWEDDING_PREVIEW-86.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-86.jpg","thumb":"ADDISWEDDING_PREVIEW-86.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11059":{"id":11059,"size":0,"label":"ADDISWEDDING_PREVIEW-87.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-87.jpg","thumb":"ADDISWEDDING_PREVIEW-87.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11060":{"id":11060,"size":0,"label":"ADDISWEDDING_PREVIEW-88.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-88.jpg","thumb":"ADDISWEDDING_PREVIEW-88.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11061":{"id":11061,"size":0,"label":"ADDISWEDDING_PREVIEW-89.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-89.jpg","thumb":"ADDISWEDDING_PREVIEW-89.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11062":{"id":11062,"size":0,"label":"ADDISWEDDING_PREVIEW-90.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-90.jpg","thumb":"ADDISWEDDING_PREVIEW-90.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11063":{"id":11063,"size":0,"label":"ADDISWEDDING_PREVIEW-91.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-91.jpg","thumb":"ADDISWEDDING_PREVIEW-91.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11064":{"id":11064,"size":0,"label":"ADDISWEDDING_PREVIEW-92.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-92.jpg","thumb":"ADDISWEDDING_PREVIEW-92.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11065":{"id":11065,"size":0,"label":"ADDISWEDDING_PREVIEW-93.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-93.jpg","thumb":"ADDISWEDDING_PREVIEW-93.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11066":{"id":11066,"size":0,"label":"ADDISWEDDING_PREVIEW-94.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-94.jpg","thumb":"ADDISWEDDING_PREVIEW-94.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11067":{"id":11067,"size":0,"label":"ADDISWEDDING_PREVIEW-95.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-95.jpg","thumb":"ADDISWEDDING_PREVIEW-95.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11068":{"id":11068,"size":0,"label":"ADDISWEDDING_PREVIEW-96.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-96.jpg","thumb":"ADDISWEDDING_PREVIEW-96.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11069":{"id":11069,"size":0,"label":"ADDISWEDDING_PREVIEW-97.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-97.jpg","thumb":"ADDISWEDDING_PREVIEW-97.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11070":{"id":11070,"size":0,"label":"ADDISWEDDING_PREVIEW-98.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-98.jpg","thumb":"ADDISWEDDING_PREVIEW-98.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11071":{"id":11071,"size":0,"label":"ADDISWEDDING_PREVIEW-99.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-99.jpg","thumb":"ADDISWEDDING_PREVIEW-99.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11072":{"id":11072,"size":0,"label":"ADDISWEDDING_PREVIEW-100.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-100.jpg","thumb":"ADDISWEDDING_PREVIEW-100.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11073":{"id":11073,"size":0,"label":"ADDISWEDDING_PREVIEW-101.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-101.jpg","thumb":"ADDISWEDDING_PREVIEW-101.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11074":{"id":11074,"size":0,"label":"ADDISWEDDING_PREVIEW-102.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-102.jpg","thumb":"ADDISWEDDING_PREVIEW-102.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11075":{"id":11075,"size":0,"label":"ADDISWEDDING_PREVIEW-103.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-103.jpg","thumb":"ADDISWEDDING_PREVIEW-103.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11076":{"id":11076,"size":0,"label":"ADDISWEDDING_PREVIEW-104.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-104.jpg","thumb":"ADDISWEDDING_PREVIEW-104.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11077":{"id":11077,"size":0,"label":"ADDISWEDDING_PREVIEW-105.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-105.jpg","thumb":"ADDISWEDDING_PREVIEW-105.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11078":{"id":11078,"size":0,"label":"ADDISWEDDING_PREVIEW-106.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-106.jpg","thumb":"ADDISWEDDING_PREVIEW-106.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11079":{"id":11079,"size":0,"label":"ADDISWEDDING_PREVIEW-107.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-107.jpg","thumb":"ADDISWEDDING_PREVIEW-107.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11080":{"id":11080,"size":0,"label":"ADDISWEDDING_PREVIEW-108.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-108.jpg","thumb":"ADDISWEDDING_PREVIEW-108.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11081":{"id":11081,"size":0,"label":"ADDISWEDDING_PREVIEW-109.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-109.jpg","thumb":"ADDISWEDDING_PREVIEW-109.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11082":{"id":11082,"size":0,"label":"ADDISWEDDING_PREVIEW-110.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-110.jpg","thumb":"ADDISWEDDING_PREVIEW-110.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11083":{"id":11083,"size":0,"label":"ADDISWEDDING_PREVIEW-111.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-111.jpg","thumb":"ADDISWEDDING_PREVIEW-111.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11084":{"id":11084,"size":0,"label":"ADDISWEDDING_PREVIEW-112.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-112.jpg","thumb":"ADDISWEDDING_PREVIEW-112.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11085":{"id":11085,"size":0,"label":"ADDISWEDDING_PREVIEW-113.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-113.jpg","thumb":"ADDISWEDDING_PREVIEW-113.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11086":{"id":11086,"size":0,"label":"ADDISWEDDING_PREVIEW-114.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-114.jpg","thumb":"ADDISWEDDING_PREVIEW-114.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11087":{"id":11087,"size":0,"label":"ADDISWEDDING_PREVIEW-115.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-115.jpg","thumb":"ADDISWEDDING_PREVIEW-115.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11088":{"id":11088,"size":0,"label":"ADDISWEDDING_PREVIEW-116.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-116.jpg","thumb":"ADDISWEDDING_PREVIEW-116.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11089":{"id":11089,"size":0,"label":"ADDISWEDDING_PREVIEW-117.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-117.jpg","thumb":"ADDISWEDDING_PREVIEW-117.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11090":{"id":11090,"size":0,"label":"ADDISWEDDING_PREVIEW-118.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-118.jpg","thumb":"ADDISWEDDING_PREVIEW-118.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11091":{"id":11091,"size":0,"label":"ADDISWEDDING_PREVIEW-119.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-119.jpg","thumb":"ADDISWEDDING_PREVIEW-119.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11092":{"id":11092,"size":0,"label":"ADDISWEDDING_PREVIEW-120.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-120.jpg","thumb":"ADDISWEDDING_PREVIEW-120.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11093":{"id":11093,"size":0,"label":"ADDISWEDDING_PREVIEW-121.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-121.jpg","thumb":"ADDISWEDDING_PREVIEW-121.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11094":{"id":11094,"size":0,"label":"ADDISWEDDING_PREVIEW-122.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-122.jpg","thumb":"ADDISWEDDING_PREVIEW-122.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11095":{"id":11095,"size":0,"label":"ADDISWEDDING_PREVIEW-123.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-123.jpg","thumb":"ADDISWEDDING_PREVIEW-123.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11096":{"id":11096,"size":0,"label":"ADDISWEDDING_PREVIEW-124.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-124.jpg","thumb":"ADDISWEDDING_PREVIEW-124.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11097":{"id":11097,"size":0,"label":"ADDISWEDDING_PREVIEW-125.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-125.jpg","thumb":"ADDISWEDDING_PREVIEW-125.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11098":{"id":11098,"size":0,"label":"ADDISWEDDING_PREVIEW-126.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-126.jpg","thumb":"ADDISWEDDING_PREVIEW-126.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11099":{"id":11099,"size":0,"label":"ADDISWEDDING_PREVIEW-127.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-127.jpg","thumb":"ADDISWEDDING_PREVIEW-127.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11100":{"id":11100,"size":0,"label":"ADDISWEDDING_PREVIEW-128.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-128.jpg","thumb":"ADDISWEDDING_PREVIEW-128.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11101":{"id":11101,"size":0,"label":"ADDISWEDDING_PREVIEW-129.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-129.jpg","thumb":"ADDISWEDDING_PREVIEW-129.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11102":{"id":11102,"size":0,"label":"ADDISWEDDING_PREVIEW-130.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-130.jpg","thumb":"ADDISWEDDING_PREVIEW-130.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11103":{"id":11103,"size":0,"label":"ADDISWEDDING_PREVIEW-131.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-131.jpg","thumb":"ADDISWEDDING_PREVIEW-131.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11104":{"id":11104,"size":0,"label":"ADDISWEDDING_PREVIEW-132.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-132.jpg","thumb":"ADDISWEDDING_PREVIEW-132.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11105":{"id":11105,"size":0,"label":"ADDISWEDDING_PREVIEW-133.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-133.jpg","thumb":"ADDISWEDDING_PREVIEW-133.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11106":{"id":11106,"size":0,"label":"ADDISWEDDING_PREVIEW-134.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-134.jpg","thumb":"ADDISWEDDING_PREVIEW-134.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11107":{"id":11107,"size":0,"label":"ADDISWEDDING_PREVIEW-135.jpg","type":"image","content":"ADDISWEDDING_PREVIEW-135.jpg","thumb":"ADDISWEDDING_PREVIEW-135.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11108":{"id":11108,"size":0,"label":"AndersonTXFB_01-1.jpg","type":"image","content":"AndersonTXFB_01-1.jpg","thumb":"AndersonTXFB_01-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11109":{"id":11109,"size":0,"label":"AndersonTXFB_01-2.jpg","type":"image","content":"AndersonTXFB_01-2.jpg","thumb":"AndersonTXFB_01-2.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11110":{"id":11110,"size":0,"label":"AndersonTXFB_01-3.jpg","type":"image","content":"AndersonTXFB_01-3.jpg","thumb":"AndersonTXFB_01-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11111":{"id":11111,"size":0,"label":"AndersonTXFB_01-4.jpg","type":"image","content":"AndersonTXFB_01-4.jpg","thumb":"AndersonTXFB_01-4.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11112":{"id":11112,"size":0,"label":"AndersonTXFB_01-5.jpg","type":"image","content":"AndersonTXFB_01-5.jpg","thumb":"AndersonTXFB_01-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11113":{"id":11113,"size":0,"label":"AndersonTXFB_01-6.jpg","type":"image","content":"AndersonTXFB_01-6.jpg","thumb":"AndersonTXFB_01-6.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11114":{"id":11114,"size":0,"label":"AndersonTXFB_01-7.jpg","type":"image","content":"AndersonTXFB_01-7.jpg","thumb":"AndersonTXFB_01-7.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11115":{"id":11115,"size":0,"label":"AndersonTXFB_01-8.jpg","type":"image","content":"AndersonTXFB_01-8.jpg","thumb":"AndersonTXFB_01-8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11116":{"id":11116,"size":0,"label":"AndersonTXFB_01-9.jpg","type":"image","content":"AndersonTXFB_01-9.jpg","thumb":"AndersonTXFB_01-9.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11117":{"id":11117,"size":0,"label":"AndersonTXFB_01-10.jpg","type":"image","content":"AndersonTXFB_01-10.jpg","thumb":"AndersonTXFB_01-10.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11118":{"id":11118,"size":0,"label":"AndersonTXFB_01-11.jpg","type":"image","content":"AndersonTXFB_01-11.jpg","thumb":"AndersonTXFB_01-11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11119":{"id":11119,"size":0,"label":"AndersonTXFB_01-12.jpg","type":"image","content":"AndersonTXFB_01-12.jpg","thumb":"AndersonTXFB_01-12.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11120":{"id":11120,"size":0,"label":"AndersonTXFB_01-13.jpg","type":"image","content":"AndersonTXFB_01-13.jpg","thumb":"AndersonTXFB_01-13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11121":{"id":11121,"size":0,"label":"AndersonTXFB_01-14.jpg","type":"image","content":"AndersonTXFB_01-14.jpg","thumb":"AndersonTXFB_01-14.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11122":{"id":11122,"size":0,"label":"AndersonTXFB_01-15.jpg","type":"image","content":"AndersonTXFB_01-15.jpg","thumb":"AndersonTXFB_01-15.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11123":{"id":11123,"size":0,"label":"AndersonTXFB_01-16.jpg","type":"image","content":"AndersonTXFB_01-16.jpg","thumb":"AndersonTXFB_01-16.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11124":{"id":11124,"size":0,"label":"AndersonTXFB_01-17.jpg","type":"image","content":"AndersonTXFB_01-17.jpg","thumb":"AndersonTXFB_01-17.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11125":{"id":11125,"size":0,"label":"AndersonTXFB_01-18.jpg","type":"image","content":"AndersonTXFB_01-18.jpg","thumb":"AndersonTXFB_01-18.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11126":{"id":11126,"size":0,"label":"AndersonTXFB_01-19.jpg","type":"image","content":"AndersonTXFB_01-19.jpg","thumb":"AndersonTXFB_01-19.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11127":{"id":11127,"size":0,"label":"AndersonTXFB_01-20.jpg","type":"image","content":"AndersonTXFB_01-20.jpg","thumb":"AndersonTXFB_01-20.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11128":{"id":11128,"size":0,"label":"AndersonTXFB_01-21.jpg","type":"image","content":"AndersonTXFB_01-21.jpg","thumb":"AndersonTXFB_01-21.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11129":{"id":11129,"size":0,"label":"AndersonTXFB_01-22.jpg","type":"image","content":"AndersonTXFB_01-22.jpg","thumb":"AndersonTXFB_01-22.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11130":{"id":11130,"size":0,"label":"AndersonTXFB_01-23.jpg","type":"image","content":"AndersonTXFB_01-23.jpg","thumb":"AndersonTXFB_01-23.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11131":{"id":11131,"size":0,"label":"AndersonTXFB_01-24.jpg","type":"image","content":"AndersonTXFB_01-24.jpg","thumb":"AndersonTXFB_01-24.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11132":{"id":11132,"size":0,"label":"AndersonTXFB_01-25.jpg","type":"image","content":"AndersonTXFB_01-25.jpg","thumb":"AndersonTXFB_01-25.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11133":{"id":11133,"size":0,"label":"AndersonTXFB_01-26.jpg","type":"image","content":"AndersonTXFB_01-26.jpg","thumb":"AndersonTXFB_01-26.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11134":{"id":11134,"size":0,"label":"AndersonTXFB_01-27.jpg","type":"image","content":"AndersonTXFB_01-27.jpg","thumb":"AndersonTXFB_01-27.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11135":{"id":11135,"size":0,"label":"AndersonTXFB_01-28.jpg","type":"image","content":"AndersonTXFB_01-28.jpg","thumb":"AndersonTXFB_01-28.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11136":{"id":11136,"size":0,"label":"AndersonTXFB_01-29.jpg","type":"image","content":"AndersonTXFB_01-29.jpg","thumb":"AndersonTXFB_01-29.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11137":{"id":11137,"size":0,"label":"AndersonTXFB_01-30.jpg","type":"image","content":"AndersonTXFB_01-30.jpg","thumb":"AndersonTXFB_01-30.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11138":{"id":11138,"size":0,"label":"AndersonTXFB_01-31.jpg","type":"image","content":"AndersonTXFB_01-31.jpg","thumb":"AndersonTXFB_01-31.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11139":{"id":11139,"size":0,"label":"AndersonTXFB_01-32.jpg","type":"image","content":"AndersonTXFB_01-32.jpg","thumb":"AndersonTXFB_01-32.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11140":{"id":11140,"size":0,"label":"AndersonTXFB_01-33.jpg","type":"image","content":"AndersonTXFB_01-33.jpg","thumb":"AndersonTXFB_01-33.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11141":{"id":11141,"size":0,"label":"AndersonTXFB_01-34.jpg","type":"image","content":"AndersonTXFB_01-34.jpg","thumb":"AndersonTXFB_01-34.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11142":{"id":11142,"size":0,"label":"AndersonTXFB_01-35.jpg","type":"image","content":"AndersonTXFB_01-35.jpg","thumb":"AndersonTXFB_01-35.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11143":{"id":11143,"size":0,"label":"AndersonTXFB_01-36.jpg","type":"image","content":"AndersonTXFB_01-36.jpg","thumb":"AndersonTXFB_01-36.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11144":{"id":11144,"size":0,"label":"AndersonTXFB_01-37.jpg","type":"image","content":"AndersonTXFB_01-37.jpg","thumb":"AndersonTXFB_01-37.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11145":{"id":11145,"size":0,"label":"AndersonTXFB_01-38.jpg","type":"image","content":"AndersonTXFB_01-38.jpg","thumb":"AndersonTXFB_01-38.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11146":{"id":11146,"size":0,"label":"AndersonTXFB_01-39.jpg","type":"image","content":"AndersonTXFB_01-39.jpg","thumb":"AndersonTXFB_01-39.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11147":{"id":11147,"size":0,"label":"AndersonTXFB_01-40.jpg","type":"image","content":"AndersonTXFB_01-40.jpg","thumb":"AndersonTXFB_01-40.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11148":{"id":11148,"size":0,"label":"AndersonTXFB_01-41.jpg","type":"image","content":"AndersonTXFB_01-41.jpg","thumb":"AndersonTXFB_01-41.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11149":{"id":11149,"size":0,"label":"AndersonTXFB_01-42.jpg","type":"image","content":"AndersonTXFB_01-42.jpg","thumb":"AndersonTXFB_01-42.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11150":{"id":11150,"size":0,"label":"AndersonTXFB_01-43.jpg","type":"image","content":"AndersonTXFB_01-43.jpg","thumb":"AndersonTXFB_01-43.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11151":{"id":11151,"size":0,"label":"AndersonTXFB_01-44.jpg","type":"image","content":"AndersonTXFB_01-44.jpg","thumb":"AndersonTXFB_01-44.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11152":{"id":11152,"size":0,"label":"AndersonTXFB_01-45.jpg","type":"image","content":"AndersonTXFB_01-45.jpg","thumb":"AndersonTXFB_01-45.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11153":{"id":11153,"size":0,"label":"AndersonTXFB_01-46.jpg","type":"image","content":"AndersonTXFB_01-46.jpg","thumb":"AndersonTXFB_01-46.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11154":{"id":11154,"size":0,"label":"AndersonTXFB_01-47.jpg","type":"image","content":"AndersonTXFB_01-47.jpg","thumb":"AndersonTXFB_01-47.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11155":{"id":11155,"size":0,"label":"AndersonTXFB_01-48.jpg","type":"image","content":"AndersonTXFB_01-48.jpg","thumb":"AndersonTXFB_01-48.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11156":{"id":11156,"size":0,"label":"AndersonTXFB_01-49.jpg","type":"image","content":"AndersonTXFB_01-49.jpg","thumb":"AndersonTXFB_01-49.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11157":{"id":11157,"size":0,"label":"AndersonTXFB_01-50.jpg","type":"image","content":"AndersonTXFB_01-50.jpg","thumb":"AndersonTXFB_01-50.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11158":{"id":11158,"size":0,"label":"AndersonTXFB_01-51.jpg","type":"image","content":"AndersonTXFB_01-51.jpg","thumb":"AndersonTXFB_01-51.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11159":{"id":11159,"size":0,"label":"AndersonTXFB_01-52.jpg","type":"image","content":"AndersonTXFB_01-52.jpg","thumb":"AndersonTXFB_01-52.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11160":{"id":11160,"size":0,"label":"AndersonTXFB_01-53.jpg","type":"image","content":"AndersonTXFB_01-53.jpg","thumb":"AndersonTXFB_01-53.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11161":{"id":11161,"size":0,"label":"AndersonTXFB_01-54.jpg","type":"image","content":"AndersonTXFB_01-54.jpg","thumb":"AndersonTXFB_01-54.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11162":{"id":11162,"size":0,"label":"AndersonTXFB_01-55.jpg","type":"image","content":"AndersonTXFB_01-55.jpg","thumb":"AndersonTXFB_01-55.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11163":{"id":11163,"size":0,"label":"AndersonTXFB_01-56.jpg","type":"image","content":"AndersonTXFB_01-56.jpg","thumb":"AndersonTXFB_01-56.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11164":{"id":11164,"size":0,"label":"AndersonTXFB_01-57.jpg","type":"image","content":"AndersonTXFB_01-57.jpg","thumb":"AndersonTXFB_01-57.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11165":{"id":11165,"size":0,"label":"AndersonTXFB_01-58.jpg","type":"image","content":"AndersonTXFB_01-58.jpg","thumb":"AndersonTXFB_01-58.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11166":{"id":11166,"size":0,"label":"AndersonTXFB_01-59.jpg","type":"image","content":"AndersonTXFB_01-59.jpg","thumb":"AndersonTXFB_01-59.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11167":{"id":11167,"size":0,"label":"AndersonTXFB_01-60.jpg","type":"image","content":"AndersonTXFB_01-60.jpg","thumb":"AndersonTXFB_01-60.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11168":{"id":11168,"size":0,"label":"AndersonTXFB_01-61.jpg","type":"image","content":"AndersonTXFB_01-61.jpg","thumb":"AndersonTXFB_01-61.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11169":{"id":11169,"size":0,"label":"AndersonTXFB_01-62.jpg","type":"image","content":"AndersonTXFB_01-62.jpg","thumb":"AndersonTXFB_01-62.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11170":{"id":11170,"size":0,"label":"AndersonTXFB_01-63.jpg","type":"image","content":"AndersonTXFB_01-63.jpg","thumb":"AndersonTXFB_01-63.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11171":{"id":11171,"size":0,"label":"AndersonTXFB_01-64.jpg","type":"image","content":"AndersonTXFB_01-64.jpg","thumb":"AndersonTXFB_01-64.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11172":{"id":11172,"size":0,"label":"AndersonTXFB_01-65.jpg","type":"image","content":"AndersonTXFB_01-65.jpg","thumb":"AndersonTXFB_01-65.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11173":{"id":11173,"size":0,"label":"AndersonTXFB_01-66.jpg","type":"image","content":"AndersonTXFB_01-66.jpg","thumb":"AndersonTXFB_01-66.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11174":{"id":11174,"size":0,"label":"AndersonTXFB_01-67.jpg","type":"image","content":"AndersonTXFB_01-67.jpg","thumb":"AndersonTXFB_01-67.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11175":{"id":11175,"size":0,"label":"AndersonTXFB_01-68.jpg","type":"image","content":"AndersonTXFB_01-68.jpg","thumb":"AndersonTXFB_01-68.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11176":{"id":11176,"size":0,"label":"AndersonTXFB_01-69.jpg","type":"image","content":"AndersonTXFB_01-69.jpg","thumb":"AndersonTXFB_01-69.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11177":{"id":11177,"size":0,"label":"AndersonTXFB_01-70.jpg","type":"image","content":"AndersonTXFB_01-70.jpg","thumb":"AndersonTXFB_01-70.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11178":{"id":11178,"size":0,"label":"AndersonTXFB_01-71.jpg","type":"image","content":"AndersonTXFB_01-71.jpg","thumb":"AndersonTXFB_01-71.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11179":{"id":11179,"size":0,"label":"AndersonTXFB_01-72.jpg","type":"image","content":"AndersonTXFB_01-72.jpg","thumb":"AndersonTXFB_01-72.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11180":{"id":11180,"size":0,"label":"AndersonTXFB_01-73.jpg","type":"image","content":"AndersonTXFB_01-73.jpg","thumb":"AndersonTXFB_01-73.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11181":{"id":11181,"size":0,"label":"AndersonTXFB_01-74.jpg","type":"image","content":"AndersonTXFB_01-74.jpg","thumb":"AndersonTXFB_01-74.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11182":{"id":11182,"size":0,"label":"AndersonTXFB_01-75.jpg","type":"image","content":"AndersonTXFB_01-75.jpg","thumb":"AndersonTXFB_01-75.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11183":{"id":11183,"size":0,"label":"AndersonTXFB_01-76.jpg","type":"image","content":"AndersonTXFB_01-76.jpg","thumb":"AndersonTXFB_01-76.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11184":{"id":11184,"size":0,"label":"AndersonTXFB_01-77.jpg","type":"image","content":"AndersonTXFB_01-77.jpg","thumb":"AndersonTXFB_01-77.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11185":{"id":11185,"size":0,"label":"AndersonTXFB_01-78.jpg","type":"image","content":"AndersonTXFB_01-78.jpg","thumb":"AndersonTXFB_01-78.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11186":{"id":11186,"size":0,"label":"AndersonTXFB_01-79.jpg","type":"image","content":"AndersonTXFB_01-79.jpg","thumb":"AndersonTXFB_01-79.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11187":{"id":11187,"size":0,"label":"AndersonTXFB_01-80.jpg","type":"image","content":"AndersonTXFB_01-80.jpg","thumb":"AndersonTXFB_01-80.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11188":{"id":11188,"size":0,"label":"AndersonTXFB_01-81.jpg","type":"image","content":"AndersonTXFB_01-81.jpg","thumb":"AndersonTXFB_01-81.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11189":{"id":11189,"size":0,"label":"AndersonTXFB_01-82.jpg","type":"image","content":"AndersonTXFB_01-82.jpg","thumb":"AndersonTXFB_01-82.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11190":{"id":11190,"size":0,"label":"AndersonTXFB_01-83.jpg","type":"image","content":"AndersonTXFB_01-83.jpg","thumb":"AndersonTXFB_01-83.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11191":{"id":11191,"size":0,"label":"AndersonTXFB_01-84.jpg","type":"image","content":"AndersonTXFB_01-84.jpg","thumb":"AndersonTXFB_01-84.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11192":{"id":11192,"size":0,"label":"AndersonTXFB_01-85.jpg","type":"image","content":"AndersonTXFB_01-85.jpg","thumb":"AndersonTXFB_01-85.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11193":{"id":11193,"size":0,"label":"AndersonTXFB_01-86.jpg","type":"image","content":"AndersonTXFB_01-86.jpg","thumb":"AndersonTXFB_01-86.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11194":{"id":11194,"size":0,"label":"AndersonTXFB_01-87.jpg","type":"image","content":"AndersonTXFB_01-87.jpg","thumb":"AndersonTXFB_01-87.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11195":{"id":11195,"size":0,"label":"AndersonTXFB_01-88.jpg","type":"image","content":"AndersonTXFB_01-88.jpg","thumb":"AndersonTXFB_01-88.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11196":{"id":11196,"size":0,"label":"AndersonTXFB_01-89.jpg","type":"image","content":"AndersonTXFB_01-89.jpg","thumb":"AndersonTXFB_01-89.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11197":{"id":11197,"size":0,"label":"AndersonTXFB_01-90.jpg","type":"image","content":"AndersonTXFB_01-90.jpg","thumb":"AndersonTXFB_01-90.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11198":{"id":11198,"size":0,"label":"AndersonTXFB_01-91.jpg","type":"image","content":"AndersonTXFB_01-91.jpg","thumb":"AndersonTXFB_01-91.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11199":{"id":11199,"size":0,"label":"AndersonTXFB_01-92.jpg","type":"image","content":"AndersonTXFB_01-92.jpg","thumb":"AndersonTXFB_01-92.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11200":{"id":11200,"size":0,"label":"AndersonTXFB_01-137.jpg","type":"image","content":"AndersonTXFB_01-137.jpg","thumb":"AndersonTXFB_01-137.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11201":{"id":11201,"size":0,"label":"AndersonTXFB_01-136.jpg","type":"image","content":"AndersonTXFB_01-136.jpg","thumb":"AndersonTXFB_01-136.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11202":{"id":11202,"size":0,"label":"AndersonTXFB_01-135.jpg","type":"image","content":"AndersonTXFB_01-135.jpg","thumb":"AndersonTXFB_01-135.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11203":{"id":11203,"size":0,"label":"AndersonTXFB_01-134.jpg","type":"image","content":"AndersonTXFB_01-134.jpg","thumb":"AndersonTXFB_01-134.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11204":{"id":11204,"size":0,"label":"AndersonTXFB_01-133.jpg","type":"image","content":"AndersonTXFB_01-133.jpg","thumb":"AndersonTXFB_01-133.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11205":{"id":11205,"size":0,"label":"AndersonTXFB_01-132.jpg","type":"image","content":"AndersonTXFB_01-132.jpg","thumb":"AndersonTXFB_01-132.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11206":{"id":11206,"size":0,"label":"AndersonTXFB_01-131.jpg","type":"image","content":"AndersonTXFB_01-131.jpg","thumb":"AndersonTXFB_01-131.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11207":{"id":11207,"size":0,"label":"AndersonTXFB_01-130.jpg","type":"image","content":"AndersonTXFB_01-130.jpg","thumb":"AndersonTXFB_01-130.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11208":{"id":11208,"size":0,"label":"AndersonTXFB_01-129.jpg","type":"image","content":"AndersonTXFB_01-129.jpg","thumb":"AndersonTXFB_01-129.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11209":{"id":11209,"size":0,"label":"AndersonTXFB_01-128.jpg","type":"image","content":"AndersonTXFB_01-128.jpg","thumb":"AndersonTXFB_01-128.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11210":{"id":11210,"size":0,"label":"AndersonTXFB_01-127.jpg","type":"image","content":"AndersonTXFB_01-127.jpg","thumb":"AndersonTXFB_01-127.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11211":{"id":11211,"size":0,"label":"AndersonTXFB_01-126.jpg","type":"image","content":"AndersonTXFB_01-126.jpg","thumb":"AndersonTXFB_01-126.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11212":{"id":11212,"size":0,"label":"AndersonTXFB_01-125.jpg","type":"image","content":"AndersonTXFB_01-125.jpg","thumb":"AndersonTXFB_01-125.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11213":{"id":11213,"size":0,"label":"AndersonTXFB_01-124.jpg","type":"image","content":"AndersonTXFB_01-124.jpg","thumb":"AndersonTXFB_01-124.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11214":{"id":11214,"size":0,"label":"AndersonTXFB_01-123.jpg","type":"image","content":"AndersonTXFB_01-123.jpg","thumb":"AndersonTXFB_01-123.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11215":{"id":11215,"size":0,"label":"AndersonTXFB_01-122.jpg","type":"image","content":"AndersonTXFB_01-122.jpg","thumb":"AndersonTXFB_01-122.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11216":{"id":11216,"size":0,"label":"AndersonTXFB_01-121.jpg","type":"image","content":"AndersonTXFB_01-121.jpg","thumb":"AndersonTXFB_01-121.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11217":{"id":11217,"size":0,"label":"AndersonTXFB_01-120.jpg","type":"image","content":"AndersonTXFB_01-120.jpg","thumb":"AndersonTXFB_01-120.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11218":{"id":11218,"size":0,"label":"AndersonTXFB_01-119.jpg","type":"image","content":"AndersonTXFB_01-119.jpg","thumb":"AndersonTXFB_01-119.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11219":{"id":11219,"size":0,"label":"AndersonTXFB_01-118.jpg","type":"image","content":"AndersonTXFB_01-118.jpg","thumb":"AndersonTXFB_01-118.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11220":{"id":11220,"size":0,"label":"AndersonTXFB_01-117.jpg","type":"image","content":"AndersonTXFB_01-117.jpg","thumb":"AndersonTXFB_01-117.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11221":{"id":11221,"size":0,"label":"AndersonTXFB_01-116.jpg","type":"image","content":"AndersonTXFB_01-116.jpg","thumb":"AndersonTXFB_01-116.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11222":{"id":11222,"size":0,"label":"AndersonTXFB_01-115.jpg","type":"image","content":"AndersonTXFB_01-115.jpg","thumb":"AndersonTXFB_01-115.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11223":{"id":11223,"size":0,"label":"AndersonTXFB_01-114.jpg","type":"image","content":"AndersonTXFB_01-114.jpg","thumb":"AndersonTXFB_01-114.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11224":{"id":11224,"size":0,"label":"AndersonTXFB_01-113.jpg","type":"image","content":"AndersonTXFB_01-113.jpg","thumb":"AndersonTXFB_01-113.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11225":{"id":11225,"size":0,"label":"AndersonTXFB_01-121-DUP.jpg","type":"image","content":"AndersonTXFB_01-121-DUP.jpg","thumb":"AndersonTXFB_01-121-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11226":{"id":11226,"size":0,"label":"ViacomFallingWhistles_AIR-92.jpg","type":"image","content":"ViacomFallingWhistles_AIR-92.jpg","thumb":"ViacomFallingWhistles_AIR-92.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11227":{"id":11227,"size":0,"label":"ViacomFallingWhistles_AIR-115.jpg","type":"image","content":"ViacomFallingWhistles_AIR-115.jpg","thumb":"ViacomFallingWhistles_AIR-115.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11228":{"id":11228,"size":0,"label":"ViacomFallingWhistles_AIR-19.jpg","type":"image","content":"ViacomFallingWhistles_AIR-19.jpg","thumb":"ViacomFallingWhistles_AIR-19.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11229":{"id":11229,"size":0,"label":"ViacomFallingWhistles_AIR-26.jpg","type":"image","content":"ViacomFallingWhistles_AIR-26.jpg","thumb":"ViacomFallingWhistles_AIR-26.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11230":{"id":11230,"size":0,"label":"ViacomFallingWhistles_AIR-83.jpg","type":"image","content":"ViacomFallingWhistles_AIR-83.jpg","thumb":"ViacomFallingWhistles_AIR-83.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11231":{"id":11231,"size":0,"label":"ViacomFallingWhistles_AIR-136.jpg","type":"image","content":"ViacomFallingWhistles_AIR-136.jpg","thumb":"ViacomFallingWhistles_AIR-136.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11232":{"id":11232,"size":0,"label":"ViacomFallingWhistles_AIR-85.jpg","type":"image","content":"ViacomFallingWhistles_AIR-85.jpg","thumb":"ViacomFallingWhistles_AIR-85.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11233":{"id":11233,"size":0,"label":"ViacomFallingWhistles_AIR-17.jpg","type":"image","content":"ViacomFallingWhistles_AIR-17.jpg","thumb":"ViacomFallingWhistles_AIR-17.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11234":{"id":11234,"size":0,"label":"ViacomFallingWhistles_AIR-5.jpg","type":"image","content":"ViacomFallingWhistles_AIR-5.jpg","thumb":"ViacomFallingWhistles_AIR-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11235":{"id":11235,"size":0,"label":"ViacomFallingWhistles_AIR-3.jpg","type":"image","content":"ViacomFallingWhistles_AIR-3.jpg","thumb":"ViacomFallingWhistles_AIR-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11236":{"id":11236,"size":0,"label":"ViacomFallingWhistles_AIR-25.jpg","type":"image","content":"ViacomFallingWhistles_AIR-25.jpg","thumb":"ViacomFallingWhistles_AIR-25.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11237":{"id":11237,"size":0,"label":"ViacomFallingWhistles_AIR-135.jpg","type":"image","content":"ViacomFallingWhistles_AIR-135.jpg","thumb":"ViacomFallingWhistles_AIR-135.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11238":{"id":11238,"size":0,"label":"ViacomFallingWhistles_AIR-20.jpg","type":"image","content":"ViacomFallingWhistles_AIR-20.jpg","thumb":"ViacomFallingWhistles_AIR-20.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11239":{"id":11239,"size":0,"label":"ViacomFallingWhistles_AIR-34.jpg","type":"image","content":"ViacomFallingWhistles_AIR-34.jpg","thumb":"ViacomFallingWhistles_AIR-34.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11240":{"id":11240,"size":0,"label":"ViacomFallingWhistles_AIR-139.jpg","type":"image","content":"ViacomFallingWhistles_AIR-139.jpg","thumb":"ViacomFallingWhistles_AIR-139.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11241":{"id":11241,"size":0,"label":"ViacomFallingWhistles_AIR-28.jpg","type":"image","content":"ViacomFallingWhistles_AIR-28.jpg","thumb":"ViacomFallingWhistles_AIR-28.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11242":{"id":11242,"size":0,"label":"ViacomFallingWhistles_AIR-41.jpg","type":"image","content":"ViacomFallingWhistles_AIR-41.jpg","thumb":"ViacomFallingWhistles_AIR-41.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11243":{"id":11243,"size":0,"label":"ViacomFallingWhistles_AIR-11.jpg","type":"image","content":"ViacomFallingWhistles_AIR-11.jpg","thumb":"ViacomFallingWhistles_AIR-11.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11244":{"id":11244,"size":0,"label":"ViacomFallingWhistles_AIR-88.jpg","type":"image","content":"ViacomFallingWhistles_AIR-88.jpg","thumb":"ViacomFallingWhistles_AIR-88.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11245":{"id":11245,"size":0,"label":"ViacomFallingWhistles_AIR-89.jpg","type":"image","content":"ViacomFallingWhistles_AIR-89.jpg","thumb":"ViacomFallingWhistles_AIR-89.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11246":{"id":11246,"size":0,"label":"ViacomFallingWhistles_AIR-101.jpg","type":"image","content":"ViacomFallingWhistles_AIR-101.jpg","thumb":"ViacomFallingWhistles_AIR-101.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11247":{"id":11247,"size":0,"label":"ViacomFallingWhistles_AIR-94.jpg","type":"image","content":"ViacomFallingWhistles_AIR-94.jpg","thumb":"ViacomFallingWhistles_AIR-94.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11248":{"id":11248,"size":0,"label":"ViacomFallingWhistles_AIR-70.jpg","type":"image","content":"ViacomFallingWhistles_AIR-70.jpg","thumb":"ViacomFallingWhistles_AIR-70.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11249":{"id":11249,"size":0,"label":"ViacomFallingWhistles_AIR-105.jpg","type":"image","content":"ViacomFallingWhistles_AIR-105.jpg","thumb":"ViacomFallingWhistles_AIR-105.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11250":{"id":11250,"size":0,"label":"ViacomFallingWhistles_AIR-82.jpg","type":"image","content":"ViacomFallingWhistles_AIR-82.jpg","thumb":"ViacomFallingWhistles_AIR-82.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11251":{"id":11251,"size":0,"label":"ViacomFallingWhistles_AIR-104.jpg","type":"image","content":"ViacomFallingWhistles_AIR-104.jpg","thumb":"ViacomFallingWhistles_AIR-104.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11252":{"id":11252,"size":0,"label":"ViacomFallingWhistles_AIR-73.jpg","type":"image","content":"ViacomFallingWhistles_AIR-73.jpg","thumb":"ViacomFallingWhistles_AIR-73.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11253":{"id":11253,"size":0,"label":"ViacomFallingWhistles_AIR-64.jpg","type":"image","content":"ViacomFallingWhistles_AIR-64.jpg","thumb":"ViacomFallingWhistles_AIR-64.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11254":{"id":11254,"size":0,"label":"ViacomFallingWhistles_AIR-95.jpg","type":"image","content":"ViacomFallingWhistles_AIR-95.jpg","thumb":"ViacomFallingWhistles_AIR-95.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11255":{"id":11255,"size":0,"label":"ViacomFallingWhistles_AIR-80.jpg","type":"image","content":"ViacomFallingWhistles_AIR-80.jpg","thumb":"ViacomFallingWhistles_AIR-80.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11256":{"id":11256,"size":0,"label":"ViacomFallingWhistles_AIR-118.jpg","type":"image","content":"ViacomFallingWhistles_AIR-118.jpg","thumb":"ViacomFallingWhistles_AIR-118.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11257":{"id":11257,"size":0,"label":"ViacomFallingWhistles_AIR-91.jpg","type":"image","content":"ViacomFallingWhistles_AIR-91.jpg","thumb":"ViacomFallingWhistles_AIR-91.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11258":{"id":11258,"size":0,"label":"ViacomFallingWhistles_AIR-22.jpg","type":"image","content":"ViacomFallingWhistles_AIR-22.jpg","thumb":"ViacomFallingWhistles_AIR-22.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11259":{"id":11259,"size":0,"label":"ViacomFallingWhistles_AIR-59.jpg","type":"image","content":"ViacomFallingWhistles_AIR-59.jpg","thumb":"ViacomFallingWhistles_AIR-59.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11260":{"id":11260,"size":0,"label":"ViacomFallingWhistles_AIR-134.jpg","type":"image","content":"ViacomFallingWhistles_AIR-134.jpg","thumb":"ViacomFallingWhistles_AIR-134.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11261":{"id":11261,"size":0,"label":"ViacomFallingWhistles_AIR-30.jpg","type":"image","content":"ViacomFallingWhistles_AIR-30.jpg","thumb":"ViacomFallingWhistles_AIR-30.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11262":{"id":11262,"size":0,"label":"ViacomFallingWhistles_AIR-114.jpg","type":"image","content":"ViacomFallingWhistles_AIR-114.jpg","thumb":"ViacomFallingWhistles_AIR-114.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11263":{"id":11263,"size":0,"label":"ViacomFallingWhistles_AIR-109.jpg","type":"image","content":"ViacomFallingWhistles_AIR-109.jpg","thumb":"ViacomFallingWhistles_AIR-109.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11264":{"id":11264,"size":0,"label":"ViacomFallingWhistles_AIR-102.jpg","type":"image","content":"ViacomFallingWhistles_AIR-102.jpg","thumb":"ViacomFallingWhistles_AIR-102.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11265":{"id":11265,"size":0,"label":"ViacomFallingWhistles_AIR-76.jpg","type":"image","content":"ViacomFallingWhistles_AIR-76.jpg","thumb":"ViacomFallingWhistles_AIR-76.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11266":{"id":11266,"size":0,"label":"ViacomFallingWhistles_AIR-37.jpg","type":"image","content":"ViacomFallingWhistles_AIR-37.jpg","thumb":"ViacomFallingWhistles_AIR-37.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11267":{"id":11267,"size":0,"label":"ViacomFallingWhistles_AIR-87.jpg","type":"image","content":"ViacomFallingWhistles_AIR-87.jpg","thumb":"ViacomFallingWhistles_AIR-87.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11268":{"id":11268,"size":0,"label":"ViacomFallingWhistles_AIR-131.jpg","type":"image","content":"ViacomFallingWhistles_AIR-131.jpg","thumb":"ViacomFallingWhistles_AIR-131.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11269":{"id":11269,"size":0,"label":"ViacomFallingWhistles_AIR-1.jpg","type":"image","content":"ViacomFallingWhistles_AIR-1.jpg","thumb":"ViacomFallingWhistles_AIR-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11270":{"id":11270,"size":0,"label":"ViacomFallingWhistles_AIR-32.jpg","type":"image","content":"ViacomFallingWhistles_AIR-32.jpg","thumb":"ViacomFallingWhistles_AIR-32.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11271":{"id":11271,"size":0,"label":"ViacomFallingWhistles_AIR-62.jpg","type":"image","content":"ViacomFallingWhistles_AIR-62.jpg","thumb":"ViacomFallingWhistles_AIR-62.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11272":{"id":11272,"size":0,"label":"ViacomFallingWhistles_AIR-52.jpg","type":"image","content":"ViacomFallingWhistles_AIR-52.jpg","thumb":"ViacomFallingWhistles_AIR-52.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11273":{"id":11273,"size":0,"label":"ViacomFallingWhistles_AIR-13.jpg","type":"image","content":"ViacomFallingWhistles_AIR-13.jpg","thumb":"ViacomFallingWhistles_AIR-13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11274":{"id":11274,"size":0,"label":"ViacomFallingWhistles_AIR-42.jpg","type":"image","content":"ViacomFallingWhistles_AIR-42.jpg","thumb":"ViacomFallingWhistles_AIR-42.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11275":{"id":11275,"size":0,"label":"ViacomFallingWhistles_AIR-120.jpg","type":"image","content":"ViacomFallingWhistles_AIR-120.jpg","thumb":"ViacomFallingWhistles_AIR-120.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11276":{"id":11276,"size":0,"label":"ViacomFallingWhistles_AIR-108.jpg","type":"image","content":"ViacomFallingWhistles_AIR-108.jpg","thumb":"ViacomFallingWhistles_AIR-108.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11277":{"id":11277,"size":0,"label":"ViacomFallingWhistles_AIR-75.jpg","type":"image","content":"ViacomFallingWhistles_AIR-75.jpg","thumb":"ViacomFallingWhistles_AIR-75.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11278":{"id":11278,"size":0,"label":"ViacomFallingWhistles_AIR-31.jpg","type":"image","content":"ViacomFallingWhistles_AIR-31.jpg","thumb":"ViacomFallingWhistles_AIR-31.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11279":{"id":11279,"size":0,"label":"ViacomFallingWhistles_AIR-49.jpg","type":"image","content":"ViacomFallingWhistles_AIR-49.jpg","thumb":"ViacomFallingWhistles_AIR-49.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11280":{"id":11280,"size":0,"label":"ViacomFallingWhistles_AIR-55.jpg","type":"image","content":"ViacomFallingWhistles_AIR-55.jpg","thumb":"ViacomFallingWhistles_AIR-55.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11281":{"id":11281,"size":0,"label":"ViacomFallingWhistles_AIR-44.jpg","type":"image","content":"ViacomFallingWhistles_AIR-44.jpg","thumb":"ViacomFallingWhistles_AIR-44.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11282":{"id":11282,"size":0,"label":"Anderson_NKhan.jpg","type":"image","content":"Anderson_NKhan.jpg","thumb":"Anderson_NKhan.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11283":{"id":11283,"size":0,"label":"Anderson_KateSpade_SS13.jpg","type":"image","content":"Anderson_KateSpade_SS13.jpg","thumb":"Anderson_KateSpade_SS13.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11284":{"id":11284,"size":0,"label":"Anderson_PassavantandLee.jpg","type":"image","content":"Anderson_PassavantandLee.jpg","thumb":"Anderson_PassavantandLee.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11285":{"id":11285,"size":0,"label":"Anderson_Cowart.jpg","type":"image","content":"Anderson_Cowart.jpg","thumb":"Anderson_Cowart.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11286":{"id":11286,"size":0,"label":"063A0650.jpg","type":"image","content":"063A0650.jpg","thumb":"063A0650.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11287":{"id":11287,"size":0,"label":"VIDEO","type":"link","content":"http:\/\/vimeopro.com\/ericryananderson\/eric-ryan-anderson-2014","thumb":"","linkTarget":"_blank","caption":"null","featuredImage":" ","filters":""},"11288":{"id":11288,"size":0,"label":"063A0553.JPG","type":"image","content":"063A0553.JPG","thumb":"063A0553.JPG","linkTarget":"","caption":"","featuredImage":"","filters":""},"11289":{"id":11289,"size":0,"label":"ERA_JROD_HIGHRES-3.jpg","type":"image","content":"ERA_JROD_HIGHRES-3.jpg","thumb":"ERA_JROD_HIGHRES-3.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11290":{"id":11290,"size":0,"label":"ERA_LMAG_HOSPITALITY-1.jpg","type":"image","content":"ERA_LMAG_HOSPITALITY-1.jpg","thumb":"ERA_LMAG_HOSPITALITY-1.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11291":{"id":11291,"size":0,"label":"ERA_OLD97S-107.jpg","type":"image","content":"ERA_OLD97S-107.jpg","thumb":"ERA_OLD97S-107.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11292":{"id":11292,"size":0,"label":"EricRyanAnderson_EA_Jeremy-5.jpg","type":"image","content":"EricRyanAnderson_EA_Jeremy-5.jpg","thumb":"EricRyanAnderson_EA_Jeremy-5.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11293":{"id":11293,"size":0,"label":"EricRyanAnderson_MeredithAdelaide-72.jpg","type":"image","content":"EricRyanAnderson_MeredithAdelaide-72.jpg","thumb":"EricRyanAnderson_MeredithAdelaide-72.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11294":{"id":11294,"size":0,"label":"EricRyanAnderson_MeredithAdelaide-84.jpg","type":"image","content":"EricRyanAnderson_MeredithAdelaide-84.jpg","thumb":"EricRyanAnderson_MeredithAdelaide-84.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11295":{"id":11295,"size":0,"label":"EricRyanAnderson_MeredithAdelaide-98.jpg","type":"image","content":"EricRyanAnderson_MeredithAdelaide-98.jpg","thumb":"EricRyanAnderson_MeredithAdelaide-98.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11296":{"id":11296,"size":0,"label":"EricRyanAnderson_MeredithAdelaide-116.jpg","type":"image","content":"EricRyanAnderson_MeredithAdelaide-116.jpg","thumb":"EricRyanAnderson_MeredithAdelaide-116.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11297":{"id":11297,"size":0,"label":"EricRyanAnderson_OliberteFW2014-173.jpg","type":"image","content":"EricRyanAnderson_OliberteFW2014-173.jpg","thumb":"EricRyanAnderson_OliberteFW2014-173.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11298":{"id":11298,"size":0,"label":"EricRyanAnderson_OliberteFW2014-281.jpg","type":"image","content":"EricRyanAnderson_OliberteFW2014-281.jpg","thumb":"EricRyanAnderson_OliberteFW2014-281.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11299":{"id":11299,"size":0,"label":"EricRyanAnderson_OliberteFW2014-291.jpg","type":"image","content":"EricRyanAnderson_OliberteFW2014-291.jpg","thumb":"EricRyanAnderson_OliberteFW2014-291.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11300":{"id":11300,"size":0,"label":"EricRyanAnderson_OliberteFW2014-301.jpg","type":"image","content":"EricRyanAnderson_OliberteFW2014-301.jpg","thumb":"EricRyanAnderson_OliberteFW2014-301.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11301":{"id":11301,"size":0,"label":"EricRyanAnderson_PDN_01.jpg","type":"image","content":"EricRyanAnderson_PDN_01.jpg","thumb":"EricRyanAnderson_PDN_01.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11302":{"id":11302,"size":0,"label":"era.png","type":"image","content":"era.png","thumb":"era.png","linkTarget":"","caption":"","featuredImage":"","filters":""},"11303":{"id":11303,"size":0,"label":"eraB.png","type":"image","content":"eraB.png","thumb":"eraB.png","linkTarget":"","caption":"","featuredImage":"","filters":""},"11304":{"id":11304,"size":0,"label":"eraC.png","type":"image","content":"eraC.png","thumb":"eraC.png","linkTarget":"","caption":"","featuredImage":"","filters":""},"11305":{"id":11305,"size":0,"label":"JRODSQUARES.jpg","type":"image","content":"JRODSQUARES.jpg","thumb":"JRODSQUARES.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11306":{"id":11306,"size":0,"label":"ERA_OLD97S-8.jpg","type":"image","content":"ERA_OLD97S-8.jpg","thumb":"ERA_OLD97S-8.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11307":{"id":11307,"size":0,"label":"ERA_LMAG_HOSPITALITY-1-DUP.jpg","type":"image","content":"ERA_LMAG_HOSPITALITY-1-DUP.jpg","thumb":"ERA_LMAG_HOSPITALITY-1-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11308":{"id":11308,"size":0,"label":"ERA_JROD_HIGHRES-3-DUP.jpg","type":"image","content":"ERA_JROD_HIGHRES-3-DUP.jpg","thumb":"ERA_JROD_HIGHRES-3-DUP.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11309":{"id":11309,"size":0,"label":"NTB_35MM.jpg","type":"image","content":"NTB_35MM.jpg","thumb":"NTB_35MM.jpg","linkTarget":"","caption":"","featuredImage":"","filters":""},"11310":{"id":11310,"size":0,"label":"Anderson_Meredith_001.JPG","type":"image","content":"Anderson_Meredith_001.JPG","thumb":"Anderson_Meredith_001.JPG","linkTarget":"","caption":"","featuredImage":"","filters":""},"11311":{"id":11311,"size":0,"label":"Anderson_Meredith_002.JPG","type":"image","content":"Anderson_Meredith_002.JPG","thumb":"Anderson_Meredith_002.JPG","linkTarget":"","caption":"","featuredImage":"","filters":""},"11312":{"id":11312,"size":0,"label":"Anderson_Meredith_001.jpg","type":"image","content":"Anderson_Meredith_001.jpg","thumb":"Anderson_Meredith_001.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11313":{"id":11313,"size":0,"label":"Anderson_Meredith_002.jpg","type":"image","content":"Anderson_Meredith_002.jpg","thumb":"Anderson_Meredith_002.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11314":{"id":11314,"size":0,"label":"Anderson_Meredith_003.jpg","type":"image","content":"Anderson_Meredith_003.jpg","thumb":"Anderson_Meredith_003.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11315":{"id":11315,"size":0,"label":"Anderson_Meredith_004.jpg","type":"image","content":"Anderson_Meredith_004.jpg","thumb":"Anderson_Meredith_004.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11316":{"id":11316,"size":0,"label":"Anderson_Meredith_005.jpg","type":"image","content":"Anderson_Meredith_005.jpg","thumb":"Anderson_Meredith_005.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11317":{"id":11317,"size":0,"label":"Anderson_Meredith_006.jpg","type":"image","content":"Anderson_Meredith_006.jpg","thumb":"Anderson_Meredith_006.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11318":{"id":11318,"size":0,"label":"Anderson_Meredith_007.jpg","type":"image","content":"Anderson_Meredith_007.jpg","thumb":"Anderson_Meredith_007.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11319":{"id":11319,"size":0,"label":"Anderson_Meredith_008.jpg","type":"image","content":"Anderson_Meredith_008.jpg","thumb":"Anderson_Meredith_008.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11320":{"id":11320,"size":0,"label":"Anderson_Meredith_009.jpg","type":"image","content":"Anderson_Meredith_009.jpg","thumb":"Anderson_Meredith_009.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11321":{"id":11321,"size":0,"label":"Anderson_Meredith_010.jpg","type":"image","content":"Anderson_Meredith_010.jpg","thumb":"Anderson_Meredith_010.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11322":{"id":11322,"size":0,"label":"Anderson_Meredith_011.jpg","type":"image","content":"Anderson_Meredith_011.jpg","thumb":"Anderson_Meredith_011.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11323":{"id":11323,"size":0,"label":"Anderson_Meredith_012.jpg","type":"image","content":"Anderson_Meredith_012.jpg","thumb":"Anderson_Meredith_012.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11324":{"id":11324,"size":0,"label":"Anderson_Meredith_013.jpg","type":"image","content":"Anderson_Meredith_013.jpg","thumb":"Anderson_Meredith_013.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11325":{"id":11325,"size":0,"label":"Anderson_Meredith_014.jpg","type":"image","content":"Anderson_Meredith_014.jpg","thumb":"Anderson_Meredith_014.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11326":{"id":11326,"size":0,"label":"Anderson_Meredith_015.jpg","type":"image","content":"Anderson_Meredith_015.jpg","thumb":"Anderson_Meredith_015.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11327":{"id":11327,"size":0,"label":"Anderson_Meredith_016.jpg","type":"image","content":"Anderson_Meredith_016.jpg","thumb":"Anderson_Meredith_016.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11328":{"id":11328,"size":0,"label":"Anderson_Meredith_017.jpg","type":"image","content":"Anderson_Meredith_017.jpg","thumb":"Anderson_Meredith_017.jpg","linkTarget":"","caption":"","featuredImage":"","filters":"Meredith"},"11329":{"id":11329,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A2187","type":"image","content":"063A2187.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11330":{"id":11330,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A2796","type":"image","content":"063A2796.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11331":{"id":11331,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A5773","type":"image","content":"063A5773.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11332":{"id":11332,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A6021","type":"image","content":"063A6021.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11333":{"id":11333,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A6315","type":"image","content":"063A6315.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11334":{"id":11334,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A6763","type":"image","content":"063A6763.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11335":{"id":11335,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A6770","type":"image","content":"063A6770.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11336":{"id":11336,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A7049","type":"image","content":"063A7049.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11337":{"id":11337,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A7094","type":"image","content":"063A7094.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11338":{"id":11338,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A7351","type":"image","content":"063A7351.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11339":{"id":11339,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A7443","type":"image","content":"063A7443.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11340":{"id":11340,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A7837","type":"image","content":"063A7837.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11341":{"id":11341,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A7872","type":"image","content":"063A7872.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11342":{"id":11342,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8009-2","type":"image","content":"063A8009-2.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11343":{"id":11343,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8107","type":"image","content":"063A8107.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11344":{"id":11344,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A2187","type":"image","content":"063A2187-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11345":{"id":11345,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8137","type":"image","content":"063A8137.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11346":{"id":11346,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8205-2","type":"image","content":"063A8205-2.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11347":{"id":11347,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A6770","type":"image","content":"063A6770-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11348":{"id":11348,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8205","type":"image","content":"063A8205.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11349":{"id":11349,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8239","type":"image","content":"063A8239.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11350":{"id":11350,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8305","type":"image","content":"063A8305.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11351":{"id":11351,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8328","type":"image","content":"063A8328.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11352":{"id":11352,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A7872","type":"image","content":"063A7872-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11353":{"id":11353,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8353","type":"image","content":"063A8353.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11354":{"id":11354,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8009-2","type":"image","content":"063A8009-2-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11355":{"id":11355,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8641-Edit","type":"image","content":"063A8641-Edit.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11356":{"id":11356,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8683","type":"image","content":"063A8683.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11357":{"id":11357,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8854","type":"image","content":"063A8854.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11358":{"id":11358,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8885","type":"image","content":"063A8885.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11359":{"id":11359,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9013","type":"image","content":"063A9013.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11360":{"id":11360,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8107","type":"image","content":"063A8107-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11361":{"id":11361,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9267","type":"image","content":"063A9267.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11362":{"id":11362,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8137","type":"image","content":"063A8137-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11363":{"id":11363,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9325","type":"image","content":"063A9325.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11364":{"id":11364,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8205","type":"image","content":"063A8205-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11365":{"id":11365,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8305","type":"image","content":"063A8305-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11366":{"id":11366,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9346","type":"image","content":"063A9346.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11367":{"id":11367,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8641-Edit","type":"image","content":"063A8641-Edit-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11368":{"id":11368,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9391","type":"image","content":"063A9391.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11369":{"id":11369,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9486","type":"image","content":"063A9486.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11370":{"id":11370,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_002","type":"image","content":"Anderson_GovBall14Pola_002.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11371":{"id":11371,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_010","type":"image","content":"Anderson_GovBall14Pola_010.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11372":{"id":11372,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8885","type":"image","content":"063A8885-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11373":{"id":11373,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9267","type":"image","content":"063A9267-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11374":{"id":11374,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_037","type":"image","content":"Anderson_GovBall14Pola_037.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11375":{"id":11375,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9346","type":"image","content":"063A9346-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11376":{"id":11376,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_042","type":"image","content":"Anderson_GovBall14Pola_042.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11377":{"id":11377,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9391","type":"image","content":"063A9391-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11378":{"id":11378,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_095","type":"image","content":"Anderson_GovBall14Pola_095.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11379":{"id":11379,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_098","type":"image","content":"Anderson_GovBall14Pola_098.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11380":{"id":11380,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9486","type":"image","content":"063A9486-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11381":{"id":11381,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_153","type":"image","content":"Anderson_GovBall14Pola_153.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11382":{"id":11382,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_SOJA_2014_Group-1","type":"image","content":"Anderson_SOJA_2014_Group-1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11383":{"id":11383,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_SOJA_2014_Individual-17","type":"image","content":"Anderson_SOJA_2014_Individual-17.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11384":{"id":11384,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_002","type":"image","content":"Anderson_GovBall14Pola_002-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11385":{"id":11385,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_037","type":"image","content":"Anderson_GovBall14Pola_037-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11386":{"id":11386,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Tony2014_PolaroidFinal_FOSTER","type":"image","content":"Anderson_Tony2014_PolaroidFinal_FOSTER.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11387":{"id":11387,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Tony2014_PolaroidFinal_HARRIS01","type":"image","content":"Anderson_Tony2014_PolaroidFinal_HARRIS01.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11388":{"id":11388,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_042","type":"image","content":"Anderson_GovBall14Pola_042-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11389":{"id":11389,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Tony2014_PolaroidFinal_INGLEHART","type":"image","content":"Anderson_Tony2014_PolaroidFinal_INGLEHART.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11390":{"id":11390,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_098","type":"image","content":"Anderson_GovBall14Pola_098-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11391":{"id":11391,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_GovBall14Pola_153","type":"image","content":"Anderson_GovBall14Pola_153-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11392":{"id":11392,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_SOJA_2014_Individual-17","type":"image","content":"Anderson_SOJA_2014_Individual-17-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11393":{"id":11393,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Tony2014_PolaroidFinal_MAYS","type":"image","content":"Anderson_Tony2014_PolaroidFinal_MAYS.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11394":{"id":11394,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Tony2014_PolaroidFinal_MUELLER","type":"image","content":"Anderson_Tony2014_PolaroidFinal_MUELLER.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11395":{"id":11395,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Tony2014_PolaroidFinal_HARRIS01","type":"image","content":"Anderson_Tony2014_PolaroidFinal_HARRIS01-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11396":{"id":11396,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Tony2014_PolaroidFinal_INGLEHART","type":"image","content":"Anderson_Tony2014_PolaroidFinal_INGLEHART-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11397":{"id":11397,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"C_ODOWD_017","type":"image","content":"C_ODOWD_017.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11398":{"id":11398,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALLPOLA-123","type":"image","content":"ERA_GOVBALLPOLA-123.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11399":{"id":11399,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Tony2014_PolaroidFinal_ODOWD","type":"image","content":"Anderson_Tony2014_PolaroidFinal_ODOWD.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11400":{"id":11400,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALLPOLA-159","type":"image","content":"ERA_GOVBALLPOLA-159.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11401":{"id":11401,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Tony2014_PolaroidFinal_SHALHOUB","type":"image","content":"Anderson_Tony2014_PolaroidFinal_SHALHOUB.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11402":{"id":11402,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"C_ODOWD_006","type":"image","content":"C_ODOWD_006.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11403":{"id":11403,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"C_ODOWD_017","type":"image","content":"C_ODOWD_017-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11404":{"id":11404,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"GROUP2_009","type":"image","content":"GROUP2_009.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11405":{"id":11405,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"N_PHARRIS_011","type":"image","content":"N_PHARRIS_011.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11406":{"id":11406,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALLPOLA-108","type":"image","content":"ERA_GOVBALLPOLA-108.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11407":{"id":11407,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"T_DALY_008","type":"image","content":"T_DALY_008.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11408":{"id":11408,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALLPOLA-123","type":"image","content":"ERA_GOVBALLPOLA-123-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11409":{"id":11409,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALLPOLA-143","type":"image","content":"ERA_GOVBALLPOLA-143.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11410":{"id":11410,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALLPOLA-159","type":"image","content":"ERA_GOVBALLPOLA-159-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11411":{"id":11411,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALLPOLA-175","type":"image","content":"ERA_GOVBALLPOLA-175.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11412":{"id":11412,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"GROUP2_009","type":"image","content":"GROUP2_009-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11413":{"id":11413,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"J_IGLEHART_008","type":"image","content":"J_IGLEHART_008.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11414":{"id":11414,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"J_MAYS_017","type":"image","content":"J_MAYS_017.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11415":{"id":11415,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"J_MAYS_023","type":"image","content":"J_MAYS_023.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11416":{"id":11416,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Jukebox009A","type":"image","content":"Jukebox009A.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11417":{"id":11417,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Jukebox010D","type":"image","content":"Jukebox010D.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11418":{"id":11418,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"N_PHARRIS_011","type":"image","content":"N_PHARRIS_011-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11419":{"id":11419,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"S_FOSTER_020","type":"image","content":"S_FOSTER_020.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11420":{"id":11420,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"T_DALY_008","type":"image","content":"T_DALY_008-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11421":{"id":11421,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"T_SHALHOUB_023","type":"image","content":"T_SHALHOUB_023.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11422":{"id":11422,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"C_ODOWD_017B","type":"image","content":"C_ODOWD_017B.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11423":{"id":11423,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A6770","type":"image","content":"063A6770-copy2.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11424":{"id":11424,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A6770","type":"image","content":"063A6770-copy3.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11425":{"id":11425,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"BLONG_POLA_R02","type":"image","content":"BLONG_POLA_R02.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11426":{"id":11426,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"BOBBY","type":"image","content":"BOBBY.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11427":{"id":11427,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST01","type":"image","content":"TEST01.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11428":{"id":11428,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST02","type":"image","content":"TEST02.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11429":{"id":11429,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST03","type":"image","content":"TEST03.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11430":{"id":11430,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST04","type":"image","content":"TEST04.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11431":{"id":11431,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST05","type":"image","content":"TEST05.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11432":{"id":11432,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST06","type":"image","content":"TEST06.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11433":{"id":11433,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST07","type":"image","content":"TEST07.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11434":{"id":11434,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST02","type":"image","content":"TEST02-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11435":{"id":11435,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST03","type":"image","content":"TEST03-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11436":{"id":11436,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST05","type":"image","content":"TEST05-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11437":{"id":11437,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST08","type":"image","content":"TEST08.jpg","thumb":"","linkTarget":"","caption":"sara forrest, sarah forrest, sara forrest photography, nike, new york city, nyc, phantogram, hipster, running, fitness photographers new york, fitness photographers, lifestyle photography, documentary photography, fashion photographers, manhattan, rebel run, ","featuredImage":"","filters":""},"11438":{"id":11438,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST09","type":"image","content":"TEST09.jpg","thumb":"","linkTarget":"","caption":"sara forrest, sarah forrest, sara forrest photography, nike, new york city, nyc, phantogram, hipster, running, fitness photographers new york, fitness photographers, lifestyle photography, documentary photography, fashion photographers, manhattan, rebel run, ","featuredImage":"","filters":""},"11439":{"id":11439,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST10","type":"image","content":"TEST10.jpg","thumb":"","linkTarget":"","caption":"sara forrest, sarah forrest, sara forrest photography, nike, new york city, nyc, phantogram, hipster, running, fitness photographers new york, fitness photographers, lifestyle photography, documentary photography, fashion photographers, manhattan, rebel run, ","featuredImage":"","filters":""},"11440":{"id":11440,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST11","type":"image","content":"TEST11.jpg","thumb":"","linkTarget":"","caption":"sara forrest, sarah forrest, sara forrest photography, nike, new york city, nyc, phantogram, hipster, running, fitness photographers new york, fitness photographers, lifestyle photography, documentary photography, fashion photographers, manhattan, rebel run, ","featuredImage":"","filters":""},"11441":{"id":11441,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST12","type":"image","content":"TEST12.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11442":{"id":11442,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST12","type":"image","content":"TEST12-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11443":{"id":11443,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST13","type":"image","content":"TEST13.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11444":{"id":11444,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST13","type":"image","content":"TEST13-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11445":{"id":11445,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST13","type":"image","content":"TEST13-copy2.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11446":{"id":11446,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"TEST14","type":"image","content":"TEST14.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11447":{"id":11447,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALL-8000","type":"image","content":"ERA_GOVBALL-8000.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11448":{"id":11448,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALL-8104","type":"image","content":"ERA_GOVBALL-8104.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11449":{"id":11449,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALL-8132","type":"image","content":"ERA_GOVBALL-8132.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11450":{"id":11450,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALL-8147","type":"image","content":"ERA_GOVBALL-8147.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11451":{"id":11451,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALL-8219","type":"image","content":"ERA_GOVBALL-8219.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11452":{"id":11452,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALL-8705","type":"image","content":"ERA_GOVBALL-8705.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11453":{"id":11453,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALL-8884","type":"image","content":"ERA_GOVBALL-8884.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11454":{"id":11454,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALL-9095","type":"image","content":"ERA_GOVBALL-9095.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11455":{"id":11455,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOVBALL-9221","type":"image","content":"ERA_GOVBALL-9221.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11456":{"id":11456,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_001","type":"image","content":"Anderson_Oliberte_Fall2014_001.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11457":{"id":11457,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_002","type":"image","content":"Anderson_Oliberte_Fall2014_002.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11458":{"id":11458,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_003","type":"image","content":"Anderson_Oliberte_Fall2014_003.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11459":{"id":11459,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_004","type":"image","content":"Anderson_Oliberte_Fall2014_004.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11460":{"id":11460,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_005","type":"image","content":"Anderson_Oliberte_Fall2014_005.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11461":{"id":11461,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_006","type":"image","content":"Anderson_Oliberte_Fall2014_006.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11462":{"id":11462,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_007","type":"image","content":"Anderson_Oliberte_Fall2014_007.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11463":{"id":11463,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_008","type":"image","content":"Anderson_Oliberte_Fall2014_008.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11464":{"id":11464,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_009","type":"image","content":"Anderson_Oliberte_Fall2014_009.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11465":{"id":11465,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_010","type":"image","content":"Anderson_Oliberte_Fall2014_010.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11466":{"id":11466,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_011","type":"image","content":"Anderson_Oliberte_Fall2014_011.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11467":{"id":11467,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_012","type":"image","content":"Anderson_Oliberte_Fall2014_012.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11468":{"id":11468,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_013","type":"image","content":"Anderson_Oliberte_Fall2014_013.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11469":{"id":11469,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_014","type":"image","content":"Anderson_Oliberte_Fall2014_014.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11470":{"id":11470,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_015","type":"image","content":"Anderson_Oliberte_Fall2014_015.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11471":{"id":11471,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_016","type":"image","content":"Anderson_Oliberte_Fall2014_016.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11472":{"id":11472,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_017","type":"image","content":"Anderson_Oliberte_Fall2014_017.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11473":{"id":11473,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Oliberte_Fall2014_018","type":"image","content":"Anderson_Oliberte_Fall2014_018.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11474":{"id":11474,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A1008W","type":"image","content":"063A1008W.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11475":{"id":11475,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A1008W","type":"image","content":"063A1008W-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11476":{"id":11476,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_THR_ShowMe_1_041","type":"image","content":"Anderson_THR_ShowMe_1_041.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11477":{"id":11477,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A0597","type":"image","content":"063A0597.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11478":{"id":11478,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"JLPOLA","type":"image","content":"JLPOLA.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11479":{"id":11479,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOTHAM_PLim-6657","type":"image","content":"ERA_GOTHAM_PLim-6657.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11480":{"id":11480,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOTHAM_PLim-6511","type":"image","content":"ERA_GOTHAM_PLim-6511.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11481":{"id":11481,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_GOTHAM_PLim-6477","type":"image","content":"ERA_GOTHAM_PLim-6477.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11482":{"id":11482,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_BOYDFILM005","type":"image","content":"Anderson_BOYDFILM005.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11483":{"id":11483,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_BOYDFILM002","type":"image","content":"Anderson_BOYDFILM002.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11484":{"id":11484,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A0478","type":"image","content":"063A0478.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11485":{"id":11485,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A0434","type":"image","content":"063A0434.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11486":{"id":11486,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8609","type":"image","content":"063A8609.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11487":{"id":11487,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8507","type":"image","content":"063A8507.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11488":{"id":11488,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9840","type":"image","content":"063A9840.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11489":{"id":11489,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A7993","type":"image","content":"063A7993.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11490":{"id":11490,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9735","type":"image","content":"063A9735.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11491":{"id":11491,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"AndersonJulyTest35MM_018A","type":"image","content":"AndersonJulyTest35MM_018A.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11492":{"id":11492,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9551","type":"image","content":"063A9551.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11493":{"id":11493,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9251","type":"image","content":"063A9251.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11494":{"id":11494,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A9027","type":"image","content":"063A9027.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11495":{"id":11495,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"063A8609","type":"image","content":"063A8609-copy1.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11496":{"id":11496,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA140616_Interpol-0466","type":"image","content":"ERA140616_Interpol-0466.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11497":{"id":11497,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_THR_PHILO_TEST01","type":"image","content":"ERA_THR_PHILO_TEST01.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11498":{"id":11498,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"ERA_THR_PHILO01-051","type":"image","content":"ERA_THR_PHILO01-051.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11499":{"id":11499,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_Jukebox_2014_Rough-155","type":"image","content":"Anderson_Jukebox_2014_Rough-155.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11500":{"id":11500,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"Anderson_SOJA_2014_Individual-5","type":"image","content":"Anderson_SOJA_2014_Individual-5.jpg","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11501":{"id":11501,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_001","type":"image","content":"EricRyanAnderson_Lifestyle_001.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11502":{"id":11502,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_002","type":"image","content":"EricRyanAnderson_Lifestyle_002.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11503":{"id":11503,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_003","type":"image","content":"EricRyanAnderson_Lifestyle_003.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11504":{"id":11504,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_004","type":"image","content":"EricRyanAnderson_Lifestyle_004.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11505":{"id":11505,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_005","type":"image","content":"EricRyanAnderson_Lifestyle_005.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11506":{"id":11506,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_006","type":"image","content":"EricRyanAnderson_Lifestyle_006.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11507":{"id":11507,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_007","type":"image","content":"EricRyanAnderson_Lifestyle_007.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11508":{"id":11508,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_008","type":"image","content":"EricRyanAnderson_Lifestyle_008.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11509":{"id":11509,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_009","type":"image","content":"EricRyanAnderson_Lifestyle_009.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11510":{"id":11510,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_010","type":"image","content":"EricRyanAnderson_Lifestyle_010.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11511":{"id":11511,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_011","type":"image","content":"EricRyanAnderson_Lifestyle_011.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11512":{"id":11512,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_012","type":"image","content":"EricRyanAnderson_Lifestyle_012.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11513":{"id":11513,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_013","type":"image","content":"EricRyanAnderson_Lifestyle_013.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11514":{"id":11514,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_014","type":"image","content":"EricRyanAnderson_Lifestyle_014.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11515":{"id":11515,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_015","type":"image","content":"EricRyanAnderson_Lifestyle_015.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11516":{"id":11516,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_016","type":"image","content":"EricRyanAnderson_Lifestyle_016.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11517":{"id":11517,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_017","type":"image","content":"EricRyanAnderson_Lifestyle_017.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11518":{"id":11518,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_018","type":"image","content":"EricRyanAnderson_Lifestyle_018.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11519":{"id":11519,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_019","type":"image","content":"EricRyanAnderson_Lifestyle_019.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11520":{"id":11520,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_020","type":"image","content":"EricRyanAnderson_Lifestyle_020.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11521":{"id":11521,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_021","type":"image","content":"EricRyanAnderson_Lifestyle_021.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11522":{"id":11522,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_022","type":"image","content":"EricRyanAnderson_Lifestyle_022.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11523":{"id":11523,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_023","type":"image","content":"EricRyanAnderson_Lifestyle_023.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11524":{"id":11524,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_024","type":"image","content":"EricRyanAnderson_Lifestyle_024.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11525":{"id":11525,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_025","type":"image","content":"EricRyanAnderson_Lifestyle_025.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11526":{"id":11526,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_026","type":"image","content":"EricRyanAnderson_Lifestyle_026.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11527":{"id":11527,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_027","type":"image","content":"EricRyanAnderson_Lifestyle_027.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11528":{"id":11528,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_028","type":"image","content":"EricRyanAnderson_Lifestyle_028.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11529":{"id":11529,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_029","type":"image","content":"EricRyanAnderson_Lifestyle_029.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11530":{"id":11530,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_030","type":"image","content":"EricRyanAnderson_Lifestyle_030.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11531":{"id":11531,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_031","type":"image","content":"EricRyanAnderson_Lifestyle_031.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11532":{"id":11532,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_032","type":"image","content":"EricRyanAnderson_Lifestyle_032.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11533":{"id":11533,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_033","type":"image","content":"EricRyanAnderson_Lifestyle_033.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11534":{"id":11534,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_034","type":"image","content":"EricRyanAnderson_Lifestyle_034.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11535":{"id":11535,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_035","type":"image","content":"EricRyanAnderson_Lifestyle_035.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11536":{"id":11536,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_036","type":"image","content":"EricRyanAnderson_Lifestyle_036.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11537":{"id":11537,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_037","type":"image","content":"EricRyanAnderson_Lifestyle_037.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11538":{"id":11538,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_038","type":"image","content":"EricRyanAnderson_Lifestyle_038.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11539":{"id":11539,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_039","type":"image","content":"EricRyanAnderson_Lifestyle_039.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11540":{"id":11540,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_040","type":"image","content":"EricRyanAnderson_Lifestyle_040.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11541":{"id":11541,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_041","type":"image","content":"EricRyanAnderson_Lifestyle_041.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11542":{"id":11542,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_042","type":"image","content":"EricRyanAnderson_Lifestyle_042.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11543":{"id":11543,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_043","type":"image","content":"EricRyanAnderson_Lifestyle_043.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11544":{"id":11544,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_044","type":"image","content":"EricRyanAnderson_Lifestyle_044.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11545":{"id":11545,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_045","type":"image","content":"EricRyanAnderson_Lifestyle_045.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11546":{"id":11546,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_046","type":"image","content":"EricRyanAnderson_Lifestyle_046.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11547":{"id":11547,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_047","type":"image","content":"EricRyanAnderson_Lifestyle_047.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11548":{"id":11548,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_048","type":"image","content":"EricRyanAnderson_Lifestyle_048.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11549":{"id":11549,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_049","type":"image","content":"EricRyanAnderson_Lifestyle_049.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11550":{"id":11550,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_050","type":"image","content":"EricRyanAnderson_Lifestyle_050.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11551":{"id":11551,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_051","type":"image","content":"EricRyanAnderson_Lifestyle_051.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11552":{"id":11552,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_052","type":"image","content":"EricRyanAnderson_Lifestyle_052.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11553":{"id":11553,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_053","type":"image","content":"EricRyanAnderson_Lifestyle_053.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11554":{"id":11554,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_054","type":"image","content":"EricRyanAnderson_Lifestyle_054.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11555":{"id":11555,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_055","type":"image","content":"EricRyanAnderson_Lifestyle_055.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11556":{"id":11556,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_056","type":"image","content":"EricRyanAnderson_Lifestyle_056.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11557":{"id":11557,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_058","type":"image","content":"EricRyanAnderson_Lifestyle_058.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11558":{"id":11558,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_057","type":"image","content":"EricRyanAnderson_Lifestyle_057.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11559":{"id":11559,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_059","type":"image","content":"EricRyanAnderson_Lifestyle_059.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""},"11560":{"id":11560,"size":{"content":{"width":0,"height":0},"thumb":{"width":0,"height":0},"featuredImage":{"width":0,"height":0}},"label":"EricRyanAnderson_Lifestyle_060","type":"image","content":"EricRyanAnderson_Lifestyle_060.JPG","thumb":"","linkTarget":"","caption":"","featuredImage":"","filters":""}};

var NAVBAR_MODEL = {"0":{"id":4,"order":0,"type":"thumbs","name":"thumbs","iconType":"both","tooltip":"show thumbnails"},"1":{"id":3,"order":1,"type":"email","name":"email","iconType":"both","tooltip":"email image"},"2":{"id":5,"order":2,"type":"prev","name":"prev","iconType":"both","tooltip":"previous image"},"3":{"id":6,"order":3,"type":"count","name":"count","iconType":"both","tooltip":"image info"},"4":{"id":7,"order":4,"type":"next","name":"next","iconType":"both","tooltip":"next image"}};

var SECTIONS_MODEL = {"ROOT_SECTION":{"id":1,"visible":1,"password":"","isOpen":1,"label":"ROOT_SECTION","media":"7,15,4,25,8,5,19,22,14,27,38,44,40,43","thumb":{"type":"fill","defaultOn":false,"masonry":false,"gap":20,"dock":"overlay","size":"large","side":"bottom","verticalMargin":50,"horizontalMargin":50}},"PHOTOGRAPHY\/Music":{"id":3,"visible":1,"password":"","isOpen":0,"label":"Music","media":"11561,11392,10238,10207,10548,10219,10599,10627,10523,11398,10816,10236,10101,10539,10229,10566,10811,11309,10538,10558,11305,10220,10636,10223,10532,10590,11426,10808,10594,10597,10607,10810,10221,10234,10519,10520,10591,10614,10621,10624,10630,10637,10522,10817,10009,10010,10646,10521,10595,10809,10045,10525,10529,11306,11307,11308,10530,10531,10814,10534,10645,10535,10536,10537,10540,10542,10543,10546,10547,10550,10555,10559,10560,10561,10568,10569,10812,10571,10589,10592,10633,10593,10596,10598,10601,10602,10603,10815,10606,10635,10609,10611,10615,10612,10613,10631,10616,10617,10618,10619,10620,10807,10622,10626,10628,10629,10632,10634,10638,10639","thumb":{"verticalMargin":30,"dock":"overlay","horizontalMargin":0,"side":"bottom","titleFontFamily":"","type":"fill","gap":20,"titleFontSize":0,"titleFontColor":"","size":"large","masonry":true,"defaultOn":true}},"SPACER_1":{"id":4,"visible":1,"password":"","isOpen":0,"label":"%SPACER%","media":"","thumb":{"type":"fill","defaultOn":false,"masonry":false,"gap":20,"dock":"overlay","size":"large","side":"bottom","verticalMargin":50,"horizontalMargin":50}},"VIDEO":{"id":5,"visible":0,"password":"","isOpen":0,"label":"VIDEO","media":"11,12","thumb":{"masonry":true,"defaultOn":true,"horizontalMargin":50,"verticalMargin":50,"dock":"overlay","type":"fill","size":"large","side":"bottom","titleFontFamily":"","gap":20,"titleFontSize":0,"titleFontColor":""}},"VIDEO\/Music":{"id":11,"visible":1,"password":"","isOpen":0,"label":"Music","media":"10727,10728,10729","thumb":{"defaultOn":true,"horizontalMargin":0,"verticalMargin":30,"masonry":true,"dock":"displace","side":"bottom","type":"fill","titleFontFamily":"","gap":20,"titleFontSize":0,"size":"large","titleFontColor":""}},"SPACER_2":{"id":7,"visible":1,"password":"","isOpen":0,"label":"%SPACER%","media":"","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"SPACER_3":{"id":8,"visible":1,"password":"","isOpen":0,"label":"%SPACER%","media":"","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"PHOTOGRAPHY\/Portrait":{"id":35,"visible":0,"password":"","isOpen":0,"label":"Portrait","media":"10775,10790,10786,10799,11330,11332,10777,10779,11561,10780,10781,10791,10793,10794,10796,10798,10801,10802,10782,10783,10784,10785,10776,10787,10788,10789,10792,10795,10797,10800,10803,10805","thumb":{"defaultOn":true,"horizontalMargin":0,"verticalMargin":30,"masonry":true,"dock":"displace","side":"bottom","type":"fill","titleFontFamily":"","gap":20,"titleFontSize":0,"size":"large","titleFontColor":""}},"PHOTOGRAPHY\/Ernest Alexander":{"id":10,"visible":1,"password":"","isOpen":0,"label":"Ernest Alexander","media":"10243,10233,10244,10012,10210,10015,10028,10029,10030,10031,10033,10086,10095,10096,10097,10098,10099,10142,10143,10144,10145,10014,10146,10147,10148,10149,10150,10152,10153,10154,10155,10156,10157,10158,10159,10160,10161,10162,10208,10209","thumb":{"masonry":true,"defaultOn":true,"horizontalMargin":0,"verticalMargin":50,"dock":"displace","type":"fill","size":"xLarge","side":"bottom","titleFontFamily":"","gap":20,"titleFontSize":0,"titleFontColor":""}},"VIDEO\/Commercial":{"id":12,"visible":1,"password":"","isOpen":0,"label":"Commercial","media":"","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"OLDOVERVIEW":{"id":14,"visible":0,"password":"","isOpen":0,"label":"OLDOVERVIEW","media":"11405,11398,11301,11393,11283,10599,11403,10083,11294,10238,11392,11401,11299,10233,10373,11292,10918,11285,10723,11288,11296,11361,11300,10232,10695,11286,11282,10136,10244,10372,10675,10691,10229,10690,10009,10022,10134,10428,10023,10039,11352,11360,11364,11370,11372,11375,11379,11382,11395,11407,10415,10044,10045,11298,10110,10113,10129,10218,10672,10219,10387,10220,10223,10236,10315,10319,10705,10339,10349,11289,11290,11293,10353,11297,10355,10356,10364,11284,10370,10404,10416,10435,10534,10539,11291,10558,10560,11295,10590,10597,10606,10607,10621,10647,10228,10664,10668,10679,10694,10698,10700,10702,10703,10094,10206,10656,10701,11336,11337,11338,11339,11340,11341,11342,11344,11345,11346,11348,11349,11350,11351,11353,11354,11355,11356,11357,11358,11359,11363,11365,11366,11367,11368,11369,11371,11374,11376,11377,11378,11380,11381,11383,11384,11386,11387,11388,11389,11390,11391,11394,11396,11397,11399,11400,11402,11404,11409,11410,11411,11414,11417,11419,11420","thumb":{"defaultOn":true,"horizontalMargin":0,"verticalMargin":30,"masonry":true,"dock":"displace","side":"bottom","type":"fill","titleFontFamily":"","gap":20,"titleFontSize":0,"size":"xLarge","titleFontColor":""}},"PHOTOGRAPHY":{"id":15,"visible":1,"password":"","isOpen":0,"label":"PHOTOGRAPHY","media":"42,30,46,3,35,16,39,45,37,10,34,32,33,41,36,31,29,17","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"PHOTOGRAPHY\/Lone Star":{"id":16,"visible":1,"password":"","isOpen":0,"label":"Lone Star","media":"10818,10819,10820,10821,10822,10823,10824,10825,10826,10827,10828,10859,10841,10849,10829,10830,10831,10855,10832,10833,10834,10835,10836,10837,10838,10839,10840,10842,10843,10844,10845,10846,10847,10848,10850,10851,10852,10853,10854,10856,10857,10858,10860,10861,10862,10863,10864,10865,10866,10867,10868,10869,10870,10871,10872,10873,10874,10875,10876,10877,10878,10879,10880,10881,10882,10883,10884,10885,10886,10887","thumb":{"defaultOn":true,"horizontalMargin":0,"verticalMargin":30,"masonry":true,"dock":"displace","side":"bottom","type":"fill","titleFontFamily":"","gap":20,"titleFontSize":0,"size":"xLarge","titleFontColor":""}},"PHOTOGRAPHY\/Polaroids":{"id":17,"visible":1,"password":"","isOpen":0,"label":"Polaroids","media":"10253,10302,10260,10313,10315,10316,10889,10317,10247,10248,10888,10890,10249,10250,10251,10252,10254,10255,10256,10257,10258,10259,10261,10262,10263,10264,10314,10265,10266,10267,10268,10269,10270,10271,10272,10273,10274,10275,10276,10277,10278,10279,10246,10280,10281,10282,10283,10284,10285,10286,10287,10288,10289,10290,10291,10292,10293,10294,10295,10296,10297,10298,10299,10300,10301,10303,10304,10305,10306,10307,10308,10309,10310,10311,10312","thumb":{"masonry":true,"defaultOn":true,"horizontalMargin":0,"verticalMargin":50,"dock":"displace","type":"fill","size":"large","side":"bottom","titleFontFamily":"","gap":20,"titleFontSize":0,"titleFontColor":""}},"INFO":{"id":19,"visible":1,"password":"","isOpen":0,"label":"INFO","media":"20,21,23,24","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"INFO\/Bio":{"id":20,"visible":1,"password":"","isOpen":0,"label":"Bio","media":"10908","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"INFO\/Contact":{"id":21,"visible":1,"password":"","isOpen":0,"label":"Contact","media":"10913","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"SPACER_4":{"id":22,"visible":1,"password":"","isOpen":0,"label":"%SPACER%","media":"","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"INFO\/Client Galleries":{"id":23,"visible":0,"password":"","isOpen":0,"label":"Client Galleries","media":"","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"INFO\/Archive":{"id":24,"visible":0,"password":"","isOpen":0,"label":"Archive","media":"","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"MOTION":{"id":25,"visible":1,"password":"","isOpen":0,"label":"MOTION","media":"11287","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"PHOTOGRAPHY\/Olibert\u00ea":{"id":29,"visible":0,"password":"","isOpen":0,"label":"Olibert\u00ea","media":"10481,10489,10490,10483,10484,10485,10486,10487,10491,10488,10482,10492,10493,10494,10495,10496,10497,10498,10499,10500,10501,10502,10503,10504,10505,10506,10507,10508,10509,10510,10511,10512,10513,10514,10515,10516,10517","thumb":{"masonry":false,"defaultOn":false,"horizontalMargin":0,"verticalMargin":30,"dock":"displace","type":"none","size":"large","side":"bottom","titleFontFamily":"","gap":20,"titleFontSize":0,"titleFontColor":""}},"SPACER_5":{"id":27,"visible":1,"password":"","isOpen":0,"label":"%SPACER%","media":"","thumb":{"horizontalMargin":0,"defaultOn":false,"masonry":false,"dock":"displace","verticalMargin":0,"type":"fill","side":"bottom","gap":0,"size":"medium"}},"PHOTOGRAPHY\/Kate Spade":{"id":34,"visible":0,"password":"","isOpen":0,"label":"Kate Spade","media":"10915,10768,10914,10916,10917,10762,10763,10764,10765,10766,10767,10769,10770,10771,10772,10773,10774,10754,10755,10756,10757,10758,10759,10760,10761,10730,10731,10732,10733,10734,10735,10736,10737,10738,10739,10740,10741,10742,10743,10744,10745,10746,10747,10748,10749,10750,10751,10752","thumb":{"verticalMargin":30,"dock":"displace","horizontalMargin":0,"side":"bottom","titleFontFamily":"","type":"fill","gap":20,"titleFontSize":0,"titleFontColor":"","size":"large","masonry":true,"defaultOn":true}},"PHOTOGRAPHY\/Overview":{"id":30,"visible":1,"password":"","isOpen":0,"label":"Overview","media":"11301,10233,11475,11479,11488,11392,10372,11405,11492,10083,11386,11283,11294,11498,10373,11403,11414,11395,11424,10599,10238,11299,11496,11494,11477,11480,11292,10918,11393,11285,10723,11288,11296,11300,10232,10695,11401,11286,11345,11282,10136,10244,10675,10691,10229,11409,10690,10009,10022,10134,10428,10023,10039,10370,11402,11361,11352,11364,11370,11372,11375,11379,11407,10415,10044,10045,11298,10110,10113,10129,10218,10672,10219,11382,10387,10220,10223,10236,10315,10319,10705,10339,10349,11289,11290,11293,10353,11297,10355,10356,10364,11284,10404,10416,10435,10534,10539,11291,10558,10560,11295,10590,10597,10606,10607,10621,10647,10228,10664,10668,10679,10694,10698,10700,10702,10703,10094,10206,10656,10701,11341,11344,11366,11376,11380,11384,11389,11391,11417","thumb":{"horizontalMargin":0,"verticalMargin":30,"dock":"displace","side":"bottom","type":"fill","size":"xLarge","titleFontFamily":"","gap":20,"titleFontSize":0,"titleFontColor":"","defaultOn":true,"masonry":true}},"PHOTOGRAPHY\/On the Bowery":{"id":31,"visible":1,"password":"","isOpen":0,"label":"On the Bowery","media":"10579,10586,10572,10585,10574,10573,10583,10576,10575,10577,10582,10578,10580,10584,10587","thumb":{"verticalMargin":30,"dock":"displace","horizontalMargin":0,"side":"bottom","titleFontFamily":"","type":"fill","gap":20,"titleFontSize":0,"titleFontColor":"","size":"large","masonry":true,"defaultOn":true}},"PHOTOGRAPHY\/Park & Hall":{"id":33,"visible":1,"password":"","isOpen":0,"label":"Park & Hall","media":"10387,10367,10363,10364,10365,10366,10368,10414,10369,10370,10371,10372,10373,10374,10375,10376,10377,10378,10362,10379,10380,10381,10382,10383,10384,10385,10386,10388,10389,10391,10392,10393,10394,10395,10396,10397,10398,10399,10400,10401,10402,10403,10404,10405,10406,10407,10408,10409,10410,10411,10413","thumb":{"horizontalMargin":0,"verticalMargin":30,"dock":"displace","side":"bottom","type":"fill","size":"xLarge","titleFontFamily":"","gap":20,"titleFontSize":0,"titleFontColor":"","defaultOn":true,"masonry":true}},"PHOTOGRAPHY\/Road to Montauk":{"id":32,"visible":1,"password":"","isOpen":0,"label":"Road to Montauk","media":"10319,10320,10321,10322,10323,10324,10325,10326,10327,10328,10329,10330,10331,10332,10333,10334,10335,10336,10337,10338,10339,10340,10341,10342,10343,10344,10345,10346,10347,10348,10349,10350,10351,10352,10353,10354,10355,10356,10357,10358,10359,10360,10361","thumb":{"horizontalMargin":0,"verticalMargin":30,"dock":"displace","side":"bottom","type":"fill","size":"large","titleFontFamily":"","gap":20,"titleFontSize":0,"titleFontColor":"","defaultOn":true,"masonry":true}},"PHOTOGRAPHY\/Passavant and Lee":{"id":36,"visible":0,"password":"","isOpen":0,"label":"Passavant and Lee","media":"10891,10892,10893,10894,10895,10896,10897,10898,10899,10900,10901,10902,10903,10904,10905,10906","thumb":{"verticalMargin":30,"dock":"displace","horizontalMargin":0,"side":"bottom","titleFontFamily":"","type":"fill","gap":20,"titleFontSize":0,"titleFontColor":"","size":"large","masonry":true,"defaultOn":true}},"PHOTOGRAPHY\/Lindsey Stirling":{"id":37,"visible":0,"password":"47hall","isOpen":0,"label":"Lindsey Stirling","media":"10918,10919,10920,10921,10922,10926,10924,10923,10925,10927,10928,10929,10930,10931,10932,10933,10934,10935,10936,10937,10938,10939,10940,10941,10942,10943,10944,10945,10946,10947,10948,10949,10950,10951,10952,10953,10954,10955,10956,10957,10958,10959","thumb":{"masonry":true,"defaultOn":true,"horizontalMargin":0,"gap":20,"verticalMargin":30,"dock":"displace","size":"large","type":"fill","titleFontFamily":"","titleFontSize":0,"titleFontColor":"","side":"bottom"}},"Addis Preview":{"id":38,"visible":0,"password":"","isOpen":0,"label":"Addis Preview","media":"10971,10963,10961,10965,10962,10964,10966,10967,10968,10969,10970,10972,10973,10960,10974,10975,10976,10977,10978,10979,10980,10981,10982,10983,10984,10985,10986,10987,10988,10989,10990,10991,10992,10993,10994,10995,10996,10997,10998,10999,11000,11001,11002,11003,11004,11005,11006,11007,11008,11009,11010,11011,11012,11013,11014,11015,11016,11017,11018,11019,11020,11021,11022,11023,11024,11025,11026,11027,11028,11029,11030,11031,11032,11033,11034,11035,11036,11037,11038,11039,11040,11041,11042,11043,11044,11045,11046,11047,11048,11049,11050,11051,11052,11053,11054,11055,11056,11057,11058,11059,11060,11061,11062,11063,11064,11065,11066,11067,11068,11069,11070,11071,11072,11073,11074,11075,11076,11077,11078,11079,11080,11081,11082,11083,11084,11085,11086,11087,11088,11089,11090,11091,11092,11093,11094,11095,11096,11097,11098,11099,11100,11101,11102,11103,11104,11105,11106,11107","thumb":{"defaultOn":true,"horizontalMargin":0,"verticalMargin":30,"masonry":true,"dock":"displace","side":"bottom","type":"fill","titleFontFamily":"","gap":20,"titleFontSize":0,"size":"large","titleFontColor":""}},"PHOTOGRAPHY\/Clear Eyes Full Hearts":{"id":39,"visible":1,"password":"","isOpen":0,"label":"Clear Eyes Full Hearts","media":"11108,11109,11110,11111,11112,11206,11113,11200,11114,11182,11190,11208,11221,11223,11225,11207,11115,11203,11205,11210,11215,11218,11116,11166,11117,11118,11119,11120,11121,11122,11123,11124,11125,11193,11126,11127,11128,11129,11130,11131,11132,11133,11134,11135,11136,11137,11138,11139,11140,11141,11211,11142,11144,11145,11146,11147,11148,11149,11151,11152,11153,11154,11155,11156,11157,11158,11159,11160,11161,11162,11163,11164,11165,11167,11168,11169,11170,11171,11172,11173,11174,11175,11176,11177,11178,11179,11180,11181,11183,11185,11186,11189,11191,11192,11194,11195,11196,11197,11198,11199,11201,11202,11204,11209,11212,11214,11217,11219,11220,11222,11224","thumb":{"defaultOn":true,"horizontalMargin":0,"verticalMargin":30,"masonry":true,"dock":"displace","side":"bottom","type":"fill","titleFontFamily":"","gap":20,"titleFontSize":0,"size":"large","titleFontColor":""}},"VIACOM_WHISTLES":{"id":40,"visible":0,"password":"","isOpen":0,"label":"VIACOM_WHISTLES","media":"11242,11278,11236,11250,11276,11281,11227,11248,11228,11238,11266,11229,11230,11231,11232,11233,11234,11235,11237,11239,11240,11241,11226,11243,11244,11245,11246,11247,11249,11251,11252,11253,11254,11255,11256,11257,11258,11259,11260,11261,11262,11263,11264,11265,11267,11268,11269,11270,11271,11272,11273,11274,11275,11277,11279,11280","thumb":{"masonry":true,"defaultOn":true,"horizontalMargin":0,"verticalMargin":30,"dock":"displace","type":"fill","side":"bottom","size":"large","titleFontFamily":"","gap":20,"titleFontSize":0,"titleFontColor":""}},"PHOTOGRAPHY\/Meredith":{"id":41,"visible":0,"password":"","isOpen":0,"label":"Meredith","media":"11323,11315,11317,11314,11325,11312,11313,11316,11318,11319,11320,11321,11322,11324,11326,11327","thumb":{"horizontalMargin":0,"type":"fill","verticalMargin":0,"masonry":true,"dock":"displace","side":"bottom","titleFontFamily":"","gap":20,"titleFontSize":0,"size":"large","defaultOn":true,"titleFontColor":""}},"PHOTOGRAPHY\/Recent Work":{"id":42,"visible":0,"password":"","isOpen":0,"label":"Recent Work","media":"11475,11500,11477,11492,11498,11496,11479,11480,11499,11485,11476,11494,11482,11497,11484,11488,11478,11490,11330,11335,11332,11486,11333,11334,11483,11329,11331,11487,11489","thumb":{"defaultOn":true,"masonry":true,"gap":26,"dock":"overlay","type":"fill","size":"xLarge","side":"bottom","verticalMargin":0,"horizontalMargin":0}},"Chin Up, Cheer Up":{"id":43,"visible":0,"password":"","isOpen":0,"label":"Chin Up, Cheer Up","media":"11427,11438,11446,11440,11442,11445,11434,11439,11435,11430,11436,11437,11433","thumb":{"defaultOn":true,"masonry":false,"gap":10,"dock":"overlay","type":"fill","size":"large","side":"bottom","verticalMargin":0,"horizontalMargin":0}},"Governors Ball":{"id":44,"visible":0,"password":"","isOpen":0,"label":"Governors Ball","media":"11449,11447,11448,11450,11451,11454,11452,11455","thumb":{"defaultOn":true,"masonry":false,"gap":10,"dock":"overlay","type":"fill","size":"large","side":"bottom","verticalMargin":0,"horizontalMargin":0}},"PHOTOGRAPHY\/Olibert\u00e9 2014":{"id":45,"visible":1,"password":"","isOpen":0,"label":"Olibert\u00e9 2014","media":"11456,11461,11458,11459,11460,11464,11462,11463,11465,11466,11467,11468,11469,11470,11471,11472","thumb":{"defaultOn":true,"masonry":false,"gap":10,"dock":"overlay","type":"fill","size":"large","side":"bottom","verticalMargin":0,"horizontalMargin":0}},"PHOTOGRAPHY\/Lifestyle":{"id":46,"visible":1,"password":"","isOpen":0,"label":"Lifestyle","media":"11501,11502,11503,10370,11504,11505,11506,10022,10415,11507,10233,10321,10356,11508,11509,11510,11511,11512,11513,11514,11515,11516,11517,11518,11519,11520,11521,11522,10349,11523,11524,11525,11526,11527,10378,11335,10574,11528,11529,11530,11531,11532,11533,11534,11535,11536,10411,11537,10023,11538,10607,11539,11540,11541,11542,11543,10350,11544,10325,10481,11545,11546,11547,10580,11548,11549,11550,11551,11552,11553,11554,11555,11556,11557,11558,11559,11560,10365,10220,10372,10336,10339","thumb":{"defaultOn":true,"masonry":true,"gap":17,"dock":"overlay","type":"fill","size":"xLarge","side":"bottom","verticalMargin":0,"horizontalMargin":0}}};

var SETTINGS_MODEL = {"dbUser":"ericryanander","url":"ericryananderson.com","browserTitle":"Eric Ryan Anderson","contactFormEmail":"studio@ericryananderson.com","contactFormFields":"Name,Email,Phone,Comments","contactFormExtra":"","copyright":"&#169; 2013 ERIC RYAN ANDERSON","metaDescription":"Eric Ryan Anderson is a commercial and editorial photographer and video director based in New York City.  Born and raised in Texas, he photographs portrait, editorial, advertising and corporate clients from his studio in Brooklyn, New York. Headshots. Music.  Documentary.  I've Got Friends.  Ericryananderson.","metaKeywords":"photography, photographer, new york city, nyc, brooklyn, commercial, editorial, portraiture, portrait, music, musician, music video, director, studio, brooklyn","rollbackVersion":9.8,"localMasterVersion":9.8,"updatePath":"everest.aphotofolio.com\/update\/","backgroundImages":"11501,11422,10370,11283,10218,10365,10558,11423","filters":"BOOK ONE,Ernest Alexander,Music I,Music II,Overview,Meredith,Lifestyle","portfolioEmailMessage":"","socialLinks":"10227,10912","useHTML":true,"globalBrowserTitle":true,"facebookImage":"","facebookAdmins":"","facebookUseMeta":true,"podUseDesktop":false,"padUseDesktop":false,"generalEmail":"","inquiryTitle":"","inquiryInfo":"","lang":"en"};

var HTTP_USER_AGENT = navigator.userAgent,
	USER_AGENT = 'html',
	PAD = 'pad',
	POD = 'pod',
	INTRO = 'intro',
	HOME = 'home',
	LANDING_MEDIA = 'landingMedia',
	IMAGE = 'image',
	VIDEO = 'video',
	HTML = 'html',
	SWF = 'swf',
	LINK = 'link',
	PDF = 'pdf',
	CONTACT = 'contactForm',
	NORMAL = 'normal',
	ERROR = 'error',
	LOAD = 'load',
	RESIZE = 'resize',
	RESIZE_END = 'resizeEnd',
	PANEL_RESIZE = 'panelResize',
	PANEL_RESIZE_END = 'panelResizeEnd',
	FOCUS = 'focus',
	BLUR = 'blur',
	TOUCH_START = 'touchstart',
	TOUCH_MOVE = 'touchmove',
	TOUCH_END = 'touchend',
	TOUCH_CANCEL = 'touchcancel',
	MOUSE_OVER = 'mouseover',
	MOUSE_DOWN = 'mousedown',
	MOUSE_MOVE = 'mousemove',
	MOUSE_UP = 'mouseup',
	MOUSE_OUT = 'mouseout',
	MOUSE_WHEEL = 'mousewheel',
	WHEEL = 'wheel',
	KEY_DOWN = 'keydown',
	KEY_UP = 'keyup',
	LEFT_ARROW = 'leftArrow',
	RIGHT_ARROW = 'rightArrow',
	ESCAPE = 'escape',
	SPACE_BAR = 'spaceBar',
	ENTER = 'enter',
	DELETE = 'delete',
	CLICK = 'click',
	DOUBLE_CLICK = 'dblclick',
	SHIFT = 'shift',
	COMMAND = 'command',
	CONTROL = 'control',
	CHANGE = 'change',
	SELECT_ACCORDION_ITEM = 'selectAccordionItem',
	ACCORDION_SELECT = 'accordionSelect',
	ACCORDION_SIZE_CHANGE = 'accordionSizeChange',
	ADDRESS_CHANGE = 'addressChange',
	ADDRESS_URI_CHANGE = 'addressUriChange',
	SITE_URI_CHANGE = 'siteUriChange',
	NAVIGATION_EVENT = 'navigationEvent',
	ADMIN_LOADED = 'adminLoaded',
	MODEL_CHANGE = 'modelChange',
	ADMIN_MODEL_CHANGE = 'adminModelChange',
	ACCOUNT_MODEL_CHANGE = 'accountModelChange',
	FONTS_MODEL_CHANGE = 'fontsModelChange',
	FILES_MODEL_CHANGE = 'filesModelChange',
	LAYOUT_MODEL_CHANGE = 'layoutModelChange',
	MEDIA_MODEL_CHANGE = 'mediaModelChange',
	NAVBAR_MODEL_CHANGE = 'navbarModelChange',
	PDF_MODEL_CHANGE = 'pdfModelChange',
	SETTINGS_MODEL_CHANGE = 'settingsModelChange',
	SECTIONS_MODEL_CHANGE = 'sectionsModelChange',
	MENU_CLICK = 'menuClick',
	NAVBAR_CLICK = 'navbarClick',
	NAVBAR_TOOLBAR_CLICK = 'navbarToolbarClick',
	NAVBAR_FULLSCREEN = 'navbarFullscreen',
	THUMB_CLICK = 'thumbClick',
	TOGGLE_PLAY_MEDIA = 'togglePlayMedia',
	PAUSED = 'paused',
	PLAY = 'play',
	PAUSE_MEDIA = 'pauseMedia',
	DESTROY_MEDIA = 'destroyMedia',
	FOOTER_CLICK = 'footerClick',
	LOGO_CLICK = 'logoClick',
	LOGO_LOADED = 'logoLoaded',
	SCROLL = 'scroll',
	CURSOR_MEDIA_UPDATE = 'cursorMediaUpdate',
	ADD_CURSOR_TARGETS = 'addCursorTargets',
	REMOVE_CURSOR_TARGETS = 'removeCursorTargets',
	LANDING_INIT = 'landingInit',
	OVERLAY_CHANGE = 'overlayChange',
	POP_UP = 'popUp',
	CURSOR_UPDATE = "overlayUpdate",
	CURSOR_ADD_ADMIN = 'cursorAddAdmin',
	FULLSCREEN_TOGGLE = 'fullscreenToggle',
	FULLSCREEN_TOGGLE_ELEMENT = 'fullscreenToggleElement',
	ORIENTATION = false,
	ORIENTATION_CHANGE = 'orientationchange',
	GESTURE_START = 'gesturestart',
	GESTURE_CHANGE = 'gesturechange',
	GESTURE_END = 'gestureend',
	RETINA = (window.retina || window.devicePixelRatio > 1),
	TOUCH_DEVICE = !!("ontouchstart" in window),
	MEDIA_ORIGINAL = 'media/original/',
	MEDIA_THUMB = 'media/thumb/',
	ICONS = 'dx/icon/',
	PHP = 'dx/php/',
	SERVICE_PATH = './',
	TRANSITION_START = 'transitionStart',
	TRANSITION_END = 'transitionEnd',
	METADATA_LOADED = 'loadedmetadata',
	VIMEO_METADATA_LOADED = 'vimeoMetadataLoaded',
	MEDIA_LOADED = 'mediaLoaded',
	UPDATE_SPEED = 0.5,
	CHILD_ADDED = 'childAdded',
	DROP = 'drop',
	DRAG_ENTER = 'dragenter',
	DRAG_LEAVE = 'dragleave',
	DRAG_OVER = 'dragover',
	DRAG_START = 'dragStart',
	DRAG_MOVE = 'dragMove',
	DRAG_STOP = 'dragStop',
	DRAG_CANCEL = 'dragCancel',
	REORDER_START = 'reorderStart',
	REORDER_MOVE = 'reorderMove',
	REORDER_STOP = 'reorderStop',
	ELEMENT_EVENTS = [TOUCH_START, TOUCH_MOVE, TOUCH_END, TOUCH_CANCEL, KEY_DOWN, KEY_UP, MOUSE_OVER, MOUSE_DOWN, MOUSE_MOVE, MOUSE_UP, MOUSE_OUT, CLICK, DOUBLE_CLICK, LOAD, ERROR, FOCUS, BLUR, DRAG_ENTER, DRAG_OVER, DRAG_LEAVE, DROP],
	THUMB_SIZES = {small: {width: 100, height: 66}, medium: {width: 200, height: 133}, large: {width: 300, height: 200}, xLarge: {width: 400, height: 266}},
	AUTHENTICATED = "authenticated",
	CONTACT_FORM_SUBMIT = 'contactFormSubmit',
	SITE_SERVICE_CONTACT_FORM_SUBMIT = 'siteServiceContactFormSubmit',
	CONTACT_FORM_SENT = 'contactFormSent',
	LAYOUT_FILE_CHANGE = 'layoutFileChange',
	SETTINGS_FILE_CHANGE = 'settingsFileChange',
	PDF_FILE_CHANGE = 'pdfFileChange',
	SHARE_FORM_SUBMIT = 'shareFormSubmit',
	SITE_SERVICE_SHARE_FORM_SUBMIT = 'siteServiceShareFormSubmit',
	SHARE_FORM_SENT = 'shareFormSent',
	INQUIRY_FORM_SUBMIT = 'inquiryFormSubmit',
	SITE_SERVICE_INQUIRY_FORM_SUBMIT = 'siteServiceInquiryFormSubmit',
	INQUIRY_FORM_SENT = 'inquiryFormSent',
	TOUCH_NAVIGATION_START = 'touchNavigationStart',
	TOUCH_NAVIGATION_MOVE = 'touchNavigationMove',
	TOUCH_NAVIGATION_END = 'touchNavigationEnd',
	MENU_DOCK_OPEN = 'menuDockOpen',
	MENU_DOCK_CLOSE = 'menuDockClose',
	INTRO_COMPLETE = 'introComplete',
	MENU_BUILT = 'menuBuilt',
	MENU_SIZE_CHANGE = 'menuSizeChange',
	ADDRESS_SECTION_CHANGE = 'addressSectionChange',
	ADDRESS_OVERLAY_CHANGE = 'addressOverlayChange',
	ADDRESS_ASSET_CHANGE = 'addressAssetChange',
	NAVBAR_NAV_CLICK = 'navbarNavClick',
	NAVBAR_OVERLAY_BTN_CLICK = 'navbaOverlayBtnClick',
	ADMIN_MOUSE_DOWN = 'adminMouseDown',
	ADMIN_MOUSE_UP = 'adminMouseUp',
	SITE_SERVICE_GET_FILE = 'siteServiceGetFile',
	PAGE_LOADED = 'pageLoaded';

	var OverwriteManager = {
		AUTO:'auto',
		NONE:'none'
	};


var REWRITE_BASE = '/';

function Accordion(vars) {

	vars = vars || {};

	var self = vars.self || new Sprite(vars),
		children = [],
		tile,
		_timer;

	self.gap = vars.gap || 1;
	self.axis = vars.axis || 'y';
	self.align = vars.align || 'left';
	self.wrap = vars.wrap || false;
	self.perpLength = vars.perpLength || self.getWidth();
	self.expand = 'auto';  /*auto, single, all*/
	self.toggle = vars.toggle || true;
	self.updateSpeed = vars.updateSpeed || 0.35;
	self.animate = vars.animate || true;

	self.addEventListener(SELECT_ACCORDION_ITEM, self.select);

	self.addItem = function(container, title, content, isOpen) {
		if(self.expand === 'all') {
			isOpen = true;
		}
		var item = {container:container, title:title, content:content, isOpen:isOpen||false};
		title.addEventListener(CLICK, self.select);
		children.push(item);
		title.accordionItem = item;
		self.addChild(container);
	};

	self.select = function(e) {
		var i = children.length;
		while(i--) {
			if(children[i] === this.accordionItem) {
				if(self.toggle) {
					toggleItem(this.accordionItem);
				} else if(!this.accordionItem.isOpen) {
					self.openItem(this.accordionItem);
				}
				self.dispatchEvent(ACCORDION_SELECT, this.accordionItem);
			} else if(self.expand === 'single') {
				self.closeItem(children[i]);
			}
		}
		self.layout();
	};

	function toggleItem(item) {
		if(item.isOpen) {
			self.closeItem(item);
		} else {
			self.openItem(item);
		}
	}

	self.closeItem = function(item) {
		if(self.expand !== 'all') {
			item.isOpen = false;
			if(item.content) {
				item.content.setOverflow('hidden');
				if(self.animate) {
					Tween(item.content, self.updateSpeed, {height:0});
				} else {
					item.content.setHeight(0);
				}
			}

			item.container.setHeight(item.title.getHeight());
		}
	};

	self.openItem = function(item) {
		item.isOpen = true;
		if(item.content) {
			if(self.animate) {
				Tween(item.content, self.updateSpeed, {height:item.contentHeight,onComplete:function(){
					item.content.setOverflow('visible');
				}});
			} else {
				item.content.setHeight(item.contentHeight);
				item.content.setOverflow('visible');
			}
		}
		item.container.setHeight(item.title.getHeight() + item.contentHeight);
	};

	function updateTile() {
		tile = new Tile();
		tile.gap = self.gap;
		tile.axis = self.axis;
		tile.align = self.align;
		tile.wrap = self.wrap;
		tile.perpLength = self.perpLength;
	}

	self.layout = function() {

		updateTile();

		var i,
			length = children.length,
			gapComp = (-self.gap * 2);

		for(i = 0; i < length; i++) {
			if(!children[i].contentHeight) {
				children[i].contentHeight = children[i].content ? children[i].content.getHeight() : 0;
			}
			if(children[i].content && !children[i].isOpen) {
				children[i].content.setHeight(0);
				children[i].content.setOverflow('hidden');
			}

			var width = children[i].title.getWidth();
			var height = children[i].isOpen ? children[i].title.getHeight() + children[i].contentHeight : children[i].title.getHeight();
			children[i].container.setWidth(width);
			children[i].container.setHeight(height);
			tile.perpLength = width > tile.perpLength ? width : tile.perpLength;

			tile.addItem(width, height);
		}

		tile.layoutItems();

		for(i = 0; i < length; i++) {
			var position = tile.getPosition(i);
			children[i].container.setX(position.x);
			if(self.updateSpeed !== 0) {
				Tween(children[i].container, self.updateSpeed, {y:position.y + self.gap + gapComp});
			} else {
				if(_timer) clearInterval(_timer);
				children[i].container.setY(position.y + self.gap + gapComp);
			}
		}

		var bounds = tile.getBounds();
		self.setWidth(tile.perpLength);

		self.setHeight(bounds.height + self.gap + gapComp);
		self.dispatchEvent(ACCORDION_SIZE_CHANGE, {width:tile.perpLength, height:bounds.height + self.gap + gapComp});
	};

	return self;

}

function Align(vars) {

	vars = vars || {};

	var _x,
		_y,
		_hRange = vars.hRange || 0,
		_vRange = vars.vRange || 0,
		_width = vars.width || 0,
		_height = vars.height || 0,
		_hAlign = vars.hAlign || 'center',
		_vAlign = vars.vAlign || 'center',
		_hOffset = vars.hOffset || 0,
		_vOffset = vars.vOffset || 0;

	return {
		setHRange: function(value) {
			_hRange = value;
		},
		setVRange: function(value) {
			_vRange = value;
		},
		setWidth: function(value) {
			_width = value;
		},
		setHeight: function(value) {
			_height = value;
		},
		setHAlign: function(value) {
			_hAlign = value;
		},
		setVAlign: function(value) {
			_vAlign = value;
		},
		setHOffset: function(value) {
			_hOffset = value;
		},
		setVOffset: function(value) {
			_vOffset = value;
		},
		getX: function() {
			if(_hAlign === 'left') {
				return _hOffset;
			} else if(_hAlign === 'right') {
				return  _hRange - _width - _hOffset;
			}
			return ((_hRange - _width) * 0.5) + _hOffset;
		},
		getY: function() {
			if(_vAlign === 'top') {
				return _vOffset;
			} else if(_vAlign === 'bottom') {
				return (_vRange - _height) - _vOffset;
			}
			return ((_vRange - _height) * 0.5) + _vOffset;
		}
	};
}

function Cursor(vars, hideElements) {

	var self = new Sprite(),
		thumbsIcon = svgPaths.cursorThumbs,
		visible = false,
		zoneWidth = 0,
		hideTargets = hideElements.slice(0),
		adminView,
		numAdminViews = 0;

	self.enabled = true;

	function init() {
		self.setWidth(200);
		self.setHeight(200);
		self.setZIndex(999);
		self.setAlpha(0);
		self.setDisplay('none');

		var svg = new Svg();
		svg.setX(80);
		svg.setY(80);
		svg.setWidth(40);
		svg.setHeight(40);
		self.addChild(svg);

		var icon = new Path();
		icon.id = 'icon';
		icon.setD(svgPaths.cursorThumbs);
		icon.setFill(LAYOUT_MODEL.cursorColor);
		svg.addChild(icon);

		self.icon = icon;
		self.svg = svg;

		vars.parentView.addChild(self);

		stage.addEventListener(MOUSE_MOVE, mouseMove);
		vars.events.addEventListener(CURSOR_UPDATE, updateCursor);
		vars.events.addEventListener(CURSOR_ADD_ADMIN, addAdminHideTargets);
	}

	function mouseMove(e) {
		var x = getX(e),
			y = getY(e),
			maskX = layoutCalcs.mediaView.x() + layoutCalcs.mediaMask.x(),
			maskY = layoutCalcs.mediaView.y() + layoutCalcs.mediaMask.y(),
			maskWidth = layoutCalcs.mediaMask.width(),
			maskHeight = layoutCalcs.mediaMask.height();

		self.setX(x - 102);
		self.setY(y - 102);
		if(typeof isAdmin === 'boolean') pushAdminChildren();
		if(getShowState(e) && x > maskX && x < maskX + maskWidth && y > maskY && y < maskY + maskHeight) {
			self.show();
		} else {
			self.hide();
		}

		zoneWidth = maskWidth * 0.333;

		var localX = x - maskX,
			localY = y - maskY,
			zone1 = zoneWidth,
			zone2 = zoneWidth * 2;

		if(LAYOUT_MODEL.cursorColor !== self.icon.getFill()) self.icon.setFill(LAYOUT_MODEL.cursorColor);

		if(localX < zone1) {
			self.icon.setD(getPrevIcon());
			self.type = 'prev';
		} else if(localX > zone1 && localX < zone2) {
			self.icon.setD(thumbsIcon);
			self.type = 'thumbs';
		} else if(localX > zone2) {
			self.icon.setD(getNextIcon());
			self.type = 'next';
		}
	}

	function click(e) {
		if(this.type === 'thumbs') {
			vars.events.dispatchEvent(NAVBAR_OVERLAY_BTN_CLICK, this.type);
		} else {
			vars.events.dispatchEvent(NAVBAR_NAV_CLICK, this.type);
		}
	}

	function updateCursor(event) {
		event.state === 'add' ? addHideTarget(event.views) : removeHideTarget(event.views);
	}

	function addAdminHideTargets(event) {
		adminView = event.view;
	}

	self.show = function() {
		if(!visible && self.enabled) {
			visible = true;
			self.setAlpha(0.85);
			self.setDisplay('block');
			self.icon.setCursor('none');
			self.svg.setCursor('none');
			self.setCursor('none');
			self.element.addEventListener('contextmenu', function(e){
				e.preventDefault();
			});
			self.element.addEventListener('dragstart', function(e){
				e.preventDefault();
			});
			if(TOUCH_DEVICE) {
				self.addEventListener(TOUCH_END, click);
			} else {
				self.addEventListener(CLICK, click);
			}
		}
	};

	self.hide = function() {
		if(visible) {
			visible = false;
			self.setAlpha(0);
			self.setDisplay('none');
			self.icon.setCursor('default');
			self.svg.setCursor('default');
			self.setCursor('default');
			self.element.removeEventListener('contextmenu', function(e){
				e.preventDefault();
			});
			self.element.removeEventListener('dragstart', function(e){
				e.preventDefault();
			});
			if(TOUCH_DEVICE) {
				self.removeEventListener(TOUCH_END, click);
			} else {
				self.removeEventListener(CLICK, click);
			}
		}
	};

	function addHideTarget(value) {
		hideTargets = hideTargets.concat(value);
	}

	function removeHideTarget(value) {
		var j = value.length;
		while(j--) {
			var target = value[j];
			var i = hideTargets.length;
			while(i--) {
				if(target === hideTargets[i]) {
					hideTargets.splice(i, 1);
				}
			}
		}
	}

	function getShowState(event) {
		var i = hideTargets.length;
		while (i--) {
			var target = hideTargets[i];
			if(target && target.hitTestPoint(event.mouseX, event.mouseY)) {
				return false;
			}
		}
		return true;
	}

	function pushAdminChildren() {
		if(adminView.children && numAdminViews !== adminView.children.length) {
			var adminChild = adminView.children[numAdminViews++];
			if(!adminChild.label || (adminChild.label && adminChild.label.getText() !== "0")) {
				hideTargets.push(adminChild);
			}
		}
	}

	function getPrevIcon() {
		switch(LAYOUT_MODEL.cursorIconStyle) {
			case 'arrow1':
				return svgPaths.arrow1Left;
			case 'arrow2':
				return svgPaths.arrow2Left;
			case 'plus1':
				return svgPaths.plus1Left;
			case 'plus2':
				return svgPaths.plus2Left;
		}
		return svgPaths.arrow2Left;
	}

	function getNextIcon() {
		switch(LAYOUT_MODEL.cursorIconStyle) {
			case 'arrow1':
				return svgPaths.arrow1Right;
			case 'arrow2':
				return svgPaths.arrow2Right;
			case 'plus1':
				return svgPaths.plus1Right;
			case 'plus2':
				return svgPaths.plus2Right;
		}
		return svgPaths.arrow2Right;
	}

	init.call(self);
	return self;
}

function Tooltip(vars) {

	var v = vars || {},
		self = this,

		container = new Sprite(),
		box = new Sprite(),
		arw = new Sprite(),
		txt = new Sprite(),

		alpha = 0,
		color = v.backgroundColor || 'rgba(0,0,0,0.7)',
		arwColor = color,
		fontColor = v.fontColor || '#FFFFFF',
		fontFamily = v.fontFamily || 'Helvetica Neue, Arial, sans-serif',
		fontSize = v.fontSize|| 11,
		corRadius = v.cornerRadius || 4,
		border = v.border || 'none',
		gap = v.gap || 12,
		padding = v.padding || 4,
		shadow = v.shadow || true,

		xPerc = 0,
		yAxis = 80,
		tipOffset = 0,
		containerAboveOffset = 0,
		containerBelowOffset = 0,

		delay = vars.delay || 350,
		delayTimer;

	self.isShowing = false;

	txt.setX(padding);
	txt.setY(padding);
	txt.setWidth(100);
	txt.setTextAlign('center');
	txt.setFontColor(fontColor);
	txt.setFontFamily('Helvetica Neue');
	txt.setFontSize(fontSize);

	box.setX(gap);
	box.setY(gap);
	box.setBorderRadius(corRadius);
	box.setBorder(border);
	box.setBackgroundColor(color);
	if(shadow) {
		box.element.style.boxShadow = '0px 1px 3px rgba(0,0,0, 0.5)';
	}

	container.setAlpha(alpha);
	container.setOverflow('hidden');
	container.setPointerEvents('none');

	arw.w = 8;
	arw.point = arw.w * 0.5;
	arw.h = 6;
	arw.setBorderRight(arw.point + 'px solid transparent');
	arw.setBorderLeft(arw.point + 'px solid transparent');

	box.addChild(txt);
	container.addChild(box);
	container.addChild(arw);

	function update(e) {
		txt.setText(e.target.tooltip);
		txt.setAlpha(1);
		box.setWidth(txt.getWidth() + (padding * 2));
		box.setHeight(txt.getHeight() + (padding * 2));
		container.setWidth(box.getWidth() + (gap * 2));
		container.setHeight(box.getHeight() + (gap * 2));
		containerAboveOffset = tipOffset + container.getHeight() + (gap * 0.5);
		containerBelowOffset = tipOffset + gap;
	}

	function setXPos(e) {
		xPerc = e.pageX / window.innerWidth;
		var tipX = Math.round(container.getWidth() * xPerc);
		if((tipX - arw.point - corRadius) < gap) {
			arw.setX(corRadius + gap);
			container.setX(e.pageX - (gap + arw.point + corRadius));
		} else if((tipX + gap + arw.point + corRadius) > container.getWidth()) {
			arw.setX((container.getWidth() - (gap + arw.getWidth() + corRadius)));
			container.setX((e.pageX - container.getWidth()) + gap + arw.point + corRadius);
		} else {
			arw.setX(tipX - (gap - arw.getWidth()));
			container.setX(e.pageX - tipX);
		}
	}

	function setYPos(e) {
		if(e.pageY < yAxis) {
			arw.setBorderTop(arw.h + 'px solid transparent');
			arw.setBorderBottom(arw.h + 'px solid ' + arwColor);
			container.setY(e.pageY + containerBelowOffset);
			arw.setY(box.y - arw.getHeight());
		} else {
			arw.setBorderBottom(arw.h + 'px solid transparent');
			arw.setBorderTop(arw.h + 'px solid ' + arwColor);
			container.setY(e.pageY - containerAboveOffset);
			arw.setY(box.getHeight() + gap);
		}
	}

	function move(e) {
		setXPos(e);
		setYPos(e);
	}

	self.show = function(e) {
		if(e.target.tooltip) {
			e.target.addEventListener(MOUSE_MOVE, move);
			e.target.addEventListener(MOUSE_OUT, hide);
			container.setTransition(0);
			container.setZIndex(1000);
			delayTimer = setTimeout(function() {
				Tween(container, 0.35, {alpha: 1});
			}, delay);
			update(e);
			self.isShowing = true;
		}
	};

	function hide(e) {
		if(delayTimer) clearTimeout(delayTimer);
		e.target.removeEventListener(MOUSE_MOVE, move);
		e.target.removeEventListener(MOUSE_OUT, hide);
		container.setAlpha(0);
		container.setWidth(0);
		container.setHeight(0);
		container.setZIndex(0);
		self.isShowing = false;
	}

	stage.addChild(container);

}


var Docks = {

	arr: [],

	register: function(dock) {
		this.arr.push(dock);
	},

	unRegister: function(dock) {
		var i = this.arr.length;
		while(i--) {
			if(dock === this.arr[i]) {
				this.arr.splice(i, 1);
			}
		}
	},

	resize: function() {
		if(this.arr.length > 0) {
			var i = this.arr.length;
			while(i--) {
				this.arr[i].resize();
			}
		}
	}
};

function Dock(obj, mask, vars) {

	var self = this;

	self.side = vars.side || 'left';
	self.align = vars.align || 'center';
	self.margin = vars.margin || 0;
	self.pOffset = vars.pOffset || 0;
	self.reveal = vars.reveal || 0;
	self.updateSpeed = vars.updateSpeed || 0;

	var axis = self.side === 'top' || self.side === 'bottom' ? 'Y' : 'X',
		pAxis = axis === 'X' ? 'Y' : 'X',
		dim = axis === 'X' ? 'Width' : 'Height',
		pDim = axis === 'X' ? 'Height' : 'Width';

	self.show = function(updateSpeed) {
		updateSpeed = !isNaN(updateSpeed) ? updateSpeed : UPDATE_SPEED;
		var tweenVars = {};
		tweenVars[axis.toLowerCase()] = showAxis();
		if(updateSpeed === 0) {
			obj['set'+axis](showAxis());
		} else {
			Tween(obj, updateSpeed, tweenVars);
		}
		toggle.state = true;
	};

	self.hide = function(updateSpeed) {
		updateSpeed = !isNaN(updateSpeed) ? updateSpeed : UPDATE_SPEED;
		var tweenVars = {};
		tweenVars[axis.toLowerCase()] = hideAxis();
		if(updateSpeed === 0) {
			obj['set'+axis](hideAxis());
		} else {
			Tween(obj, updateSpeed, tweenVars);
		}
		toggle.state = false;
	};

	self.toggle = function() {
		toggle.flip();
	};

	self.toggleState = function() {
		return toggle.state;
	};

	self.resize = function(updateSpeed) {
		updateSpeed = !isNaN(updateSpeed) ? updateSpeed : UPDATE_SPEED;
		var tweenVars = {};
		tweenVars[axis.toLowerCase()] = toggle.state ? showAxis() : hideAxis();
		tweenVars[pAxis.toLowerCase()] = perpAxis();
		if(updateSpeed === 0) {
			obj['set'+axis](toggle.state ? showAxis() : hideAxis());
			obj['set'+pAxis](perpAxis());
		} else {
			Tween(obj, updateSpeed, tweenVars);
		}
	};

	function showAxis() {
		if(self.side === 'left' || self.side === 'top') {
			return self.margin;
		}
		return mask['get'+dim]() - obj['get'+dim]() - self.margin;
	}

	function hideAxis() {
		if(self.side === 'left' || self.side === 'top') {
			if(USER_AGENT === POD) {
				return - mask['get'+dim]() + self.reveal;
			}
			return - obj['get'+dim]() + self.reveal;
		}
		return mask['get'+dim]() - self.reveal;
	}

	function perpAxis() {
		switch(self.align) {
			case 'top':
			case 'left':
				return self.pOffset;
			case 'right':
			case 'bottom':
				return mask['get'+pDim]() - obj['get'+pDim]() - self.pOffset;
		}
		return ((mask['get'+pDim]() - obj['get'+pDim]()) * 0.5) + self.pOffset;
	}

	var toggle = new Toggle({
		target: this,
		on: this.show,
		off: this.hide
	});

	toggle.state = true;
	Docks.register(this);

	self.resize(0);
}

function Drag(vars) {

	var _dragZone = vars.dragZone || false,
		_events = new EventSandbox(),
		_thresholdMet = false;

	var self = {
		listeners: _events.listeners,
		dispatchEvent: _events.dispatchEvent,
		addEventListener: _events.addEventListener,
		removeEventListener: _events.removeEventListener,
		dragElements: vars.dragElements || false,
		threshold: vars.threshold || 20,
		setDragZone: function(value) {
			_dragZone = value;
			if(self.dragElements) {
				self.item = vars.item;
				_dragZone.addEventListener(MOUSE_DOWN, dragStart);
				_dragZone.addEventListener(TOUCH_START, dragStart);
			} else {
				throw 'you must define the dragElement before the dragZone';
			}
		},
		getDragZone: function() {
			return _dragZone;
		}
	};

	if(self.dragElements) {
		_dragZone.addEventListener(MOUSE_DOWN, dragStart);
		_dragZone.addEventListener(TOUCH_START, dragStart);
	} else {
		throw 'you must define a dragElement';
	}

	function dragStart(e) {
		stage.dragElements = self.dragElements;
		var i = stage.dragElements.length;
		while(i--) {
			stage.dragElements[i].setTransition(0);
			stage.dragElements[i].cancel = false;
			stage.dragElements[i].startX = stage.dragElements[i].getX();
			stage.dragElements[i].startY = stage.dragElements[i].getY();
			stage.dragElements[i].mouseOffsetX = stage.dragElements[i].parent.globalToLocal({x:getX(e), y:getY(e)}).x - stage.dragElements[i].getX();
			stage.dragElements[i].mouseOffsetY = stage.dragElements[i].parent.globalToLocal({x:getX(e), y:getY(e)}).y - stage.dragElements[i].getY();

		}
		stage.addEventListener(MOUSE_UP, dragStop);
		stage.addEventListener(TOUCH_END, dragStop);
		stage.addEventListener(MOUSE_MOVE, dragMove);
		stage.addEventListener(TOUCH_MOVE, dragMove);
		stage.addEventListener(TOUCH_CANCEL, dragCancel);
		stage.addEventListener(KEY_DOWN, escapeCheck);
		self.dispatchEvent(DRAG_START, e);
	}

	function dragMove(e) {
		var i = stage.dragElements.length;
		while(i--) {
			stage.dragElements[i].dragging = true;
			if(!stage.dragElements[i].cancel) {
				stage.dragElements[i].setAlpha(0.98);
				stage.dragElements[i].setX(stage.dragElements[i].parent.globalToLocal({x:getX(e), y:getY(e)}).x - stage.dragElements[i].mouseOffsetX);
				stage.dragElements[i].setY(stage.dragElements[i].parent.globalToLocal({x:getX(e), y:getY(e)}).y - stage.dragElements[i].mouseOffsetY);
				stage.dragElements[i].deltaX = stage.dragElements[i].getX() - stage.dragElements[i].startX;
				stage.dragElements[i].deltaY = stage.dragElements[i].getY() - stage.dragElements[i].startY;
				_thresholdMet = (Mth.abs(stage.dragElements[i].deltaX) > self.threshold || Mth.abs(stage.dragElements[i].deltaY) > self.threshold);
			}
		}
		self.dispatchEvent(DRAG_MOVE, e);
	}

	function dragStop(e) {
		stage.removeEventListener(MOUSE_MOVE, dragMove);
		stage.removeEventListener(TOUCH_MOVE, dragMove);
		stage.removeEventListener(MOUSE_UP, dragStop);
		stage.removeEventListener(TOUCH_END, dragStop);
		stage.removeEventListener(TOUCH_CANCEL, dragCancel);
		stage.removeEventListener(KEY_DOWN, escapeCheck);
		var i = stage.dragElements.length;
		while(i--) {
			stage.dragElements[i].dragging = false;
			stage.dragElements[i].setAlpha(1);
		}
		self.dispatchEvent(DRAG_STOP, e);
	}

	function escapeCheck(e) {
		if(keyCode[e.keyCode] === ESCAPE) {
			dragCancel(e);
		}
	}

	function dragCancel(e) {
		stage.removeEventListener(MOUSE_MOVE, dragMove);
		stage.removeEventListener(TOUCH_MOVE, dragMove);
		stage.removeEventListener(MOUSE_UP, dragStop);
		stage.removeEventListener(TOUCH_END, dragStop);
		stage.removeEventListener(TOUCH_CANCEL, dragCancel);
		stage.removeEventListener(KEY_DOWN, escapeCheck);
		var i = stage.dragElements.length;
		while(i--) {
			stage.dragElements[i].dragging = false;
			stage.dragElements[i].setAlpha(1);
		}
		self.dispatchEvent(DRAG_CANCEL, e);
	}

	return self;
}

function Fullscreen(vars) {

	var self = this;
	self.dxFullscreen = 0;

	vars.events.addEventListener(FULLSCREEN_TOGGLE, toggle);
	vars.events.addEventListener(FULLSCREEN_TOGGLE_ELEMENT, toggleElement);

	document.addEventListener('webkitfullscreenchange', change, false);
	document.addEventListener('mozfullscreenchange', change, false);


	function toggle(event) {
		if (isNotFullscreen()) {
			enterFullscreen(document.documentElement);
		} else {
			exitFullscreen();
		}
	}

	function enterFullscreen (element) {
		self.dxFullscreen = 1;
		if (element.requestFullScreen) {
			element.requestFullScreen();
		} else if (element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if (element.webkitRequestFullScreen) {
			element.webkitRequestFullScreen();
			element.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	}

	function exitFullscreen() {
		self.dxFullscreen = 0;
		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
	}

	function toggleElement(e) {
		var element = e.fullscreenTarget;
		if (isFullscreen()) {
			exitFullscreen();
		} else {
			enterFullscreen(element);
			/*TODO show vid controls for FIREFOX
			element.setAttribute("controls", "true");*/
		}
	}

	function change(event) {
		setTimeout(function () {
			self.dxFullscreen = document.isFullScreen | document.mozFullScreen | document.webkitIsFullScreen;
		}, 100);
	}

	function isNotFullscreen() {
		return !document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement;
	}
}

function LazyLoader(obj, mask, vars) {

	vars = vars || {};

	var type = vars.type || 'default',
		threshold = 0.5;
		loadBoundary = {
			get x() { return mask.getX() - ((mask.getWidth() * threshold) * 0.5) },
			get y() { return mask.getY() - ((mask.getHeight() * threshold) * 0.5) },
			get width() { return mask.getWidth() + (mask.getWidth() * threshold) },
			get height() { return mask.getHeight() + (mask.getHeight() * threshold) }
		},
		isWithinBounds = {
			fade: function(child) {

			},
			default: function(child) {
				return obj.getX() + child.getX() >= loadBoundary.x &&
					obj.getX() + child.getX() <= loadBoundary.width &&
					obj.getY() + child.getY() >= loadBoundary.y &&
					obj.getY() + child.getY() <= loadBoundary.height;
			}
		};

	function loadProcessor() {
		var i = 0,
			l = obj.children.length;
		for(; i < l; i++) {
			var child = obj.children[i];
			var inBounds = isWithinBounds[type](child);
			if(inBounds && !child.loaded) {
				child.load();
			} else if(!inBounds && child.loaded) {
				/*child.unload();*/
			}
		}
	}

	this.update = function update() {
		loadProcessor();
	};
}

function Scale(vars) {

	vars = vars || {};

	var _x,
		_y,
		_hRange = vars.hRange || 0,
		_vRange = vars.vRange || 0,
		_width = vars.width || 0,
		_height = vars.height || 0,
		_type = vars.type || 'fit',
		_ratio = -1,
		_rangeRatio = -1;

	return {
		setHRange: function(value) {
			_hRange = value;
		},
		setVRange: function(value) {
			_vRange = value;
		},
		setType: function(value) {
			_type = value;
		},
		getType: function() {
			return _type;
		},
		setWidth: function(value) {
			_width = value;
		},
		setHeight: function(value) {
			_height = value;
		},
		getWidth: function() {
			this.calc();
			return _width;
		},
		getHeight: function() {
			if(_ratio !== (_width / _height) || _rangeRatio !== (_hRange / _vRange)) this.calc();
			return _height;
		},
		calc:function() {
			_ratio = _width / _height;
			_rangeRatio = _hRange / _vRange;
			if(_type === 'fill' || _type === 'fillSite') {
				if(_rangeRatio > _ratio) {
					_width = _hRange;
					_height = _hRange / _ratio;
				} else {
					_width = _vRange * _ratio;
					_height = _vRange;
				}
			} else {
				if(_rangeRatio < _ratio) {
					_width = _hRange;
					_height = _hRange / _ratio;
				} else {
					_width = _vRange * _ratio;
					_height = _vRange;
				}
			}
		}
	}
}

var Scrolls = {

	arr: [],

	focusTarget:null,

	register: function(scroll) {
		this.arr.push(scroll);
	},

	unRegister: function(scroll) {
		var i = this.arr.length;
		while(i--) {
			if(scroll === this.arr[i]) {
				this.arr.splice(i, 1);
			}
		}
	},

	resize: function() {
		if(this.arr.length > 0) {
			var i = this.arr.length;
			while(i--) {
				this.arr[i].resize();
			}
		}
	},

	hitMask: function(x, y) {
		var i = this.arr.length;
		while(i--) {
			this.arr[i].active = this.arr[i].mask.hitTestPoint(x, y);
		}
	},

	hitScrollBar: function(x, y) {
		var i = this.arr.length;
		while(i--) {
			if(this.arr[i].bar && this.arr[i].bar.handle.hitTestAbs(x, y)) {
				return this.arr[i].bar.back.hitTestAbs(x, y);
			}
		}
		return false;
	},

	getScrolling: function() {
		var i = this.arr.length;
		while(i--) {
			if(this.arr[i].getScrolling()) {
				return this.arr[i].getScrolling();
			}
		}
		return false;
	}

};

function Scroll(obj, mask, vars) {

	vars.events = vars.events || globalEvents;

	var _percent = 0,
		_position = 0,
		_scrollDist = 0,
		_scrolling = false;

	var scroll = {
		obj: obj,
		mask: mask,
		listeners: vars.events.listeners,
		dispatchEvent: vars.events.dispatchEvent,
		addEventListener: vars.events.addEventListener,
		removeEventListener: vars.events.removeEventListener,
		types: vars.types || ['bar','wheel','touch'],
		axis: vars.axis || 'y',
		align: vars.align || 'center',
		margin: vars.margin || 0,
		maxTime: vars.maxTime || 300,
		minTime: vars.minTime || 1,
		maxSpeed: vars.maxSpeed || 500,
		minSpeed: vars.minSpeed || 5,
		color: vars.color || '#00FF00',
		hover: vars.hover || '#FF0000',
		alpha: vars.alpha || 0.35,
		zIndex: vars.zIndex,
		side: vars.side || 'right',
		offsetX: vars.offsetX || 0,
		offsetY: vars.offsetY || 0,
		topPad:vars.topPad || 0,
		parent: vars.parent || mask.parent,
		width: vars.width || 7,
		getPercent: function(){
			return _percent;
		},
		setPercent: function(value){
			value = value < 0 ? 0 : value;
			value = value > 1 ? 1 : value;
			_percent = value;
		},
		getPosition: function(){
			return _position;
		},
		setPosition: function(value){
			_position = value;
		},
		getScrollDist: function(){
			return _scrollDist;
		},
		setScrollDist: function(value){
			_scrollDist = value;
		},
		getScrolling: function(){
			return _scrolling;
		},
		setScrolling: function(value){
			_scrolling = value;
		}
	};

	scroll.axis = vars.axis === 'y' ? 'Y' : 'X';
	scroll.perpAxis = vars.axis === 'x' ? 'Y' : 'X';
	scroll.dim = vars.axis === 'x' ? 'Width' : 'Height';
	scroll.perpDim = vars.axis === 'x' ? 'Height' : 'Width';

	scroll.alignment = function() {
		switch(scroll.align) {
			case 'middle':
			case 'center':
				return 0.5;
			case 'right':
			case 'bottom':
				return 1;
		}
		return 0;
	};

	scroll.scrollTo = function(value) {
		if(scroll.bar) {
			scroll.bar.scrollTo(value, 0);
		}
	};

	scroll.update = function(speed, accel) {
		if((this.margin + this.obj['get' + this.dim]() + this.margin) < this.mask['get' + this.dim]()) {
			_percent = scroll.alignment();
		}

		_scrollDist = ((this.margin + this.obj['get' + this.dim]() + this.margin) - this.mask['get' + this.dim]());
		_position = -(_scrollDist * _percent) + this.margin;
		var tweenVars = {};
		tweenVars[scroll.axis] = Mth.round(_position);
		tweenVars.ease = 'cubic-bezier(0.5, 0.5, 0.5, 0.8)';
		if(scroll.hvr) {
			tweenVars.onUpdate = function() {
				scroll.dispatchEvent(SCROLL);
			};
		}
		if(accel) scroll.obj['set' + scroll.axis](_position + 0.1);
		Tween(obj, speed, tweenVars);

		if(scroll.bar) {
			scroll.bar.moveHandle();
		}
	};

	scroll.resize = function(speed) {
		if(scroll.bar) {
			scroll.bar.resize();
		}
		scroll.update(speed || 0);
	};

	scroll.remove = function() {
		if(scroll.bar) {
			scroll.bar.remove();
		}
		if(scroll.wheel) {
			scroll.wheel.remove();
		}
		if(scroll.hvr) {
			scroll.hvr.remove();
		}
		if(scroll.touch) {
			scroll.touch.remove();
		}
	};

	scroll.setAlpha = function(value) {
		if(scroll.bar) {
			scroll.bar.setAlpha(value);
		}
	};

	scroll.getBack = function() {
		if(scroll.bar) {
			return scroll.bar.getBack();
		}
		return null;
	};

	function buildBehaviors() {
		var i = vars.types.length;
		while(i--) {
			switch (vars.types[i]) {
				case 'bar':
					scroll.bar = new ScrollBar(scroll);
					break;
				case 'wheel':
					scroll.wheel = new ScrollWheel(scroll);
					break;
				case 'hover':
					scroll.hvr = new ScrollHover(scroll);
					break;
				case 'touch':
					scroll.touch = new ScrollTouch(scroll);
					break;
			}
		}
	}

	buildBehaviors();
	scroll.resize();
	scroll.update();
	Scrolls.register(scroll);

	return scroll;
}

function ScrollBar(scroll) {

	var obj = scroll.obj,
		mask = scroll.mask,
		moveDist = 0,
		startPoint = 0;

	var back = this.back = new Sprite();
	back.setAlpha(0);
	back.setSelectable(true);
	back.setBackgroundColor(scroll.color);
	if(scroll.zIndex) back.setZIndex(scroll.zIndex);
	scroll.parent.addChild(back);

	var handle = this.handle = new Sprite();
	handle.setAlpha(0);
	handle.setBackgroundColor(scroll.color);
	if(scroll.zIndex) handle.setZIndex(scroll.zIndex);
	handle.setSelectable(false);
	handle.setCursor('default');
	handle.style.pointerEvents = 'auto';
	scroll.parent.addChild(handle);

	handle.addEventListener(MOUSE_OVER, onMouseOver);
	handle.addEventListener(MOUSE_OUT, onMouseOut);
	handle.addEventListener(MOUSE_DOWN, onMouseDown);
	handle.addEventListener(TOUCH_START, onMouseDown);

	this.moveHandle = function() {
		handle.setTransition(0);
		handle['set' + scroll.axis](((back['get' + scroll.dim]() - handle['get' + scroll.dim]()) * scroll.getPercent()) + back['get' + scroll.axis]());
	};

	this.resize = function() {

		back.setTransition(0);
		handle.setTransition(0);

		back['set' + scroll.perpDim](scroll.width);
		back['set' + scroll.dim](mask['get' + scroll.dim]());
		if(scroll.parent === mask) {
			back['set' + scroll.axis](getOffset());
		} else {
			back['set' + scroll.axis](mask['get' + scroll.axis]() + getOffset());
		}

		handle['set' + scroll.perpDim](scroll.width);

		if(scroll.side === 'left' || scroll.side === 'top') {
			back['set' + scroll.perpAxis](mask['get' + scroll.perpAxis]() - scroll.width + getPerpOffset());
			handle['set' + scroll.perpAxis](mask['get' + scroll.perpAxis]() - scroll.width + getPerpOffset());
		} else {
			back['set' + scroll.perpAxis](mask['get' + scroll.perpAxis]() + mask['get' + scroll.perpDim]() + getPerpOffset());
			handle['set' + scroll.perpAxis](mask['get' + scroll.perpAxis]() + mask['get' + scroll.perpDim]() + getPerpOffset());
		}

		this.moveHandle();

		setTimeout(this.checkVisible, 0);
	};

	this.checkVisible = function() {
		back.style['transition'] = 'opacity 750ms';
		handle.style['transition'] = 'opacity 750ms';
		var handleSize = (mask['get' + scroll.dim]() / (obj['get' + scroll.dim]() + (scroll.margin * 2))) * mask['get' + scroll.dim]();
		if(handleSize < back['get' + scroll.dim]()) {
			back.setDisplay('block');
			back.setAlpha(scroll.alpha);
			handle.setDisplay('block');
			handle.setAlpha(1);
			handle['set' + scroll.dim](handleSize);
		} else if(!isNaN(handleSize)) {
			back.setAlpha(0);
			back.setDisplay('none');
			handle.setAlpha(0);
			handle.setDisplay('none');
			handle['set' + scroll.dim](back['get' + scroll.dim]());
			this.scrollTo(0, 0);
		}
	};

	this.remove = function() {
		scroll.parent.removeChild(handle);
		scroll.parent.removeChild(back);
		handle.removeEventListener(MOUSE_OVER, onMouseOver);
		handle.removeEventListener(MOUSE_OUT, onMouseOut);
		handle.removeEventListener(MOUSE_DOWN, onMouseDown);
		handle.removeEventListener(TOUCH_START, onMouseDown);
	};

	this.setAlpha = function(value) {
		handle.setAlpha(value);
		back.setAlpha(value);
	};

	this.getBack = function() {
		return back;
	};

	this.scrollTo = function(value) {
		scroll.setPercent(value);
		scroll.update(0);
		scroll.dispatchEvent(SCROLL);
	};

	function onMouseOver(e) {
		Tween(handle, 0.35, {backgroundColor:scroll.hover});
	}

	function onMouseOut(e) {
		if(!scroll.getScrolling()) {
			Tween(handle, 0.35, {backgroundColor:scroll.color});
		}
	}

	function onMouseDown(e) {
		Tween(handle, 0.35, {backgroundColor:scroll.hover});
		startPoint = getPoint(e) - back['get' + scroll.axis]() - handle['get' + scroll.axis]();
		stage.addEventListener(MOUSE_UP, onMouseUp, false);
		stage.addEventListener(MOUSE_MOVE, onMouseMove, false);
		stage.addEventListener(TOUCH_END, onMouseUp, false);
		stage.addEventListener(TOUCH_MOVE, onMouseMove, false);
	}

	function onMouseMove(e) {
		if(scroll.getScrolling() === false) {
			scroll.setScrolling(true);
			e.preventDefault();
		}
		moveDist = getPoint(e) - startPoint - back['get' + scroll.axis]();
		scroll.setPercent((moveDist - back['get' + scroll.axis]()) / (back['get' + scroll.dim]() - handle['get' + scroll.dim]()));
		scroll.update(0);
		scroll.dispatchEvent(SCROLL);
	}

	function onMouseUp(event) {
		setTimeout(function(){
			scroll.setScrolling(false);
		}, 10);
		Tween(handle, 0.35, {backgroundColor:scroll.color});
		stage.removeEventListener(MOUSE_MOVE, onMouseMove);
		stage.removeEventListener(MOUSE_UP, onMouseUp);
		stage.removeEventListener(TOUCH_END, onMouseUp, false);
		stage.removeEventListener(TOUCH_MOVE, onMouseMove, false);
	}

	function getPoint(e) {
		if(scroll.axis === 'X') {
			return getX(e);
		}
		return getY(e);
	}

	function getOffset() {
		if(scroll.axis === 'X') {
			return scroll.offsetX;
		}
		return scroll.offsetY;
	}

	function getPerpOffset() {
		if(scroll.axis === 'Y') {
			return scroll.offsetX;
		}
		return scroll.offsetY;
	}

}

function ScrollTouch(scroll) {

	var startPoint = 0,
		startPercent = 0,
		moveDist = 0;

	scroll.mask.addEventListener(TOUCH_START, onStart);

	function onStart(e) {
		startPoint = getPoint(e);
		startPercent = scroll.getPercent() || 0;
		scroll.mask.addEventListener(TOUCH_END, onEnd);
		scroll.mask.addEventListener(TOUCH_MOVE, onMove);
	}

	function onMove(e) {
		if(scroll.getScrolling() === false) {
			scroll.setScrolling(true);
			e.preventDefault();
		}
		moveDist = getPoint(e) - startPoint;
		scroll.setPercent(-(moveDist / (scroll.getScrollDist())) + startPercent);
		scroll.setPercent(scroll.getPercent() < 0 ? 0 : scroll.getPercent());
		scroll.setPercent(scroll.getPercent() > 1 ? 1 : scroll.getPercent());

		scroll.update(0);
		scroll.dispatchEvent(SCROLL);
	}

	function getPoint(e) {
		if(scroll.axis === 'X') {
			return TOUCH_DEVICE ? event.touches[0].pageX : event.clientX;
		}
		return TOUCH_DEVICE ? event.touches[0].pageY : event.clientY;
	}

	function onEnd(e) {
		scroll.setScrolling(false);
		scroll.mask.removeEventListener(TOUCH_END, onEnd);
		scroll.mask.removeEventListener(TOUCH_MOVE, onMove);
	}

	this.remove = function() {
		if(startPoint) {
			scroll.setScrolling(false);
			scroll.mask.removeEventListener(TOUCH_END, onEnd);
			scroll.mask.removeEventListener(TOUCH_MOVE, onMove);
		}
	};

}

function ScrollWheel(scroll) {
	var mask = scroll.mask;
	var self = this;

	scroll.active = false;

	mask.addEventListener(MOUSE_OVER, addWheelListener);
	stage.addEventListener(WHEEL, onMouseWheel);
	stage.addEventListener(MOUSE_WHEEL, onMouseWheel);

	function addWheelListener(event) {
		Scrolls.focusTarget = self;
	}

	function onMouseWheel(e) {
		if(scroll.active) {
			var delta = scroll.axis === 'X' ? e.deltaX : e.deltaY;
			update(delta);
		}
	}

	function update(delta) {
		delta = scroll.align === 'bottom' ? delta = -delta : delta;
		if(isNaN(scroll.getPercent())) scroll.setPercent(0);
		var _scrollDist = (scroll.margin + scroll.obj['get' + scroll.dim]() + scroll.margin) - scroll.mask['get' + scroll.dim]();
		var newPosition = scroll.getPercent() + (delta / _scrollDist);
		scroll.setPercent(newPosition < 0 ? 0 : newPosition);
		scroll.setPercent(newPosition > 1 ? 1 : newPosition);

		scroll.update(0);
		scroll.dispatchEvent(SCROLL);
	}

	this.remove = function() {
	};
}

function ScrollHover(scroll) {
	var self = this,
		percent = 0,
		weightedTime = 0,
		num = 0,
		hoverTimer,
		hovering = false;

	self.enabled = true;

	scroll.mask.addEventListener(MOUSE_OVER, add);

	function add() {
		scroll.mask.addEventListener(MOUSE_MOVE, update);
		stage.addEventListener(MOUSE_MOVE, hoverCheck);
	}

	function hoverCheck(e) {
		if(scroll.mask.hitTestPoint(getX(e), getY(e))) {
			hovering = true;
		} else if(hovering) {
			hovering = false;
			remove();
		}
	}

	function remove() {
		clearTimeout(hoverTimer);
		scroll.mask.removeEventListener(MOUSE_MOVE, update);
		stage.removeEventListener(MOUSE_MOVE, hoverCheck);
	}

	function update(e) {
		if(self.enabled) {
			hoverTimer = setTimeout(function() {
				var mousePoint = scroll.axis === 'X' ? e.mouseX : e.mouseY;
				percent = mousePoint / scroll.mask['get' + scroll.dim]();

				if(percent === 0 && mousePoint > scroll.mask['get' + scroll.dim]() * 0.5) {
					percent = 0.98;
				} else if (percent === 0 && mousePoint < scroll.mask['get' + scroll.dim]() * 0.5) {
					percent = 0.15;
				}
				scroll.setPercent(percent > 0.5 ? 1 : 0);

				var destination = scroll.getPosition();
				var currentPosition = scroll.obj['get' + scroll.axis]();
				var distance = Math.abs(destination - currentPosition);

				var time = Math.abs(distance) / scroll.maxSpeed;
				weightedTime += (time - weightedTime) * 0.10;
				weightedTime += (ease() - weightedTime) * 0.10;
				scroll.update(weightedTime, true);

				if(!hovering) {
					var pos,
						tweenVars = {};

					pos = scroll.obj['get' + scroll.axis]();

					tweenVars[scroll.axis] = Mth.round(pos);
					Tween(scroll.obj, 1, tweenVars);
				}
			}, 50);
		}
	}

	function ease() {
		var half = percent < 0.5 ? percent * 2 : (percent * -2) + 2;
		return Math.round((Math.pow(half, 5) * scroll.maxSpeed) + scroll.minSpeed);
	}

	return self;

}

function Sort(vars) {

	var _events = new EventSandbox(),
		_dragElements,
		_disabled = false,
		_timer;

	var self = {
		isDragging:false,
		group:vars.group,
		zoneSize:vars.zoneSize||0.5,
		listeners:_events.listeners,
		dispatchEvent:_events.dispatchEvent,
		addEventListener:_events.addEventListener,
		removeEventListener:_events.removeEventListener,
		dragGroup:[],
		getDisabled: function() {
			return _disabled;
		},
		setDisabled: function(value) {
			_disabled = value;
		}
	};

	self.direction = '';
	self.startIndex = 0;

	function sort(e) {
		var i,
			mouse = {x:getX(e), y:getY(e)},
			length = self.group.length;
		for(i = 0; i < length; i++) {
			if(this.indexOf(self.group[i]) === -1 && self.group[i].hitTestAbs(mouse.x, mouse.y)) {
				dispatchReorder(getDragIndex(i), e);
				break;
			}
		}
	}

	function dispatchReorder(newPos, e) {
		if(newPos !== self.stopIndex) {
			self.sorted = true;
			self.dragGroup = getDragGroup(self.group);
			spliceDragGroup(newPos);
			e.group = self.group;
			e.stopIndex = self.stopIndex;
			self.dispatchEvent(REORDER_MOVE, e);
		}
	}

	function getDragIndex(i) {
		return self.group.indexOf(_dragElements[0]) < i ? i - (_dragElements.length - 1) : i;
	}

	function getDragGroup(arr) {
		var group = [];
		var i = arr.length;
		while(i--) {
			if(_dragElements && _dragElements.indexOf(arr[i]) > -1) {
				var item = arr.splice(i, 1)[0];
				group.push(item);
			}
		}
		return group;
	}

	function spliceDragGroup(to) {
		self.stopIndex = to;
		var insertPoint = to;
		var i = self.dragGroup.length;
		while(i--) {
			self.group.splice(insertPoint, 0, self.dragGroup[i]);
			insertPoint++;
		}
	}

	function dragStart(e) {
		_dragElements = this.dragElements;
		self.startIndex = this.dragElements[0].startIndex;
		self.dispatchEvent(REORDER_START, e);
		setZindex();
	}

	function dragMove(e) {
		var ths = this;
		self.isDragging = true;
		if(!_disabled) {
			if(_timer) clearTimeout(_timer);
			_timer = setTimeout(function(){
				sort.apply(ths.dragElements, [e]);
			}, 80);
		}
	}

	function dragStop(e) {
		if(self.isDragging && self.sorted) {
			self.direction = self.startIndex < self.stopIndex  ? 'forward' : 'backward';
			self.startIndex = self.startIndex;
			if(_dragElements) _dragElements[0].stopIndex = self.stopIndex;
			self.dispatchEvent(REORDER_STOP, {dragElements:_dragElements, stopIndex:self.stopIndex});
			self.isDragging = false;
			self.sorted = false;
			self.update();
		}
	}

	function setZindex() {
		var i = self.group.length;
		while(i--) {
			var item = self.group[i];
			if(_dragElements.indexOf(item) === -1) {
				item.setZIndex(2);
			} else {
				item.setZIndex(3);
			}
		}
	}

	self.update = function() {
		var i = self.group.length;
		while(i--) {
			var item = self.group[i];
			item.startIndex = i;
			if(item.drag && item.drag.dragElements) {
				item.drag.addEventListener(DRAG_START, dragStart);
				item.drag.addEventListener(DRAG_MOVE, dragMove);
				item.drag.addEventListener(DRAG_STOP, dragStop);
			}
		}
	};

	return self;
}

function Tile(vars) {

	vars = vars || {};

	var self = this,
		items = [],
		col,
		row,
		colHeights = [],
		rowWidths = [];

	self.axis = vars.axis || 'y';
	self.perpLength = vars.perpLength || 300;
	self.align = vars.align || 'left';
	self.wrap = vars.wrap || true;
	self.gap = vars.gap || 0;
	self.width = 0;
	self.height = 0;

	self.layoutItems = function() {

		col = 0;
		row = 0;
		colHeights = [];
		rowWidths = [];

		var i = 0,
			l = items.length,
			minCol,
			minRow;

		for(i; i < l; i++) {

			incrementGridArrays(i);

			if(!colHeights[col]) colHeights[col] = 0;
			if(!rowWidths[row]) rowWidths[row] = 0;

			minCol = self.axis === 'y' ? minIndex(col, colHeights) : col;
			minRow = self.axis === 'y' ? row : minIndex(row, rowWidths);

			if(minRow !== 0) colHeights[minCol] += self.gap;
			if(minCol !== 0) rowWidths[minRow] += self.gap;

			items[i].x = setX('x', items[i].width, minCol, minRow);
			items[i].y = setY('y', items[i].height, minCol, minRow);

			colHeights[minCol] += items[i].height;
			rowWidths[minRow] += items[i].width;

		}

		self.width = maxBound(rowWidths);
		self.height = maxBound(colHeights);
	};

	self.addItem = function(width, height) {
		items.push({width:width, height:height, x:0, y:0});
	};

	self.setSize = function(index, width, height) {
		items[index].width = width;
		items[index].height = height;
	};

	self.getPosition = function(index) {
		return items[index];
	};

	self.getBounds = function() {
		return {width:self.width, height:self.height};
	};

	function setX(axis, dim, minCol, minRow) {
		if(self.wrap) {
			return self.axis === 'y' ? minCol * (dim + self.gap) : rowWidths[minRow];
		} else {
			return alignPointNoWrap(axis, dim, rowWidths, minRow);
		}
	}

	function setY(axis, dim, minCol, minRow) {
		if(self.wrap) {
			return self.axis === 'y' ? colHeights[minCol] : minRow * (dim + self.gap);
		} else {
			return alignPointNoWrap(axis, dim, colHeights, minCol);
		}
	}

	function alignPointNoWrap(axis, dim, axisArr, index) {
		if((axis === 'x' && self.align === 'center') || (axis === 'y' && self.align === 'middle')) {
			return (self.perpLength - dim) * 0.5;
		} else if((axis === 'x' && self.align === 'right') || (axis === 'y' && self.align === 'bottom')) {
			return (self.perpLength - dim);
		} else {
			return axisArr[index];
		}
	}

	function incrementGridArrays(i) {
		if(self.axis === 'y') {
			if(!self.wrap || rowWidths[row] + items[i].width > self.perpLength) {
				row += 1;
			}
			if(self.wrap && rowWidths[row] + items[i].width < self.perpLength) {
				col += 1;
			} else {
				col = 0;
			}
		} else {
			if(!self.wrap || colHeights[col] + items[i].height > self.perpLength) {
				col += 1;
			}
			if(self.wrap && colHeights[col] + items[i].height < self.perpLength) {
				row += 1;
			} else {
				row = 0;
			}
		}
	}

	function minIndex(index, arr) {
		var i = arr.length,
			min = arr[0];
		while(i--) {
			if(arr[i] <= min) {
				min = arr[i];
				index = i;
			}
		}
		return index;
	}

	function maxBound(arr) {
		var i = arr.length,
			max = 0;
		while(i--) {
			if(arr[i] > max) max = arr[i];
		}
		return max;
	}
}

function Toggle(vars) {

	this.state = vars.state || false;
	this.target = vars.target;
	this.onCb = vars.on;
	this.offCb = vars.off;

	this.on = function() {
		this.onCb.call(this.target);
		this.state = true;
	};

	this.off = function() {
		this.offCb.call(this.target);
		this.state = false;
	};

	this.flip = function() {
		if(this.state) {
			this.off();
		} else {
			this.on();
		}
	};
}

function Tooltip(vars) {

	var v = vars || {},
		self = this,

		container = new Sprite(),
		box = new Sprite(),
		arw = new Sprite(),
		txt = new Sprite(),

		alpha = 0,
		color = v.backgroundColor || 'rgba(0,0,0,0.7)',
		arwColor = color,
		fontColor = v.fontColor || '#FFFFFF',
		fontFamily = v.fontFamily || 'Helvetica Neue, Arial, sans-serif',
		fontSize = v.fontSize|| 11,
		corRadius = v.cornerRadius || 4,
		border = v.border || 'none',
		gap = v.gap || 12,
		padding = v.padding || 4,
		shadow = v.shadow || true,

		xPerc = 0,
		yAxis = 80,
		tipOffset = 0,
		containerAboveOffset = 0,
		containerBelowOffset = 0,

		delay = vars.delay || 350,
		delayTimer;

	self.isShowing = false;

	txt.setX(padding);
	txt.setY(padding);
	txt.setWidth(100);
	txt.setTextAlign('center');
	txt.setFontColor(fontColor);
	txt.setFontFamily('Helvetica Neue');
	txt.setFontSize(fontSize);

	box.setX(gap);
	box.setY(gap);
	box.setBorderRadius(corRadius);
	box.setBorder(border);
	box.setBackgroundColor(color);
	if(shadow) {
		box.element.style.boxShadow = '0px 1px 3px rgba(0,0,0, 0.5)';
	}

	container.setAlpha(alpha);
	container.setOverflow('hidden');
	container.setPointerEvents('none');

	arw.w = 8;
	arw.point = arw.w * 0.5;
	arw.h = 6;
	arw.setBorderRight(arw.point + 'px solid transparent');
	arw.setBorderLeft(arw.point + 'px solid transparent');

	box.addChild(txt);
	container.addChild(box);
	container.addChild(arw);

	function update(e) {
		txt.setText(e.target.tooltip);
		txt.setAlpha(1);
		box.setWidth(txt.getWidth() + (padding * 2));
		box.setHeight(txt.getHeight() + (padding * 2));
		container.setWidth(box.getWidth() + (gap * 2));
		container.setHeight(box.getHeight() + (gap * 2));
		containerAboveOffset = tipOffset + container.getHeight() + (gap * 0.5);
		containerBelowOffset = tipOffset + gap;
	}

	function setXPos(e) {
		xPerc = e.pageX / window.innerWidth;
		var tipX = Math.round(container.getWidth() * xPerc);
		if((tipX - arw.point - corRadius) < gap) {
			arw.setX(corRadius + gap);
			container.setX(e.pageX - (gap + arw.point + corRadius));
		} else if((tipX + gap + arw.point + corRadius) > container.getWidth()) {
			arw.setX((container.getWidth() - (gap + arw.getWidth() + corRadius)));
			container.setX((e.pageX - container.getWidth()) + gap + arw.point + corRadius);
		} else {
			arw.setX(tipX - (gap - arw.getWidth()));
			container.setX(e.pageX - tipX);
		}
	}

	function setYPos(e) {
		if(e.pageY < yAxis) {
			arw.setBorderTop(arw.h + 'px solid transparent');
			arw.setBorderBottom(arw.h + 'px solid ' + arwColor);
			container.setY(e.pageY + containerBelowOffset);
			arw.setY(box.y - arw.getHeight());
		} else {
			arw.setBorderBottom(arw.h + 'px solid transparent');
			arw.setBorderTop(arw.h + 'px solid ' + arwColor);
			container.setY(e.pageY - containerAboveOffset);
			arw.setY(box.getHeight() + gap);
		}
	}

	function move(e) {
		setXPos(e);
		setYPos(e);
	}

	self.show = function(e) {
		if(e.target.tooltip) {
			e.target.addEventListener(MOUSE_MOVE, move);
			e.target.addEventListener(MOUSE_OUT, hide);
			container.setTransition(0);
			container.setZIndex(1000);
			delayTimer = setTimeout(function() {
				Tween(container, 0.35, {alpha: 1});
			}, delay);
			update(e);
			self.isShowing = true;
		}
	};

	function hide(e) {
		if(delayTimer) clearTimeout(delayTimer);
		e.target.removeEventListener(MOUSE_MOVE, move);
		e.target.removeEventListener(MOUSE_OUT, hide);
		container.setAlpha(0);
		container.setWidth(0);
		container.setHeight(0);
		container.setZIndex(0);
		self.isShowing = false;
	}

	stage.addChild(container);

}


function TouchNav(vars) {

	var self = this,
		moveDistanceX = 0,
		moveDistanceY = 0,
		touchStartPointX = 0,
		touchStartPointY = 0,
		touchTimeStart,
		touchTimeEnd,
		isMove = false,
		sectionLength,
		assetId;

		self.startdrag = false;

	self.touchStart = function(e) {
		if(e.touches.length === 1 && stage.getZoom() <= 1) {
			/*e.preventDefault();*/
			self.startdrag = true;
			moveDistanceX = 0;
			moveDistanceY = 0;
			touchStartPointX = e.touches[0].pageX;
			touchStartPointY = e.touches[0].pageY;
			touchTimeStart = new Date().getTime();
			vars.events.dispatchEvent(TOUCH_NAVIGATION_START);
			e.target.addEventListener(TOUCH_MOVE, touchMove, false);
			stage.addEventListener(TOUCH_END, touchEnd, false);
			e.target.addEventListener(TOUCH_CANCEL, touchEnd, false);
		}
	};

	self.update = function(e) {
		sectionLength = e.section.media ? csvToArray(e.section.media).length : csvToArray(e.section).length;
		assetId = e.assetId;
	};

	var sectionEnd = false;

	function touchMove(e) {
		if(self.startdrag) {
			moveDistanceX = e.touches[0].pageX - touchStartPointX;
			moveDistanceY = e.touches[0].pageY - touchStartPointY;

			if(isSwipeDirection() && (!isMove || self.swipeDirection !== getSwipeDirection(moveDistanceX, moveDistanceY))) {
				e.preventDefault();
				isMove = true;
				self.swipeDirection = getSwipeDirection(moveDistanceX, moveDistanceY);
				var navDir = self.swipeDirection === 'left' ? 'next' : 'prev';
				sectionEnd = getSectionEnd(navDir, assetId, sectionLength);
				vars.events.dispatchEvent(TOUCH_NAVIGATION_MOVE, navDir);
			}

			if(self.oldMediaItem && sectionLength > 1 && isSwipeDirection()) {
				self.oldMediaItem.setTransition(0);
				self.oldMediaItem.setTranslateX(moveDistanceX);
			}

			if(self.newMediaItem) {
				if(!sectionEnd && isSwipeDirection()) {
					if(self.swipeDirection == 'left') {
						self.newMediaItem.setTransition(0);
						self.newMediaItem.setTranslateX(moveDistanceX + layoutCalcs.mediaView.width());
					} else if(self.swipeDirection == 'right') {
						self.newMediaItem.setTransition(0);
						self.newMediaItem.setTranslateX(moveDistanceX - layoutCalcs.mediaView.width());
					}
				} else if(sectionEnd) {
					self.newMediaItem.setTransition(0);
					self.newMediaItem.setTranslateX(moveDistanceX);
				}
			}
		}
	}

	function isSwipeDirection() {
		return (getSwipeDirection(moveDistanceX, moveDistanceY) === 'left' || getSwipeDirection(moveDistanceX, moveDistanceY) === 'right');
	}

 	function getSectionEnd(direction, id, length) {
		if(length < 2) {
			return true;
		} else if(id === length - 1 && direction === 'prev') {
			return false;
		} else if(id === 0 && direction === 'next') {
			return false;
		}
		return !(id < length - 1 && id > 0);
	}

	function getSwipeDirection(moveDistanceX, moveDistanceY) {
		if(Mth.abs(moveDistanceX) > Mth.abs(moveDistanceY)) {
			return moveDistanceX > 0 ? 'right' : 'left';
		} else if(Mth.abs(moveDistanceX) < Mth.abs(moveDistanceY)) {
			return moveDistanceY > 0 ? 'down' : 'up';
		}
		return false;
	}

	function touchEnd(e) {
		e.target.removeEventListener(TOUCH_MOVE, touchMove, false);
		stage.removeEventListener(TOUCH_END, touchEnd, false);
		e.target.removeEventListener(TOUCH_CANCEL, touchEnd, false);
		touchTimeEnd = new Date().getTime();
		self.startdrag = false;
		self.swipeDirection = false;
		vars.events.dispatchEvent(TOUCH_NAVIGATION_END, {flickSpeed:flickSpeed(touchTimeStart, touchTimeEnd), isMove:isMove});
		isMove = false;
		sectionEnd = false;
	}

	function flickSpeed(start, end) {
		var flickSpeed = ( ( end - start ) / Math.abs(moveDistanceX) );
		flickSpeed > 1.5 ? flickSpeed = 1.5 : flickSpeed;
		flickSpeed < 0.5 ? flickSpeed = 0.5 : flickSpeed;
		return flickSpeed * 0.4;
	}

	function getTouchMoveThreshold() {
		return Math.round(layoutCalcs.mediaView.width() * 0.02);
	}
}

function Transition() {

}

function Popup(vars, info) {

	var self = new ViewProxy({events:vars.events}),
		blocker = new Sprite(),
		updateSpeed = 0.35;
	self.setOverflow('hidden');

	self.events.addEventListener(RESIZE_END, function(e) {
		self.resize();
	});

	init();

	self.resize = function() {
		Tween(blocker, updateSpeed, {width:stage.getWidth(), height:stage.getHeight()});
		centerDiv(Popup.plusDiv, updateSpeed);
		centerDiv(Popup.likeDiv, updateSpeed);
	};

	function init() {

		buildBlocker();
		self.setZIndex(1000);
		setTimeout(function() {
			if(info.label.indexOf('like') > -1) {
				buildLike();
			} else {
				buildPlusOne();
			}
		}, updateSpeed * 1000);
		self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: [blocker], state:'add'});
	}

	function buildPlusOne() {
		buildPlusOneDiv();
		injectPlusOneScript();
	}

	function buildLike() {
		buildLikeDiv();
		injectLikeScript();
	}

	function injectLikeScript() {
		if(!document.getElementById('likesrc')) {
			var fileref = document.createElement('script');
			fileref.setAttribute('id', 'likesrc');
			fileref.setAttribute("type", "text/javascript");
			fileref.setAttribute("src", 'http://connect.facebook.net/en_US/all.js#xfbml=1');
			document.getElementsByTagName("head")[0].appendChild(fileref);
		}
	}

	function injectPlusOneScript() {
		if(!document.getElementById('plus1src')) {
			var fileref = document.createElement('script');
			fileref.setAttribute('id', 'plus1src');
			fileref.setAttribute("type", "text/javascript");
			fileref.setAttribute("src", 'https://apis.google.com/js/plusone.js');
			document.getElementsByTagName("head")[0].appendChild(fileref);
		}
	}

	function buildPlusOneDiv() {
		if(!Popup.plusDiv) {
			var plusDiv = makeDiv('plus1', 100, 40);
			plusDiv.setOverflow('hidden');
			plusDiv.setInnerHTML("<div class='g-plusone' data-href='" + info.path + "' size='medium'></div>");
			var closeButton = makeCloseButton();
			closeButton.setX(plusDiv.getWidth() - 2);
			closeButton.setY(2);
			plusDiv.addChild(closeButton);
			stage.addChild(plusDiv);
			Popup.plusDiv = plusDiv;
			Popup.plusClose = closeButton;
		} else {
			Popup.plusDiv.setAlpha(0);
			Popup.plusDiv.setDisplay("block");
			centerDiv(Popup.plusDiv, 0);
			Popup.plusDiv.setAlpha(1);
		}
		Popup.plusClose.addEventListener(CLICK, close);
	}

	function buildLikeDiv() {
		if(!Popup.likeDiv) {
			var likeDiv = makeDiv('like', 320, 40);
			likeDiv.setOverflow('visible');
			likeDiv.setInnerHTML("<div id='fb-root'></div><fb:like send='true' data-width='250' show_faces='false'></fb:like>");
			var closeButton = makeCloseButton();
			closeButton.setX(likeDiv.getWidth() - 2);
			closeButton.setY(5);
			likeDiv.addChild(closeButton);
			stage.addChild(likeDiv);
			Popup.likeDiv = likeDiv;
			Popup.likeClose = closeButton;
		} else {
			Popup.likeDiv.setAlpha(0);
			Popup.likeDiv.setDisplay("block");
			centerDiv(Popup.likeDiv, 0);
			Popup.likeDiv.setAlpha(1);
		}
		Popup.likeClose.addEventListener(CLICK, close);
	}

	function buildBlocker() {
		blocker.setBackgroundColor(hexToRgba('#000000', 0.65));
		blocker.setWidth(stage.getWidth());
		blocker.setAlpha(0);
		blocker.addEventListener(CLICK, close);
		self.addChild(blocker);
		blocker.setHeight(stage.getHeight());
		stage.addChild(self);
		Tween(blocker, updateSpeed, {alpha:1});
	}

	function makeDiv(name, width, height) {
		var div = new Sprite();
		div.element.setAttribute('id', name);
		div.setBackgroundColor(hexToRgba('#FFFFFF', 0.85));
		div.setX((stage.getWidth() - width) * 0.5);
		div.setY((stage.getHeight() - height) * 0.5);
		div.setWidth(width);
		div.setHeight(height);
		div.setPaddingTop(20);
		div.setPaddingBottom(2);
		div.setPaddingLeft(15);
		div.setZIndex(2000);
		return div;
	}

	function makeCloseButton() {
		var button = new Svg();
		button.setWidth(14);
		button.setHeight(14);
		button.setRotate(0);
		var icon = new Path();
		icon.id = 'icon';
		icon.setD(svgPaths.close);
		icon.setStrokeWidth(2);
		icon.setFill('none');
		icon.setStroke('#555');
		button.addChild(icon);

		return button;
	}

	function centerDiv(div, speed) {
		if(div && div.style.getDisplay() === 'block') {
			var centerX = (stage.getWidth() - div.getWidth()) * 0.5;
			var centerY = (stage.getHeight() - div.getHeight()) * 0.5;
			Tween(div, speed, {x:centerX, y:centerY});
		}
	}

	function close(e) {
		e.target.removeEventListener(CLICK, close);
		Tween(blocker, updateSpeed, {alpha:0, onComplete:function() {
			stage.removeChild(self);
			self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: [blocker], state:'remove'});
		}});
		if(Popup.plusDiv) Popup.plusDiv.setDisplay("none");
		if(Popup.likeDiv) Popup.likeDiv.setDisplay("none");
	}

	return self;
}

function Tween(target, duration, vars) {

	var delay = vars.delay || 0,
		rAF;

	function init() {

		setTransition(duration);

		var i; for(i in vars) {
			if(!/delay|ease|overwrite|css|onComplete|onCompleteScope|onCompleteParams|onUpdate|onUpdateScope|onUpdateParams/.test(i)) {
				setStyle(i, vars[i]);
			}
		}

		if(vars.onUpdate) rAF = requestAnimationFrame(update);

		setTimeout(complete, duration * 1000);
	}

	function update() {
		if(rAF) rAF = requestAnimationFrame(update);
		vars.onUpdate.apply(vars.onUpdateScope || target, vars.onUpdateParams || [false]);
	}

	function complete() {
		if(rAF) cancelAnimationFrame(rAF);
		if(vars.onComplete) vars.onComplete.apply(vars.onCompleteScope || target, vars.onCompleteParams || [false]);
	}

	function setTransition(value) {
		if(value) {
			value = 'all ' + value + 's';
			if(vars.ease) value += ' ' + vars.ease;
		} else {
			value = '';
		}
		if(target.style) {
			target.style.WebkitTransition = value;
			target.style.MozTransition = value;
			target.style.OTransition = value;
			target.style.transition = value;
		}
	}

	function setStyle(style, value) {
		if(target.element) {
			target['set' + style.charAt(0).toUpperCase() + style.slice(1)](value);
		} else if(target.style) {
			target.style[style] = value;
		}
	}

	setTimeout(init, delay * 1000);

}


function Bitmap(vars) {
	vars = vars || {};
	vars.type = 'img';
	vars.className = 'Bitmap';
	return new Sprite(vars);
}

function Button(vars) {
	vars = vars || {};
	vars.type = 'button';
	vars.className = 'Button';
	return new Sprite(vars);
}

function ControllerProxy(vars) {

	var proxy = vars || {};

	proxy.events = proxy.events || new EventSandbox();

	var template = {
		listeners: proxy.events.listeners,
		dispatchEvent: proxy.events.dispatchEvent,
		addEventListener: proxy.events.addEventListener,
		removeEventListener: proxy.events.removeEventListener
	};

	var i;

	for(i in template) {
		proxy[i] = template[i];
	}

	return proxy;
}

function EventSandbox() {

	function findInArray(needle, haystack) {
		var i = haystack.length;
		while(i--) {
			if(needle.callback === haystack[i].callback && needle.target === haystack[i].target) {
				return true;
			}
		}
		return false;
	}

	return {
		listeners: {},
		addEventListener: function(type, callback) {
			if(!this.listeners[type]) this.listeners[type] = [];
			var newListener = {type:type, callback:callback, target:this};
			if(!findInArray(newListener, this.listeners[type])) {
				this.listeners[type].push(newListener);
			}

		},
		removeEventListener: function(type, callback) {
			if(!this.listeners[type]) {
				stack('removeEventListener: type not registered');
			}
			if(!callback || typeof callback !== 'function') {
				delete this.listeners[type];
			} else if(this.listeners[type]) {
				var i = 0,
					l = this.listeners[type].length;
				for(;i < l; i ++) {
					if(this.listeners[type][i] && this.listeners[type][i].callback === callback && this.listeners[type][i].target === this) {
						this.listeners[type].splice(i, 1);
					}
				}
			}
		},
		removeAllListeners: function(target) {
			var i;
			for(i in this.listeners) {
				var j = 0,
				l = this.listeners[i].length;
				for(;j < l; j++) {
					if(this.listeners[i][j] && this.listeners[i][j].target === target) {
						this.listeners[i].splice(j, 1);
					}
				}
			}
		},
		dispatchEvent: function(type, data) {
			if(this.listeners[type]) {
				for(var i = 0, l = this.listeners[type].length; i < l; i ++) {
					if(this.listeners[type][i] && !this.listeners[type][i].callback) {
						stack('VALID CALLBACK NOT FOUND ON LISTENER: ', this.listeners[type][i]);
						break;
					}
					if(this.listeners[type][i] && this.listeners[type][i].target === this) {
						this.listeners[type][i].callback.apply(this.listeners[type][i].target, [data]);
					}
				}
			}
		}
	}
}

var globalEvents = new EventSandbox();

function Input(vars) {
	vars = vars || {};
	vars.type = 'input';
	vars.className = 'Input';
	var input = new Sprite(vars);
	input.setBorderRadius(0);
	input.style['-webkit-appearance'] = 'none';
	input.element.addEventListener('focus', function() {
		if(BROWSER_NAME === 'Safari') document.webkitCancelFullScreen();
		stage.disableKeyNavigation = true;
	});
	input.element.addEventListener('blur', function() {
		stage.disableKeyNavigation = false;
	});
	return input;
}

function LoadingIndicator(vars) {

	vars = vars || {};

	var _type = vars.type || 'spinner',
		_alpha = vars.alpha || 0.75,
		_color = vars.color || '#FFFFFF';

	var self = new Sprite();
	self.setWidth(24);
	self.setHeight(24);
	self.setAlpha(_alpha);
	self.setOverflow('hidden');

	var png = new Bitmap();
	png.setWidth(24);
	png.setHeight(288);
	if(_color === '#FFFFFF') {
		png.setSrc(REWRITE_BASE + ICONS + 'loader1.png');
	} else {
		png.setSrc(REWRITE_BASE + ICONS + 'loader2.png');
	}
	self.addChild(png);

	function move() {
		if(_interval) {
			if(png.getY() > -264) {
				png.setY(png.getY() - 24);
			} else {
				png.setY(0);
			}
		}
	}

	var _interval = setInterval(move, 80);

	self.removeLoader = function() {
		clearInterval(_interval);
	};

	return self;
}

function Sprite(vars) {

	vars = vars || {};
	vars.events = vars.events || globalEvents;

	var element;

	if(vars.type === 'svg' || vars.type === 'g' || vars.type === 'circle' || vars.type === 'ellipse' || vars.type === 'line' || vars.type === 'path' || vars.type === 'polygon' || vars.type === 'polyline' || vars.type === 'rect') {
		element = document.createElementNS("http://www.w3.org/2000/svg", vars.type);
	} else {
		element = document.createElement(vars.type || 'div');
	}

	var style = element.style;
	var _private = {};

	style.opacity = vars.alpha || 1;
	style.display = vars.display || 'block';
	style.position =  vars.position || 'absolute';

	if(vars.border) {
		style.border = vars.border;
	}

	var proxy = {

		className: vars.className || 'Sprite',

		element: element,

		children: [],

		listeners: vars.events.listeners,
		dispatchEvent: vars.events.dispatchEvent,
		addEventListener: function(type, callback) {
			var l = ELEMENT_EVENTS.length;
			while(l--) {
				if(type === ELEMENT_EVENTS[l]) {
					element.addEventListener(type, this.event, false);
				}
			}
			vars.events.addEventListener.apply(this, [type, callback]);
		},
		removeEventListener: function(type, callback) {
			var l = ELEMENT_EVENTS.length;
			while(l--) {
				if(type === ELEMENT_EVENTS[l]) {
					element.removeEventListener(type, this.event, false);
				}
			}
			vars.events.removeEventListener.apply(this, [type, callback]);
		},
		removeAllListeners: function() {
			vars.events.removeAllListeners.call(this);
		},

		addChild: function(child) {
			if(!element[child.element]) {
				element.appendChild(child.element);
			}
			child.parent = this;
			this.children.push(child);
			child.dispatchEvent(CHILD_ADDED, child);
		},

		childAdded: function(child) {
			var i = this.children.length;
			while(i--) {
				if(this.children[i].topParentName === 'Stage' && this.children[i] === child) {
					this.children[i].dispatchEvent(CHILD_ADDED, this.children[i]);
				}
			}
		},

		contains: function(value) {
			var i = this.children.length;
			while(i--) {
				if(this.children[i] === value) {
					return true;
				}
			}
			return false;
		},

		getChildAt: function(index) {
			return this.children[index];
		},

		moveToTop: function(child) {
			var highestZ = 0;
			var i = this.children.length;
			while(i--) {
				if(this.children[i].getZIndex() >= highestZ && !this.children[i] === child) {
					highestZ = this.children[i].getZIndex() + 1;
				} else {
					this.children[i].setZIndex(this.children[i].getZIndex()-1);
				}
			}
			child.setZIndex(highestZ);
		},

		gettopParentName: function() {
			if(this.parent) {
				return this.parent.topParentName;
			}
			return name;
		},

		removeChild: function(child) {
			if(child.events) child.events.removeAllListeners(child);
			if(child.removeLoader) child.removeLoader();
			if(child.element.parentNode) {
				element.removeChild(child.element);
			}
			var i = this.children.length;
			while(i--) {
				if(this.children[i] === child) {
					this.children.splice(i, 1);
				}
			}
			child = null;
		},

		removeChildren: function(parent) {
			if(parent && parent.children && parent.children.length > 0) {
				var i = parent.children.length;
				while(i--) {
					this.removeChildren(parent.children[i]);
					parent.removeChild(parent.children[i]);
				}
			}
		},

		getClass: function() {
			return element.className;
		},

		setClass: function(value) {
			element.setAttribute('class', value);
		},

		hasClass: function(cls) {
			return new RegExp('(^|\\s)' + cls + '(\\s|$)').test(element.className);
		},

		event: function(e) {
			if(!_private.selectable && !TOUCH_DEVICE && vars.type !== 'input' && vars.type !== 'textarea') {
				e.preventDefault();
			}
			if(vars.type === 'img') {
				if(e.target.width && !vars.width) {
					proxy.setWidth(e.target.width);
				}
				if(e.target.height && !vars.height) {
					proxy.setHeight(e.target.height);
				}
			}
			var event = {
				type: e.type,
				target:proxy,
				currentTarget:proxy,
				keyCode: e.which,
				pageX:e.pageX,
				pageY:e.pageY,
				clientX:e.clientX,
				clientY:e.clientY,
				offsetX:e.offsetX || e.layerX,
				offsetY:e.offsetY || e.layerY,
				mouseX: e.clientX - proxy.getStageX(),
				mouseY: e.clientY - proxy.getStageY(),
				preventDefault: function() {
					e.preventDefault();
				},
				stopPropagation: function() {
					e.stopPropagation();
				},
				dataTransfer: e.dataTransfer
			};

			if(e.type === FOCUS) {
				if(stage.activeFocus !== proxy) {
					if(stage.activeFocus) stage.activeFocus.dispatchEvent(BLUR, event);
					stage.activeFocus = proxy;
				}
			}

			if(e.type === CLICK || e.type === MOUSE_DOWN || e.type === TOUCH_START) {
				if(stage.activeFocus) stage.activeFocus.dispatchEvent(BLUR, event);
				proxy.focus();
				stage.activeFocus = proxy;
				setTimer(proxy.dispatchEvent, 0, [FOCUS, event], proxy);
			}

			if(e.touches) event.touches = e.touches;
			proxy.dispatchEvent(e.type, event);
		},

		hitTestPoint: function(xPoint, yPoint) {
			if(!_private.width) _private.width = element.offsetWidth || element.scrollWidth || _private.width || 0;
			return style.display !== 'none' && xPoint >= this.getStageX() && xPoint <= this.getStageX() + _private.width && yPoint >= this.getStageY() && yPoint <= this.getStageY() + _private.height;
		},

		hitTestAbs: function(xPoint, yPoint) {
			return style.display !== 'none' && xPoint >= this.getAbsX() && xPoint <= this.getAbsX() + _private.width && yPoint >= this.getAbsY() && yPoint <= this.getAbsY() + _private.height;
		},

		globalToLocal: function(point) {
			return {x:point.x - this.getStageX(), y:point.y - this.getStageY()};
		},

		localToGlobal: function(point) {
			return {x:point.x + this.getStageX(), y:point.y + this.getStageY()};
		},

		/* general */
		getSrc: function() {
			return _private.src;
		},
		setSrc: function(value) {
			if(value) {
				_private.src = value;
				element.setAttribute('src', value);
			} else {
				stack(value);
			}
		},

		getText: function() {
			return _private.text;
		},
		setText: function(value) {
			_private.text = value;
			element.innerHTML = value;
		},

		getInnerHTML: function() {
			return _private.text;
		},
		setInnerHTML: function(value) {
			_private.text = value;
			element.innerHTML = value;
		},

		getAlpha: function() {
			return _private.alpha;
		},
		setAlpha: function(value) {
			_private.alpha = value;
			style.opacity = value;
		},

		getOpacity: function() {
			return _private.alpha;
		},
		setOpacity: function(value) {
			_private.alpha = value;
			style.opacity = value;
		},

		getNumChildren: function() {
			return this.children.length;
		},

		getDisplay: function() {
			return _private.display;
		},
		setDisplay: function(value) {
			_private.display = value;
			style.display = value;
		},

		getPosition: function() {
			return _private.position;
		},
		setPosition: function(value) {
			_private.position = value;
			style.position = value;
		},

		getTop: function() {
			return _private.y || 0;
		},
		setTop: function(value) {
			_private.y = value;
			style.top = value + 'px';
		},

		getX: function() {
			return element.offsetLeft || _private.x || 0;
		},
		setX: function(value) {
			_private.x = value;
			if(vars.type === 'rect') {
				element.setAttribute('x', value);
			} else {
				style.left = value + 'px';
			}
		},

		getY: function() {
			return element.offsetTop || _private.y || 0;
		},
		setY: function(value) {
			_private.y = value;
			if(vars.type === 'rect') {
				element.setAttribute('y', value);
			} else {
				style.top = value + 'px';
			}
		},

		getTranslate: function() {
			return _private.translate;
		},
		setTranslate: function(value) {
			_private.translate = value;
			_private.translateX = value[0];
			_private.translateY = value[1];
			style['-webkit-transform'] = 'translate3d(' + _private.translateX + 'px,' + _private.translateY + 'px, 0px)';
			style['-ms-transform'] = 'translate3d(' + _private.translateX + 'px,' + _private.translateY + 'px, 0px)';
			style['transform'] = 'translate3d(' + _private.translateX + 'px,' + _private.translateY + 'px, 0px)';
		},

		getTranslateX: function() {
			var matrix = new TransformMatrix(element);
			return matrix.x;
		},
		setTranslateX: function(value) {
			_private.translateX = value;
			_private.translateY = _private.translateY || 0;
			style['-webkit-transform'] = 'translate3d(' + _private.translateX + 'px,' + _private.translateY + 'px, 0px)';
			style['-ms-transform'] = 'translate3d(' + _private.translateX + 'px,' + _private.translateY + 'px, 0px)';
			style['transform'] = 'translate3d(' + _private.translateX + 'px,' + _private.translateY + 'px, 0px)';
		},

		getTranslateY: function() {
			var matrix = new TransformMatrix(element);
			return matrix.y;
		},
		setTranslateY: function(value) {
			_private.translateY = value;
			_private.translateX = _private.translateX || 0;
			style['-webkit-transform'] = 'translate3d(' + _private.translateX + 'px,' + _private.translateY + 'px, 0px)';
			style['-ms-transform'] = 'translate3d(' + _private.translateX + 'px,' + _private.translateY + 'px, 0px)';
			style['transform'] = 'translate3d(' + _private.translateX + 'px,' + _private.translateY + 'px, 0px)';
		},

		getTranslateZ: function() {
			return _private.translateZ;
		},
		setTranslateZ: function(value) {
			_private.translateZ = value;
			_private.translateZ = _private.translateZ || 0;
			style['-webkit-transform'] = 'translate3d(' + _private.translateZ + 'px,' + _private.translateZ + 'px, 0px)';
			style['-ms-transform'] = 'translate3d(' + _private.translateZ + 'px,' + _private.translateZ + 'px, 0px)';
			style['transform'] = 'translate3d(' + _private.translateZ + 'px,' + _private.translateZ + 'px, 0px)';
		},

		getTransition: function() {
			return _private.transition;
		},
		setTransition: function(value) {
			_private.transition = value;
			value = value === 0 ? 'none' : 'all ' + value + 's cubic-bezier(1, 0, 0.3, 1)';
			style.WebkitTransition = value;
			style.MozTransition = value;
			style.OTransition = value;
			style.transition = value;
		},

		getStageX: function() {
			if(this.parent) {
				return this.parent.getStageX() + this.getX();
			}
			return this.getX();
		},

		getStageY: function() {
			if(this.parent) {
				return this.parent.getStageY() + this.getY();
			}
			return this.getY();
		},

		getAbsX: function() {
			var x = 0;
			var el = this.element;
			if(el.offsetParent) {
				do { x += el.offsetLeft; } while(el = el.offsetParent);
			}
			return x;
		},

		getAbsY: function() {
			var y = 0;
			var el = this.element;
			if(el.offsetParent) {
				do { y += el.offsetTop; } while(el = el.offsetParent);
			}
			return y;
		},

		getRight: function() {
			return _private.right;
		},
		setRight: function(value) {
			_private.right = value;
			style.right = value + 'px';
		},

		getBottom: function() {
			return _private.bottom;
		},
		setBottom: function(value) {
			_private.bottom = value;
			style.bottom = value + 'px';
		},

		getLeft: function() {
			return _private.x || 0;
		},
		setLeft: function(value) {
			_private.x = value;
			style.left = value + 'px';
		},

		getWidth: function() {
			_private.width = element.offsetWidth || _private.width || 0;
			return _private.width;
		},
		setWidth: function(value) {
			_private.width = value;
			if(vars.type === 'svg' || vars.type === 'rect') {
				element.setAttribute('width', value);
			} else {
				style.width = value + 'px';
			}
		},

		getHeight: function() {
			_private.height = element.offsetHeight || _private.height || 0;
			return _private.height;
		},
		setHeight: function(value) {
			_private.height = value;
			if(vars.type === 'svg' || vars.type === 'rect') {
				element.setAttribute('height', value);
			} else {
				style.height = value + 'px';
			}
		},

		getZIndex: function() {
			return _private.zIndex;
		},
		setZIndex: function(value) {
			_private.zIndex = value;
			style.zIndex = value;
		},

		getCursor: function() {
			return _private.cursor;
		},
		setCursor: function(value) {
			_private.cursor = value;
			style.cursor = value;
		},

		getShadow: function() {
			return _private.shadow;
		},
		setShadow: function(value) {
			_private.shadow = value;
			style.boxShadow = value;
		},

		getVisibility: function() {
			return _private.visibility;
		},
		setVisibility: function(value) {
			_private.visibility = value;
			style.visibility = value;
		},

		getOverflow: function() {
			return _private.overflow;
		},
		setOverflow: function(value) {
			_private.overflow = value;
			style.overflow = value;
		},


		/* background */

		getBackground: function() {
			return _private.background;
		},
		setBackground: function(value) {
			_private.background = value;
			style.background = value;
		},


		getBackgroundColor: function() {
			return _private.bgColor;
		},
		setBackgroundColor: function(value) {
			_private.bgColor = value;
			style.backgroundColor = value;
		},

		getParent: function(){
			return _private.parent;
		},
		setParent: function(value){
			_private.parent = value;
		},

		/* border */

		getBorder: function() {
			return _private.border;
		},
		setBorder: function(value) {
			_private.border = value;
			style.border = value;
		},
		getBorderTop: function() {
			return _private.borderTop;
		},
		setBorderTop: function(value) {
			_private.borderTop = value;
			style.borderTop = value;
		},
		getBorderRight: function() {
			return _private.borderRight;
		},
		setBorderRight: function(value) {
			_private.borderRight = value;
			style.borderRight = value;
		},
		getBorderBottom: function() {
			return _private.borderBottom;
		},
		setBorderBottom: function(value) {
			_private.borderBottom = value;
			style.borderBottom = value;
		},
		getBorderLeft: function() {
			return _private.borderLeft;
		},
		setBorderLeft: function(value) {
			_private.borderLeft = value;
			style.borderLeft = value;
		},

		getBorderColor: function() {
			return _private.borderColor;
		},
		setBorderColor: function(value) {
			_private.borderColor = value;
			style.borderColor = value;
		},

		getBorderRadius: function() {
			return _private.borderRadius;
		},
		setBorderRadius: function(value) {
			_private.borderRadius = value;
			style.borderRadius = value + 'px';
		},


		/* fonts */

		getFont: function() {
			return _private.font;
		},
		setFont: function(value) {
			_private.font = value;
			style.font = value;
		},

		getFontColor: function() {
			return _private.fontColor;
		},
		setFontColor: function(value) {
			_private.fontColor = value;
			style.color = value;
		},

		getFontSize: function() {
			return _private.fontSize;
		},
		setFontSize: function(value) {
			_private.fontSize = value;
			if(isNaN(value) && value.search('%') > -1) {
				style.fontSize = value;
			} else if(isNaN(value) && value.search('px') > -1) {
				style.fontSize = value;
			} else if(!isNaN(value)) {
				style.fontSize = value + 'px';
			} else {
				style.fontSize = value;
			}
		},

		getFontFamily: function() {
			return _private.fontFamily;
		},
		setFontFamily: function(value) {
			_private.fontFamily = value;
			style.fontFamily = value;
		},

		getFontWeight: function() {
			return _private.fontWeight;
		},
		setFontWeight: function(value) {
			_private.fontWeight = value;
			style.fontWeight = value;
		},

		getTextAlign: function() {
			return _private.textAlign;
		},
		setTextAlign: function(value) {
			_private.textAlign = value;
			style.textAlign = value;
		},

		getTextAlignVertical: function() {
			return _private.textAlignVertical;
		},
		setTextAlignVertical: function(value) {
			_private.textAlignVertical = value;
			if(value === 'middle' || value === 'bottom') {
				style.position = 'relative';
				style.display = 'table-cell';
			}
			style.verticalAlign = value;
		},

		getTextDecoration: function() {
			return _private.textDecoration;
		},
		setTextDecoration: function(value) {
			_private.textDecoration = value;
			style.textDecoration = value;
		},

		getTextTransform: function() {
			return _private.textTransform;
		},
		setTextTransform: function(value) {
			_private.textTransform = value;
			style.textTransform = value;
		},

		getTextWrap: function() {
			return _private.textWrap;
		},
		setTextWrap: function(value) {
			_private.textWrap = value;
			style.whiteSpace = value ? 'normal' : 'nowrap';
		},

		getLetterSpacing: function() {
			return _private.letterSpacing;
		},
		setLetterSpacing: function(value) {
			_private.letterSpacing = value;
			style.letterSpacing = value;
		},

		getLineHeight: function() {
			return _private.lineHeight;
		},
		setLineHeight: function(value) {
			_private.lineHeight = value;
			style.lineHeight = value;
		},

		getList: function() {
			return _private.list;
		},
		setList: function(value) {
			_private.list = value;
			style.list = value;
		},

		getValue: function() {
			return element.value;
		},
		setValue: function(value) {
			element.value = value;
		},


		/* margin */

		getMargin: function() {
			return _private.margin;
		},
		setMargin: function(value) {
			_private.margin = value;
			style.margin = value + 'px';
		},

		getMarginTop: function() {
			return _private.marginTop;
		},
		setMarginTop: function(value) {
			_private.marginTop = value;
			style.marginTop = value + 'px';
		},

		getMarginRight: function() {
			return _private.marginRight;
		},
		setMarginRight: function(value) {
			_private.marginRight = value;
			style.marginRight = value + 'px';
		},

		getMarginBottom: function() {
			return _private.marginBottom;
		},
		setMarginBottom: function(value) {
			_private.marginBottom = value;
			style.marginBottom = value + 'px';
		},

		getMarginLeft: function() {
			return _private.marginLeft;
		},
		setMarginLeft: function(value) {
			_private.marginLeft = value;
			style.marginLeft = value + 'px';
		},


		/* padding */

		getPadding: function() {
			return _private.padding;
		},
		setPadding: function(value) {
			_private.padding = value;
			style.padding = value + 'px';
		},

		getPaddingTop: function() {
			return _private.paddingTop;
		},
		setPaddingTop: function(value) {
			_private.paddingTop = value;
			style.paddingTop = value + 'px';
		},

		getPaddingRight: function() {
			return _private.paddingRight;
		},
		setPaddingRight: function(value) {
			_private.paddingRight = value;
			style.paddingRight = value + 'px';
		},

		getPaddingBottom: function() {
			return _private.paddingBottom;
		},
		setPaddingBottom: function(value) {
			_private.paddingBottom = value;
			style.paddingBottom = value + 'px';
		},

		getPaddingLeft: function() {
			return _private.paddingLeft;
		},
		setPaddingLeft: function(value) {
			_private.paddingLeft = value;
			style.paddingLeft = value + 'px';
		},
		getSelectable: function() {
			return _private.selectable;
		},
		setSelectable: function(value) {
			_private.selectable = value;
			if(!_private.selectable && vars.type !== 'input' && vars.type !== 'textarea') {
				style.cursor = 'default';
				style['-webkit-touch-callout'] = 'none';
				style['-webkit-user-select'] = 'none';
				style['-khtml-user-select'] = 'none';
				style['-moz-user-select'] = 'none';
				style['-ms-touch-callout'] = 'none';
				style['user-select'] = 'none';
			} else {
				style.cursor = 'auto';
				style['-webkit-touch-callout'] = 'auto';
				style['-webkit-user-select'] = 'auto';
				style['-khtml-user-select'] = 'auto';
				style['-moz-user-select'] = 'auto';
				style['-ms-touch-callout'] = 'auto';
				style['user-select'] = 'auto';
			}
		},

		getRotate: function() {
			return _private.rotate;
		},
		setRotate: function(value) {
			_private.rotate = value;
			style['-webkit-transform'] = 'rotate('+value+'deg)';
			style['-moz-transform'] = 'rotate('+value+'deg)';
			style['-o-transform'] = 'rotate('+value+'deg)';
			style.transform = 'rotate('+value+'deg)';
		},

		/*svg support*/

		getD: function() {
			return element.getAttribute('d');
		},
		setD: function(value) {
			element.setAttribute('d', value);
		},

		getPoints: function() {
			return element.getAttribute('points');
		},
		setPoints: function(value) {
			element.setAttribute('points', value);
		},

		getCx: function() {
			return _private.cx;
		},
		setCx: function(value) {
			_private.cx = value;
			element.setAttribute('cx', value);
		},

		getCy: function() {
			return _private.cy;
		},
		setCy: function(value) {
			_private.cy = value;
			element.setAttribute('cy', value);
		},

		getX1: function() {
			return _private.x1;
		},
		setX1: function(value) {
			_private.x1 = value;
			element.setAttribute('x1', value);
		},

		getX1: function() {
			return _private.y1;
		},
		setY1: function(value) {
			_private.y1 = value;
			element.setAttribute('y1', value);
		},

		getX2: function() {
			return _private.x2;
		},
		setX2: function(value) {
			_private.x2 = value;
			element.setAttribute('x2', value);
		},

		getY2: function() {
			return _private.y2;
		},
		setY2: function(value) {
			_private.y2 = value;
			element.setAttribute('y2', value);
		},

		getR: function() {
			return _private.r;
		},
		setR: function(value) {
			_private.r = value;
			element.setAttribute('r', value);
		},

		getRx: function() {
			return _private.rx;
		},
		setRx: function(value) {
			_private.rx = value;
			element.setAttribute('rx', value);
		},

		getRy: function() {
			return _private.ry;
		},
		setRy: function(value) {
			_private.ry = value;
			element.setAttribute('ry', value);
		},

		getFill: function() {
			return _private.fill;
		},
		setFill: function(value) {
			_private.fill = value;
			/*element.setAttribute('fill', value);*/
			style.fill = value;
		},

		getStroke: function() {
			return _private.stroke;
		},
		setStroke: function(value) {
			_private.stroke = value;
			/*element.setAttribute('stroke', value);*/
			style.stroke = value;
		},

		getStrokeWidth: function() {
			return _private.strokeWidth;
		},
		setStrokeWidth: function(value) {
			_private.strokeWidth = value;
			element.setAttribute('stroke-width', value);
		},

		getPointerEvents: function() {
			return _private.pointerEvents;
		},
		setPointerEvents: function(value) {
			_private.pointerEvents = value;
			element.style.pointerEvents = value;
		},

		focus: function() {
			stage.activeFocus = proxy;
			if(vars.type === 'input' || vars.type === 'textarea') element.focus();
		},
		blur: function() {
			if(vars.type === 'input' || vars.type === 'textarea') element.blur();
		}

	};

	function removeListeners(child) {
		var i = child.events.listeners.length;
		while(i--) {
			child.removeEventListener(child.events.listeners[i].type, child.events.listeners[i].callback);
		}
	}

	proxy.addEventListener(CHILD_ADDED, proxy.childAdded);

	if(!_private.selectable) {
		proxy.setSelectable(false);
	}

	if(vars.id) {
		proxy.id = vars.id;
	}

	if(vars.collection) {
		proxy.setClass(vars.collection);
	}

	element.proxy = proxy;
	proxy.style = element.style;

	var i; for(i in vars) {
		proxy[i] = vars[i];
	}

	return proxy;

}

function Stage() {

	var _selectable = false,
		_zoom = 1,
		_private = {};

	var name = 'Stage';

	var stage = {

		children: [],
		dragElements: [],
		activeFocus:undefined,
		element:document.body,
		listeners: globalEvents.listeners,
		dispatchEvent: globalEvents.dispatchEvent,
		addEventListener: globalEvents.addEventListener,
		removeEventListener: globalEvents.removeEventListener,

		addChild: function(child) {
			if(!child.element.parentNode) {
				document.body.appendChild(child.element);
			}
			child.parent = this;
			this.children.push(child);
			child.dispatchEvent(CHILD_ADDED, child);
		},

		getTopParentName: function() {
			return name;
		},

		addChildAt: function(child, index) {

		},

		removeChild: function(child) {
			if(child.events) child.events.removeAllListeners(child);
			if(child.removeLoader) child.removeLoader();
			if(child.element.parentNode) {
				document.body.removeChild(child.element);
			}
			var i = this.children.length;
			while(i--) {
				if(this.children[i] === child) {
					this.children.splice(i, 1);
				}
			}
			child = null;
		},

		removeChildren: function(parent) {
			if(parent && parent.children && parent.children.length > 0) {
				var i = parent.children.length;
				while(i--) {
					this.removeChildren(parent.children[i]);
					parent.removeChild(parent.children[i]);
				}
			}
		},

		moveToTop: function(child) {
			var highestZ = 0;
			var i = this.children.length;
			while(i--) {
				if(this.children[i].getZIndex() > highestZ && !this.children[i] === child) {
					highestZ = this.children[i].getZIndex() + 1;
				}
			}
			child.setZIndex(highestZ);
		},

		domContentLoaded: function() {
			if(!_selectable) {
				document.body.style.overflow = 'hidden';
			}
			stage.dispatchEvent(LOAD, stage);
		},

		documentReadyListener: function() {
			if(typeof document.onreadystatechange === 'object' || document.readyState === 'loading') {
				if(typeof document.onreadystatechange !== 'object') {
					window.onload = stage.domContentLoaded;
				}
				document.onreadystatechange = function() {
					if(document.readyState === 'complete') {
						stage.domContentLoaded();
						stage.style = document.body.style;
						stage.element = document.body;
					}
				};
			} else {
				document.addEventListener(LOAD, stage.domContentLoaded, false );
			}
		},

		globalToLocal: function (point) {
			return {x:point.x, y:point.y};
		},

		localToGlobal: function (point) {
			return {x:point.x, y:point.y};
		},

		event: function(e) {

			var event = {
				type:e.type,
				target:e.target,
				currentTarget:e.target,
				x:e.x,
				y:e.y,
				keyCode:e.which,
				pageX:e.pageX,
				pageY:e.pageY,
				screenX:e.screenX,
				screenY:e.screenY,
				clientX:e.clientX,
				clientY:e.clientY,
				mouseX:e.clientX - stage.getStageX(),
				mouseY:e.clientY - stage.getStageY(),
				deltaX:e.deltaX || -e.wheelDeltaX || 0,
				deltaY:e.deltaY || -e.wheelDeltaY || 0,
				preventDefault: function() {
					e.preventDefault();
				},
				stopPropagation: function() {
					e.stopPropagation();
				},
				dataTransfer: e.dataTransfer
			};

			if(e.touches) event.touches = e.touches;

			if(e.type === WHEEL || e.type === MOUSE_WHEEL) {
				event.type = MOUSE_WHEEL;
				/*event.preventDefault(); was for FF scrolling */
			}

			if(e.type === RESIZE || e.type === GESTURE_CHANGE || e.type === GESTURE_END || e.type === ORIENTATION_CHANGE) {
				_zoom = document.documentElement.clientWidth / window.innerWidth;
			}

			if(Scrolls && e.clientX && e.clientY) {
				Scrolls.hitMask(e.clientX, e.clientY);
			}

			stage.dispatchEvent(e.type, event);
		},

		getChildren: function() {
			return children;
		},
		getImages: function() {
			return document.images;
		},
		getForms: function() {
			return document.forms;
		},
		getLinks: function() {
			return document.links;
		},
		getAnchors: function() {
			return document.anchors;
		},
		getScripts: function() {
			return document.scripts;
		},
		getPlugins: function() {
			return document.plugins;
		},
		getEmbeds: function() {
			return document.embeds;
		},
		getWidth: function() {
			return window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
		},
		getHeight: function() {
			return window.innerHeight || document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
		},
		getStageX: function() {
			return 0;
		},
		getStageY: function() {
			return 0;
		},
		getSelectable: function() {
			return _selectable;
		},
		setSelectable: function(value) {
			_selectable = value;
		},
		getZoom: function() {
			return _zoom;
		},
		setZoom: function(value) {
			_zoom = value;
		},
		getBackgroundColor: function() {
			return _private.backgroundColor;
		},
		setBackgroundColor: function(value) {
			_private.backgroundColor = value;
			document.body.style.backgroundColor = value;
		},

		getTransition: function() {
			return _private.transition;
		},
		setTransition: function(value) {
			_private.transition = value;
			value = value === 0 ? 'none' : 'all ' + value + 's';
			document.body.style.WebkitTransition = value;
			document.body.style.MozTransition = value;
			document.body.style.OTransition = value;
			document.body.style.transition = value;
		},

		getDisableKeyNavigation: function() {
			return _private.disableKeyNavigation;
		},
		setDisableKeyNavigation: function(value) {
			_private.disableKeyNavigation = value;
		},

		focus: function() {
			stage.activeFocus = stage;
		},
		blur: function() {}
	};

	stage.proxy = stage;

	window.addEventListener(TOUCH_START, stage.event, false);
	window.addEventListener(TOUCH_MOVE, stage.event, false);
	window.addEventListener(TOUCH_END, stage.event, false);
	window.addEventListener(TOUCH_CANCEL, stage.event, false);
	window.addEventListener(MOUSE_OVER, stage.event, false);
	window.addEventListener(MOUSE_DOWN, stage.event, false);
	window.addEventListener(MOUSE_MOVE, stage.event, false);
	window.addEventListener(MOUSE_UP, stage.event, false);
	window.addEventListener(MOUSE_OUT, stage.event, false);
	window.addEventListener(CLICK, stage.event, false);
	window.addEventListener(RESIZE, stage.event, false);
	window.addEventListener(ORIENTATION_CHANGE, stage.event, false);
	window.addEventListener(SCROLL, stage.event, false);
	window.addEventListener(GESTURE_START, stage.event, false);
	window.addEventListener(GESTURE_CHANGE, stage.event, false);
	window.addEventListener(GESTURE_END, stage.event, false);
	window.addEventListener(KEY_UP, stage.event, false);
	window.addEventListener(KEY_DOWN, stage.event, false);

	var resizeTimer, rotateTimer;

	window.addEventListener(RESIZE, function(e) {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			stage.dispatchEvent(RESIZE_END, e);
		}, 200);
	}, false);

	window.addEventListener(ORIENTATION_CHANGE, function(e) {
		stage.dispatchEvent(ORIENTATION_CHANGE, e);
	}, false);

	window.addEventListener(WHEEL, stage.event, false);
	window.addEventListener(MOUSE_WHEEL, stage.event, false);

	stage.documentReadyListener();

	return stage;
}

var stage = Stage.call(Stage);

function addChild(child) {
	stage.addChild(child);
}

var svgPaths = {
	navArrowRight:'M1.4,11.984l4.244-4.242L1.4,3.5',
	navArrowLeft:'M6.644,3.5L2.4,7.742l4.244,4.242',
	navArrowUp:'m 1.4562498,8.21905 5.1000001,-5.4 5.7000001,5.4',
	navArrowDown:'m 1.4562498,1.44405 5.1000001,5.4 5.7000001,-5.4',
	navFullscreen:'M0,0h15v9H0V0z M2,2v5h11V2H2z',
	navEmailPixel:'M15,12V3h-0.93v1h-0.945v1h-0.93v1h-0.932v1h-0.951v1H9.375v0.992H8.438v1.001H6.562V8.991H5.625V8H4.688V7H3.742V6h-0.93V5 H1.875V4H0.93V3H0v9H15z M0.93,9.993h0.945v-1h0.938V8h0.93v0.992h-0.93v1H1.875V11H0.93V9.993z M2.812,4v1h0.93v1h0.945v1h0.938v1 h0.938v0.992h1.875V8h0.938V7h0.938V6h0.951V5h0.93V4h0.931V3H1.875v1H2.812z M11.266,8h0.93v0.992h0.93v1h0.945V11h-0.945V9.993 h-0.93v-1h-0.93V8z',
	navEmailVector:'M 10.7,3.65 15,0.1 V 9 H 0 V 0 L 4.65,3.45 0.85,7.6 1.3,8.05 5.2,3.85 7.8,5.9 10.15,4.1 13.65,7.952 14.15,7.502 10.7,3.65 z M 1.45,0 h 12.3 L 7.8,4.8 1.45,0 z',
	navInfo:'M0.576,10.401c0.384,0.922,0.924,1.724,1.621,2.404c2.891,2.908,7.741,2.946,10.624,0c2.904-2.923,2.905-7.705,0-10.629 c-2.88-2.92-7.736-2.881-10.624,0c-0.738,0.715-1.274,1.525-1.607,2.43C-0.192,6.534-0.197,8.465,0.576,10.401 C0.959,11.324,0.192,9.479,0.576,10.401z M1.9,5.131c0.311-0.753,0.745-1.403,1.303-1.95c2.34-2.383,6.292-2.274,8.654,0 c2.316,2.448,2.284,6.187,0,8.643c-2.387,2.29-6.288,2.39-8.654,0c-0.543-0.542-0.985-1.207-1.329-1.994 c-0.293-0.738-0.44-1.528-0.44-2.371C1.435,6.661,1.59,5.885,1.9,5.131C2.211,4.378,1.59,5.885,1.9,5.131z M4.792,7.981 c0.562,0,1.844-0.876,2.245-0.566C7.531,7.798,6.011,9.726,5.89,9.94c-0.605,1.064-0.754,2.451,0.931,2.24 c1.02-0.128,2.904-0.903,3.41-1.88c0.232-0.451-2.039,0.442-2.189,0.278C7.69,10.194,8.925,8.421,9.058,8.096 C10.812,3.81,4.583,6.318,4.792,7.981C4.796,7.981,4.792,7.981,4.792,7.981z M8.151,4.869c0.465,0.459,1.194,0.432,1.651-0.025 c0.701-0.62,0.341-1.849-0.572-2.041C8.067,2.56,7.326,4.053,8.151,4.869C8.38,5.094,7.923,4.644,8.151,4.869z',
	navCart:'M 1.248,8.985 H 3.254 V 6.989 H 1.248 V 8.985 z M 0,0 1.05,5.992 h 9.235 L 11.271,0.966 13.248,0.968 13.256,0 H 0 z m 3.25,3.981 h -1 v -1 h 1 v 1 z M 3.25,2 h -1 V 1 h 1 v 1 z m 2,2 h -1 V 3 h 1 v 1 z m 0,-2 h -1 V 1 h 1 v 1 z m 2,1.979 h -1 v -1 h 1 v 1 z M 7.25,2 h -1 V 1 h 1 v 1 z m 2,1.979 h -1 v -1 h 1 v 1 z M 9.25,2 h -1 V 1 h 1 V 2 z M 7.252,9.007 H 9.25 V 7.002 H 7.252 v 2.005 z',
	navThumbs:'M0,3h7v4H0V3z M0,8h7v4H0V8z M8,3h7v4H8V3z M8,8h7v4H8V8z',
	play:'M2,1l8,5.031L2,11V1z',
	pause:'M2,2h3v8H2V2z M7,2h3v8H7V2z',
	volume:'M2,7.5h1V10H2V7.5z M4,7h1v3H4V7z M6,6h1v4H6V6z M8,4h1v6H8V4z M10,2h1v8h-1V2z M0,8h1v2H0V8z',
	mute:'M2,8h1v2H2V8z M4,8h1v2H4V8z M6,8h1v2H6V8z M8,8h1v2H8V8z M10,8h1v2h-1V8z M0,8h1v2H0V8z',
	arrowLeft:'M 8.812269,1.1126749 2.1862736,7.2790289 8.876226,13.512325',
	arrowRight:'M 1.0939806,1.1126749 7.719976,7.2790289 1.0300236,13.512325',
	arrowUp:'M 1.1126747,8.812269 7.2790287,2.1862738 13.512325,8.876226',
	arrowDown:'M 1.1126747,1.0939808 7.2790287,7.719976 13.512325,1.0300238',
	arrowUpSmall:'M10.535,8.768L7,5.232L3.465,8.768',
	arrowDownSmall:'M3.465,5.232L7,8.768l3.535-3.535',
	arrowRightSmall:'M4.5,12.001L9.5,7l-5-5',
	arrowLeftSmall:'M9.5,2l-5,5.001l5,5',
	edit:'M4.244,7.759L9.203,2.8l2.151,2.151L6.396,9.909L2.8,11.397L4.244,7.759z',
	remove:'M4,7h6 M7,2.5C4.515,2.5,2.5,4.515,2.5,7c0,2.486,2.015,4.5,4.5,4.5 c2.486,0,4.5-2.014,4.5-4.5C11.5,4.515,9.485,2.5,7,2.5z',
	spacer:'M12,10H2V4h10V10z',
	eyeSmall:'M2.613,6.95c0-1.391,2.021-2.518,4.513-2.518 c2.493,0,4.514,1.127,4.514,2.518 M7.125,5.691c-0.682,0-1.235,0.553-1.235,1.235c0,0.681,0.553,1.235,1.235,1.235 c0.681,0,1.235-0.554,1.235-1.235C8.36,6.244,7.807,5.691,7.125,5.691z',
	lock:'M10,9.875H4v-5h6V9.875z M9.5,4.625c0-1.381-1.119-2.5-2.5-2.5s-2.5,1.119-2.5,2.5 M5.5,4.625c0-0.828,0.672-1.5,1.5-1.5s1.5,0.672,1.5,1.5',
	plusSmall:'M3,7h8 M7,11V3',
	listSmall:'M11,4H3V3h8V4z M11,9H3v1h8V9z M11,6H3v1h8V6z',
	thumbXSmall:'M6,6H2V3h4V6z M12,3H8v3h4V3z M6,8H2v3h4V8z M12,8H8v3h4V8z',
	close:'M3,3l8,8 M3,11l8-8',
	plus:'M 6.5591462,0.00335385 6.5033537,13.059146',
	minus:'M 0.0033543,6.5033535 13.059146,6.5591465',
	operand:'m 0.07131516,7.5193495 14.99702684,0',
	fullscreen:'M0,2h2.77v0.898H0.923v1.801H0V2z M0,8.3h0.923v1.8H2.77V11H0V8.3z M1.846,3.8h8.308v5.4H1.846V3.8z M2.77,4.699V8.3h6.46 V4.699H2.77z M9.23,2H12v2.699h-0.923V2.898H9.23V2z M9.23,10.1h1.847V8.3H12V11H9.23V10.1z',
	resize:'M5.795,11l5.121-5.125 M9.293,11l1.623-1.623 M2.314,10.979l8.608-8.607',
	arrow1Left:'M7.5,20l18.338,18.336l6.662-6.664L20.829,20L32.5,8.327l-6.662-6.663L7.5,20z',
	arrow1Right:'M7.5,8.329L19.172,20L7.5,31.674l6.664,6.662L32.5,20L14.164,1.665L7.5,8.329z',
	arrow2Left:'M9.5,20l16.792,16.151l4.208-4.046L17.913,20L30.5,7.896l-4.208-4.047L9.5,20z',
	arrow2Right:'M9.5,7.894L22.084,20L9.5,32.104l4.208,4.047L30.5,20L13.708,3.849L9.5,7.894z',
	plus1Left:'M0,15v10h40V15H0z',
	plus1Right:'M0,15v9.999h15V40h9.998V24.999H40V15H24.998V0H15v15H0z',
	plus2Left:'M5,17v6h30v-6H5z',
	plus2Right:'M5,17.001v5.998h12V35h6V22.999H35v-5.998H23.001V5h-6v12.001H5z',
	cursorThumbs:'M8,12h11v7H8V12z M8,20h11v7H8V20z M20,12h11v7H20V12z M20,20h11v7H20V20z',
	updates:'M7,3.563v4.649 M7,1.541C3.986,1.541,1.541,3.986,1.541,7c0,3.017,2.445,5.459,5.459,5.459c3.017,0,5.459-2.442,5.459-5.459C12.459,3.986,10.014,1.541,7,1.541z M6.333,9.022L7,9.832l0.665-0.81H6.333z'
};

function Svg(vars) {
	vars = vars || {};
	vars.type = vars.type || 'svg';
	vars.className = vars.className || 'Svg';
	return new Sprite(vars);
}

function SvgGroup(vars) {
	vars = vars || {};
	vars.type = 'g';
	vars.className = 'SvgGroup';
	return new Svg(vars);
}

function Circle(vars) {
	vars = vars || {};
	vars.type = 'circle';
	vars.className = 'Circle';
	return new Svg(vars);
}

function Ellipse(vars) {
	vars = vars || {};
	vars.type = 'ellipse';
	vars.className = 'Ellipse';
	return new Svg(vars);
}

function Line(vars) {
	vars = vars || {};
	vars.type = 'line';
	vars.className = 'Line';
	return new Svg(vars);
}

function Path(vars) {
	vars = vars || {};
	vars.type = 'path';
	vars.className = 'Path';
	return new Svg(vars);
}

function Polygon(vars) {
	vars = vars || {};
	vars.type = 'polygon';
	vars.className = 'Circle';
	return new Svg(vars);
}

function Rect(vars) {
	vars = vars || {};
	vars.type = 'rect';
	vars.className = 'Rect';
	return new Svg(vars);
}

function SvgText(vars) {
	vars = vars || {};
	vars.type = 'text';
	vars.className = 'SvgText';
	return new Svg(vars);
}


function TextArea(vars) {
	var textarea;
	vars = vars || {};
	vars.type = 'textarea';
	vars.className = 'TextArea';
	textarea = new Sprite(vars);
	textarea.type = 'textarea';
	textarea.setBorderRadius(0);
	textarea.style['-webkit-appearance'] = 'none';
	textarea.element.addEventListener('focus', function() {
		if(BROWSER_NAME === 'Safari') document.webkitCancelFullScreen();
		stage.disableKeyNavigation = true;
	});
	textarea.element.addEventListener('blur', function() {
		stage.disableKeyNavigation = false;
	});
	return textarea;
}

function TextField(vars) {
	vars = vars || {};
	vars.type = 'span';
	vars.className = 'TextField';
	var textField = new Sprite(vars);
	textField.setSelectable(true);
	textField.setFontFamily('sans-serif');
	textField.element.addEventListener('focus', function() {
		stage.disableKeyNavigation = true;
	});
	textField.element.addEventListener('blur', function() {
		stage.disableKeyNavigation = false;
	});
	return textField;
}

function ViewProxy(vars) {

	var proxy = vars || {};

	proxy.events = proxy.events || new EventSandbox();

	return new Sprite(proxy);
}

function VideoPlayer(vars) {
	vars = vars || {};
	vars.type = 'video';
	vars.className = 'VideoPlayer';
	var self = new Sprite(vars);

	self.play = function(){
		self.element.play();
		self.paused = false;
		self.dispatchEvent(PLAY);
		if(isPod()) video.element.webkitEnterFullscreen();
	};

	self.pause = function(){
		self.element.pause();
		self.paused = true;
		self.dispatchEvent(PAUSED);
	};

	self.togglePlay = function(){
		if(self.element.paused) {
			self.play();
		} else {
			self.pause();
		}
	};

	self.setSize = function(w, h, updateSpeed) {
		if(updateSpeed === 0) {
			self.setWidth(w);
			self.setHeight(h);
		} else {
			Tween(self, updateSpeed, {width:w, height:h});
		}
	};

	self.setSource = function(value) {
		if(typeSupported('video/mp4','avc1.42E01E, mp4a.40.2')) {
			self.setSrc(REWRITE_BASE + MEDIA_ORIGINAL + value);
			self.element.addEventListener('ended', ended);
		} else if(fileExist(REWRITE_BASE + MEDIA_ORIGINAL + value.replace(getExt(value),'webm'))) {
			self.setSrc(REWRITE_BASE + MEDIA_ORIGINAL + value.replace(getExt(value),'webm'));
			self.element.addEventListener('ended', ended);
		} else {
			window.open(REWRITE_BASE + MEDIA_ORIGINAL + value);
		}
	};

	self.destroy = function() {
		if(self) self.element.removeEventListener('ended', ended);
		self = null;
	};

	function typeSupported(vidType,codType) {
		var isSupported = self.element.canPlayType(vidType + ';codecs="' + codType + '"');
		if (isSupported === "") return false;
		return true;
	}

	function showErrorAlert(e) {
		alert("Unfortunately self browser will not play H.264 videos.\nPlease open another browser to view self video.");
	}

	function ended() {
		self.dispatchEvent('playbackEnd');
	}

	stage.addEventListener(KEY_DOWN, checkSpaceBar);

	function checkSpaceBar(e) {
		if(self && keyCode[e.keyCode] === SPACE_BAR) {
			self.togglePlay();
		}
	}

	return self;
}


function VideoHud(video) {

	var hud = new Sprite(),
		btnWidth = 50,
		btnHeight = 50,
		bgColor = 'rgba(0,0,0,0.65)',
		playBtn,
		fullscreenBtn,
		muteBtn,
		scrub,
		curVideoWidth,
		curVideoHeight,
		scrubWidth,
		showing = true;

	function init() {
		hud.setHeight(50);
		hud.setWidth(video.getWidth());
		hud.setAlpha(1);
		hud.setZIndex(5);

		video.parent.addChild(hud);

		var playIcon;

		if(video.element.paused) {
			playIcon = svgPaths.pause;
		} else {
			playIcon = svgPaths.play;
		}

		playBtn = buildBtn(playIcon, togglePlay);
		hud.addChild(playBtn);

		fullscreenBtn = buildBtn(svgPaths.fullscreen, fullscreen);
		hud.addChild(fullscreenBtn);

		muteBtn = buildBtn(svgPaths.volume, toggleMute);
		hud.addChild(muteBtn);

		buildScrubZone();

		hud.interval = setInterval(updateProgress, 100);
		video.element.addEventListener('ended', ended);
		stage.addEventListener(MOUSE_MOVE, videoHit);

	}

	function videoHit(e) {
		var x = getX(e),
			y = getY(e);
		if(video.hitTestPoint(x, y)) {
			show();
		} else if(USER_AGENT !== PAD) {
			hide();
		}
	}

	function show() {
		if(!showing) {
			showing = true;
			hud.setDisplay('block');
			Tween(hud, UPDATE_SPEED, {alpha:1});
		}
	}

	function hide() {
		if(showing) {
			showing = false;
			Tween(hud, UPDATE_SPEED, {alpha:0, onComplete:function(){
				hud.setDisplay('none');
				showing = false;
			}});
		}
	}

	hud.resize = function(updateSpeed, x, y, videoWidth, videoHeight) {

		var hudX = x,
			hudY = y,
			hudWidth = videoWidth,
			hudHeight = videoHeight;

		if(video.parentView === 'MediaView' && LAYOUT_MODEL.defaultVideoScale === 'fill') {
			hudX = 0;
			hudY = 0;
			hudWidth = layoutCalcs.mediaMask.width();
			hudHeight = layoutCalcs.mediaMask.height();
		}

		if(video.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fill') {
			hudX = 0;
			hudY = 0;
			hudWidth = layoutCalcs.mediaMask.width();
			hudHeight = layoutCalcs.mediaMask.height();
		}

		if(video.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
			hudX = 0;
			hudY = 0;
			hudWidth = layoutCalcs.landingMediaView.width();
			hudHeight = layoutCalcs.landingMediaView.height();
		}

		curVideoWidth = videoWidth;
		curVideoHeight = videoHeight;
		scrubWidth = hudWidth - (btnWidth * 3);

		if(updateSpeed === 0) {
			hud.setWidth(hudWidth);
			hud.setX(Mth.round(hudX));
			hud.setY(Mth.round(hudY + hudHeight - hud.getHeight()));
			fullscreenBtn.setX(hudWidth - fullscreenBtn.getWidth());
			muteBtn.setX(hudWidth - fullscreenBtn.getWidth() - muteBtn.getWidth());
			scrub.setX(btnWidth);
			scrub.setWidth(scrubWidth);
			scrub.hit.setWidth(scrubWidth);
		} else {
			Tween(hud, updateSpeed, {width:hudWidth, x:hudX, y:hudY + videoHeight - hud.getHeight()});
			Tween(fullscreenBtn, updateSpeed, {x:hudWidth - fullscreenBtn.getWidth()});
			Tween(muteBtn, updateSpeed, {x:hudWidth - fullscreenBtn.getWidth() - muteBtn.getWidth()});
			Tween(scrub, updateSpeed, {x:btnWidth, width:scrubWidth});
			Tween(scrub.hit, updateSpeed, {width:scrubWidth});
		}

		updateProgress();
	};

	function buildBtn(path, action) {
		var btn = new Sprite();
		btn.setWidth(btnWidth);
		btn.setHeight(btnHeight);
		btn.setBackgroundColor(bgColor);

		var svg = new Svg();
		svg.id = 'svg';
		svg.setWidth(12);
		svg.setHeight(12);
		svg.setX(Mth.round((btn.getWidth() - svg.getWidth()) * 0.5));
		svg.setY(Mth.round((btn.getHeight() - svg.getHeight()) * 0.5));
		btn.addChild(svg);

		var icon = new Path();
		icon.id = 'icon';
		icon.setD(path);
		icon.setFill('#FFFFFF');
		icon.setAlpha(0.65);
		svg.addChild(icon);

		var hit = new Sprite();
		hit.setWidth(btn.getWidth());
		hit.setHeight(btn.getHeight());
		btn.addChild(hit);

		btn.hit = hit;
		btn.icon = icon;
		btn.svg = svg;
		hit.icon = icon;
		hit.bg = btn;

		hit.addEventListener(CLICK, action);

		hit.addEventListener(MOUSE_OVER, function(e){
			Tween(this.bg, UPDATE_SPEED, {backgroundColor:'rgba(0,0,0,0.85)'});
			Tween(this.icon, UPDATE_SPEED, {alpha:1});
		});
		hit.addEventListener(MOUSE_OUT, function(){
			Tween(this.bg, UPDATE_SPEED, {backgroundColor:bgColor});
			Tween(this.icon, UPDATE_SPEED, {alpha:0.65});
		});

		return btn;
	}

	function buildScrubZone() {
		scrub = new Sprite();
		scrub.setZIndex(-1);
		scrub.setBackgroundColor(bgColor);
		scrub.setHeight(btnHeight);
		hud.addChild(scrub);

		scrub.buffer = new Sprite();
		scrub.buffer.setBackgroundColor('#FFFFFF');
		scrub.buffer.setAlpha(0.12);
		scrub.buffer.setWidth(1);
		scrub.buffer.setHeight(hud.getHeight());
		scrub.addChild(scrub.buffer);

		scrub.progress = new Sprite();
		scrub.progress.setBackgroundColor('#FFFFFF');
		scrub.progress.setAlpha(0.25);
		scrub.progress.setWidth(1);
		scrub.progress.setHeight(hud.getHeight());
		scrub.addChild(scrub.progress);

		scrub.time = new Sprite();
		scrub.time.setFontFamily("pixel");
		scrub.time.setFontColor('#FFFFFF');
		scrub.time.setFontSize(8);
		scrub.time.setText("00:00 / 00:00");
		scrub.time.setAlpha(0.5);
		scrub.addChild(scrub.time);
		scrub.time.setX(10);
		scrub.time.setY(18);

		scrub.head = new Sprite();
		scrub.head.setBackgroundColor('#FFFFFF');
		scrub.head.setAlpha(0);
		scrub.head.setX(30);
		scrub.head.setWidth(1);
		scrub.head.setHeight(hud.getHeight());
		scrub.addChild(scrub.head);

		scrub.headTime = new Sprite();
		scrub.headTime.setFontFamily("pixel");
		scrub.headTime.setFontColor('#FFFFFF');
		scrub.headTime.setFontSize(8);
		scrub.headTime.setAlpha(0);
		scrub.headTime.setText("00:00 / 00:00");
		scrub.headTime.setX(30);
		scrub.headTime.setY(-15);
		scrub.addChild(scrub.headTime);

		scrub.hit = new Sprite();
		scrub.hit.setHeight(hud.getHeight());
		scrub.addChild(scrub.hit);

		scrub.hit.addEventListener(MOUSE_OVER, function(e){
			Tween(scrub.buffer, UPDATE_SPEED, {alpha:0.2});
			Tween(scrub.progress, UPDATE_SPEED, {alpha:0.4});
			Tween(scrub.time, UPDATE_SPEED, {alpha:1});
			Tween(scrub.head, UPDATE_SPEED, {alpha:1});
			Tween(scrub.headTime, UPDATE_SPEED, {alpha:0.65});
			Tween(scrub, UPDATE_SPEED, {backgroundColor:'rgba(0,0,0,0.85)'});
		});
		scrub.hit.addEventListener(MOUSE_OUT, function(){
			Tween(scrub.buffer, UPDATE_SPEED, {alpha:0.12});
			Tween(scrub.progress, UPDATE_SPEED, {alpha:0.25});
			Tween(scrub.time, UPDATE_SPEED, {alpha:0.5});
			Tween(scrub.head, UPDATE_SPEED, {alpha:0});
			Tween(scrub.headTime, UPDATE_SPEED, {alpha:0});
			Tween(scrub, UPDATE_SPEED, {backgroundColor:bgColor});
		});

		scrub.hit.addEventListener(MOUSE_MOVE, scrubMove);
		scrub.hit.addEventListener(CLICK, scrubSeek);
	}

	function scrubMove(e) {
		var pointX = getScrubPoint(e);
		scrub.head.setTransition(0);
		scrub.head.setX(pointX);
		scrub.headTime.setTransition(0);
		scrub.headTime.setX(pointX);
		scrub.headTime.setText(dropInPoint = secondsToTime((pointX / scrubWidth) * video.element.duration));
	}

	function scrubSeek(e) {
		var pointX = getScrubPoint(e);
		scrub.head.setX(pointX);
		scrub.headTime.setX(pointX);
		var dropInPoint = (pointX / scrubWidth) * video.element.duration;
		video.element.currentTime = dropInPoint;
		scrub.headTime.setText(secondsToTime(dropInPoint));
		updateProgress(video);
	}

	function updateProgress() {
		if(video.element.buffered.length > 0) {
			bufferedPercent = video.element.buffered.end(0) / video.element.duration;
			scrub.buffer.setWidth(bufferedPercent * scrubWidth);

			playedPercent = video.element.currentTime / video.element.duration;
			scrub.progress.setWidth(playedPercent * scrubWidth);

			scrub.time.setText(secondsToTime(video.element.currentTime) + " / " + secondsToTime(video.element.duration));
		}
	}

	function ended() {
		clearInterval(hud.interval);
	}

	function togglePlay(e) {
		if(video.element.paused) {
			video.play();
			playBtn.icon.setD(svgPaths.pause);
			hud.interval = setInterval(updateProgress, 50);
		} else {
			video.pause();
			playBtn.icon.setD(svgPaths.play);
			/*clearInterval(hud.interval);*/
		}
	}

	function toggleMute(e) {
		if(video.element.volume == 1) {
			mute();
			muteBtn.icon.setD(svgPaths.mute);
		} else {
			unmute();
			muteBtn.icon.setD(svgPaths.volume);
		}
	}

	function mute() {
		video.element.volume = 0;
	}

	function unmute() {
		video.element.volume = 1;
	}

	function getScrubPoint(e) {
		if(TOUCH_DEVICE && e.touches) {
			return e.touches[0].offsetX - btnWidth - scrub.getX();
		} else {
			return e.offsetX;
		}
	}

	function secondsToTime(seconds) {
		minutes = Math.floor(seconds / 60);
		seconds = Math.floor(seconds - (minutes * 60));
		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;
		return minutes + ":" + seconds;
	}

	function fullscreen(e) {
		if (document.webkitIsFullScreen) {
			document.webkitCancelFullScreen();
		} else {
			if (video.element.mozRequestFullScreen) {
				video.element.mozRequestFullScreen();
				video.element.setAttribute("controls", "true");
			} else if (video.element.webkitRequestFullscreen) {
				video.element.webkitRequestFullscreen();
			}
		}
		document.addEventListener('webkitfullscreenchange', fullscreenChange, false);
		document.addEventListener('mozfullscreenchange', fullscreenChange, false);
	}

	function fullscreenChange(e) {
		var isFull = document.isFullScreen | document.mozFullScreen | document.webkitIsFullScreen;
		if(!isFull) {
			video.element.removeAttribute("controls");
		}
	}

	init.call(hud);
	return hud;
}


function VimeoPlayer(vars) {

	vars = vars || {};
	vars.type = 'iframe';
	vars.className = 'VimeoPlayer';
	var self = new Sprite(vars),
		currentPlayer,
		source;

	self.style.pointerEvents = 'auto';

	init();

	self.setSource = function(value) {
		try {
			source = 'http://player.vimeo.com/video/' + value.split("vimeo:")[1];
			if(isPod()) {
				/*window.location.href = source;*/
				window.open(source);
			} else {
				self.setSrc(source + '?api=1&player_id=' + currentPlayer);
				self.element.setAttribute('width', 640);
				self.element.setAttribute('height', 480);
				self.element.setAttribute('frameborder', 0);
				self.element.setAttribute('webkitallowfullscreen', true);
				self.element.setAttribute('mozallowfullscreen', true);
				self.element.setAttribute('allowfullscreen', true);
				window.addEventListener("message", messaging, false);
			}
		} catch(e) {
			trace('there may have been a vimeo api error: ', e);
		}
	};

	self.togglePlay = function(e) {
		if(self.paused) {
			self.play();
		} else {
			self.pause();
		}
	};

	self.play = function() {
		self.element.contentWindow.postMessage(JSON.stringify({
			"method": "play",
			"value": ""
		}), source);
		self.paused = false;
		self.dispatchEvent(PLAY);
	};

	self.pause = function() {
		if(self.element.contentWindow) {
			self.element.contentWindow.postMessage(JSON.stringify({
				"method": "pause",
				"value": ""
			}), source);
		}
		self.paused = true;
		self.dispatchEvent(PAUSED);
	};

	self.setSize = function(w, h, updateSpeed) {
		self.setWidth(w);
		self.setHeight(h);
	};

	self.align = function(x, y) {
		self.style.left = x + "px";
		self.style.top = y + "px";
	};

	self.destroy = function() {
		window.removeEventListener("message", messaging, false);
		self = null;
	};

	function ended() {
		self.dispatchEvent('playbackEnd');
	}

	function init() {
		currentPlayer = 'player' + VimeoPlayer.getNextPlayerID();
		self.element.id = currentPlayer;
		self.element.setAttribute('frameborder', 0);
		self.element.setAttribute('webkitallowfullscreen', true);
		self.element.setAttribute('mozallowfullscreen', true);
		self.element.setAttribute('allowfullscreen', true);
		self.setZIndex(0);
	}

	function messaging(e) {

		var data = JSON.parse(e.data);

		if(data.event === 'ready') {
			onReady(data);
		}

		if(data.method === 'getVideoWidth') {
			self.setWidth(data.value);
			self.videoWidth = data.value;
			self.element.setAttribute('width', data.value);

		}

		if(data.method === 'getVideoHeight') {
			self.setHeight(data.value);
			self.videoHeight = data.value;
			self.element.setAttribute('height', data.value);
			self.dispatchEvent(VIMEO_METADATA_LOADED);
		}

		if(data.event === 'finish') {
			ended();
		}
	}

	function onReady(data) {
		self.element.contentWindow.postMessage(JSON.stringify({
			"method": "getVideoWidth",
			"value": data.player_id
		}), source);

		self.element.contentWindow.postMessage(JSON.stringify({
			"method": "getVideoHeight",
			"value": data.player_id
		}), source);

		self.element.contentWindow.postMessage(JSON.stringify({
			"method": 'addEventListener',
			"value": 'finish'
		}), source);
	}


	stage.addEventListener(KEY_DOWN, checkSpaceBar);

	function checkSpaceBar(e) {
		if(self && keyCode[e.keyCode] === SPACE_BAR) {
			self.togglePlay();
		}
	}

	return self;
}

VimeoPlayer.playerID = 1;

VimeoPlayer.getNextPlayerID = function() {
	return VimeoPlayer.playerID++;
};


var JSONP = function JSONP(vars) {

	var jsonp = document.createElement("script"),
		head = document.getElementsByTagName("head")[0];

	jsonp.setAttribute("src", vars.url + "?" + vars.params + '&callback=' + vars.callback);
	jsonp.setAttribute("id", "jsonp");
	var id = document.getElementById("jsonp");

	if(!id) {
		head.appendChild(jsonp);
	} else {
		head.replaceChild(jsonp, id);
	}
};


function JSONXHR(vars) {

	var url = vars.url,
		params = vars.params,
		callback = vars.callback;

	var formData = new FormData();
	var i; for(i in params) {
		formData.append(i, params[i]);
	}

	var xhr = new XMLHttpRequest();
	xhr.addEventListener("load", response, false);
	xhr.addEventListener("error", error, false);
	xhr.addEventListener("abort", error, false);
	xhr.open("POST", url);
	xhr.send(formData);

	function response(e) {
		if(e.target.response) callback.apply(callback, [JSON.parse(e.target.response)]);
	}

	function error(e) {
		console.error(e);
	}
}

var Mth = {

	E: 2.718281828459045,
	LN2: 0.6931471805599453,
	LN10: 2.302585092994046,
	LOG2E: 1.4426950408889634,
	LOG10E: 0.4342944819032518,
	PI: 3.141592653589793,
	SQRT1_2: 0.7071067811865476,
	SQRT2: 1.4142135623730951,

	floor: function(x) {
		return x | 0;
	},

	ceil: function(x) {
		return x + 1 | 0;
	},

	round: function(x) {
		return x + 0.5 | 0;
	},

	abs: function(x) {
		return x > 0 ? x : -x;
	},

	pow: function(value, exp) {
		var result = value;
		while(--exp) { result *= value; }
		return result;
	},

	max: Math.max,
	min: Math.min,
	random: Math.random,
	acos: Math.acos,
	asin: Math.asin,
	atan: Math.atan,
	atan2: Math.atan2,
	cos: Math.cos,
	exp: Math.exp,
	log: Math.log,
	sin: Math.sin,
	sqrt: Math.sqrt,
	tan: Math.tan
};


var trace = console.log.bind ? console.log.bind(console) : function(){console.log(arguments);};
/*var stack = console.trace.bind ? console.trace.bind(console) : function(){console.trace(arguments);};*/

function isSection(id) {
	return Number(id) < 10000 && id !== '';
}

function isMediaItem(id) {
	return Number(id) > 9999;
}

function getObjById(obj, id) {
	var i;
	id = Number(id);
	for(i in obj) {
		if(obj[i].id === id) {
			obj[i].key = i;
			return obj[i];
		}
	}
	return false;
}

function getSectionById(id) {
	return getObjById(SECTIONS_MODEL, Number(id));
}

function getMediaById(id) {
	return getObjById(MEDIA_MODEL, Number(id));
}

function getThumbQuery(width, height, file) {
	var x = RETINA ? 2 : 1;
	return REWRITE_BASE + MEDIA_THUMB + encodeURIComponent(file) + '?w=' + (width * x) + '&h=' + (height * x) ;
}

function getObjectLength(obj) {
	var length = 0, key;
	for (key in obj) if (obj.hasOwnProperty(key)) length++;
	return length;
}

function getFirstValidSub(section) {
	var media = csvToArray(section.media);
	var i; for(i in media) {
		var subSection = getSectionById(media[i]);
		if(isSection(media[i]) && subSection.label !== '%SPACER%' && subSection.visible === 1) {
			return subSection;
		}
	}
	return false;
}

function getValidMedia(media) {
	var i = 0,
		l = media.length,
		validMedia = [];
	for(; i < l; i++) {
		if(media[i] > 9999) {
			validMedia.push(media[i]);
		}
	}
	return validMedia.toString();
}

function TransformMatrix(element) {
	var computed = window.getComputedStyle(element, null);
	var matrix = computed.getPropertyValue("-webkit-transform") ||
		computed.getPropertyValue("-moz-transform") ||
		computed.getPropertyValue("-ms-transform") ||
		computed.getPropertyValue("-o-transform") ||
		computed.getPropertyValue("transform") ||
		"cannot get transform matrix";
	matrix = csvToArray(matrix.replace(/[\smatrix()]/gi, ''));
	this.matrix = matrix;
	this.x = matrix[4];
	this.y = matrix[5];
}

function csvToArray(value) {
	if(value) {
		if(value === '') {
			return [];
		} else if(typeof value === 'number') {
			return [value.toString()];
		} else if(typeof value === 'boolean') {
			return [value.toString()];
		} else if(value.toString().indexOf(',') > -1) {
			return value.split(',');
		} else {
			return [value.toString()];
		}
	}
	return [];
}

function setTimer(callback, delay, params, scope) {
	scope = scope || arguments.callee.caller || this;
	setTimeout(function() {
		if(params) {
			callback.apply(scope, params);
		} else {
			callback.call(scope);
		}
	}, delay);
}

function uniquePush(arr, item) {
	var i = 0,
		length = arr.length;
	for(;i<length;i++){
		if(arr.indexOf(item) === -1) {
			arr.push(item);
		}
	}
	return arr;
}

function findInArray(needle, haystack) {
	var i = haystack.length;
	while(i--) {
		if(needle === haystack[i]) {
			return true;
		}
	}
	return false;
}

function hasClass(o, cls) {
	return new RegExp('(^|\\s)' + cls + '(\\s|$)').test(o.className);
}

function parseColor(c) {
	var color = [];
	if (c && c.charAt(0) === "#") {
		c = parseInt(c.substr(1), 16);
		color = [(c >> 16) & 255, (c >> 8) & 255, c & 255];
	} else if(c && c.charAt(0) === "r") {
		c = c.replace(/[rgba()]/ig,'').split(',');
		color = [Number(c[0]), Number(c[1]), Number(c[2]), Number(c[3])];
	}
	return color;
}

function getLoaderColor(bgColor) {
	var avg, tint, c;
	c = parseColor(bgColor);
	avg = (c[0] + c[1] + c[2]) * 0.33;
	tint = avg < 128 ? '#FFFFFF' : '#000000';
	return tint;
}

function hexToRgba(hex, alpha) {
	hex = hex.charAt(0) === "#" ? parseInt(hex.substr(1), 16) : hex;
	return 'rgba(' + ((hex >> 16) & 255) + ',' + ((hex >> 8) & 255) + ',' + (hex & 255) + ',' + alpha + ')';
}

function getDocHeight() {
	return Math.max(Math.max(document.body.scrollHeight, document.documentElement.scrollHeight), Math.max(document.body.offsetHeight, document.documentElement.offsetHeight), Math.max(document.body.clientHeight, document.documentElement.clientHeight));
}

function percentToPixels(value, range) {
	if(!isNaN(value) || typeof value === 'string') {
		if(!isNaN(value)){
			return Number(value);
		} else if(value.search('%') > -1) {
			value = value.replace('%', '');
			value = (Number(value) * 0.01) * range;
			return Math.round(value);
		} else if(value.search('px') > -1) {
			value = value.replace('px', '');
			return Number(value);
		}
		return Number(value);
	}
}

function getExt(file) {
	return file.split('.').pop();
}

function getX(e) {
	if (!TOUCH_DEVICE && !e.touches) {
		return e.clientX;
	} else if(TOUCH_DEVICE && e.touches) {
		return e.touches[0].pageX;
	}
	return false;
}

function getY(e) {
	if (!TOUCH_DEVICE && !e.touches) {
		return e.clientY;
	} else if(TOUCH_DEVICE && e.touches) {
		return e.touches[0].pageY;
	}
	return false;
}

function hardwareAccel(el) {
	el.style.webkitTransform = 'translateZ(0)';
	el.style.webkitPerspective = '1000';
	el.style.webkitBackfaceVisibility = 'hidden';
}

function spacesToCamelCase(str) {
	return str.replace(/(\s[a-z])/ig, function($1) {
		return $1.replace(/\s/g,'').toUpperCase();
	});
}

function CamelCaseToSpaces(str) {
	return str.replace(/([A-Z])/g, function($1) {
		return $1.replace($1,' '+$1.toLowerCase());
	});
}

function cssToJs(str) {
	return str.replace(/(-[a-z])/g, function($1) {
		return $1.replace('-','').toUpperCase();
	});
}

function jsToCss(str) {
	return str.replace(/([A-Z])/g, function($1) {
		return $1.replace($1,'-'+$1.toLowerCase());
	});
}

function isFullscreenCapable() {
	return document.documentElement.requestFullScreen || document.documentElement.mozRequestFullScreen || document.documentElement.webkitRequestFullScreen;
}

function replaceCSS(file, replace) {
	var links = document.getElementsByTagName("link"),
		old,
		i = links.length;
	while(i--) {
		if(links[i].getAttribute('href').search(replace) > -1) {
			old = document.getElementsByTagName("link")[i];
		}
	}
	var newCss = document.createElement("link");
	newCss.setAttribute("rel", "stylesheet");
	newCss.setAttribute("type", "text/css");
	newCss.setAttribute("href", REWRITE_BASE + PHP + file + '?' + new Date());
	document.getElementsByTagName("head")[0].replaceChild(newCss, old);
}

function prepend(child, parent) {
	parent.insertBefore(child, parent.firstChild);
}

function addDashes(str) {
	str = str.replace(/ /g, '-');
	str = str.replace(/%20/g, '-');
	return str;
}

function removeDashes(str) {
	if(str && str.search('-') > -1) {
		str = str.replace(/-/g, ' ');
	}
	return str;
}

function hitTarget(element) {
	return {
		hitTestPoint: function(xPoint, yPoint) {
			return xPoint >= this.getAbsX() && xPoint <= this.getAbsX() + element.offsetWidth && yPoint >= this.getAbsY() && yPoint <= this.getAbsY() + element.offsetHeight;
		},
		hitTestAbs: function(xPoint, yPoint) {
			return xPoint >= this.getAbsX() && xPoint <= this.getAbsX() + element.offsetWidth && yPoint >= this.getAbsY() && yPoint <= this.getAbsY() + element.offsetHeight;
		},
		getAbsX: function() {
			var x = 0;
			var el = element;
			if (el.offsetParent) {
				do { x += el.offsetLeft; } while (el = el.offsetParent);
			}
			return x;
		},
		getAbsY: function() {
			var y = 0;
			var el = element;
			if (el.offsetParent) {
				do { y += el.offsetTop; } while (el = el.offsetParent);
			}
			return y;
		}
	};
}

function fileExist(urlToFile) {
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlToFile, false);
    xhr.send();
	return xhr.status === 200;
}

window.requestAnimationFrame = window.requestAnimationFrame || (function(){
	return  window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			function( callback ){ window.setTimeout(callback, 1000 / 60); };
})();

window.cancelAnimationFrame = window.cancelAnimationFrame || (function(){
	return  window.webkitCancelAnimationFrame ||
			window.mozCancelAnimationFrame    ||
			function( timer ){ window.clearTimeout(timer); };
})();

window.performance = window.performance || {};

performance.now = (function() {
	return 	performance.now       ||
			performance.mozNow    ||
			performance.msNow     ||
			performance.oNow      ||
			performance.webkitNow ||
			function() { return new Date().getTime(); };
})();

function exists(obj, pathToProp) {
	var props = pathToProp.split('.'),
		curObj = obj,
		i = 0,
		l = props.length;
	if(curObj) {
		for(; i < l; i++) {
			if(curObj[props[i]]) {
				curObj = curObj[props[i]];
			} else {
				return false;
			}
		}
		return curObj;
	}
	return false;
}

function getVendorName(ua) {
	if (/chrome/.test(ua)) {
		return 'Chrome';
	} else if (/safari/.test(ua)) {
		return 'Safari';
	} else if (/firefox/.test(ua)) {
		return 'Firefox';
	} else if (/opera/.test(ua)) {
		return 'Opera';
	} else if (/msie|rv:/.test(ua)) {
		return 'MSIE';
	}
	return 'Other';
}

var BROWSER_NAME = (function() {
	var ua = navigator.userAgent.toLowerCase();
	if(/mobile/.test(ua)) {
		return 'Mobile ' + getVendorName(ua);
	} else {
		return getVendorName(ua);
	}
})();

function isPod() {
	return isAndroidMobile() || isAppleMobile();
}

function isAppleMobile() {
	var ua = HTTP_USER_AGENT.toLowerCase();
	return	(/ipod/.test(ua) || /iphone/.test(ua));
}

function isAndroidMobile() {
	var ua = HTTP_USER_AGENT.toLowerCase();
	if(/android/.test(ua)) {
		return (/galaxy nexus/.test(ua)) || /* android galaxy nexus phone */
				(/nexus 5/.test(ua)) ||		/* android nexus 5 phone */
				(/touch/.test(ua)) ||		/* android phone */
				(/htc/.test(ua)) ||			/* android htc phone */
				(/droid/.test(ua)) ||		/* android droid x phone and others */
				(/sch/.test(ua))			/* android galaxy sIII phone */
	}
	return false;
}

function isPad() {
	var ua = HTTP_USER_AGENT.toLowerCase();
	return	(/pad/.test(ua)) || 								/* iPad and others */
			(/silk/.test(ua)) || 								/* kindle fire hd */
			(/android/.test(ua) && /nexus 7/.test(ua)) || 		/* andriod nexus 7 pad */
			false;
}

USER_AGENT = (function() {
	if (isPad() && !SETTINGS_MODEL.padUseDesktop) {
		return PAD;
	} else if (isPod() && !SETTINGS_MODEL.podUseDesktop) {
		return POD;
	}
	return HTML;
})();

function setViewport() {

	var viewport = document.createElement('meta');
	viewport.setAttribute('name', 'viewport');
	if(USER_AGENT === HTML && isPod()) {
		viewport.setAttribute('content', 'user-scalable=0, initial-scale=0.5, minimum-scale=0.5, maximum-scale=0.5');
	} else if(USER_AGENT === HTML && isPad()) {
		viewport.setAttribute('content', 'user-scalable=0, initial-scale=1, minimum-scale=1, maximum-scale=1');
	} else if (isAndroidMobile()) {
		viewport.setAttribute('content', 'target-densitydpi=200dpi, initial-scale=1, width=device-width');
	} else if (isAppleMobile()) {
		viewport.setAttribute('content', 'user-scalable=0, initial-scale=1, minimum-scale=1, maximum-scale=1');
	} else if (isPad()) {
		viewport.setAttribute('content', 'width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1');
	} else if(USER_AGENT == POD) {
		viewport.setAttribute('content', 'user-scalable=0, initial-scale=1, minimum-scale=1, maximum-scale=1');
	}
	document.getElementsByTagName("head")[0].appendChild(viewport);

	if(isPod() || isPad()) {
		var webAppCapable = document.createElement('meta');
		webAppCapable.setAttribute('name', 'apple-mobile-web-app-capable');
		webAppCapable.setAttribute('content', 'yes');
		document.getElementsByTagName("head")[0].appendChild(webAppCapable);

		var webAppStatusBar = document.createElement('meta');
		webAppStatusBar.setAttribute('name', 'apple-mobile-web-app-status-bar-style');
		webAppStatusBar.setAttribute('content', 'black');
		document.getElementsByTagName("head")[0].appendChild(webAppStatusBar);

		var appleTouchIcon = document.createElement('link');
		appleTouchIcon.setAttribute('href', REWRITE_BASE + 'dx/icon/apple-touch-icon.png');
		appleTouchIcon.setAttribute('rel', 'apple-touch-icon');
		appleTouchIcon.setAttribute('sizes', '144x144');
		document.getElementsByTagName("head")[0].appendChild(appleTouchIcon);

	}
}

setViewport();

var keyCode = {
	"8":DELETE,
	"13":ENTER,
	"27":ESCAPE,
	"32":SPACE_BAR,
	"37":LEFT_ARROW,
	"39":RIGHT_ARROW,
	"16":SHIFT,
	"91":COMMAND,
	"93":COMMAND,
	"224":COMMAND,
	"17":CONTROL
};

var keyName = {
	DELETE:8,
	ENTER:13,
	ESCAPE:27,
	SPACE_BAR:32,
	LEFT_ARROW:37,
	RIGHT_ARROW:39,
	SHIFT:16,
	COMMAND:91,
	COMMAND:93,
	COMMAND:224,
	CONTROL:17
};

var socialIcons = {
	'[one]':{filename:"plus1@2x.png", width:33, height:20},
	'[plus one]':{filename:"plus1@2x.png", width:33, height:20},
	'[google plus]':{filename:"google-plus@2x.png", width:20, height:20},
	'[like]':{filename:"like@2x.png", width:20, height:20},
	'[facebook]':{filename:"facebook@2x.png", width:20, height:20},
	'[linkedin]':{filename:"linkedin@2x.png", width:20, height:20},
	'[twitter]':{filename:"twitter@2x.png", width:20, height:20},
	'[mailto]':{filename:"email@2x.png", width:20, height:20},
	'[youtube]':{filename:"youtube@2x.png", width:20, height:20},
	'[vimeo]':{filename:"vimeo@2x.png", width:20, height:20},
	'[instagram]':{filename:"instagram@2x.png", width:20, height:20},
	'[blogger]':{filename:"blogger@2x.png", width:20, height:20},
	'[rss]':{filename:"rss@2x.png", width:20, height:20},
	'[tumblr]':{filename:"tumblr@2x.png", width:20, height:20},
	'[flickr]':{filename:"flickr@2x.png", width:20, height:20},
	'[wordpress]':{filename:"wordpress@2x.png", width:20, height:20},
	'[pinterest]':{filename:"pinterest@2x.png", width:20, height:20}
};


function SiteController(vars) {

	var self = new ControllerProxy({events:new EventSandbox()});
	var popup,
		siteInit = true;

	vars.parent = self;
	vars.siteControllerEvents = self.events;

	self.siteView = new SiteView(vars);
	stage.addChild(self.siteView);

	var cursorHideElements = [];

	function init() {
		self.siteView.updateStyle();
		vars.parentView = self.siteView;
		vars.parentController = self;
		vars.events = self.events;
		vars.siteControllerEvents = self.events;
		self.touchNav = new vars.TouchNav({events:self.events});
		self.mediaController = new vars.MediaController(vars);
		self.menuController = new vars.MenuController(vars);
		cursorHideElements.push(self.menuController.menuView.menuText);
		self.footerController = new vars.FooterController(vars);
		cursorHideElements.push(self.footerController.footerView);
		self.logoController = new vars.LogoController(vars);
		cursorHideElements.push(self.logoController.logoView);
		if(USER_AGENT !== POD) {
			self.navbarController = new vars.NavbarController(vars);
			cursorHideElements.push(self.navbarController.navbarView);
		}
		self.addressController = new vars.AddressController(vars);
		self.cursor = new vars.Cursor(vars, cursorHideElements);
		self.tooltip = new vars.Tooltip(vars);
		self.fullscreen = new vars.Fullscreen(vars);
		self.service = new vars.SiteService(vars);
		self.events.dispatchEvent(LAYOUT_MODEL_CHANGE, {id:'layoutModelInit'});
		self.events.addEventListener(NAVBAR_OVERLAY_BTN_CLICK, navbarOverlayBtnClick);
		self.events.addEventListener(NAVBAR_NAV_CLICK, navbarNavClick);
		self.events.addEventListener(NAVBAR_FULLSCREEN, navbarFullscreen);
		self.events.addEventListener(MENU_CLICK, menuClick);
		self.events.addEventListener(FOOTER_CLICK, footerClick);
		self.events.addEventListener(LOGO_CLICK, logoClick);
		self.events.addEventListener(THUMB_CLICK, thumbClick);
		self.events.addEventListener(ADDRESS_URI_CHANGE, addressChange);
		self.events.addEventListener(ADMIN_LOADED, adminEventListeners);
		self.events.addEventListener(ADMIN_LOADED, addAdminToCursor);
		self.events.addEventListener(CURSOR_MEDIA_UPDATE, updateCursor);
		self.events.addEventListener(OVERLAY_CHANGE, updateCursor);
		self.events.addEventListener(CONTACT_FORM_SUBMIT, contactFormSubmit);
		self.events.addEventListener(SHARE_FORM_SUBMIT, shareFormSubmit);
		self.events.addEventListener(INQUIRY_FORM_SUBMIT, inquiryFormSubmit);
		self.events.addEventListener(POP_UP, onPopup);
		self.events.addEventListener('OVER_LOGO', checkMenuLogoOverlap);

		self.events.addEventListener(PAGE_LOADED, loadCustomJs);

		stage.addEventListener(RESIZE_END, resize);
		stage.addEventListener(ORIENTATION_CHANGE, orientationChange);
		stage.addEventListener(KEY_DOWN, keyDown);
		self.events.dispatchEvent(RESIZE_END);

		ORIENTATION = stage.getWidth() > stage.getHeight() ? 'horizontal' : 'vertical';
	}

	function adminEventListeners(e) {
		dx.admin.events.addEventListener(ACCOUNT_MODEL_CHANGE, accountModelChange);
		dx.admin.events.addEventListener(FONTS_MODEL_CHANGE, fontsModelChange);
		dx.admin.events.addEventListener(FILES_MODEL_CHANGE, filesModelChange);
		dx.admin.events.addEventListener(LAYOUT_MODEL_CHANGE, layoutModelChange);
		dx.admin.events.addEventListener(MEDIA_MODEL_CHANGE, mediaModelChange);
		dx.admin.events.addEventListener(NAVBAR_MODEL_CHANGE, navbarModelChange);
		dx.admin.events.addEventListener(PDF_MODEL_CHANGE, pdfModelChange);
		dx.admin.events.addEventListener(SETTINGS_MODEL_CHANGE, settingsModelChange);
		dx.admin.events.addEventListener(SECTIONS_MODEL_CHANGE, sectionsModelChange);
		dx.admin.events.addEventListener(ADMIN_MOUSE_DOWN, adminMouseDown);
		dx.admin.events.addEventListener(ADMIN_MOUSE_UP, adminMouseUp);
	}

	function loadCustomJs(e) {
		self.events.dispatchEvent(SITE_SERVICE_GET_FILE, {name:'js'});
	}

	function adminMouseDown() {
		self.events.dispatchEvent(ADMIN_MOUSE_DOWN);
	}

	function adminMouseUp() {
		self.events.dispatchEvent(ADMIN_MOUSE_UP);
	}

	function checkForIntro() {
		if(!self.introView) {
			self.introView = new vars.IntroView(vars);
			self.events.addEventListener(RESIZE_END, self.introView.updatePosition);
		}
	}

	function resize(e) {
		if(!isVimeoFullscreen()) {
			ORIENTATION = stage.getWidth() > stage.getHeight() ? 'horizontal' : 'vertical';
			self.events.dispatchEvent(RESIZE_END);
			setTimeout(function(){
				Scrolls.resize();
			}, UPDATE_SPEED * 1000);
		}
		self.siteView.updatePosition(e);
	}

	function orientationChange(e) {
		ORIENTATION = stage.getWidth() > stage.getHeight() ? 'horizontal' : 'vertical';
		self.events.dispatchEvent(ORIENTATION_CHANGE);
	}

	function onPopup(e) {
		popup = new vars.Popup(vars, e);
	}

	function isVimeoFullscreen() {
		if(screen.height === stage.getHeight() && screen.width === stage.getWidth() && !self.fullscreen.dxFullscreen) {
			showElementsForVimeo('none');
			return true;
		} else {
			showElementsForVimeo('block');
			return false;
		}
	}

	function showElementsForVimeo(display) {
		self.menuController.menuView.setDisplay(display);
		self.footerController.footerView.setDisplay(display);
		self.logoController.logoView.setDisplay(display);
		if(self.navbarController) self.navbarController.navbarView.setDisplay(display);
	}

	function navbarOverlayBtnClick(e) {
		self.events.dispatchEvent(ADDRESS_OVERLAY_CHANGE, e);
	}

	function navbarNavClick(e) {
		self.events.dispatchEvent(ADDRESS_ASSET_CHANGE, e);
	}

	function navbarFullscreen(e) {
		self.events.dispatchEvent(FULLSCREEN_TOGGLE);
	}

	function footerClick(e) {
		self.events.dispatchEvent(ADDRESS_SECTION_CHANGE, e);
	}

	function menuClick(e) {
		self.events.dispatchEvent(ADDRESS_SECTION_CHANGE, e);
	}

	function thumbClick(e) {
		self.events.dispatchEvent(ADDRESS_SECTION_CHANGE, e);
	}

	function logoClick(e) {
		self.events.dispatchEvent(ADDRESS_SECTION_CHANGE, e);
	}

	function keyDown(e) {
		if((e.keyCode === keyName.LEFT_ARROW || e.keyCode === keyName.RIGHT_ARROW) && !stage.disableKeyNavigation) {
			self.events.dispatchEvent(ADDRESS_ASSET_CHANGE, keyCode[e.keyCode]);
		}
	}

	function addressChange(e) {
		if((!e.path || e.info.isIntroGallery) && siteInit) checkForIntro();
		self.events.dispatchEvent(SITE_URI_CHANGE, e);
		siteInit = false;
	}

	function updateCursor(e) {
		self.events.dispatchEvent(CURSOR_UPDATE, e);
	}

	function addAdminToCursor(e) {
		self.events.dispatchEvent(CURSOR_ADD_ADMIN, e);
	}

	function accountModelChange(e) {
		self.events.dispatchEvent(ACCOUNT_MODEL_CHANGE, e);
	}

	function fontsModelChange(e) {
		self.events.dispatchEvent(FONTS_MODEL_CHANGE, e);
	}

	function filesModelChange(e) {
		/*trace('filesModelChange', e);*/
	}

	function layoutModelChange(e) {
		if(e && /site/.test(e.id)) self.siteView.updateStyle();
		self.events.dispatchEvent(LAYOUT_MODEL_CHANGE, e);
	}

	function mediaModelChange(e) {
		self.addressController.setTitle();
		self.events.dispatchEvent(MEDIA_MODEL_CHANGE, e);
	}

	function navbarModelChange(e) {
		self.events.dispatchEvent(NAVBAR_MODEL_CHANGE, e);
	}

	function pdfModelChange(e) {
		self.events.dispatchEvent(PDF_MODEL_CHANGE, e);
	}

	function settingsModelChange(e) {
		self.events.dispatchEvent(SETTINGS_MODEL_CHANGE, e);
	}

	function sectionsModelChange(e) {
		self.events.dispatchEvent(SECTIONS_MODEL_CHANGE, e);
	}

	function contactFormSubmit(e) {
		self.events.dispatchEvent(SITE_SERVICE_CONTACT_FORM_SUBMIT, e);
	}

	function shareFormSubmit(e) {
		self.events.dispatchEvent(SITE_SERVICE_SHARE_FORM_SUBMIT, e);
	}

	function inquiryFormSubmit(e) {
		self.events.dispatchEvent(SITE_SERVICE_INQUIRY_FORM_SUBMIT, e);
	}

	function checkMenuLogoOverlap() {
		stage.addEventListener(MOUSE_MOVE, menuLogoHitTest);
	}

	function endCheckMenuLogoOverlap() {
		self.logoController.logoView.element.style.pointerEvents = 'auto';
	}

	function menuLogoHitTest(e) {
		if(self.menuController.menuView.menuText.hitTestAbs(e.pageX, e.pageY)) {
			self.logoController.logoView.element.style.pointerEvents = 'none';
		} else {
			self.logoController.logoView.element.style.pointerEvents = 'auto';
		}
	}

	init.call(self);
	return self;
}

stage.addEventListener(LOAD, function() {
	dx.site = new SiteController({
		Sprite:Sprite,
		TextField:TextField,
		Bitmap:Bitmap,
		Tile:Tile,
		FooterController:FooterController,
		FooterView:FooterView,
		LogoController:LogoController,
		LogoView:LogoView,
		MediaController:MediaController,
		MediaView:MediaView,
		LandingMediaView:LandingMediaView,
		CaptionView:CaptionView,
		MenuController:MenuController,
		MenuView:MenuView,
		NavbarController:NavbarController,
		NavbarView:NavbarView,
		AddressController:AddressController,
		OverlayController:OverlayController,
		OverlayView:OverlayView,
		ImageView:ImageView,
		VideoView:VideoView,
		PageView:PageView,
		ContactView:ContactView,
		VideoPlayer:VideoPlayer,
		VimeoPlayer:VimeoPlayer,
		LoadingIndicator:LoadingIndicator,
		ThumbsModule:ThumbsModule,
		Cursor:Cursor,
		Tooltip:Tooltip,
		Fullscreen:Fullscreen,
		PasswordView:PasswordView,
		IntroView:IntroView,
		SiteService:SiteService,
		Popup:Popup,
		TouchNav:TouchNav
	});

});

function SiteView(vars) {

	var self = new ViewProxy({events:vars.siteControllerEvents});
	self.setOverflow('hidden');

	self.addEventListener(CHILD_ADDED, function(child) {
		self.updatePosition();
	});

	self.updatePosition = function(e) {
		self.setWidth(stage.getWidth());
		self.setHeight(stage.getHeight());
	};

	self.updateStyle = function(e) {
		if(stage.getBackgroundColor()) {
			Tween(self, UPDATE_SPEED, {backgroundColor:LAYOUT_MODEL.siteBackgroundColor});
		} else {
			self.setBackgroundColor(LAYOUT_MODEL.siteBackgroundColor);
		}
	};

	return self;

}

var layoutCalcs = {

	_logoWidth:0, _logoHeight:0,
	_footerWidth:0, _footerHeight:0,
	_fotomotoWidth:0, _fotomotoHeight:0,
	_navbarHeight:0,
	_thumbsVisible:false, _section:undefined,
	_maskView:{}, _activeView:undefined,
	mobileFooterPadding:5,

	navbarView:{
		x:function(_navbarWidth) {
			var offsetX = percentToPixels(LAYOUT_MODEL.navbarOffsetX, stage.getWidth());
			if(USER_AGENT !== POD) {
				switch(LAYOUT_MODEL.navbarAlignHorizontal) {
					case 'left':
						return offsetX;
						break;
					case 'center':
						if(USER_AGENT === PAD) return ((stage.getWidth() - _navbarWidth) * 0.5);
						return ((stage.getWidth() - _navbarWidth) * 0.5) + offsetX;
						break;
					case 'right':
						return stage.getWidth() - _navbarWidth + offsetX;
						break;
				}
			}
			return 0;
		},
		y:function(_navbarHeight) {
			_navbarHeight = _navbarHeight || layoutCalcs._navbarHeight;
			layoutCalcs._navbarHeight = _navbarHeight;
			var offsetY = percentToPixels(LAYOUT_MODEL.navbarOffsetY, stage.getHeight());
			if(USER_AGENT !== POD) {
				switch(LAYOUT_MODEL.navbarAlignVertical) {
					case 'top':
						return offsetY;
						break;
					case 'center':
						return ((stage.getHeight() - _navbarHeight) * 0.5) + offsetY;
						break;
					case 'bottom':
						if(USER_AGENT === PAD && layoutCalcs.isDisplace() && layoutCalcs._section.thumb.side === 'bottom') {
							return stage.getHeight() - 22;
						}
						return stage.getHeight() - _navbarHeight + offsetY;
						break;
				}
			}
			return 0;
		}
	},

	footerView:{
		x:function(_footerWidth) {
			_footerWidth = _footerWidth || layoutCalcs._footerWidth;
			layoutCalcs._footerWidth = _footerWidth;
			var offsetX = percentToPixels(LAYOUT_MODEL.copyrightOffsetX, stage.getWidth());
			if(USER_AGENT !== POD) {
				switch(LAYOUT_MODEL.copyrightAlignHorizontal) {
					case 'left':
						return offsetX;
						break;
					case 'center':
						if(USER_AGENT === PAD) return ((stage.getWidth() - _footerWidth) * 0.5);
						return ((stage.getWidth() - _footerWidth) * 0.5) + offsetX;
						break;
					case 'right':
						return stage.getWidth() - _footerWidth + offsetX;
						break;
				}
			} else if(USER_AGENT === POD) {
				return layoutCalcs.mobileFooterPadding;
			}
			return 0;
		},
		y:function(_footerHeight) {
			_footerHeight = _footerHeight || layoutCalcs._footerHeight;
			layoutCalcs._footerHeight = _footerHeight;
			var offsetY = percentToPixels(LAYOUT_MODEL.copyrightOffsetY, stage.getHeight());
			if(USER_AGENT !== POD) {
				switch(LAYOUT_MODEL.copyrightAlignVertical) {
					case 'top':
						return offsetY;
						break;
					case 'center':
						return ((stage.getHeight() - _footerHeight) * 0.5) + offsetY;
						break;
					case 'bottom':
						if(USER_AGENT === PAD && layoutCalcs.isDisplace() && layoutCalcs._section.thumb.side === 'bottom') {
							return stage.getHeight() - 22;
						}
						return stage.getHeight() - _footerHeight + offsetY;
						break;
				}
			} else if(USER_AGENT === POD) {
				return stage.getHeight() - _footerHeight - layoutCalcs.mobileFooterPadding;
			}
			return 0;
		}
	},

	logoView:{
		x:function(_logoWidth) {
			_logoWidth = _logoWidth || layoutCalcs._logoWidth;
			layoutCalcs._logoWidth = _logoWidth;
			var offsetX = 0;
			if(USER_AGENT === POD) {
				return 0;
			} else if(USER_AGENT === PAD) {
				offsetX = percentToPixels(LAYOUT_MODEL.tabletLogoOffsetX, stage.getWidth());
				switch(LAYOUT_MODEL.logoAlignHorizontal) {
					case 'left':
						return offsetX;
						break;
					case 'center':
						return ((stage.getWidth() - _logoWidth) * 0.5) + offsetX;
						break;
					case 'right':
						return stage.getWidth() - _logoWidth - offsetX;
						break;
				}
			} else {
				offsetX = percentToPixels(LAYOUT_MODEL.logoOffsetX, stage.getWidth());
				switch(LAYOUT_MODEL.logoAlignHorizontal) {
					case 'left':
						return offsetX;
						break;
					case 'center':
						return ((stage.getWidth() - _logoWidth) * 0.5) + offsetX;
						break;
					case 'right':
						return stage.getWidth() - _logoWidth - offsetX;
						break;
				}
			}
			return 0;
		},
		y:function(_logoHeight) {
			_logoHeight = _logoHeight || layoutCalcs._logoHeight;
			layoutCalcs._logoHeight = _logoHeight;
			var offsetY = 0;
			if(USER_AGENT === POD) {
				return 0;
			} else if(USER_AGENT === PAD) {
				offsetY = percentToPixels(LAYOUT_MODEL.tabletLogoOffsetY, stage.getHeight());
				switch(LAYOUT_MODEL.logoAlignVertical) {
					case 'top':
						return offsetY;
						break;
					case 'center':
						return ((stage.getHeight() - _logoHeight) * 0.5) + offsetY;
						break;
					case 'bottom':
						return stage.getHeight() - _logoHeight - offsetY;
						break;
				}
			} else {
				offsetY = percentToPixels(LAYOUT_MODEL.logoOffsetY, stage.getHeight());
				switch(LAYOUT_MODEL.logoAlignVertical) {
					case 'top':
						return offsetY;
						break;
					case 'center':
						return ((stage.getHeight() - _logoHeight) * 0.5) + offsetY;
						break;
					case 'bottom':
						return stage.getHeight() - _logoHeight - offsetY;
						break;
				}
			}
			return 0;
		}
	},

	menuBullet:{
		updateSize: function(isSub) {
			var testBuild = document.createElement('div'),
				fontSize = isSub ? LAYOUT_MODEL.menuSubFontSize : LAYOUT_MODEL.menuFontSize;
			testBuild.innerHTML = menuBullet[LAYOUT_MODEL.menuBulletType];
			testBuild.setAttribute('style', 'margin:0; padding:0; position:absolute; display:block; white-space:nowrap; opacity:0; font-family:' + LAYOUT_MODEL.menuFont + '; font-size:' + fontSize + 'px;');
			document.body.appendChild(testBuild);
			layoutCalcs.menuBullet.width = parseInt(window.getComputedStyle(testBuild, null).width, 10);
			layoutCalcs.menuBullet.height = parseInt(window.getComputedStyle(testBuild, null).height, 10);
			document.body.removeChild(testBuild);
			testBuild = null;
		}
	},

	menuText:{
		x:function(_menuTextWidth) {
			if(USER_AGENT !== POD) {
				switch(LAYOUT_MODEL.menuTextAlignHorizontal) {
					case 'left':
						return percentToPixels(LAYOUT_MODEL.menuTextPaddingHorizontal, stage.getWidth());
						break;
					case 'center':
						return ((layoutCalcs.menuView.width() - _menuTextWidth) * 0.5);
						break;
					case 'right':
						return layoutCalcs.menuView.width() - _menuTextWidth - percentToPixels(LAYOUT_MODEL.menuTextPaddingHorizontal, stage.getWidth());
						break;
				}
			}
			return 0;
		},
		y:function(_menuTextHeight) {
			if(USER_AGENT === PAD && LAYOUT_MODEL.menuTextAlignVertical === 'top' && LAYOUT_MODEL.logoAlignHorizontal === LAYOUT_MODEL.menuAlignHorizontal && percentToPixels(LAYOUT_MODEL.menuTextPaddingVertical, stage.getHeight()) < layoutCalcs.logoView.y() + layoutCalcs._logoHeight) {
				return layoutCalcs._logoHeight;
			} else if(USER_AGENT === POD) {
				return layoutCalcs._logoHeight;
			} else {
				switch(LAYOUT_MODEL.menuTextAlignVertical) {
					case 'top':
						return percentToPixels(LAYOUT_MODEL.menuTextPaddingVertical, stage.getHeight());
						break;
					case 'center':
						if(_menuTextHeight > layoutCalcs.menuView.height()) {
							return 0;
						}
						return ((layoutCalcs.menuView.height() - _menuTextHeight) * 0.5) + percentToPixels(LAYOUT_MODEL.menuTextPaddingVertical, stage.getHeight());
						break;
					case 'bottom':
						return layoutCalcs.menuView.height() - _menuTextHeight - percentToPixels(LAYOUT_MODEL.menuTextPaddingVertical, stage.getHeight());
						break;
				}
			}
			return 0;
		}
	},

	menuView:{
		x:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return 0;
			}
			if(LAYOUT_MODEL.menuAlignHorizontal === 'right') {
				return percentToPixels(LAYOUT_MODEL.sitePaddingRight, stage.getWidth());
			} else {
				return percentToPixels(LAYOUT_MODEL.sitePaddingLeft, stage.getWidth());
			}
		},
		y:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return 0;
			}
			return percentToPixels(LAYOUT_MODEL.sitePaddingTop, stage.getHeight()) + percentToPixels(LAYOUT_MODEL.menuOffsetY, stage.getHeight());
		},
		width:function() {
			if(USER_AGENT === POD) {
				return stage.getWidth();
			} else if(USER_AGENT === PAD) {
				return percentToPixels(LAYOUT_MODEL.menuWidth, stage.getWidth());
			} else {
				return percentToPixels(LAYOUT_MODEL.menuWidth, stage.getWidth());
			}
		},
		height:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return stage.getHeight();
			}
			return (stage.getHeight() - (percentToPixels(LAYOUT_MODEL.sitePaddingTop, stage.getHeight()) + percentToPixels(LAYOUT_MODEL.sitePaddingBottom, stage.getHeight()))) - percentToPixels(LAYOUT_MODEL.menuOffsetY, stage.getHeight());
		}
	},

	mediaView:{
		x:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return 0;
			} else if(LAYOUT_MODEL.menuAlignHorizontal === 'right') {
				return percentToPixels(LAYOUT_MODEL.sitePaddingLeft, stage.getWidth());
			}
			return percentToPixels(LAYOUT_MODEL.sitePaddingLeft, stage.getWidth()) + percentToPixels(LAYOUT_MODEL.menuWidth, stage.getWidth()) - percentToPixels(LAYOUT_MODEL.menuOffsetX, stage.getWidth());
		},
		y:function() {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return 0;
			}
			return percentToPixels(LAYOUT_MODEL.sitePaddingTop, stage.getHeight());
		},
		width:function(thumbHMargin) {
			var margin = thumbHMargin || 0;
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return stage.getWidth();
			} else if(LAYOUT_MODEL.menuAlignHorizontal === 'right') {
				return (stage.getWidth() - (percentToPixels(LAYOUT_MODEL.sitePaddingLeft, stage.getWidth()) + percentToPixels(LAYOUT_MODEL.sitePaddingRight, stage.getWidth()) + layoutCalcs.menuView.width() + percentToPixels(LAYOUT_MODEL.menuOffsetX, stage.getWidth()) + margin));
			}
			return (stage.getWidth() - (percentToPixels(LAYOUT_MODEL.sitePaddingLeft, stage.getWidth()) + percentToPixels(LAYOUT_MODEL.sitePaddingRight, stage.getWidth()) + layoutCalcs.menuView.width() - percentToPixels(LAYOUT_MODEL.menuOffsetX, stage.getWidth()) + margin));
		},
		height:function() {
			if(USER_AGENT === PAD && LAYOUT_MODEL.navbarAlignVertical === 'bottom' && layoutCalcs.isDisplace() && layoutCalcs._section.thumb.side === 'bottom') {
				return stage.getHeight() - 30;
			}
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				return stage.getHeight();
			}
			return (stage.getHeight() - (percentToPixels(LAYOUT_MODEL.sitePaddingTop, stage.getHeight()) + percentToPixels(LAYOUT_MODEL.sitePaddingBottom, stage.getHeight())));
		},
		size:{
			getX: function() {
				return layoutCalcs.mediaView.x();
			},
			getY: function() {
				return layoutCalcs.mediaView.y();
			},
			getWidth: function() {
				return layoutCalcs.mediaView.width();
			},
			getHeight: function() {
				return layoutCalcs.mediaView.height();
			}
		}
	},

	landingMediaView:{
		x:function() {
			if(LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
				return 0;
			}
			return layoutCalcs.mediaView.x();
		},
		y:function() {
			if(LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
				return 0;
			}
			return layoutCalcs.mediaView.y();
		},
		width:function() {
			if(LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
				return stage.getWidth();
			}
			return layoutCalcs.mediaView.width();
		},
		height:function() {
			if(LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
				return stage.getHeight();
			}
			return layoutCalcs.mediaView.height();
		}
	},

	mediaMask: {
		x:function() {
			if(layoutCalcs._activeView === 'contact') {
				return 0;
			}
			if(USER_AGENT === POD) {
				return layoutCalcs._activeView === 'page' ? 10 : 0;
			}
			var section = layoutCalcs._section,
				x = percentToPixels(LAYOUT_MODEL[layoutCalcs._activeView + 'MarginLeft'], stage.getWidth());
			if(layoutCalcs.isDisplace() && section.thumb.side === 'left') {
				return x + percentToPixels(section.thumb.horizontalMargin, stage.getWidth()) + THUMB_SIZES[section.thumb.size].width;
			}
			return x;
		},
		y:function() {
			if(layoutCalcs._activeView === 'contact') {
				return 0;
			}
			if(USER_AGENT === POD) {
				return 0;
			}
			var section = layoutCalcs._section,
				y = percentToPixels(LAYOUT_MODEL[layoutCalcs._activeView + 'MarginTop'], stage.getHeight());
			if(layoutCalcs.isDisplace() && section.thumb.side === 'top') {
				return y + percentToPixels(section.thumb.verticalMargin, stage.getHeight()) + THUMB_SIZES[section.thumb.size].height;
			}
			return y;
		},
		width:function() {
			if(layoutCalcs._activeView === 'contact') {
				return layoutCalcs.mediaView.width();
			}
			if(USER_AGENT === POD) {
				return layoutCalcs._activeView === 'page' ? stage.getWidth() - 20 : stage.getWidth();
			}
			var section = layoutCalcs._section,
				width = layoutCalcs.mediaView.width() - (percentToPixels(LAYOUT_MODEL[layoutCalcs._activeView + 'MarginLeft'], stage.getWidth()) + percentToPixels(LAYOUT_MODEL[layoutCalcs._activeView + 'MarginRight'], stage.getWidth()));
			if(layoutCalcs.isDisplace() && (section.thumb.side === 'left' || section.thumb.side === 'right')) {
				return width - percentToPixels(section.thumb.horizontalMargin, stage.getWidth()) - THUMB_SIZES[section.thumb.size].width;
			}
			return width;
		},
		height:function() {
			if(layoutCalcs._activeView === 'contact') {
				return layoutCalcs.mediaView.height();
			}
			if(USER_AGENT === POD) {
				return stage.getHeight();
			}
			var section = layoutCalcs._section,
				height = layoutCalcs.mediaView.height() - (percentToPixels(LAYOUT_MODEL[layoutCalcs._activeView + 'MarginTop'], stage.getHeight()) + percentToPixels(LAYOUT_MODEL[layoutCalcs._activeView + 'MarginBottom'], stage.getHeight()));
			if(layoutCalcs.isDisplace() && (section.thumb.side === 'top' || section.thumb.side === 'bottom')) {
				return height - percentToPixels(section.thumb.verticalMargin, stage.getHeight()) - THUMB_SIZES[section.thumb.size].height;
			}
			return height;
		}
	},

	captionView:{
		x: function() {
			var section = layoutCalcs._section,
				margin = x = percentToPixels(LAYOUT_MODEL[layoutCalcs._activeView + 'MarginLeft'], stage.getWidth()),
				offset = section && section.thumb && section.thumb.side === 'left' && layoutCalcs._thumbsVisible ? percentToPixels(section.thumb.horizontalMargin, stage.getWidth()) + THUMB_SIZES[section.thumb.size].width : 0;
			if(USER_AGENT === POD) return (layoutCalcs.mediaMask.width() - layoutCalcs.captionView.width) * 0.5 + offset;
			return (layoutCalcs.mediaMask.width() - layoutCalcs.captionView.width) * 0.5 + offset + margin;
		},
		y: function() {
			var bottomOffset = 33;
			if(layoutCalcs.isDisplace() && layoutCalcs._section.thumb.side === 'bottom') {
				return layoutCalcs.mediaView.height() - percentToPixels(layoutCalcs._section.thumb.verticalMargin, stage.getHeight()) - THUMB_SIZES[layoutCalcs._section.thumb.size].height - layoutCalcs.captionView.height - bottomOffset;
			} else {
				return layoutCalcs.mediaView.height() - layoutCalcs.captionView.height - bottomOffset;
			}
		},
		size: function(captionText) {
			layoutCalcs.captionView.currentText = captionText || layoutCalcs.captionView.currentText;
			var testBuild = document.createElement('span'), padding = 10;
			testBuild.innerHTML = layoutCalcs.captionView.currentText;
			testBuild.setAttribute('style', 'position:absolute; display:block; white-space:nowrap; opacity:0; font-family:' + LAYOUT_MODEL.captionFont + '; font-size:' + LAYOUT_MODEL.captionFontSize + 'px;');
			document.body.appendChild(testBuild);
			if(testBuild.clientWidth > layoutCalcs.mediaMask.width()) {
				layoutCalcs.captionView.width = layoutCalcs.mediaMask.width();
				testBuild.style.width = layoutCalcs.mediaMask.width() + 'px';
				testBuild.style.whiteSpace = 'normal';
				layoutCalcs.captionView.height = testBuild.clientHeight + padding;
			} else {
				layoutCalcs.captionView.width = testBuild.clientWidth + padding;
				layoutCalcs.captionView.height = testBuild.clientHeight + padding;
			}
			document.body.removeChild(testBuild);
			testBuild = null;
		}
	},

	isDisplace:function() {
		return USER_AGENT !== POD && layoutCalcs._section && layoutCalcs._thumbsVisible ? layoutCalcs._section.thumb && layoutCalcs._section.thumb.type === 'strip' && layoutCalcs._section.thumb.dock === 'displace': false;
	}

};

function SiteService(vars) {

	var self = new ControllerProxy({events:vars.events});

	function init() {
		self.events.addEventListener(SITE_SERVICE_CONTACT_FORM_SUBMIT, contactFormSubmit);
		self.events.addEventListener(SITE_SERVICE_SHARE_FORM_SUBMIT, shareFormSubmit);
		self.events.addEventListener(SITE_SERVICE_INQUIRY_FORM_SUBMIT, inquiryFormSubmit);
		self.events.addEventListener(SITE_SERVICE_GET_FILE, getFile);
	}

	function contactFormSubmit(e) {
		new JSONP({url:REWRITE_BASE + PHP + 'SiteService.php', params:'method=contactForm' + e, callback:'dx.site.service.contactResponse'});
	}

	self.contactResponse = function(response) {
		if(response.status === 'pass') {
			dx.site.events.dispatchEvent(CONTACT_FORM_SENT, response);
		} else {
			throw 'contact form failed';
		}
	};

	function shareFormSubmit(e) {
		new JSONP({url:REWRITE_BASE + PHP + 'SiteService.php', params:'method=shareForm' + e, callback:'dx.site.service.shareResponse'});
	}

	self.shareResponse = function(response) {
		if(response.status === 'pass') {
			dx.site.events.dispatchEvent(SHARE_FORM_SENT, response);
		} else {
			throw 'share form failed';
		}
	};

	function inquiryFormSubmit(e) {
		new JSONP({url:REWRITE_BASE + PHP + 'SiteService.php', params:'method=inquiryForm' + e, callback:'dx.site.service.inquiryResponse'});
	}

	self.inquiryResponse = function(response) {
		if(response.status === 'pass') {
			dx.site.events.dispatchEvent(INQUIRY_FORM_SENT, response);
		} else {
			throw 'inquiry form failed';
		}
	};

	function getFile(e) {
		new JSONXHR({url:REWRITE_BASE + PHP + 'SiteService.php', params:{
			method:'getFile',
			name:e.name
		}, callback:self.customJsResponse});
	}

	self.customJsResponse = function(response) {
		if(response.content) {
			var head = document.getElementsByTagName('head')[0];
			var script = document.createElement('script');
		    script.type = 'text/javascript';
		    script.id = 'customJs';
		    script.innerHTML = response.content.replace(/\\/g, '');
			var id = document.getElementById("customJs");
			if(!id) {
				head.appendChild(script);
			} else {
				head.replaceChild(script, id);
			}
		}
	};

	init.call(self);
	return self;
}

var Address = {

	path: '',
	pathNames: [],
	listeners: [],
	firstload: true,
	docTitle: document.title || '',
	base: REWRITE_BASE,
	ga: (ACCOUNT_MODEL.pageStatistics.search("UA-") > -1),

	addEventListener: function(type, callback) {

		if(window.history.pushState && window.location.hash) {
			this.removeHash(window.location.hash);
		} else if(!window.history.pushState) {
			this.addHash();
		}

		this.listeners.push({'type':type, 'listener':callback});

		this.setListeners();

		setTimeout(function(){
			Address.fireListeners.call(Address);
		}, 0);
	},

	setListeners: function() {
		if(window.attachEvent) {
			this.savedHash = window.location.hash;
			setInterval(this.checkHash, 200);
		} else {
			window.onpopstate = window.onhashchange = function() {
				Address.fireListeners();
			};
		}
	},

	fireListeners: function() {
		var i;
		this.path = this.getPath();
		this.pathNames = this.getPathNames();
		for(i in this.listeners) {
			if(document.webkitIsFullScreen) {
				this.listeners[i].listener(this.value);
			} else {
				this.listeners[i].listener(this.path);
			}
		}
	},

	setValue: function(value) {
		value = this.addDashes(value);
		this.firstload = false;
		this.value = value = this.trim(value) === '' ? '' : value = this.trim(value) + '/';
		if(value.indexOf("http") === 0) {
			window.location = value;
		} else if(window.history.pushState && !document.webkitIsFullScreen) {
			window.history.pushState({value:this.base + value}, '', this.base + value);
			this.fireListeners();
		} else if(!document.webkitIsFullScreen) {
			window.location = this.base + '#/' + value;
		} else {
			this.fireListeners();
		}
		if(window._gaq && this.ga) {
			window._gaq.push(['_trackPageview']);
		}
	},

	setPath: function(value) {
		this.setValue(value.replace("?", "%3F"));
	},

	back: function() {
		history.back();
	},

	setLocation: function(value) {
		window.location = value;
	},

	getBase: function() {
		return this.base;
	},

	getPath: function() {
		if(this.value && document.webkitIsFullScreen) {
			return decodeURIComponent(this.trim(this.value));
		} else if(window.location.hash) {
			return decodeURIComponent(this.trim(window.location.hash));
		} else {
			return decodeURIComponent(this.trim(window.location.pathname));
		}
	},

	getQuery: function() {
		return window.location.search;
	},

	getPathNames: function() {
		return this.getPath().split('/');
	},

	setTitle: function(title) {
		document.title = this.docTitle = title;
	},

	getTitle: function() {
		return this.docTitle || document.title;
	},

	addHash: function() {
		window.location = this.base + '#/' + this.trim(this.getPath());
	},

	removeHash: function(hash) {
		window.location = this.base + this.trim(hash);
	},

	getHost: function() {
		return window.location.host;
	},

	addDashes: function(str) {
		str = str.replace(/ /g, '-');
		str = str.replace(/%20/g, '-');
		return str;
	},

	trim: function(str) {
		if(this.base !== '/') {
			str = str.replace(this.base, '');
		}
		return str.replace(/^#/, '').replace(/^\s+|\s+$/g, '').replace(/^\/|\/$/g, '');
	},

	checkHash: function() {
		if(Address.savedHash !== window.location.hash) {
			Address.savedHash = window.location.hash;
			Address.fireListeners();
		}
	}
};

function AddressController(vars) {

	var self = new ControllerProxy({parentView:vars.parentView, parentController:vars.parentController, events:vars.siteControllerEvents});

	var section = {},
		path = '',
		assetId = 0,
		mediaTitle,
		mode = '',
		lastSectionId = -1,
		lastUri;


	Address.addEventListener(ADDRESS_CHANGE, uriChange);

	self.events.addEventListener(ADDRESS_SECTION_CHANGE, sectionChange);
	self.events.addEventListener(ADDRESS_OVERLAY_CHANGE, overlayChange);
	self.events.addEventListener(ADDRESS_ASSET_CHANGE, assetChange);

	function uriChange(uri) {
		if(lastUri !== uri && section) {
			parseState(uri);
			lastSectionId = section.id;
			var state = {
				section:section,
				path:path,
				assetId:assetId,
				mode:mode,
				info:{
					isIntroGallery:section.isIntroGallery
				}
			};
			self.setTitle();
			layoutCalcs._section = state.section;
			layoutCalcs._thumbsVisible = hasMode('thumbs');
			self.events.dispatchEvent(ADDRESS_URI_CHANGE, state);
		}
		lastUri = uri;
	}

	function parseState(uri) {
		uri = uri.split("/");
		mode = isOverlayState(uri);
		delete section.isIntroGallery;
		section = getSection(uri);
		assetId = isAssetNumber(uri) ? isAssetNumber(uri) : 0;
		assetId = assetId !== 0 ? assetId -1 : assetId;
		if(!isAssetNumber(uri) && isThumbsFirst(section) && hasMode('thumbs')) {
			assetId = -1;
		}
	}

	function getSection(uri) {
		path = dashCheck(uri);
		if(path) {
			return SECTIONS_MODEL[path];
		} else if(SETTINGS_MODEL.backgroundImages) {
			return {id:LANDING_MEDIA,label:LANDING_MEDIA,visible:1,password:'',media:SETTINGS_MODEL.backgroundImages};
		} else {
			getFirstMediaItem();
			assetId = 0;
			section = SECTIONS_MODEL[path];
			section.isIntroGallery = true;
			if(section && section.thumb && section.thumb.type !== 'none' && section.thumb.defaultOn) {
				addMode('thumbs');
			}
			return section;
		}
	}

	function dashCheck(uri) {
		var tests = [
			uri[0] + '/' + uri[1],
			uri[0]
		];
		var length = tests.length;
		if(length > 0) {
			var i = 0;
			for(; i < length; i++) {
				if(SECTIONS_MODEL[testDashes(tests[i])]) {
					return testDashes(tests[i]);
				}
			}
		}
		return false;
	}

	function testDashes(str) {
		var arr = str.split('-');
		var combos = binaryCombos(arr.length);
		var x = combos.length;
		while(x--) {
			var length = arr.length;
			var y = 0;
			var newString = '';
			for(;y < length; y++) {
				newString += arr[y];
				if(y < length - 1) {
					newString += combos[x][y];
				}
			}
			if(SECTIONS_MODEL[newString]) {
				return newString;
			}
		}
		return false;
	}

	function binaryCombos(n){
		var result = [],
			x,
			y;
		for(y = 0; y < Math.pow(2, n); y++){
			var combo = [];
			for(x = 0; x < n; x++){
				if((y >> x) & 1)
					combo.push(" ");
				else
					combo.push("-");
			}
			result.push(combo);
		}
		return result;
	}

	function getFirstMediaItem() {
		var firstSection = getFirstValidSub(SECTIONS_MODEL['ROOT_SECTION']);
		var firstSub = getFirstValidSub(firstSection);

		if(firstSub) {
			path = firstSection.label + "/" + firstSub.label;
			checkThumbsDefaultOn(firstSub);
			checkCaptionDefault();
		} else {
			path = firstSection.label;
			checkThumbsDefaultOn(firstSection);
			checkCaptionDefault();
		}
	}

	function checkThumbsDefaultOn(section) {
		if(exists(section, 'thumb.type') !== 'none' && exists(section, 'thumb.defaultOn')) {
			addMode('thumbs');
		}
	}

	function isThumbsFirst(section) {
		return (exists(section, 'thumb.defaultOn') && exists(section, 'thumb.type') === 'fill' && USER_AGENT !== POD && LAYOUT_MODEL.overlayAlpha === 1)
	}

	function checkCaptionDefault() {
		if(LAYOUT_MODEL.captionDefault) addMode('caption');
	}

	function isAssetNumber(uri) {
		if(!isNaN(uri[2]) && uri[2].length > 0) {
			return Number(uri[2]);
		} else if(!isNaN(uri[1])) {
			return Number(uri[1]);
		}
		return false;
	}

	function isOverlayState(uri) {
		var overlay = /thumbs|email|inquiry|caption|fotomoto/;
		if(overlay.test(uri[3])) {
			return uri[3];
		} else if(overlay.test(uri[2])) {
			return uri[2];
		} else if(overlay.test(uri[1])) {
			return uri[1];
		}
		return '';
	}

	function isLink(str) {
		return (str.indexOf("http") === 0 || str.indexOf("/") === 0 || str.indexOf("ftp") === 0);
	}

	function isEmail(str) {
		return /[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/g.test(str);
	}

	function formatEmail(str) {
		return "mailTo:" + str.replace("mailto:", "").replace("mailTo:", "");
	}

	function isSocialPopup(label) {
		return (label === '[one]' || label === '[like]');
	}

	function buildPath(value) {
		var newPath = value || path || '';
		var strPath = '';
		if(newPath) {
			strPath = REWRITE_BASE + newPath + '/';
			if(assetId > -1) {
				strPath += (Number(assetId) + 1) + '/';
			}
			if(mode.length > 0) {
				strPath += mode + '/';
			}
		}
		return strPath;
	}

	function sectionChange(e) {
		var newPath = e.path || path;
		if(isSocialPopup(e.label)) {
			self.events.dispatchEvent(POP_UP, e);
		} else if(isEmail(newPath)) {
			window.location.href = formatEmail(newPath);
		} else if(e.linkTarget === '_blank') {
			window.open(newPath);
		} else if(e.linkTarget === '_self' && e.type === 'external') {
			window.location.href = newPath;
		} else {
			path = e.path;
			assetId = e.assetId || 0;
			mode = e.mode;
			if(exists(e, 'section.thumb.type') === 'none') {
				removeMode('thumbs');
			}
			if(e.section && isThumbsFirst(e.section) && assetId === 0) {
				assetId = -1;
			}
			checkCaptionDefault();
			removeMode('inquiry');
			removeMode('email');
			Address.setPath(buildPath(e.path));
		}
	}

	function overlayChange(e) {
		toggleOverlayState(e);
		Address.setPath(buildPath());
	}

	function assetChange(e) {
		if(e === 'prev' || e === LEFT_ARROW) {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				assetId = assetId > 0 ? assetId - 1 : 0;
			} else {
				assetId = assetId > 0 ? assetId - 1 : csvToArray(section.media).length - 1;
			}
		} else if(e === 'next' || e === RIGHT_ARROW) {
			if(USER_AGENT === POD || USER_AGENT === PAD) {
				assetId = assetId < csvToArray(section.media).length - 1 ? assetId + 1 : csvToArray(section.media).length - 1;
			} else {
				assetId = assetId < csvToArray(section.media).length - 1 ? assetId + 1 : 0;
			}
		}

		removeMode('inquiry');
		removeMode('email');

		Address.setPath(buildPath());
	}

	function hasMode(value) {
		return mode ? mode.indexOf(value) !== -1 : false;
	}

	function addMode(value) {
		removeMode('email');
		removeMode('inquiry');
		if(!hasMode(value)) {
			mode = mode ? mode + '-' + value : value;
		}
	}

	function removeMode(value) {
		mode = mode ? mode.replace(value, '').replace(/^-|-$/g, '').replace(/(--)/g, '-') : '';
	}

	function toggleOverlayState(value) {
		if(hasMode(value)) {
			removeMode(value);
		} else {
			addMode(value);
		}
	}

	self.setTitle = function() {
		var media = csvToArray(section.media);
		var mediaItem = MEDIA_MODEL[media[assetId]];

		if(mediaItem && mediaItem.label && section.id !== LANDING_MEDIA && !SETTINGS_MODEL.globalBrowserTitle) {
			if(mediaItem.label === SETTINGS_MODEL.browserTitle) {
				Address.setTitle(mediaItem.label);
			} else {
				Address.setTitle(mediaItem.label + ' - ' + decodeURIComponent(SETTINGS_MODEL.browserTitle));
			}
		} else {
			if(mediaItem && mediaItem.label && section.id !== LANDING_MEDIA) {
				var fullpath = path.split('/');
				mediaTitle = " - " + fullpath[fullpath.length - 1] + " - " + (assetId + 1);
			} else {
				mediaTitle = '';
			}
			Address.setTitle(decodeURIComponent(SETTINGS_MODEL.browserTitle) + mediaTitle);
		}
	};

	return self;
}

function FooterController(vars) {

	var self = new ControllerProxy({parentView:vars.parentView, parentController:vars.parentController, events:vars.siteControllerEvents});

	self.updateSpeed = 0;

	self.footerView = new vars.FooterView(vars);
	self.footerView.setAlpha(0);
	self.parentView.addChild(self.footerView);

	self.events.addEventListener(LAYOUT_MODEL_CHANGE, modelChange);
	self.events.addEventListener(MEDIA_MODEL_CHANGE, modelChange);
	self.events.addEventListener(SETTINGS_MODEL_CHANGE, modelChange);
	self.events.addEventListener(RESIZE_END, self.footerView.updatePosition);
	self.events.addEventListener(MENU_DOCK_CLOSE, menuDockClose);
	self.events.addEventListener(MENU_DOCK_OPEN, menuDockOpen);

	self.footerView.addEventListener(FOOTER_CLICK, clickListener);

	function clickListener(e) {
		var navEvent = {
			type: e.type === 'link' ? 'external' : 'internal',
			path: e.content,
			linkTarget: e.linkTarget,
			label: e.label
		};
		self.events.dispatchEvent(FOOTER_CLICK, navEvent);
	}

	function modelChange(e) {
		if(e) {
			if(/copyright/.test(e.id) || /social/.test(e.id) || /layoutModel/.test(e.id) || /copyrightOffset/.test(e.id)) {
				self.footerView.updateStyle(e);
				self.footerView.updatePosition(e);
			} else if(e.filters === 'Footer Links') {
				self.footerView.updateStyle(e);
				self.footerView.updatePosition(e);
			}
		}
	}

	function menuDockClose() {
		self.footerView.hide();
	}

	function menuDockOpen() {
		self.footerView.show();
	}

	return self;
}

function FooterView(vars) {

	var textGroup,
		iconGroup,
		self = new ViewProxy({events:vars.events});
	self.setZIndex(3);
	self.updateSpeed = 0;
	self.setAlpha(0);

	self.element.setAttribute('class', 'footerview');

	var tile,
		footerBtns;

	self.addEventListener(CHILD_ADDED, function(child) {
		setTimeout(function() {
			if(USER_AGENT !== POD) self.show();
		}, 1200);
	});

	self.updateStyle = function(e) {
		self.setText('');
		self.children = [];
		buildLinks();
		self.updatePosition();
	};

	self.updatePosition = function(e) {
		if(self.updateSpeed === 0) {
			self.setX(layoutCalcs.footerView.x(self.getWidth()));
			self.setY(layoutCalcs.footerView.y(self.getHeight()));
		} else {
			Tween(self, self.updateSpeed, {x:layoutCalcs.footerView.x(self.getWidth())});
			Tween(self, self.updateSpeed, {y:layoutCalcs.footerView.y(self.getHeight())});
		}

		self.updateSpeed = UPDATE_SPEED;
	};

	self.hide = function() {
		Tween(self, UPDATE_SPEED, {alpha:0, onComplete:function() {
			self.setDisplay('none');
		}});
	};

	self.show = function() {
		self.setDisplay('block');
		self.updateStyle();
		Tween(self, UPDATE_SPEED, {alpha:1});
	};

	function buildLinks() {
		footerBtns = csvToArray(SETTINGS_MODEL.socialLinks);
		if(SETTINGS_MODEL.copyright !== '') footerBtns.unshift(SETTINGS_MODEL.copyright);
		var i = 0,
			length = footerBtns.length,
			axis = USER_AGENT === POD ? 'y' : 'x';

		iconGroup = new FooterTileGroup('x');
		textGroup = new FooterTileGroup(axis);

		self.addChild(textGroup);
		self.addChild(iconGroup);

		for(;i<length;i++) {
			var item = footerBtns[i];
			var btn = new Sprite();
			var isIconGroup = false;

			var link;

			var mediaItem = getMediaById(item);
			if(mediaItem) {
				if(isValidSocialIcon(item)) {
					var icon = socialIcons[mediaItem.label.toLowerCase()];
					link = loadIcon(icon.filename, icon.width, icon.height);
					isIconGroup = true;
				} else {
					link = buildLabel(mediaItem.label);
				}
			} else {
				link = buildLabel(item);
			}

			btn.addChild(link);
			btn.setWidth(link.getWidth());
			btn.setHeight(link.getHeight());
			btn.label = link;

			if(isIconGroup) {
				iconGroup.addChild(btn);
			} else {
				textGroup.addChild(btn);
			}

			if(MEDIA_MODEL[item]) {

				btn.btn = btn;
				btn.data = MEDIA_MODEL[item];

				if(TOUCH_DEVICE) {
					btn.addEventListener(TOUCH_END, function(e){
						self.dispatchEvent(FOOTER_CLICK, e.target.data);
					});
				} else {
					btn.addEventListener(CLICK, function(e){
						self.dispatchEvent(FOOTER_CLICK, e.target.data);
					});
					btn.addEventListener(MOUSE_OVER, over);
					btn.addEventListener(MOUSE_OUT, out);
				}
			}
		}

		if(length > 0) layoutBtns();
	}

	function loadIcon(src, width, height) {
		var icon = new vars.Bitmap();
		icon.setSrc(REWRITE_BASE + ICONS + src);
		icon.setWidth(width);
		icon.setHeight(height);
		icon.addEventListener(LOAD, iconLoaded);
		return icon;
	}

	function iconLoaded(e) {
		layoutBtns();
		self.updatePosition();
	}

	function isValidSocialIcon(item) {
		var mediaItem = getMediaById(item);
		return item && mediaItem && mediaItem.label && socialIcons[mediaItem.label.toLowerCase()] && !isNaN(item);
	}

	function buildLabel(name) {
		var tester = document.createElement('span');
		tester.style.position = 'absolute';
		tester.style.display = 'block';
		tester.style.padding = '0';
		tester.style.margin = '0';
		tester.style.fontFamily = LAYOUT_MODEL.copyrightFont;
		tester.style.fontSize = LAYOUT_MODEL.copyrightFontSize.toString().replace('px', '') + 'px';
		tester.innerHTML = name;
		document.body.appendChild(tester);
		var label = new vars.Sprite();
		label.setFontFamily(LAYOUT_MODEL.copyrightFont);
		label.setFontSize(LAYOUT_MODEL.copyrightFontSize.toString().replace('px', '') + 'px');
		label.setFontColor(LAYOUT_MODEL.copyrightColor);
		label.setText(name);
		label.setTextWrap(USER_AGENT === POD);
		label.setWidth(tester.offsetWidth);
		label.setHeight(tester.offsetHeight);
		document.body.removeChild(tester);
		tester = null;
		return label;
	}

	function over(e) {
		Tween(this.btn, UPDATE_SPEED, {alpha:0.5});
	}

	function out(e) {
		Tween(this.btn, UPDATE_SPEED, {alpha:1});
	}

	function layoutBtns() {
		var groups = self.children,
			i,
			length = groups.length;

		textGroup.layoutContent();
		iconGroup.layoutContent();

		tile = new Tile();
		tile.gap = 6;
		tile.axis = USER_AGENT === POD ? 'y' : 'x';
		tile.align = 'left';
		tile.wrap = false;
		if(tile.axis === 'x') {
			tile.perpLength = stage.getHeight();
		} else {
			tile.perpLength = stage.getWidth();
		}

		for(i = 0; i < length; i++) {
			tile.addItem(groups[i].getWidth(), groups[i].getHeight());
		}

		tile.layoutItems();

		for(i = 0; i < length; i++) {
			var position = tile.getPosition(i);
			groups[i].setX(position.x);
			groups[i].setY(position.y);
		}

		var bounds = tile.getBounds();
		self.setTransition(0);
		self.setWidth(bounds.width);
		self.setHeight(bounds.height);
	}

	function FooterTileGroup(axis) {
		var me = new Sprite();

		me.layoutContent = function() {
			var t = new Tile();
			t.gap = 6;
			t.axis = axis;
			t.align = 'left';
			t.wrap = false;
			t.perpLength = 150;

			var i = 0,
				panels = me.children,
				length = panels.length;

			for(; i < length; i++) {
				t.addItem(panels[i].getWidth(), panels[i].getHeight());
			}

			t.layoutItems();

			for(i = 0; i < length; i++) {
				var position = t.getPosition(i);
				panels[i].setX(position.x);
				panels[i].setY(position.y);
			}

			var bounds = t.getBounds();
			me.setWidth(bounds.width);
			me.setHeight(bounds.height);
		};

		return me;
	}

	return self;
}



function LogoController(vars) {

	var self = new ControllerProxy({parentView:vars.parentView, parentController:vars.parentController, events:vars.siteControllerEvents});
	self.updateSpeed = 0;

	self.logoView = new vars.LogoView({parentView:self.parentView, parentController:self, events:vars.siteControllerEvents});
	self.logoView.setAlpha(0);

	self.events.addEventListener('LOGO_CHILD_ADDED', show);

	self.parentView.addChild(self.logoView);

	self.events.addEventListener(LAYOUT_MODEL_CHANGE, layoutModelChange);
	self.events.addEventListener(RESIZE_END, self.logoView.updatePosition);
	self.events.addEventListener(MENU_DOCK_CLOSE, menuDockClose);
	self.events.addEventListener(MENU_DOCK_OPEN, menuDockOpen);

	self.logoView.addEventListener(CLICK, click);
	self.logoView.addEventListener(MOUSE_OVER, over);
	self.logoView.addEventListener(MOUSE_OUT, out);

	function layoutModelChange(e) {
		self.logoView.updateStyle(e);
		self.logoView.updatePosition(e);
	}

	function over(e) {
		Tween(this, UPDATE_SPEED, {alpha:0.5});
	}

	function out(e) {
		Tween(this, UPDATE_SPEED, {alpha:1});
	}

	function menuDockClose() {
		hide();
	}

	function menuDockOpen() {
		show();
	}

	function hide() {
		self.logoView.style.pointerEvents = 'none';
		Tween(self.logoView, UPDATE_SPEED, {alpha:0, display:'none'});
	}

	function show() {
		self.logoView.setDisplay('block');
		Tween(self.logoView, UPDATE_SPEED, {delay:1, alpha:1, onComplete:function(){
			self.logoView.style.pointerEvents = 'auto';
		}});
	}

	function click(e) {
		var navEvent = {
			type:'internal',
			path:'',
			target:'_self'
		};
		self.events.dispatchEvent(LOGO_CLICK, navEvent);
	}

	show();

	return self;

}

function LogoView(vars) {

	var self = new ViewProxy({events:vars.events});
	self.setTextWrap(false);
	self.setZIndex(5);
	self.updateSpeed = 0;

	self.addEventListener(CHILD_ADDED, function(child) {
		self.updatePosition();
		setTimeout(function(){
			self.updatePosition();
			self.updateSpeed = UPDATE_SPEED;
		}, 500);
		self.events.dispatchEvent('LOGO_CHILD_ADDED');
	});

	self.addEventListener(MOUSE_OVER, overLogo);

	function overLogo(e) {
		self.events.dispatchEvent('OVER_LOGO');
	}

	self.updatePosition = function(e) {
		self.setTransition(0);
		if(self.txt) {
			self.setWidth(self.txt.getWidth());
			self.setHeight(self.txt.getHeight());
		} else if(self.img) {
			self.setWidth(self.img.getWidth());
			self.setHeight(self.img.getHeight());
		}
		Tween(self, self.updateSpeed, {x:layoutCalcs.logoView.x(self.getWidth()), y:layoutCalcs.logoView.y(self.getHeight())});
	};

	self.updateStyle = function(e) {
		if(e && e.id && /layoutModelInit|logoFile|tabletLogoFile|mobileLogoFile|logoFont|logoFontSize|logoFontColor|logoAlignHorizontal|logoAlignVertical|logoOffsetX|logoOffsetY|tabletLogoOffsetX|tabletLogoOffsetY/.test(e.id)){
			var src;

			if(USER_AGENT === POD && LAYOUT_MODEL.mobileLogoFile) {
				src = REWRITE_BASE + MEDIA_ORIGINAL + LAYOUT_MODEL.mobileLogoFile;
			} else if(USER_AGENT === PAD && LAYOUT_MODEL.tabletLogoFile) {
				src = REWRITE_BASE + MEDIA_ORIGINAL + LAYOUT_MODEL.tabletLogoFile;
			} else if(LAYOUT_MODEL.logoFile !== '') {
				src = REWRITE_BASE + MEDIA_ORIGINAL + LAYOUT_MODEL.logoFile;
			}

			if(src) {
				if(self.txt) {
					self.removeChild(self.txt);
					self.txt = undefined;
				}
				self.img = new Bitmap();
				self.img.setSrc(src);
				self.img.setAlpha(0);
				self.addChild(self.img);
				self.img.addEventListener(LOAD, logoImgLoaded);
			} else {
				if(self.img) {
					self.removeChild(self.img);
					self.img = undefined;
				}
				self.txt = new Sprite();
				self.txt.setText(LAYOUT_MODEL.logoText);
				self.addChild(self.txt);
				if(LAYOUT_MODEL.logoFontSize !== self.getFontSize()) {
					self.setFontSize(LAYOUT_MODEL.logoFontSize);
				}
				if(LAYOUT_MODEL.logoFontColor !== self.getFontColor()) {
					Tween(self, vars.parentController.updateSpeed, {fontColor:LAYOUT_MODEL.logoFontColor});
				}
				if(LAYOUT_MODEL.logoFont !== self.getFontFamily()) {
					self.setFontFamily(LAYOUT_MODEL.logoFont);
				}
			}
		}
	};

	function logoImgLoaded(e) {
		if(this.getSrc().toLowerCase().search('@2x') > -1) {
			this.setWidth(this.getWidth() * 0.5);
			this.setHeight(this.getHeight() * 0.5);
			self.setWidth(this.getWidth());
			self.setHeight(this.getHeight());
		}
		self.updatePosition();
		self.events.dispatchEvent(LOGO_LOADED);
		Tween(self.img, UPDATE_SPEED, {alpha:1});
	}

	self.updateStyle();

	return self;
}

function MediaController(vars) {

	var self = new ControllerProxy({parentView:vars.parentView, parentController:vars.parentController, events:vars.siteControllerEvents});
	self.updateSpeed = 0;

	var isTransitioning = false,
		curMediaItem,
		newMediaItem,
		oldMediaItem,
		mediaItems = [],
		slideDirection = 'left',
		section,
		assetId,
		mediaUpdate = false,
		mode,
		passwordView,
		event,
		isSwiping = false,
		garbageTimer,
		spamTimer,
		modelChange = false,
		transitionType = LAYOUT_MODEL.transitionType;

	self.touchNav = self.parentController.touchNav;

	self.mediaView = new vars.MediaView(vars);
	self.parentView.addChild(self.mediaView);

	self.landingMediaView = new vars.LandingMediaView(vars);
	self.parentView.addChild(self.landingMediaView);

	vars.mediaView = self.mediaView;

	self.overlayController = new vars.OverlayController(vars);

	self.mediaView.addEventListener(TOUCH_START, touchStart);

	self.events.addEventListener(LAYOUT_MODEL_CHANGE, layoutModelChange);
	self.events.addEventListener(SITE_URI_CHANGE, siteUriChange);
	self.events.addEventListener(SECTIONS_MODEL_CHANGE, sectionsModelChange);
	self.events.addEventListener(SETTINGS_MODEL_CHANGE, settingsModelChange);
	self.events.addEventListener(MEDIA_MODEL_CHANGE, mediaModelChange);
	self.events.addEventListener(RESIZE_END, resizeEnd);
	self.events.addEventListener(ADD_CURSOR_TARGETS, addCursorTargets);
	self.events.addEventListener(REMOVE_CURSOR_TARGETS, removeCursorTargetsOnDemand);
	self.events.addEventListener(AUTHENTICATED, onAuthenticated);

	self.events.addEventListener(TOUCH_NAVIGATION_MOVE, touchNavigation);
	self.events.addEventListener(TOUCH_NAVIGATION_END, touchNavEnd);
	self.events.addEventListener('OPAQUE_OVERLAY_CLOSE', removeOldMediaItem);

	function touchNavEnd(e) {
		if(e && section.id !== LANDING_MEDIA) {
			transition(e.flickSpeed);
		}
	}

	function touchNavigation(e) {
		vars.siteControllerEvents.dispatchEvent(NAVBAR_NAV_CLICK, e);
	}

	function addCursorTargets(e) {
		self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: e.targets, state:'add', id: e.id});
	}

	function removeCursorTargetsOnDemand(e) {
		self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: e.targets, state:'remove', id: e.id});
	}

	function resizeEnd(e) {
		self.mediaView.updatePosition(e, section);
		self.landingMediaView.updatePosition(e, section);
	}

	function siteUriChange(e) {
		if(e.assetId === -1 && e.path) {
			removeAllMediaItems();
			self.mediaView.show();
			self.mediaView.updatePosition();
		}
		self.touchNav.update(e);
		checkPauseMedia(e);
		if(getAuthentication(e)) return;
		section = e.section;
		mode = e.mode;
		if(e.section.id === LANDING_MEDIA) {
			self.mediaView.hide();
			self.landingMediaView.show();
			self.landingMediaView.updateContent(e);
		} else {
			self.landingMediaView.hide();
			self.landingMediaView.stop();
			self.mediaView.show();
			updateContent(e);
		}
		addRemoveCursorTargets(e);
		layoutCalcs.captionView.size();
		if((newMediaItem && USER_AGENT === POD) || (newMediaItem && USER_AGENT === PAD)) {
			newMediaItem.setTranslateX(0);
			newMediaItem.setY(0);
		}
	}

	function checkPauseMedia(e) {

		if(e.mode.search('thumbs') > -1 && e.section.thumb.type === 'fill') {
			self.events.dispatchEvent(PAUSE_MEDIA);
		}

		if(e.mode.search('inquiry') > -1 || e.mode.search('email') > -1) {
			self.events.dispatchEvent(PAUSE_MEDIA);
		}

		if(e.section !== section || e.assetId !== assetId) {
			self.events.dispatchEvent(DESTROY_MEDIA);
		}

		if(mode && e.mode.indexOf('thumbs') === -1 && mode.indexOf('thumbs') > -1) {
			checkAutoPlay(true, e);
		}
	}

	function checkAutoPlay(play, e) {
		if(LAYOUT_MODEL.videoAutoPlay && newMediaItem && newMediaItem.mediaItem.type === VIDEO) {
			if(mode.indexOf('thumbs') === -1 || section.thumb.type !== 'fill') {
				newMediaItem.source = newMediaItem.mediaItem.content;
			} else if(play && section.thumb.type === 'fill' && e && e.section === section && e.assetId === assetId) {
				newMediaItem.video.play();
			}
		}
	}

	function addRemoveCursorTargets(e) {
		if(section && csvToArray(section.media).length < 2) {
			self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: [self.mediaView], state:'add'});
		} else {
			self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: [self.mediaView], state:'remove'});
		}
	}

	function getAuthentication(e) {
		event = e;
		if(e.section && e.section.password.length > 0 && !e.section.hasPassed) {
			if(!passwordView) {
				createPasswordView();
				self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: [self.mediaView], state:'add'});
				self.events.dispatchEvent(PAUSE_MEDIA);
			}
			passwordView.setPassword(e.section.password);
			return true;
		} else if(passwordView) {
			removePasswordView(0);
		}
		return false;
	}

	function createPasswordView() {
		passwordView = new PasswordView(vars);
		self.mediaView.addChild(passwordView);
		passwordView.setZIndex(6000);
		self.mediaView.show();
		self.events.addEventListener(RESIZE_END, passwordView.updatePosition);
	}

	function removePasswordView(duration) {
		Tween(passwordView, duration, {alpha:0, onComplete:function() {
			self.mediaView.removeChild(passwordView);
			self.events.removeEventListener(RESIZE_END, passwordView.updatePosition);
			passwordView.kill();
			passwordView = null;
		}});
	}

	function onAuthenticated() {
		removePasswordView(0.35);
		event.section.hasPassed = true;
		siteUriChange(event);
	}

	function layoutModelChange(e) {
		transitionType = LAYOUT_MODEL.transitionType;
		self.mediaView.updateStyle(e);
		self.mediaView.updatePosition(e, section);
		self.landingMediaView.updateStyle(e);
		self.landingMediaView.updatePosition(e);
		if(e) {
			if(/image/.test(e.id) || /video/.test(e.id) || /page/.test(e.id) || /contact/.test(e.id)) {
				mediaModelChange(e);
			}
		}
	}

	function sectionsModelChange(e) {
		if(section && e.field === 'thumb' && e.id === section.id) {
			mediaUpdate = true;
			section = getSectionById(section.id);
			updateContent({
				section:section,
				path:'',
				assetId:assetId
			});
			if(newMediaItem) newMediaItem.updatePosition();
		}

		addRemoveCursorTargets();
	}

	function mediaModelChange(e) {
		if(section) {
			mediaUpdate = true;
			var evtObj = {
				section:{id:section.id, label:section.label, visible:1 , password:'', media:section.media},
				path:'',
				assetId:assetId,
				mode:''
			};
			modelChange = true;
			transitionType = 'fade';
			updateContent(evtObj);
			self.landingMediaView.updateContent(evtObj);
		} else {
			self.landingMediaView.updateContent();
		}
	}

	function settingsModelChange(e) {
		if(section) {
			var section = getSectionById(section.id);
			var evtObj = {
				section:{id:section.id, label:section.label, visible:1 , password:'', media:section.media, thumb:section.thumb},
				path:'',
				assetId:0,
				mode:''
			};
			modelChange = true;
			transitionType = 'fade';
			updateContent(evtObj);
			self.landingMediaView.updateContent(evtObj);
		} else {
			self.landingMediaView.updateContent();
		}
	}

	function updateContent(e) {

		section = e.section;
		slideDirection = setSlideDirection(e.assetId);
		if(section && section.id === LANDING_MEDIA) {
			self.mediaView.hide();
		}

		var media = csvToArray(getValidMedia(csvToArray(e.section.media)));

		if(media.length > 0) {
			var mediaItem = MEDIA_MODEL[media[assetId]];
			if(mediaItem && mediaItem.type !== LINK) {

				self.mediaView.show();
				self.mediaView.updatePosition();

				if(mediaItem === curMediaItem && curMediaItem.type !== CONTACT) {
					newMediaItem.updatePosition();
					if(curMediaItem.type === VIDEO) {

						if(mode.indexOf('thumbs') > -1 && section && section.thumb && section.thumb.defaultOn && section.thumb.type === 'fill') {
							if(newMediaItem.video.getSrc()) {
								newMediaItem.video.pause();
							}
						} else if(mode.indexOf('thumbs') === -1 && LAYOUT_MODEL.videoAutoPlay) {
							if(newMediaItem.video.getSrc()) {
								newMediaItem.video.play();
							} else {
								newMediaItem.source = curMediaItem.content;
							}
						}
					}
				} else {
					if(TOUCH_DEVICE) {
						buildNewMediaItem(mediaItem);
					} else {
						clearTimeout(spamTimer);
						spamTimer = setTimeout(function() {
							buildNewMediaItem(mediaItem);
						}, 0);
					}
				}
			}
		}
	}

	function setSlideDirection(newId) {
		if(newId < assetId) {
			assetId = newId;
			return 'right';
		}
		assetId = newId;
		return 'left';
	}

	function buildNewMediaItem(mediaItem) {

		if(newMediaItem){
			self.events.removeEventListener(RESIZE_END, newMediaItem.updatePosition);
			self.events.removeEventListener(LAYOUT_MODEL_CHANGE, newMediaItem.updateStyle);
			self.events.removeEventListener(LAYOUT_MODEL_CHANGE, newMediaItem.updatePosition);
		}
		if(mediaItem !== curMediaItem || mediaUpdate) {
			mediaUpdate = false;
			curMediaItem = mediaItem;
			var mediaVars = {events:vars.events, parentView:'MediaView', mediaItem:mediaItem, loaderColor:getLoaderColor(LAYOUT_MODEL.containerColor)};
			switch(mediaItem.type) {
				case IMAGE:
					newMediaItem = new vars.ImageView(mediaVars, section);
					newMediaItem.mediaItem = mediaItem;
					break;
				case VIDEO:
					newMediaItem = new vars.VideoView(mediaVars, section);
					newMediaItem.mediaItem = mediaItem;
					checkAutoPlay(false);
					break;
				case HTML:
					newMediaItem = new vars.PageView(mediaVars, section);
					newMediaItem.mediaItem = mediaItem;
					break;
				case CONTACT:
					newMediaItem = new vars.ContactView(mediaVars, section);
					newMediaItem.mediaItem = mediaItem;
					break;
			}

			mediaItems.push(newMediaItem);

			self.mediaView.addChild(newMediaItem);
			self.events.addEventListener(RESIZE_END, newMediaItem.updatePosition);
			self.events.addEventListener(LAYOUT_MODEL_CHANGE, newMediaItem.updateStyle);
			self.events.addEventListener(LAYOUT_MODEL_CHANGE, newMediaItem.updatePosition);

			oldMediaItem = mediaItems[mediaItems.length - 2];
			newMediaItem = mediaItems[mediaItems.length - 1];

			if(oldMediaItem) {
				oldMediaItem.removeCursorTargets();
			}

			self.touchNav.oldMediaItem = oldMediaItem;
			self.touchNav.newMediaItem = newMediaItem;

			/* text page alpha 0 on init for iphone fix here */
			if(transitionType === 'slide' || self.touchNav.startdrag || (USER_AGENT === POD && mediaItem.type === HTML)) {
				newMediaItem.setAlpha(1);
			}

			if(!self.touchNav.startdrag) {
				isSwiping = false;
				if(modelChange) {
					transition(LAYOUT_MODEL.transitionDuration);
					modelChange = false;
					transitionType = LAYOUT_MODEL.transitionType;
				} else {
					transition(LAYOUT_MODEL.transitionDuration);
				}
			}

			if(newMediaItem.mediaItem.type === HTML) {
				self.events.dispatchEvent(PAGE_LOADED);
			}
		}
	}

	function touchStart(e) {
		isSwiping = true;
		self.touchNav.touchStart(e);
	}

	function removeOldMediaItem() {
		var mediaItem = mediaItems[mediaItems.length - 2];
		if(mediaItem) {
			mediaItems.splice(mediaItems.length - 2, 1);
			self.mediaView.removeChild(mediaItem);
		}
	}

	function removeTimer() {
		clearTimeout(garbageTimer);
		garbageTimer = setTimeout(function() {
			removeOld();
		}, LAYOUT_MODEL.transitionDuration*1000);
	}

	function removeOld() {
		var i = mediaItems.length;
		while(i--) {
			var mediaItem = mediaItems[i];
			if(i < mediaItems.length - 1 && !isTransitioning && !isSwiping) {
				mediaItems.splice(i, 1);
				self.mediaView.removeChild(mediaItem);
			}
		}
	}

	function removeOldTouchDevice() {
		var i = mediaItems.length;
		while(i--) {
			var mediaItem = mediaItems[i];
			if(i < mediaItems.length - 2) {
				mediaItems.splice(i, 1);
				self.mediaView.removeChild(mediaItem);
			}
		}
	}

	function removeAllMediaItems() {
		curMediaItem = null;
		var i = mediaItems.length;
		while(i--) {
			var mediaItem = mediaItems[i];
			if(i < mediaItems.length) {
				mediaItems.splice(i, 1);
				self.mediaView.removeChild(mediaItem);
			}
		}
	}

	function transition(transitionDuration) {

		var ease = TOUCH_DEVICE || isTransitioning ? 'cubic-bezier(0.1, 0.6, 0.4, 1)' : 'cubic-bezier(0.7, 0, 0.3, 1)';

		self.events.dispatchEvent(TRANSITION_START);

		var startX = mediaItems[mediaItems.length - 2] ? Number(mediaItems[mediaItems.length - 2].getTranslateX()) : 0;

		if(!TOUCH_DEVICE) mediaItems[mediaItems.length - 1].setTranslateX(slideDirection === 'right' ? startX - layoutCalcs.mediaView.width() : startX + layoutCalcs.mediaView.width());

		if(oldMediaItem) oldMediaItem.removeEventListener(TOUCH_START, self.touchNav.touchStart);

		if(transitionType === 'fade' && !isSwiping) {
			newMediaItem.setTranslateX(0);
			newMediaItem.setY(0);
			Tween(newMediaItem, transitionDuration, {alpha:1, ease:ease, onComplete:transitionComplete});
		} else {
			for(var i = 0; i < mediaItems.length; i++) {
				var x = (mediaItems.length - (i + 1)) * layoutCalcs.mediaView.width();
				if(slideDirection === 'right') {
					Tween(mediaItems[i], transitionDuration, {translateX:x, ease:ease, onComplete:transitionComplete});
				} else {
					Tween(mediaItems[i], transitionDuration, {translateX:-x, ease:ease, onComplete:transitionComplete});
				}
			}
			if(oldMediaItem) self.events.dispatchEvent(PAUSE_MEDIA);
		}

		isTransitioning = true;
		isSwiping = false;
	}

	function transitionComplete(e) {
		isTransitioning = false;
		self.events.dispatchEvent(TRANSITION_END);
		if(TOUCH_DEVICE) removeOldTouchDevice();
		removeTimer();
	}

	return self;
}

function LandingMediaView(vars) {

	var self = new ViewProxy({events:vars.events});
	self.setAlpha(0);
	self.updateSpeed = 0;

	var assetNumber = 0,
		newMediaItem,
		curMediaItem,
		mediaItems = [],
		section,
		timerSpeed,
		isTransitioning = false,
		isInit = true;

	if(LAYOUT_MODEL.landingMediaRandomize) {
		assetNumber = getRandomInt(0, csvToArray(SETTINGS_MODEL.backgroundImages).length - 1);
	} else {
		assetNumber = 0;
	}

	self.addEventListener(CHILD_ADDED, function(child) {
		self.updatePosition();
		self.addEventListener(TOUCH_END, touchEnd);
		self.events.addEventListener(PLAY, videoPlay);
	});

	self.updateStyle = function(e) {
		self.updatePosition();
	};

	self.updateContent = function(e) {
		var event = e;
		if(isInit) {
			if(LAYOUT_MODEL.introFile === '') {
				initLandingMedia(e);
			} else {
				self.events.addEventListener(INTRO_COMPLETE, function() {
					initLandingMedia(event);
				});
			}
		} else {
			initLandingMedia(e);
		}
	};

	function initLandingMedia(e) {
		if(e && e.section) section = e.section;
		if(section && section.id === LANDING_MEDIA) {
			self.show();
			self.updatePosition();
			var mediaId = csvToArray(SETTINGS_MODEL.backgroundImages)[assetNumber];
			if(mediaId && MEDIA_MODEL[mediaId]) {
				if(isInit) {
					isInit = false;
					self.events.dispatchEvent(LANDING_INIT);
				}
				newMediaItem = buildNewMediaItem(MEDIA_MODEL[mediaId]);
			} else if(csvToArray(SETTINGS_MODEL.backgroundImages).length > 0) {
				self.getNextSlide();
			} else if(csvToArray(SETTINGS_MODEL.backgroundImages).length === 0) {
				self.hide();
				self.stop();
			}
			if(!self.cursorTargets) {
				self.cursorTargets = [self];
				self.events.dispatchEvent(ADD_CURSOR_TARGETS, {targets:self.cursorTargets, state:'add'});
			}
		} else {
			self.hide();
			self.stop();
		}
	}

	function playbackEnd(e) {
		self.getNextSlide();
	}

	function videoPlay(e) {
		self.stop();
	}

	self.show = function() {
		self.setDisplay('block');
		Tween(self, UPDATE_SPEED, {alpha:1});
	};

	self.hide = function() {
		Tween(self, UPDATE_SPEED, {alpha:0,onComplete:function() {
			self.setDisplay('none');
		}});
	};

	self.start = function() {
		if(section.id === LANDING_MEDIA && csvToArray(SETTINGS_MODEL.backgroundImages).length > 1) {
			self.getNextSlide();
		}
	};

	self.getNextSlide = function() {
		var landingMedia = csvToArray(SETTINGS_MODEL.backgroundImages),
			length = landingMedia.length;
		if(LAYOUT_MODEL.landingMediaRandomize) {
			var newNum = getRandomInt(0, length - 1);
			assetNumber = assetNumber !== newNum ? newNum : getRandomInt(0, length - 1);
		} else {
			assetNumber = assetNumber < length - 1 ? assetNumber + 1 : 0;
		}
		initLandingMedia();
	};

	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function touchEnd(e) {
		vars.events.dispatchEvent(TOUCH_NAVIGATION_END, false);
	}

	function buildNewMediaItem(mediaItem) {
		if(newMediaItem && mediaItem !== newMediaItem.mediaItem){
			self.events.removeEventListener(RESIZE_END, newMediaItem.updatePosition);
			self.events.removeEventListener(LAYOUT_MODEL_CHANGE, newMediaItem.updateStyle);
			self.events.removeEventListener(LAYOUT_MODEL_CHANGE, newMediaItem.updatePosition);
		}
		curMediaItem = mediaItem;
		var mediaVars = {events:vars.events, parentView:'LandingMediaView', mediaItem:mediaItem, loaderColor:getLoaderColor(LAYOUT_MODEL.siteBackgroundColor)};
		switch(mediaItem.type) {
			case IMAGE:
				newMediaItem = new ImageView(mediaVars);
				newMediaItem.mediaItem = mediaItem;
				break;
			case VIDEO:
				newMediaItem = new VideoView(mediaVars);
				newMediaItem.mediaItem = mediaItem;
				if(USER_AGENT !== POD) {
					if(LAYOUT_MODEL.landingMediaScaleType === 'fillSite' || LAYOUT_MODEL.videoAutoPlay) {
						newMediaItem.source = mediaVars.mediaItem.content;
					}
				}
				break;
			case HTML:
				newMediaItem = new PageView(mediaVars);
				newMediaItem.mediaItem = mediaItem;
				break;
			case CONTACT:
				newMediaItem = new ContactView(mediaVars);
				newMediaItem.mediaItem = mediaItem;
				break;
		}
		mediaItems.push(newMediaItem);
		self.addChild(newMediaItem);
		self.events.addEventListener(RESIZE_END, newMediaItem.updatePosition);
		self.events.addEventListener(LAYOUT_MODEL_CHANGE, newMediaItem.updateStyle);
		self.events.addEventListener(LAYOUT_MODEL_CHANGE, newMediaItem.updatePosition);
		self.events.addEventListener(SETTINGS_MODEL_CHANGE, function(){
			var e = {
				section:{id:section.id, label:LANDING_MEDIA, visible:1 ,password:'', media:SETTINGS_MODEL.backgroundImages},
				path:'',
				assetId:0,
				mode:''
			};
			initLandingMedia(e);
		});
		if(assetNumber === 0) transition();
		self.events.addEventListener(MEDIA_LOADED, mediaLoaded);
		return newMediaItem;
	}

	function mediaLoaded(e) {
		self.events.removeEventListener(MEDIA_LOADED, mediaLoaded);
		timerSpeed = LAYOUT_MODEL.landingMediaSpeed * 1000;
		clearTimeout(self.timer);
		self.timer = undefined;
		if(assetNumber !== 0) transition();
		self.timer = setTimeout(function() {
			if(curMediaItem.type === VIDEO && (LAYOUT_MODEL.landingMediaScaleType === 'fillSite' || LAYOUT_MODEL.videoAutoPlay)) {
				/*let video play*/
			} else {
				self.start();
			}
		}, timerSpeed);
	}

	function removeOld() {
		var i = mediaItems.length;
		while(i--) {
			var mediaItem = mediaItems[i];
			if(i < mediaItems.length - 1) {
				mediaItems.splice(i, 1);
				if(mediaItem.mediaItem.type === VIDEO) mediaItem.destroy();
				self.removeChild(mediaItem);
			}
		}
	}

	function transition() {
		isTransitioning = true;
		var newMediaItem = mediaItems[mediaItems.length - 1];
		if(newMediaItem) {
			newMediaItem.setTranslateX(0);
			newMediaItem.setY(0);
			Tween(newMediaItem, LAYOUT_MODEL.landingMediaTransitionDuration, {alpha:1, onComplete:transitionComplete});
		}
		self.events.dispatchEvent(PAUSE_MEDIA);
	}

	function transitionComplete(e) {
		isTransitioning = false;
		self.events.dispatchEvent(TRANSITION_END);
		var mediaId = csvToArray(SETTINGS_MODEL.backgroundImages)[assetNumber];
		if(MEDIA_MODEL[mediaId] && MEDIA_MODEL[mediaId].type === VIDEO && newMediaItem && newMediaItem.video) {
			newMediaItem.video.events.listeners['playbackEnd'] = [];
			newMediaItem.video.addEventListener('playbackEnd', playbackEnd);
		}
		setTimeout(removeOld, 100);
	}

	self.stop = function() {
		clearTimeout(self.timer);
		self.timer = undefined;
		if(self.events.listeners[MEDIA_LOADED]) {
			self.events.removeEventListener(MEDIA_LOADED, mediaLoaded);
		}
	};

	self.updatePosition = function(e) {
		self.setX(layoutCalcs.landingMediaView.x());
		self.setY(layoutCalcs.landingMediaView.y());
		self.setWidth(layoutCalcs.landingMediaView.width());
		self.setHeight(layoutCalcs.landingMediaView.height());
	};

	return self;
}

function MediaView(vars) {

	var self = new ViewProxy({events:vars.events, type:'article'});
	self.setAlpha(0);
	self.setOverflow('hidden');
	self.updateSpeed = 0;
	self.setZIndex(1);
	self.element.setAttribute('class', 'mediaView');

	self.addEventListener(CHILD_ADDED, function(child) {
		self.updatePosition();
		/*self.style['-webkit-backface-visibility'] = 'hidden';*/
	});

	self.updateStyle = function(e) {
		var clr = LAYOUT_MODEL.containerColor !== self.getBackgroundColor() ? LAYOUT_MODEL.containerColor : self.getBackgroundColor();
		Tween(self, self.updateSpeed, {backgroundColor:clr});
	};

	self.show = function() {
		self.setDisplay('block');
		Tween(self, UPDATE_SPEED, {alpha:1});
	};

	self.hide = function() {
		Tween(self, UPDATE_SPEED, {alpha:0,onComplete:function() {
			self.setDisplay('none');
		}});
	};

	self.updatePosition = function() {
		if(self.updateSpeed === 0) {
			self.setX(layoutCalcs.mediaView.x());
			self.setY(layoutCalcs.mediaView.y());
			self.setWidth(layoutCalcs.mediaView.width());
			self.setHeight(layoutCalcs.mediaView.height());
		} else {
			Tween(self, UPDATE_SPEED, {x:layoutCalcs.mediaView.x(), y:layoutCalcs.mediaView.y(), width:layoutCalcs.mediaView.width(), height:layoutCalcs.mediaView.height()});
		}
		self.updateSpeed = UPDATE_SPEED;
	};

	return self;
}

function ContactView(vars, section) {

	var self = new ViewProxy({events:vars.events}),
		formContainer = new Sprite(),
		contactExtra = new Sprite(),
		contactFields = new Sprite(),
		responseMessage = new Sprite(),
		contactFormObj,
		formFieldsArr,
		contactLabel,
		_section = section,
		_thumbState;

	self.setAlpha(0);
	self.updateSpeed = 0;

	function init() {
		self.addChild(formContainer);

		if(vars.mediaItem.content.contactFormFields) {
			contactFormObj = vars.mediaItem.content;
		} else {
			contactFormObj = {contactFormEmail:SETTINGS_MODEL.contactFormEmail, contactFormFields:SETTINGS_MODEL.contactFormFields, contactFormSubject:'Contact Form Submitted', contactFormExtra:SETTINGS_MODEL.contactFormExtra};
		}

		formFieldsArr = csvToArray(contactFormObj.contactFormFields);

		contactExtra.setText(contactFormObj.contactFormExtra.replace(/\r\n/g, '<br>').replace(/\\r\\n/g, '<br>').replace(/\n\r/g, '<br>').replace(/\\n\\r/g, '<br>').replace(/\r/g, '<br>').replace(/\\r/g, '<br>').replace(/\n/g, '<br>').replace(/\\n/g, '<br>').replace(/\\/g, ''));
		contactExtra.setFontFamily(LAYOUT_MODEL.contactFont);
		contactExtra.setFontSize(LAYOUT_MODEL.contactFontSize);
		contactExtra.setFontColor(LAYOUT_MODEL.contactFontColor);
		contactExtra.setTextAlign('right');
		contactExtra.setTextWrap(false);
		formContainer.addChild(contactExtra);
		formContainer.addChild(contactFields);

		var contactImgs = formContainer.element.getElementsByTagName('img'),
			i = contactImgs.length;

		if(contactImgs.length > 0) {
			while(i--) {
				var img = contactImgs[i];
				img.addEventListener(LOAD, function(e) {
					self.updatePosition();
				});
			}
		}

		if(contactExtra.getWidth() > 320) {
			contactExtra.setWidth(320);
			contactExtra.setTextWrap(true);
			contactExtra.style['word-wrap'] = 'break-word';
		}

		var i = 0,
			length = formFieldsArr.length,
			fieldY = 0;

		for(;i < length; i++) {
			var input = buildFormInput(formFieldsArr[i]);
			input.setX(16);
			input.setY(fieldY);
			contactFields.addChild(input);
			fieldY += formFieldsArr[i] === 'Comments' ? 180 : 50;
		}

		var submit = new Button();
		submit.setText('Submit');
		submit.setX(16);
		submit.setY(fieldY + 5);
		submit.setFontSize(12);
		submit.setFontColor(LAYOUT_MODEL.contactFontColor);
		submit.setBackgroundColor(LAYOUT_MODEL.contactFieldRectColor);
		submit.setPaddingTop(2);
		submit.setPaddingRight(14);
		submit.setPaddingBottom(2);
		submit.setPaddingLeft(14);
		contactFields.addChild(submit);
		submit.addEventListener(FOCUS, onFocusSubmit);
		submit.addEventListener(BLUR, onBlurSubmit);
		submit.addEventListener(MOUSE_OVER, onFocusSubmit);
		submit.addEventListener(MOUSE_OUT, onBlurSubmit);
		submit.addEventListener(CLICK, onSubmit);
		fieldY += 25;

		contactFields.setWidth(318);
		contactFields.setHeight(fieldY);
		contactFields.setBorderLeft('1px solid ' + hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0.3));

		if(LAYOUT_MODEL.contactTitleShow) {
			contactLabel = new TextField();
			contactLabel.setText(vars.mediaItem.label);
			formContainer.addChild(contactLabel);
			contactLabel.setFontFamily(LAYOUT_MODEL.contactTitleFont);
			contactLabel.setFontSize(LAYOUT_MODEL.contactTitleFontSize);
			contactLabel.setFontColor(LAYOUT_MODEL.contactFontColor);
			contactLabel.setTextWrap(false);
			contactLabel.setX(contactExtra.getWidth() + 16);
			contactLabel.setY(percentToPixels(LAYOUT_MODEL.contactTitleFontSize, 100) - 36);
		}

		formContainer.setWidth(contactExtra.getWidth() + contactFields.getWidth());
		var formHeight = 22;
		if(LAYOUT_MODEL.contactTitleShow) {
			formHeight += contactLabel.getHeight();
		}
		if(contactExtra.getHeight() > contactFields.getHeight()) {
			formHeight += contactExtra.getHeight();
		} else {
			formHeight += contactFields.getHeight();
		}
		formHeight += submit.getHeight() + 5;
		formContainer.setHeight(formHeight);

		self.cursorTargets = [formContainer];
		self.events.dispatchEvent(ADD_CURSOR_TARGETS, {targets:self.cursorTargets});

		responseMessage.setAlpha(0);
		responseMessage.setX(contactExtra.getWidth() + 17);
		responseMessage.setY(44);
		responseMessage.setFontSize(14);
		formContainer.addChild(responseMessage);

		self.events.addEventListener(CONTACT_FORM_SENT, contactResponse);

		var types = USER_AGENT === POD ? ['wheel','touch'] : ['bar','wheel','touch'];

		self.scroll = new Scroll(formContainer, self, {
			types:types,
			axis:'y',
			align:'top',
			color:LAYOUT_MODEL.pageScrollbarColor,
			hover:LAYOUT_MODEL.pageScrollbarHover,
			side:LAYOUT_MODEL.pageScrollbarAlignment,
			width:8,
			margin:20,
			offsetX:-8
		});

		self.updateStyle();
		self.updatePosition();
	}

	function buildFormInput(label) {
		var holder = new Sprite();
		holder.setWidth(300);
		var title = new TextField();
		title.setFontFamily(LAYOUT_MODEL.contactTitleFont);
		title.setFontColor(LAYOUT_MODEL.contactFontColor);
		title.setFontSize(11);
		title.setText(label);
		holder.addChild(title);
		var input = label === 'Comments' ? new TextArea() : new Input();
		input.setBackgroundColor(hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0.1));
		input.setBorder('1px solid ' + hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0.3));
		input.setFontFamily(LAYOUT_MODEL.contactTitleFont);
		input.setFontColor(LAYOUT_MODEL.contactFontColor);
		input.setFontSize(14);
		input.setWidth(300);
		input.setHeight(label === 'Comments' ? 150 : 20);
		input.setY(17);

		input.addEventListener(FOCUS, onFocus);
		input.addEventListener(BLUR, onBlur);

		holder.addChild(input);
		holder.id = label;
		holder.input = input;
		return holder;
	}

	function onFocus(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0.2), border:'1px solid ' + hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0.7)});
	}

	function onBlur(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0.1), border:'1px solid ' + hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0.3)});
	}

	function onFocusSubmit(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0.8)});
	}

	function onBlurSubmit(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 1)});
	}

	function contactResponse(e) {
		responseMessage.setText(e.response);
		Tween(contactFields, UPDATE_SPEED, {alpha:0, onComplete:function(e){
			contactFields.setDisplay('none');
		}});
		Tween(responseMessage, UPDATE_SPEED, {alpha:1});
	}

	function onSubmit(e) {
		var i = 0,
			length = contactFields.children.length,
			str = '&Title=' + encodeURIComponent(vars.mediaItem.label) + '&to=' + encodeURIComponent(contactFormObj.contactFormEmail) + '&subject=' + encodeURIComponent(contactFormObj.contactFormSubject);
		for(; i < length; i++) {
			var fields = contactFields.children[i];
			if(fields.id && fields.input) {
				if(fields.id === 'Name' && fields.input.getValue() === '') {
					alert('Name field cannot be blank.');
					return false;
				} else if(fields.id === 'Email' && (fields.input.getValue().indexOf("@") === -1 || fields.input.getValue().indexOf(".") === -1)) {
					alert('Please enter valid email address.');
					return false;
				} else if(fields.id === 'Comments' && fields.input.getValue() === '') {
					alert('Comments field cannot be blank.');
					return false;
				}
				str += '&' + fields.id + '=' + encodeURIComponent(fields.input.getValue());
			}
		}
		str = str.replace(/\r/g,"<br>").replace(/\n/g,"<br>");
		self.events.dispatchEvent(CONTACT_FORM_SUBMIT, str);
	}

	self.removeCursorTargets = function() {
		self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: self.cursorTargets, state:'remove'});
	};

	self.updateStyle = function(e) {
		var color = vars.parentView === 'LandingMediaView' ? LAYOUT_MODEL.siteBackgroundColor : LAYOUT_MODEL.containerColor;
		if(LAYOUT_MODEL.containerColor !== self.getBackgroundColor()) {
			if(self.updateSpeed === 0) {
				self.setBackgroundColor(color);
			} else {
				Tween(self, self.updateSpeed, {backgroundColor:color});
			}
		}
	};

	self.updateContent = function(e) {
		if(e.section.id === LANDING_MEDIA) {
			self.hide();
		} else if(isMediaItem(e.section.media) && getMediaById(e.section.media).type !== LINK) {
			self.setText(getMediaById(e.section.media).content);
			self.show();
		} else if(e.assetId > -1) {
			self.setText(MEDIA_MODEL[e.section.media.split(",")[e.assetId]].content);
			self.show();
		}
	};

	self.show = function() {
		self.setDisplay('block');
		Tween(self, UPDATE_SPEED, {alpha:1});
	};

	self.hide = function() {
		Tween(self, UPDATE_SPEED, {alpha:0,onComplete:function() {
			self.setDisplay('none');
		}});
	};

	self.updatePosition = function(e) {
		layoutCalcs._activeView = 'contact';
		formContainer.setWidth(contactExtra.getWidth() + contactFields.getWidth());
		if(formContainer.getWidth() > layoutCalcs.mediaMask.width()) {
			contactExtra.setX(16);
			contactFields.setX(0);
			if(LAYOUT_MODEL.contactTitleShow) {
				contactLabel.setX(16);
				contactLabel.setY(percentToPixels(LAYOUT_MODEL.contactTitleFontSize, 100));
				contactExtra.setY(contactLabel.getY() + percentToPixels(LAYOUT_MODEL.contactTitleFontSize, 100) + 16);
				contactFields.setY(contactExtra.getY() + contactExtra.getHeight() + 16);
				} else {
				contactFields.setY(contactExtra.getHeight());
			}
			self.scroll.align = 'top';
			contactFields.setBorderLeft('1px solid ' + hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0));
			contactExtra.setTextAlign('left');
		} else {
			contactExtra.setX(-16);
			contactFields.setX(contactExtra.getWidth());
			contactFields.setY(44);
			contactFields.setBorderLeft('1px solid ' + hexToRgba(LAYOUT_MODEL.contactFieldRectColor, 0.3));
			contactExtra.setTextAlign('right');
			if(LAYOUT_MODEL.contactTitleShow) {
				contactLabel.setTextWrap(true);
				contactLabel.setX(contactExtra.getWidth() + 16);
				contactLabel.setY(percentToPixels(LAYOUT_MODEL.contactTitleFontSize, 100) - 36);
			}
			self.scroll.align = 'center';
		}

		formContainer.setHeight(contactFields.getY() + contactFields.getHeight());

		var x = layoutCalcs.mediaMask.x(),
			y =  layoutCalcs.mediaMask.y(),
			width = layoutCalcs.mediaMask.width(),
			height = layoutCalcs.mediaMask.height();

		if(self.updateSpeed === 0) {
			self.setX(x);
			self.setY(y);
			self.setWidth(width);
			self.setHeight(height);
			formContainer.setX(getFormX());
			formContainer.setY(getFormY());
		} else {
			Tween(self, self.updateSpeed, {x:x, y:y, width:width, height:height});
			Tween(formContainer, self.updateSpeed, {x:getFormX(), y:getFormY()});
		}

		self.updateSpeed = UPDATE_SPEED;

		if(self.scroll) self.scroll.resize();
	};

	function getFormX() {
		layoutCalcs._activeView = 'contact';
		if(formContainer.getWidth() > layoutCalcs.mediaMask.width()) {
			return ((layoutCalcs.mediaMask.width() - contactFields.getWidth()) * 0.5) - 8;
		} else {
			return ((layoutCalcs.mediaMask.width() - formContainer.getWidth()) * 0.5);
		}
	}

	function getFormY() {
		layoutCalcs._activeView = 'contact';
		return ((layoutCalcs.mediaMask.height() - formContainer.getHeight()) * 0.5);
	}

	self.addEventListener(CHILD_ADDED, init);

	return self;
}

function ImageView(vars, section) {

	var self = new ViewProxy({events:vars.events});
	self.setAlpha(0);
	self.updateSpeed = 0;
	self.setOverflow('hidden');
	hardwareAccel(self.element);

	var image = new Bitmap(),
		mask = new Sprite(),
		loader,
		originalImageWidth = 0,
		originalImageHeight = 0,
		imageWidth = 0,
		imageHeight = 0,
		maskWidth = 0,
		maskHeight = 0,
		loaded = false,
		_section = section,
		_thumbState;

	image.setAlpha(0);
	image.setSelectable(false);
	mask.setOverflow('hidden');

	self.addEventListener(CHILD_ADDED, function(child) {
		/*if(vars.mediaItem.size.width) {
			/*trace('original size info available:', vars.mediaItem.size);
		}*/
		image.setSrc(REWRITE_BASE + MEDIA_ORIGINAL + vars.mediaItem.content);
		image.element.addEventListener('contextmenu', function(e){
			e.preventDefault();
		});
		image.element.addEventListener('dragstart', function(e){
			e.preventDefault();
		});
		image.style['image-rendering'] = 'optimizeQuality';
		image.style['image-rendering'] = '-webkit-optimize-contrast';
		if(BROWSER_NAME === 'Safari') image.style['-webkit-backface-visibility'] = 'hidden';
		image.addEventListener(LOAD, imageLoaded);
		self.addChild(mask);
		mask.addChild(image);
		loader = new LoadingIndicator({color:vars.loaderColor});
		mask.addChild(loader);
		self.updateStyle();
		self.updatePosition();
	});

	self.updateStyle = function(e) {
		var color = vars.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite' ? LAYOUT_MODEL.siteBackgroundColor : LAYOUT_MODEL.containerColor;
		if(BROWSER_NAME === 'Mobile Safari') color = hexToRgba(color, 0.99);
		Tween(self, self.updateSpeed, {backgroundColor:color});
	};

	self.updateContent = function(e) {

	};

	self.hide = function() {
		if(self.getAlpha() === 1) {
			Tween(self, UPDATE_SPEED, {alpha:0, onComplete:function(){
				self.setDisplay('none');
			}});
		}
	};

	self.show = function() {
		if(self.getAlpha() === 0) {
			self.setDisplay('block');
			Tween(self, UPDATE_SPEED, {alpha:1});
		}
	};

	self.removeCursorTargets = function() {

	};

	self.updatePosition = function(e) {

		layoutCalcs._activeView = 'imageVideo';
		var x = 0,
			maskX = layoutCalcs.mediaMask.x(),
			y = 0,
			maskY = layoutCalcs.mediaMask.y(),
			width = layoutCalcs.mediaView.width(),
			height = layoutCalcs.mediaView.height();

		maskWidth = layoutCalcs.mediaMask.width();
		maskHeight = layoutCalcs.mediaMask.height();

		if(vars.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
			x = maskX = layoutCalcs.landingMediaView.x();
			y = maskY = layoutCalcs.landingMediaView.y();
			width = maskWidth = layoutCalcs.landingMediaView.width();
			height = maskHeight = layoutCalcs.landingMediaView.height();
		}

		if(self.updateSpeed === 0) {
			self.setX(x);
			self.setY(y);
			self.setWidth(width);
			self.setHeight(height);

			mask.setX(maskX);
			mask.setY(maskY);
			mask.setWidth(maskWidth);
			mask.setHeight(maskHeight);
		} else {
			Tween(self, self.updateSpeed, {
				x:x,
				y:y,
				width:width,
				height:height});
			Tween(mask, self.updateSpeed, {
				x:maskX,
				y:maskY,
				width:maskWidth,
				height:maskHeight});
		}

		positionLoader();

		if(loaded) {
			resizeImage();
			alignImage();
		}
	};

	function imageLoaded(e) {
		self.updateSpeed = 0;
		loaded = true;
		originalImageWidth = this.getWidth();
		originalImageHeight = this.getHeight();
		loader.removeLoader();
		mask.removeChild(loader);
		self.updatePosition();
		setTimeout(function() {
			if(vars.parentView === 'MediaView') {
				Tween(image, LAYOUT_MODEL.transitionDuration, {alpha:1, onComplete:function(){
					self.events.dispatchEvent(MEDIA_LOADED, e);
				}});
			} else if(vars.parentView === 'LandingMediaView') {
				Tween(image, LAYOUT_MODEL.landingMediaTransitionDuration, {alpha:1, onComplete:function(){
					self.events.dispatchEvent(MEDIA_LOADED, e);
				}});
			}
			self.updateSpeed = UPDATE_SPEED;
		}, 100);
	}

	function resizeImage() {
		var scale = new Scale();
		scale.setWidth(originalImageWidth);
		scale.setHeight(originalImageHeight);
		scale.setHRange(maskWidth);
		scale.setVRange(maskHeight);
		scale.setType(LAYOUT_MODEL.imageScaleType);
		if(vars.parentView === 'LandingMediaView') {
			scale.setType(LAYOUT_MODEL.landingMediaScaleType);
		}
		if(scale.getType() === 'native') {
			if(originalImageWidth > maskWidth || originalImageHeight > maskHeight) {
				scale.setType('fit');
			} else {
				scale.setHRange(originalImageWidth);
				scale.setVRange(originalImageHeight);
			}
		}
		if((scale.getType() === 'fillSite' || scale.getType() === 'fill') && (originalImageHeight > originalImageWidth && stage.getWidth() > stage.getHeight())) {
			scale.setType('fit');
		} else if((scale.getType() === 'fillSite' || scale.getType() === 'fill') && (originalImageWidth > originalImageHeight && stage.getHeight() > stage.getWidth())) {
			scale.setType('fit');
		}
		imageWidth = Mth.round(scale.getWidth());
		imageHeight = Mth.round(scale.getHeight());
		if(self.updateSpeed === 0) {
			image.setWidth(imageWidth);
			image.setHeight(imageHeight);
		} else {
			Tween(image, self.updateSpeed, {width:imageWidth, height:imageHeight});
		}
	}

	function alignImage() {
		var align = new Align();
		align.setWidth(imageWidth);
		align.setHeight(imageHeight);
		if(vars.parentView === 'LandingMediaView') {
			align.setHAlign(LAYOUT_MODEL.landingMediaAlignHorizontal);
			align.setVAlign(LAYOUT_MODEL.landingMediaAlignVertical);
		} else {
			align.setHAlign(LAYOUT_MODEL.imageAlignHorizontal);
			align.setVAlign(LAYOUT_MODEL.imageAlignVertical);
		}
		align.setHRange(maskWidth);
		align.setVRange(maskHeight);
		if(self.updateSpeed === 0) {
			image.setX(Mth.round(align.getX()));
			image.setY(Mth.round(align.getY()));
		} else {
			Tween(image, self.updateSpeed, {x:Mth.round(align.getX()), y:Mth.round(align.getY())});
		}
	}

	function positionLoader() {
		if(loader) {
			loader.setX((maskWidth - loader.getWidth()) * 0.5);
			loader.setY((maskHeight - loader.getHeight()) * 0.5);
		}
	}

	return self;
}

function PageView(vars, section) {

	var self = new ViewProxy({events:vars.events});
	self.setAlpha(0);
	self.updateSpeed = 0;
	self.cursorTargets = [];
	self.setClass('dx_page_text');
	self.style['width'] = '100%';

	var title,
		text = new TextField(),
		image,
		mask = new Sprite(),
		loader,
		originalImageWidth = 0,
		originalImageHeight = 0,
		imageGap = 30,
		maskWidth = 0,
		maskHeight = 0,
		loaded = false,
		_section = section,
		_thumbState,
		isVerticalPage = isVerticalPageBuild(),
		pageImgs;

	mask.setOverflow('hidden');

	self.addEventListener(CHILD_ADDED, init);

	function init() {
		if(isValidPageTitle()) {
			title = new TextField();
			title.setText(vars.mediaItem.label.replace(/\\/g, ''));
			title.setTextWrap(true);
			self.addChild(title);
		}

		if(hasFeaturedImage() && !isVerticalPageBuild()) {
			/*loader = new LoadingIndicator();*/
			image = new Bitmap();
			image.setSrc(REWRITE_BASE + MEDIA_ORIGINAL + vars.mediaItem.featuredImage);
			image.addEventListener(LOAD, imageLoaded);
			image.style['image-rendering'] = 'optimizeQuality';
			image.style['image-rendering'] = '-webkit-optimize-contrast';
			image.style['-webkit-backface-visibility'] = 'hidden';
			image.setAlpha(0);
			self.addChild(image);
			/*self.addChild(loader);*/
		} else {
			self.events.dispatchEvent(MEDIA_LOADED, {});
			setTimeout(function(){
				self.updateSpeed = LAYOUT_MODEL.transitionDuration;
			}, 0);
		}

		text.setText('');

		if(isVerticalPageBuild()) {
			if(LAYOUT_MODEL.pageTitle && vars.mediaItem.label !== '') {
				text.setText(text.getText() + "<br/><h1><b></b>" + vars.mediaItem.label.replace(/\\/g, '') + "</b></h1><br/>");
			}
			if(hasFeaturedImage()) {
				text.setText(text.getText() + "<br/><img style='max-width:100%;' src='" + REWRITE_BASE + MEDIA_ORIGINAL + vars.mediaItem.featuredImage + "'/><br/><br/>");
			}
		}

		/*
		* try to determine if its html content or plain text
		* */

		if(vars.mediaItem.content.search("<") > -1 && vars.mediaItem.content.search("<") < 5) {
			text.setText(text.getText() + vars.mediaItem.content);
		} else {
			text.setText(text.getText() + vars.mediaItem.content.replace(/\t/g, '&#9;').replace(/\r\n/g, '<br>').replace(/\\r\\n/g, '<br>').replace(/\n\r/g, '<br>').replace(/\\n\\r/g, '<br>').replace(/\r/g, '<br>').replace(/\\r/g, '<br>').replace(/\n/g, '<br>').replace(/\\n/g, '<br>').replace(/\\/g, ''));
		}
		self.addChild(mask);
		mask.addChild(text);

		isVerticalPage = isVerticalPageBuild();

		self.updateStyle();
		self.updatePosition();

		if(!hasFeaturedImage()) {
			addCursorTargets();
		}

		/*this is to prevent page images from being cropped when browser is small*/
		pageImgs = text.element.getElementsByTagName('img');
		var i = pageImgs.length;
		if(i > 0) {
			while(i--) {
				var img = pageImgs[i];
				img.addEventListener(LOAD, function(e) {
					self.updatePosition();
				});
			}
		}
	}

	function findIframeTargets(text) {
		var iframe = text.element.getElementsByTagName('iframe');
		for(i in iframe) {
			self.cursorTargets.push(hitTarget(iframe[i]));
		}
	}

	function findLinkTargets(text) {
		var a = text.element.getElementsByTagName('a');
		for(i in a) {
			if(a[i].href != undefined && a[i].href != '') {
				self.cursorTargets.push(hitTarget(a[i]));
			}
		}
	}

	function findEmbedTargets(text) {
		var embed = text.element.getElementsByTagName('embed');
		for(i in embed) {
			self.cursorTargets.push(hitTarget(embed[i]));
		}
	}

	function findObjectTargets(text) {
		var object = text.element.getElementsByTagName('object');
		for(i in object) {
			self.cursorTargets.push(hitTarget(object[i]));
		}
	}

	function updateScroll() {

		var scrollBarWidth = 6,
			hOffset = 0;

		switch(LAYOUT_MODEL.pageScrollbarAlignment) {
			case 'left':
				if(isVerticalPageBuild()) {
					hOffset = -4;
				} else  {
					hOffset = -Mth.abs(LAYOUT_MODEL.pageScrollbarHorizontalOffset) - 4;
				}
				break;
			case 'right':
				if(isVerticalPageBuild()) {
					hOffset = 10;
				} else  {
					hOffset = LAYOUT_MODEL.pageScrollbarHorizontalOffset;
				}
				break;
		}

		if(self.scroll) {
			self.scroll.remove();
		}

		self.scroll = new Scroll(text, mask, {
			types:['bar','wheel','touch'],
			axis:'y',
			align:'top',
			margin:0,
			color:LAYOUT_MODEL.pageScrollbarColor,
			hover:LAYOUT_MODEL.pageScrollbarHover,
			side:LAYOUT_MODEL.pageScrollbarAlignment,
			width:scrollBarWidth,
			offsetX:hOffset,
			offsetY:LAYOUT_MODEL.pageScrollbarVerticalOffset
		});

		setTimeout(function() {
			self.scroll.resize();
		}, 200);
	}

	function isValidPageTitle() {
		return LAYOUT_MODEL.pageTitle && vars.mediaItem.label !== '' && !isVerticalPageBuild();
	}

	function hasFeaturedImage() {
		return vars.mediaItem.featuredImage !== '';
	}

	self.updateStyle = function(e) {
		if(!title && isValidPageTitle()) {
			self.updateSpeed = 0;
			title = new TextField();
			title.setTextWrap(false);
			title.setText(vars.mediaItem.label);
			self.addChild(title);
		} else if(title && !isValidPageTitle()) {
			self.removeChild(title);
			title = undefined;
		}
		if(isValidPageTitle()) {
			title.setFontFamily(LAYOUT_MODEL.pageTitleFont);
			title.setFontColor(LAYOUT_MODEL.pageTitleFontColor);
			title.setFontSize(LAYOUT_MODEL.pageTitleFontSize);
		}
		text.setFontFamily(LAYOUT_MODEL.pageTextFont);
		text.setFontColor(LAYOUT_MODEL.pageTextFontColor);
		text.setFontSize(LAYOUT_MODEL.pageTextFontSize);

		var color = vars.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite' ? LAYOUT_MODEL.siteBackgroundColor : LAYOUT_MODEL.containerColor;
		if(self.updateSpeed === 0) {
			self.setBackgroundColor(color);
		} else {
			Tween(self, self.updateSpeed, {backgroundColor:color});
		}
	};

	self.updateContent = function(e) {
		if(e.section.id === LANDING_MEDIA) {
			self.hide();
		} else if(isMediaItem(e.section.media) && getMediaById(e.section.media).type !== LINK) {
			self.show();
		} else if(e.assetId > -1) {
			self.show();
		}
	};

	self.show = function() {
		self.setDisplay('block');
		Tween(self, UPDATE_SPEED, {alpha:1});
	};

	self.hide = function() {
		Tween(self, UPDATE_SPEED, {alpha:0,onComplete:function() {
			self.setDisplay('none');
		}});
	};

	self.removeCursorTargets = function() {
		self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: self.cursorTargets, state:'remove'});
	};

	self.updatePosition = function(e) {
		layoutCalcs._activeView = 'page';
		var x = 0,
			maskX = layoutCalcs.mediaMask.x(),
			y = 0,
			maskY = layoutCalcs.mediaMask.y(),
			width = layoutCalcs.mediaView.width(),
			height = layoutCalcs.mediaView.height();

		maskWidth = layoutCalcs.mediaMask.width() - 6;
		maskHeight = layoutCalcs.mediaMask.height();

		if(vars.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
			x = maskX = layoutCalcs.landingMediaView.x();
			y = maskY = layoutCalcs.landingMediaView.y();
			width = layoutCalcs.landingMediaView.width();
			maskWidth = width;
			height = maskHeight = layoutCalcs.landingMediaView.height();
		}

		if(hasFeaturedImage() && !isVerticalPageBuild()) {
			maskX += originalImageWidth + imageGap;
			maskWidth -= (originalImageWidth + imageGap);
		}

		if(!isVerticalPage && isVerticalPageBuild()) {
			if(image) {
				self.removeChild(image);
				image = undefined;
			}
			if(title) {
				self.removeChild(title);
				title = undefined;
			}
			init();
		} else if(isVerticalPage && !isVerticalPageBuild()) {
			text.setText('');
			init();
		}

		self.setX(x);
		self.setY(y);
		self.setWidth(width);
		self.setHeight(height);

		if(title && isValidPageTitle()) {
			title.setX(maskX);
			title.setY(maskY);
			mask.setX(maskX);
			mask.setY(maskY + title.getHeight() + imageGap);
			mask.setWidth(maskWidth);
			mask.setHeight(maskHeight - (title.getHeight() + imageGap));
		} else {
			mask.setX(maskX);
			mask.setY(maskY);
			mask.setWidth(maskWidth);
			mask.setHeight(maskHeight);
		}

		if(image) {
			image.setX(maskX - originalImageWidth - imageGap);
			image.setY(hasFeaturedImage() && isValidPageTitle() ? maskY + title.getHeight() + imageGap : maskY);
		}

		if(pageImgs) {
			/*this is to prevent page images from being cropped when browser is small*/
			var i = pageImgs.length;
			while(i--) {
				var img = pageImgs[i];
				if(img.offsetLeft === 0 && img.offsetWidth > maskWidth) {
					img.width = maskWidth;
				}
			}
		}

		updateScroll();

		/*positionLoader();*/
	};

	function imageLoaded(e) {
		self.updateSpeed = 0;
		loaded = true;
		originalImageWidth = image.getWidth();
		originalImageHeight = image.getHeight();
		/*Tween(loader, UPDATE_SPEED, {alpha:0, onComplete:function(){
			loader.removeLoader();
			loader = null;
		}});*/
		Tween(image, 1, {alpha:1});
		self.updateSpeed = LAYOUT_MODEL.transitionDuration;
		self.updatePosition();
		self.events.dispatchEvent(MEDIA_LOADED, e);
		addCursorTargets();
	}

	function addCursorTargets() {
		findLinkTargets(text);
		findIframeTargets(text);
		findEmbedTargets(text);
		findObjectTargets(text);
		self.cursorTargets.push(self.scroll.bar.back);
		self.events.dispatchEvent(ADD_CURSOR_TARGETS, {targets:self.cursorTargets});
		self.scroll.bar.handle.addEventListener(MOUSE_DOWN, function(){
			self.events.dispatchEvent(ADD_CURSOR_TARGETS, {targets:[mask.parent]});
		});
		stage.addEventListener(MOUSE_UP, function(){
			self.events.dispatchEvent(REMOVE_CURSOR_TARGETS, {targets:[mask.parent]});
		});
	}

	function positionLoader() {
		if(loader) {
			Tween(loader, self.updateSpeed, {x:(maskWidth - loader.getWidth()) * 0.5, y:(maskHeight - loader.getHeight()) * 0.5});
		}
	}

	function isVerticalPageBuild() {
		if(USER_AGENT === POD) return true;
		if(layoutCalcs.mediaView.width() < layoutCalcs.mediaView.height()) return true;
		if(originalImageWidth && layoutCalcs.mediaView.width() < layoutCalcs.mediaMask.x() + originalImageWidth + imageGap + 250) return true;
		return false;
	}

	return self;
}

function VideoView(vars, section) {

	var self = new ViewProxy({events:vars.events});
	self.setAlpha(0);
	self.updateSpeed = 0;
	self.setOverflow('hidden');
	self.cursorTargets = [];

	var image = new Bitmap(),
		mask = new Sprite(),
		video,
		bigPlayIcon = new Bitmap(),
		loader,
		originalImageWidth = 0,
		originalImageHeight = 0,
		imageWidth = 0,
		imageHeight = 0,
		maskWidth = 0,
		maskHeight = 0,
		originalVideoWidth = 0,
		originalVideoHeight = 0,
		videoWidth = 0,
		videoHeight = 0,
		videoLoaded = false,
		posterLoaded = false;

	image.setAlpha(0);
	image.setZIndex(1);
	mask.setOverflow('hidden');

	bigPlayIcon.setTransition(0);
	bigPlayIcon.setWidth(70);
	bigPlayIcon.setHeight(70);
	bigPlayIcon.setSrc(REWRITE_BASE + ICONS + 'play@2x.png');
	bigPlayIcon.setAlpha(0);
	bigPlayIcon.setZIndex(3);
	self.cursorTargets.push(bigPlayIcon);
	self.events.dispatchEvent(ADD_CURSOR_TARGETS, {targets:self.cursorTargets});
	self.events.addEventListener(PAUSE_MEDIA, pauseMedia);
	self.events.addEventListener(DESTROY_MEDIA, destroyMedia);
	self.events.addEventListener(TRANSITION_END, createVideoPlayer);
	self.events.addEventListener(ADMIN_MOUSE_DOWN, vimeoPointerEventsOff);
	self.events.addEventListener(ADMIN_MOUSE_UP, vimeoPointerEventsOn);

	self.addEventListener(CHILD_ADDED, function(child) {
		loader = new LoadingIndicator({color:vars.loaderColor});
		loader.setZIndex(2);
		image.style['image-rendering'] = 'optimizeQuality';
		image.style['image-rendering'] = '-webkit-optimize-contrast';
		image.style['-webkit-backface-visibility'] = 'hidden';
		image.addEventListener(LOAD, imageLoaded);
		bigPlayIcon.addEventListener(LOAD, bigPlayIconLoaded);
		image.setSrc(vars.mediaItem.thumb.length > 0 ? REWRITE_BASE + MEDIA_ORIGINAL + vars.mediaItem.thumb : REWRITE_BASE + ICONS + "videoLarge.png");
		image.element.addEventListener('contextmenu', function(e){
			e.preventDefault();
		});
		image.element.addEventListener('dragstart', function(e){
			e.preventDefault();
		});
		self.addChild(mask);
		mask.addChild(image);
		mask.addChild(loader);
		mask.addChild(bigPlayIcon);
		if(vars.mediaItem && vars.mediaItem.size && vars.mediaItem.size.width) {
			/*trace('original size info available:', vars.mediaItem.size);*/
		}
		self.updateStyle();
		self.updatePosition();
	});

	self.play = function() {
		if(video) video.play();
	};

	self.pause = function() {
		if(video) video.pause();
	};

	self.destroy = function() {
		destroyMedia();
	};

	function vimeoPointerEventsOff() {
		if(isVimeo()) {
			video.pointerEvents = 'none';
		}
	}

	function vimeoPointerEventsOn() {
		if(isVimeo()) {
			video.setPointerEvents('auto');
		}
	}

	function bigPlayIconLoaded(e) {
		if(vars.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite' && !isPod() && !isPad()) {
			/*dont show big play*/
		} else {
			bigPlayIcon.setWidth(70);
			bigPlayIcon.setHeight(70);
			Tween(bigPlayIcon, UPDATE_SPEED, {alpha:1});
		}
	}

	function pauseMedia(e) {
		if(videoLoaded) {
			if(isVimeo()) {
				video.removeEventListener(VIMEO_METADATA_LOADED, onVimeoMetadataLoaded);
			} else {
				video.element.removeEventListener(METADATA_LOADED, metadataLoaded, false);
				video.element.removeEventListener(ERROR, loadError, false);
			}
			self.events.removeEventListener(TRANSITION_END, createVideoPlayer);
			video.pause();
		}
	}

	function destroyMedia(e) {
		if(video) {
			pauseMedia(e);
			self.events.removeEventListener(PAUSE_MEDIA, pauseMedia);
			mask.removeChild(video);
			video.destroy();
			self.events.removeEventListener(DESTROY_MEDIA, destroyMedia);
		}
	}

	function isVimeo() {
		return vars.mediaItem.content.indexOf("vimeo:") > -1;
	}

	function createVideoPlayer() {
		if(isVimeo() && !video) {
			video = new VimeoPlayer({events:vars.events});
			self.video = video;
			video.addEventListener(VIMEO_METADATA_LOADED, onVimeoMetadataLoaded);
			if(self.source) self.video.setSource(self.source);
			mask.addChild(video);
			video.setAlpha(0);
		} else if(!video) {
			video = new VideoPlayer({events:vars.events});
			self.video = video;
			video.element.addEventListener(METADATA_LOADED, metadataLoaded, false);
			video.element.addEventListener(ERROR, loadError, false);
			if(self.source) self.video.setSource(self.source);
			mask.addChild(video);
			video.setDisplay('none');
			video.setAlpha(0);
		}

		video.addEventListener(PAUSED, paused);
		video.addEventListener(PLAY, playEvent);
		video.addEventListener('playbackEnd', playbackEnd);

		bigPlayIcon.addEventListener(CLICK, bigPlayClick);
	}

	function paused(e) {
		if(isPod()) {
			video.setDisplay('none');
		}
	}

	function playEvent(e) {
		if(isPod()) {
			video.setDisplay('block');
			video.setAlpha(1);
		}
		self.events.dispatchEvent(PLAY);
	}

	function playbackEnd() {
		self.events.dispatchEvent(PAUSED);
	}

	function bigPlayClick(e) {
		if(isVimeo() && isPod()) {
			/*dont do nuthin*/
		} else {
			Tween(loader, UPDATE_SPEED, {alpha:1});
		}
		positionLoader();
		video.setSource(vars.mediaItem.content);
		if(isPod() || isPad()) {
			self.play();
			/*video.element.webkitEnterFullscreen();*/
			video.element.addEventListener('webkitendfullscreen', playbackEnd);
		}
	}

	function metadataLoaded(e) {
		video.parentView = vars.parentView;
		if(vars.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
			/*dont build hud*/
		} else {
			self.hud = new VideoHud(video);
		}
		initializePlay(this);
		video.setDisplay('block');
		Tween(video, UPDATE_SPEED, {alpha:1});
		self.cursorTargets.push(self.hud);
		self.events.dispatchEvent(ADD_CURSOR_TARGETS, {targets:[self.hud]});
	}

	function onVimeoMetadataLoaded(e) {
		initializePlay(this);
		setTimeout(function(){
			Tween(video, UPDATE_SPEED, {alpha:1})
		}, 500);
		if(!isCursorTarget(video) && vars.parentView !== 'LandingMediaView') {
			self.cursorTargets.push(video);
			self.events.dispatchEvent(ADD_CURSOR_TARGETS, {targets:[video]});
		}
	}

	function loadError(e) {
		loader.removeLoader();
		mask.removeChild(loader);
	}

	function isCursorTarget(value) {
		var i = self.cursorTargets.length;
		while(i--) {
			if(self.cursorTargets[i] === value) return true;
		}
		return false;
	}

	function initializePlay(element) {
		videoLoaded = true;
		self.updateSpeed = 0;
		originalVideoWidth = element.videoWidth;
		originalVideoHeight = element.videoHeight;
		self.updatePosition();
		loader.removeLoader();
		mask.removeChild(loader);
		setTimeout(function(){
			Tween(image, UPDATE_SPEED, {alpha:0,onComplete:function(){
				image.setDisplay('none');
			}});
		}, UPDATE_SPEED*1000);
		Tween(bigPlayIcon, UPDATE_SPEED, {alpha:0,onComplete:function(){
			bigPlayIcon.setDisplay('none');
		}});
		if(!isPod() && !isPad()) self.play();
		if(vars.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
			video.addEventListener(CLICK, video.togglePlay);
		}
	}

	self.updateStyle = function(e) {
		var color = vars.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite' ? LAYOUT_MODEL.siteBackgroundColor : LAYOUT_MODEL.containerColor;
		if(self.updateSpeed === 0) {
			self.setBackgroundColor(color);
		} else {
			Tween(self, self.updateSpeed, {backgroundColor:color});
		}
	};

	self.updateContent = function(e) {

	};

	self.hide = function() {
		if(self.getAlpha() === 1) {
			Tween(self, UPDATE_SPEED, {alpha:0,onComplete:function(){
				self.setDisplay('none');
			}});
		}
	};

	self.show = function() {
		if(self.getAlpha() === 0) {
			self.setDisplay('block');
			Tween(self, UPDATE_SPEED, {alpha:1});
		}
	};

	self.removeCursorTargets = function() {
		self.events.dispatchEvent(CURSOR_MEDIA_UPDATE, {views: self.cursorTargets, state:'remove'});
	};

	self.updatePosition = function(e) {
		layoutCalcs._activeView = 'imageVideo';
		var x = 0,
			maskX = layoutCalcs.mediaMask.x(),
			y = 0,
			maskY = layoutCalcs.mediaMask.y(),
			width = layoutCalcs.mediaView.width(),
			height = layoutCalcs.mediaView.height();

		maskWidth = layoutCalcs.mediaMask.width();
		maskHeight = layoutCalcs.mediaMask.height();

		if(vars.parentView === 'LandingMediaView' && LAYOUT_MODEL.landingMediaScaleType === 'fillSite') {
			x = maskX = layoutCalcs.landingMediaView.x();
			y = maskY = layoutCalcs.landingMediaView.y();
			width = maskWidth = layoutCalcs.landingMediaView.width();
			height = maskHeight = layoutCalcs.landingMediaView.height();
		}

		if(self.updateSpeed === 0) {
			self.setX(x);
			self.setY(y);
			self.setWidth(width);
			self.setHeight(height);

			mask.setX(maskX);
			mask.setY(maskY);
			mask.setWidth(maskWidth);
			mask.setHeight(maskHeight);
		} else {
			Tween(self, self.updateSpeed, {x:x});
			Tween(self, self.updateSpeed, {y:y});
			Tween(self, self.updateSpeed, {width:width});
			Tween(self, self.updateSpeed, {height:height});

			Tween(mask, self.updateSpeed, {x:maskX});
			Tween(mask, self.updateSpeed, {y:maskY});
			Tween(mask, self.updateSpeed, {width:maskWidth});
			Tween(mask, self.updateSpeed, {height:maskHeight});
		}

		positionLoader();
		alignBigPlay();

		if(posterLoaded && !videoLoaded) {
			resizeImage();
			alignImage();
		}

		if(videoLoaded) {
			resizeVideo();
			alignVideo();
		}

	};

	function imageLoaded(e) {
		self.updateSpeed = 0;
		posterLoaded = true;
		originalImageWidth = this.getWidth();
		originalImageHeight = this.getHeight();
		self.updatePosition();
		resizeImage();
		alignImage();
		Tween(loader, UPDATE_SPEED, {alpha:0});
		setTimeout(function() {
			if(vars.parentView === 'MediaView') {
				Tween(image, LAYOUT_MODEL.transitionDuration, {alpha:1, onComplete:function(){
					self.events.dispatchEvent(MEDIA_LOADED, e);
				}});
			} else if(vars.parentView === 'LandingMediaView') {
				Tween(image, LAYOUT_MODEL.landingMediaTransitionDuration, {alpha:1, onComplete:function(){
					self.events.dispatchEvent(MEDIA_LOADED, e);
				}});
			}
			self.updateSpeed = UPDATE_SPEED;
		}, 100);
		self.updateSpeed = UPDATE_SPEED;
	}

	function resizeImage() {
		var scale = new Scale();
		scale.setWidth(originalImageWidth);
		scale.setHeight(originalImageHeight);
		scale.setHRange(maskWidth);
		scale.setVRange(maskHeight);
		if(LAYOUT_MODEL.defaultVideoScale === 'fill') {
			scale.setType('fill');
		} else if(LAYOUT_MODEL.defaultVideoScale === 'stretch') {
			scale.setType('fit');
		} else if(LAYOUT_MODEL.defaultVideoScale === 'no stretch') {
			scale.setType('native');
		}
		if(vars.parentView === 'LandingMediaView') {
			scale.setType(LAYOUT_MODEL.landingMediaScaleType);
		}
		if(scale.getType() === 'native') {
			if(originalImageWidth > maskWidth || originalImageHeight > maskHeight) {
				scale.setType('fit');
			} else {
				scale.setHRange(originalImageWidth);
				scale.setVRange(originalImageHeight);
			}
		}
		if((scale.getType() === 'fillSite' || scale.getType() === 'fill') && (originalImageHeight > originalImageWidth && stage.getWidth() > stage.getHeight())) {
			scale.setType('fit');
		} else if((scale.getType() === 'fillSite' || scale.getType() === 'fill') && (originalImageWidth > originalImageHeight && stage.getHeight() > stage.getWidth())) {
			scale.setType('fit');
		}
		imageWidth = Mth.round(scale.getWidth());
		imageHeight = Mth.round(scale.getHeight());
		if(self.updateSpeed === 0) {
			image.setWidth(imageWidth);
			image.setHeight(imageHeight);
		} else {
			Tween(image, self.updateSpeed, {width:imageWidth, height:imageHeight});
		}
	}

	function alignImage() {
		var align = new Align();
		align.setWidth(imageWidth);
		align.setHeight(imageHeight);
		if(vars.parentView === 'LandingMediaView') {
			align.setHAlign(LAYOUT_MODEL.landingMediaAlignHorizontal);
			align.setVAlign(LAYOUT_MODEL.landingMediaAlignVertical);
		} else {
			align.setHAlign(LAYOUT_MODEL.imageAlignHorizontal);
			align.setVAlign(LAYOUT_MODEL.imageAlignVertical);
		}
		align.setHRange(maskWidth);
		align.setVRange(maskHeight);
		if(self.updateSpeed === 0) {
			image.setX(Mth.round(align.getX()));
			image.setY(Mth.round(align.getY()));
		} else {
			Tween(image, self.updateSpeed, {x:Mth.round(align.getX()), y:Mth.round(align.getY())});
		}
	}

	function resizeVideo() {
		var scale = new Scale();
		scale.setWidth(originalVideoWidth);
		scale.setHeight(originalVideoHeight);
		scale.setHRange(maskWidth);
		scale.setVRange(maskHeight);
		scale.setType(LAYOUT_MODEL.defaultVideoScale);
		if(isPod() || isPod()) {
			scale.setType('fit');
		} else if(vars.parentView === 'LandingMediaView') {
			scale.setType(LAYOUT_MODEL.landingMediaScaleType);
		} else {
			if(LAYOUT_MODEL.defaultVideoScale === 'stretch') {
				scale.setType('fit');
			} else if(LAYOUT_MODEL.defaultVideoScale === 'no stretch') {
				scale.setType('native');
			}
		}
		if(scale.getType() === 'native') {
			if(originalVideoWidth > maskWidth || originalVideoHeight > maskHeight) {
				scale.setType('fit');
			} else {
				scale.setHRange(originalVideoWidth);
				scale.setVRange(originalVideoHeight);
			}
		}
		videoWidth = Mth.round(scale.getWidth());
		videoHeight = Mth.round(scale.getHeight());
		video.setSize(videoWidth, videoHeight, self.updateSpeed);
	}

	function alignVideo() {
		var align = new Align();
		align.setWidth(videoWidth);
		align.setHeight(videoHeight);
		if(vars.parentView === 'LandingMediaView') {
			align.setHAlign(LAYOUT_MODEL.landingMediaAlignHorizontal);
			align.setVAlign(LAYOUT_MODEL.landingMediaAlignVertical);
		} else {
			align.setHAlign(LAYOUT_MODEL.imageAlignHorizontal);
			align.setHAlign(LAYOUT_MODEL.imageAlignVertical);
		}
		align.setHRange(maskWidth);
		align.setVRange(maskHeight);
		if(self.updateSpeed === 0) {
			video.setX(Mth.round(align.getX()));
			video.setY(Mth.round(align.getY()));
		} else {
			Tween(video, self.updateSpeed, {x:Mth.round(align.getX()), y:Mth.round(align.getY())});
		}
		if(self.hud) self.hud.resize(self.updateSpeed, Mth.round(align.getX()), Mth.round(align.getY()), videoWidth, videoHeight);
	}

	function positionLoader() {
		if(loader) {
			Tween(loader, self.updateSpeed, {x:(maskWidth - loader.getWidth()) * 0.5, y:(maskHeight - loader.getHeight()) * 0.5});
		}
	}

	function alignBigPlay() {
		if(self.updateSpeed === 0) {
			bigPlayIcon.setX((maskWidth - bigPlayIcon.getWidth()) * 0.5);
			bigPlayIcon.setY((maskHeight - bigPlayIcon.getHeight()) * 0.5);
		} else {
			Tween(bigPlayIcon, self.updateSpeed, {x:(maskWidth - bigPlayIcon.getWidth()) * 0.5, y:(maskHeight - bigPlayIcon.getHeight()) * 0.5});
		}
	}

	return self;
}

function OverlayController(vars) {
	var self = new ControllerProxy({parentView:vars.parentView, parentController:vars.parentController, events:vars.siteControllerEvents});
	var mode,
		sectionChange = false,
		section,
		assetId,
		uriInfo,
		path,
		settings,
		mediaItems,
		event,
		manualCaptionOff = false,
		mouseOverThumbsOnce = false,
		isModeChange = false;

	self.overlayView = new vars.OverlayView(vars);
	vars.mediaView.addChild(self.overlayView);

	self.captionView = new vars.CaptionView(vars);
	vars.mediaView.addChild(self.captionView);

	self.events.addEventListener(LAYOUT_MODEL_CHANGE, layoutModelChange);
	self.events.addEventListener(SITE_URI_CHANGE, checkOverlays);
	self.events.addEventListener(SECTIONS_MODEL_CHANGE, sectionUpdate);
	self.events.addEventListener(SETTINGS_MODEL_CHANGE, settingsUpdate);
	self.events.addEventListener(RESIZE_END, resizeEnd);
	self.events.addEventListener(AUTHENTICATED, onAuthenticated);

	function sectionUpdate(e) {
		sectionChange = true;
		e.section = e.section ? e.section : section;
		section = e.section ? e.section : section;
		if(e.field === 'thumb' && e.id === section.id) {
			e.mode = mode;
		} else if(e.field === 'media' && e.id === section.id) {
			e.mode = mode;
			section.media = e.value;
		}
		if(e.section) checkThumbs(e);
	}

	function settingsUpdate(e) {
		if(self.inquiry) {
			self.inquiry.updateInquiryInfo();
		}
	}

	function layoutModelChange(e) {
		if(e && self.share) {
			self.share.updateStyle(e);
			self.share.updatePosition(e);
		}
		if(e && self.inquiry) {
			self.inquiry.updateStyle(e);
			self.inquiry.updatePosition(e);
		}
		if(e && /caption/.test(e.id)) {
			self.captionView.updateStyle(e);
			self.captionView.updatePosition(e);
		}
		if(e && /overlay/.test(e.id) && (section.thumb.dock === 'overlay' || section.thumb.type === 'fill')) {
			sectionChange = true;
			checkThumbs({mode:mode, section:section});
		}
		if(e && /thumbnail/.test(e.id)) {
			sectionChange = true;
			checkThumbs({mode:mode, section:section});
		}
	}

	function resizeEnd(e) {
		layoutCalcs.captionView.size();
		self.captionView.updateSpeed = UPDATE_SPEED;
		self.captionView.updatePosition(e);
		self.overlayView.updatePosition(e);
		if(self.share) self.share.updatePosition(e);
		if(self.inquiry) self.inquiry.updatePosition(e);
		resizeThumbs(e);
	}

	function checkOverlays(e) {
		uriInfo = e;
		checkCaption(e);
		if(getAuthentication(e)) return;
		if(USER_AGENT !== POD) checkThumbs(e);
		checkShare(e);
		checkInquiry(e);
		checkFotomoto(e);
	}

	function checkFotomoto(e) {
		if(e.section && e.mode.indexOf('fotomoto') > -1) {
			var media = csvToArray(e.section.media);
			var mediaItem = getMediaById(media[e.assetId]);
			if(ACCOUNT_MODEL.fotoMotoId !== '' && mediaItem && mediaItem.type === IMAGE) {
				window.FOTOMOTO.API.showWindow(FOTOMOTO.API.PRINT, 'http://' + window.location.host + REWRITE_BASE + MEDIA_ORIGINAL + mediaItem.content);
			}
			self.events.dispatchEvent(NAVBAR_OVERLAY_BTN_CLICK, 'fotomoto');
		}
	}

	function checkCaption(e) {
		self.captionView.updateContent(uriInfo);
		self.captionView.updatePosition(e);

		if(e.mode.indexOf('caption') > -1) {
			if(self.captionView.getText() !== '' && USER_AGENT !== POD) {
				self.captionView.show();
			} else {
				self.captionView.hide();
			}
		}
	}

	function getAuthentication(e) {
		event = e;
		if(e.section && e.section.password.length > 0 && !e.section.hasPassed) {
			if(self.captionView) self.captionView.hide();
			return true;
		}
		return false;
	}

	function onAuthenticated() {
		checkThumbs(event);
		if(self.captionView && LAYOUT_MODEL.captionDefault) self.captionView.show();
		self.captionView.updateContent(event);
	}

	function checkThumbs(e) {

		/*
		 this function needs to be extremely simple
		 and merely obey the uri

		 if thumbs is in the uri then you build and turn them on
		 else you turn them off
		 */

		if(mode !== e.mode && section === e.section && assetId === e.assetId) {
			isModeChange = true;
		}

		mode = e.mode || '';
		section = e.section || section;
		assetId = e.assetId;
		path = e.path || path;
		mediaItems = csvToArray(e.section.media) || mediaItems;

		if(section.thumb && section.thumb.type === 'fill' && /thumbs/.test(mode)) {
			self.events.dispatchEvent(PAUSE_MEDIA);
		}

		if(self.thumbs && (self.thumbs.section !== section || sectionChange)) {
			self.events.dispatchEvent(OVERLAY_CHANGE, {views:[self.thumbs], state:'remove'});
			self.overlayView.removeChild(self.thumbs);
			delete self.thumbs;
		}

		if(e.section && e.section.thumb && e.section.thumb.type !== 'none') {

			settings = e.section.thumb;

			if(!self.thumbs && mode.indexOf('thumbs') > -1 ) {
				self.thumbs = new vars.ThumbsModule({
					assetId:assetId,
					path:path,
					mediaItems:mediaItems,
					settings:settings,
					events:vars.events,
					mediaView:vars.mediaView
				});
				self.thumbs.section = section;
				self.overlayView.addChild(self.thumbs);
				self.events.dispatchEvent(OVERLAY_CHANGE, {views:[self.thumbs], state:'add'});
				stage.addEventListener(MOUSE_MOVE, thumbsMouseMove);
			}
			if(self.thumbs && mode.indexOf('thumbs') > -1 && !self.thumbs.toggleState()) {
				self.thumbs.show();
				self.events.dispatchEvent(OVERLAY_CHANGE, {views:[self.thumbs], state:'add'});
			}
			if(self.thumbs && self.thumbs.toggleState() && mode.indexOf('thumbs') === -1) {
				if(isModeChange) {
					hideThumbs();
				} else {
					self.events.addEventListener(TRANSITION_START, mediaTransitionStart);
				}
			}
		}

		thumbHoldersBuilt();
		isModeChange = false;
		sectionChange = false;
	}

	function mediaTransitionStart() {
		self.events.removeEventListener(TRANSITION_START, mediaTransitionStart);
		if(LAYOUT_MODEL.overlayAlpha === 1) {
			self.events.dispatchEvent('OPAQUE_OVERLAY_CLOSE');
		}
		hideThumbs();
	}

	function thumbHoldersBuilt() {
		if(self.thumbs) {
			self.thumbs.highlight(-1);
			if(assetId > -1) {
				self.thumbs.highlight(assetId);
			} else {
				self.thumbs.highlight(-1);
			}
			if(self.thumbs.scroll && self.thumbs.toggleState() && !self.thumbs.mouseHover) {
				self.thumbs.scroll.setPercent(assetId / (mediaItems.length - 1));
				self.thumbs.setTransition(0);
				self.thumbs.scroll.update(0);
			}
		}
	}

	function hideThumbs() {
		self.thumbs.hide();
		self.events.dispatchEvent(OVERLAY_CHANGE, {views:[self.thumbs], state:'remove'});
	}

	function resizeThumbs() {
		if(self.thumbs) {
			self.thumbs.resize();
		}
	}

	function thumbsMouseMove(e) {
		var x = e.clientX,
			y = e.clientY;

		if(self.thumbs) {
			self.thumbs.mouseHover = self.thumbs.mask.hitTestPoint(x, y);
			if(self.thumbs.settings.type === 'strip' && self.thumbs.settings.dock === 'overlay') {
				if(self.thumbs.mask.hitTestPoint(x, y)) {
					mouseOverThumbsOnce = true;
					if(!self.thumbs.toggleState()) {
						vars.events.dispatchEvent(NAVBAR_OVERLAY_BTN_CLICK, 'thumbs');
						self.events.dispatchEvent(OVERLAY_CHANGE, {views:[self.thumbs], state:'add'});
					}
				} else {
					if(mouseOverThumbsOnce && self.thumbs.toggleState()) {
						vars.events.dispatchEvent(NAVBAR_OVERLAY_BTN_CLICK, 'thumbs');
						self.events.dispatchEvent(OVERLAY_CHANGE, {views:[self.thumbs], state:'remove'});
						mouseOverThumbsOnce = false;
					}
				}
			} else if(self.thumbs.settings.type === 'strip') {
				if(!self.thumbs.mask.hitTestPoint(x, y)) {

				}
			}
		}
	}

	function checkShare(e) {
		if(!self.share && e.mode.indexOf('email') > -1) {
			self.share = new ShareView(vars);
			self.overlayView.addChild(self.share);
			self.events.dispatchEvent(OVERLAY_CHANGE, {views:[self.share], state:'add'});
			self.share.updateContent(e);
		} else if(self.share && e.mode.indexOf('email') > -1) {
			self.share.updateContent(e);
			self.share.show();
		} else if(self.share && e.mode.indexOf('email') === -1) {
			self.share.hide();
		}
	}

	function checkInquiry(e) {
		if(!self.inquiry && e.mode.indexOf('inquiry') > -1) {
			self.inquiry = new InquiryView(vars);
			self.overlayView.addChild(self.inquiry);
			self.events.dispatchEvent(OVERLAY_CHANGE, {views:[self.inquiry], state:'add'});
			self.inquiry.updateContent(e);
		} else if(self.inquiry && e.mode.indexOf('inquiry') > -1) {
			self.inquiry.updateContent(e);
			self.inquiry.show();
		} else if(self.inquiry && e.mode.indexOf('inquiry') === -1) {
			self.inquiry.hide();
		}
	}

	return self;
}

function OverlayView(vars) {

	var self = new ViewProxy({events:vars.events});
	self.updateSpeed = 0;

	self.addEventListener(CHILD_ADDED, function(child) {
		self.updatePosition();
	});

	self.show = function() {
		self.setDisplay('block');
		Tween(self, UPDATE_SPEED, {alpha:1});
	};

	self.hide = function() {
		Tween(self, UPDATE_SPEED, {alpha:0,onComplete:function() {
			self.setDisplay('none');
		}});
	};

	self.updatePosition = function(e) {
		/*Tween(self, self.updateSpeed, {x:layoutCalcs.mediaView.x()});
		Tween(self, self.updateSpeed, {y:layoutCalcs.mediaView.y()});*/
		self.updateSpeed = UPDATE_SPEED;
	};

	return self;
}

function ThumbsModule(vars) {

	"use strict";

	var self = new Sprite({events:vars.events}),
		mask,
		grid,
		holderIndex = 0,
		axis = vars.settings.side === 'left' || vars.settings.side === 'right' || vars.settings.type === 'fill' ? 'y' : 'x',
		align = axis === 'y' ? 'top' : 'left',
		thumbArr = [],
		thumbsStyle,
		toggle,
		dock,
		tile = new Tile({
			marginHorizontal: percentToPixels(vars.settings.horizontalMargin, stage.getWidth()),
			marginVertical: percentToPixels(vars.settings.verticalMargin, stage.getHeight()),
			gap: Number(vars.settings.gap)
		}),
		assetId,
		closeHit,
		lazyLoaderTimeout;

	self.currentThumb = 1;
	self.settings = vars.settings;
	self.tile = tile;
	self.pointerEvents = 'auto';

	self.build = function() {
		self.updateSpeed = 0;
		self.holdersBuilt = false;
		self.id = 'thumbs';
		self.setOverflow('hidden');
		self.setZIndex(200);
		self.setAlpha(0);

		mask = self.mask = new Sprite();
		mask.id = 'mask';
		mask.setOverflow('hidden');
		if(vars.settings.dock === 'overlay' || vars.settings.type === 'fill') {
			mask.setBackgroundColor(hexToRgba(LAYOUT_MODEL.overlayColor, LAYOUT_MODEL.overlayAlpha));
		}
		self.addChild(mask);
		self.mask = mask;

		grid = new Sprite();
		grid.id = 'grid';

		mask.grid = grid;

		self.lazyLoader = new LazyLoader(grid, mask);

		holderIndex = 0;

		toggle = new Toggle({
			target: self,
			on: self.hide,
			off: self.show
		});

		if(USER_AGENT === PAD || TOUCH_DEVICE) {

			self.scroll = new Scroll(grid, mask, {
				types:['touch'],
				axis:axis,
				align:align,
				margin:percentToPixels(vars.settings.verticalMargin, stage.getHeight())
			});

		} else if(LAYOUT_MODEL.thumbnailScrollType === 'scrollbar' && vars.settings.type === 'fill') {

			makeCloseHit();

			var scrollBarWidth = 8;
			var scrollBarOffsetX = LAYOUT_MODEL.thumbnailScrollbarAlignment === 'right' ? -LAYOUT_MODEL.thumbnailScrollbarHorizontalOffset - scrollBarWidth : LAYOUT_MODEL.thumbnailScrollbarHorizontalOffset + scrollBarWidth;

			self.scroll = new Scroll(grid, mask, {
				types:['bar','wheel','touch'],
				axis:'y',
				align:align,
				color:LAYOUT_MODEL.thumbnailScrollbarColor,
				hover:LAYOUT_MODEL.thumbnailScrollbarHover,
				side:LAYOUT_MODEL.thumbnailScrollbarAlignment,
				width:scrollBarWidth,
				offsetX:scrollBarOffsetX,
				offsetY:LAYOUT_MODEL.thumbnailScrollbarVerticalOffset,
				margin:percentToPixels(vars.settings.verticalMargin, stage.getHeight())
			});

		} else if(LAYOUT_MODEL.thumbnailScrollType === 'mouse cursor' && vars.settings.type === 'fill') {

			makeCloseHit();

			self.scroll = new Scroll(grid, mask, {
				types:['hover','touch'],
				axis:'y',
				align:align,
				margin:percentToPixels(vars.settings.verticalMargin, stage.getHeight())
			});

		} else if(LAYOUT_MODEL.thumbnailScrollType === 'mouse cursor' || vars.settings.type === 'strip') {

			self.scroll = new Scroll(grid, mask, {
				types:['hover','touch'],
				axis:axis,
				align:align
			});

		}

		self.scroll.addEventListener(SCROLL, function(){
			clearTimeout(lazyLoaderTimeout);
			lazyLoaderTimeout = setTimeout(function(){
				if(self.lazyLoader) self.lazyLoader.update();
			}, 15);
		});

		defineThumbsStyle();
		toggle.state = true;

		if(vars.mediaItems.length > 0) makeHolders();

		self.resize();
		toggle.state = false;
	};

	function makeCloseHit() {
		closeHit = new Sprite();
		closeHit.setBackgroundColor(hexToRgba(LAYOUT_MODEL.overlayColor, 0));
		closeHit.setWidth(layoutCalcs.mediaView.width());
		closeHit.setHeight(layoutCalcs.mediaView.height());
		closeHit.addEventListener(CLICK, closeOverlay);
		self.addChild(closeHit);
	}

	function makeHolders() {

		var items = vars.mediaItems,
			length = items.length;

		holderIndex = 0;

		for(; holderIndex < length; holderIndex++) {
			var mediaItem = getMediaById(items[holderIndex]);
			var type = mediaItem.type;
			var holder = new Sprite();
			holder.setAlpha(0);
			holder.mediaItem = mediaItem;

			var width = THUMB_SIZES[vars.settings.size].width;
			var height = THUMB_SIZES[vars.settings.size].height;
			var thumb = '';

			holder.setWidth(width);
			holder.setHeight(height);

			if(mediaItem.type === 'image') thumb = mediaItem.content;

			if(mediaItem.thumb) thumb = mediaItem.thumb;

			if(thumb && vars.settings.masonry && vars.settings.type === 'fill') {
				holder.filename = getThumbQuery(width, 600, thumb);
			} else if(thumb && vars.settings.type === 'strip' && (vars.settings.side === 'top' || vars.settings.side === 'bottom')) {
				holder.filename = getThumbQuery(600, height, thumb);
			} else if(thumb && vars.settings.type === 'strip' && (vars.settings.side === 'left' || vars.settings.side === 'right')) {
				holder.filename = getThumbQuery(width, 600, thumb);
			} else if(thumb) {
				holder.filename = getThumbQuery(width, height, thumb);
			} else {
				holder.filename = getDefaultThumb(type);
			}

			if(type === VIDEO) {
				buildPlayIcon(holder, width, height);
			}

			holder.index = holderIndex;

			var c1, avg, tint;

			if(vars.settings.type === 'fill' && !vars.settings.masonry) {
				c1 = parseColor(LAYOUT_MODEL.overlayColor);
				holder.setBackgroundColor(hexToRgba(LAYOUT_MODEL.overlayColor, LAYOUT_MODEL.thumbnailGridTransparency));
				avg = (c1[0] + c1[1] + c1[2]) * 0.33;
				tint = avg < 128 ? '#FFFFFF' : '#000000';
			} else {
				c1 = parseColor('[[containerColor]]');
				avg = (c1[0] + c1[1] + c1[2]) * 0.33;
				tint = avg < 128 ? '#FFFFFF' : '#000000';
			}
			holder.loader = new LoadingIndicator({type:'spinner', alpha:0.65, color:tint});
			holder.addChild(holder.loader);
			positionLoader(holder.loader, holder);

			var hit = new Sprite();
			hit.id = 'hit';
			hit.setZIndex(4);
			hit.setAlpha(0.0001);
			hit.setWidth(width);
			hit.setHeight(height);
			hit.setBackgroundColor(LAYOUT_MODEL.thumbnailHoverColor);
			if(BROWSER_NAME === 'Safari') hit.style['-webkit-backface-visibility'] = 'hidden';
			holder.addChild(hit);

			holder.hit = hit;

			tile.addItem(width, height);
			grid.addChild(holder);
			thumbArr.push(holder);

			hit.addEventListener(MOUSE_OVER, onMouseOver);
			hit.addEventListener(MOUSE_OUT, onMouseOut);
			hit.addEventListener(CLICK, select);

			holder.load = load;
			holder.unload = unload;

			if(holderIndex === length - 1) {
				mask.addChild(grid);
				self.holdersBuilt = true;
				self.updateSpeed = UPDATE_SPEED;
				if(self.scroll && vars.settings.type === 'strip') {
					self.scroll.align = 'center';
				}
				self.resize();
			}
		}

	}

	function getDefaultThumb(type) {
		var link = REWRITE_BASE + ICONS;
		switch(type) {
			case VIDEO:
				link += 'video@2x.png';
				break;
			case HTML:
				link += 'text@2x.png';
				break;
			case SWF:
				link += 'swf@2x.png';
				break;
			case LINK:
				link += 'link@2x.png';
				break;
			case PDF:
				link += 'pdf@2x.png';
				break;
			case CONTACT:
				link += 'contact@2x.png';
				break;
		}
		return link;
	}

	function defineThumbsStyle() {
		switch(vars.settings.type) {
			case 'fill':
				thumbsStyle = new ThumbsFill({
					container: self,
					mask: mask,
					items: thumbArr,
					side: vars.settings.side,
					size: vars.settings.size,
					horizontalMargin: percentToPixels(vars.settings.horizontalMargin, stage.getWidth()),
					verticalMargin: percentToPixels(vars.settings.verticalMargin, stage.getHeight()),
					masonry: vars.settings.masonry,
					tile: tile,
					parent: self,
					grid: grid
				});
				break;
			case 'strip':
				var reveal = vars.settings.dock === 'displace' ? 0 : 24;
				if(BROWSER_NAME === 'Safari') self.style['-webkit-backface-visibility'] = 'hidden';
				dock = new Dock(self, layoutCalcs.mediaView.size, {
					side:vars.settings.side,
					align:'left',
					offset:0,
					margin:0,
					reveal:reveal,
					updateSpeed:UPDATE_SPEED
				});
				thumbsStyle = new ThumbsStrip({
					container: self,
					mask: mask,
					items: thumbArr,
					side: vars.settings.side,
					size: vars.settings.size,
					horizontalMargin: percentToPixels(vars.settings.horizontalMargin, stage.getWidth()),
					verticalMargin: percentToPixels(vars.settings.verticalMargin, stage.getHeight()),
					tile: tile,
					parent: self
				});
				break;
		}
	}

	function buildPlayIcon(holder, width, height) {
		var playBtn = new Bitmap();
		playBtn.setSrc(REWRITE_BASE + ICONS + 'play@2x.png');
		playBtn.setWidth(50);
		playBtn.setHeight(50);
		playBtn.setAlpha(0);
		playBtn.setZIndex(3);
		playBtn.thumbHolder = holder;
		playBtn.index = holder.index;
		playBtn.setX(Math.floor((width - 50) * 0.5));
		playBtn.setY(Math.floor((height - 50) * 0.5));
		Tween(playBtn, 1, {alpha:0.85});
		holder.playBtn = playBtn;
		holder.addChild(playBtn);
	}

	function onMouseOver(e) {
		if(assetId !== e.target.parent.index) {
			Tween(e.target, 0.2, {alpha:LAYOUT_MODEL.thumbnailHoverAlpha, backgroundColor:LAYOUT_MODEL.thumbnailHoverColor});
		} else {
			Tween(e.target, 0.2, {alpha:LAYOUT_MODEL.thumbnailHoverAlpha, backgroundColor:LAYOUT_MODEL.thumbnailSelectedColor});
		}
	}

	function onMouseOut(e) {
		if(assetId !== e.target.parent.index) {
			Tween(e.target, 0.25, {alpha:0});
		} else {
			Tween(e.target, 0.2, {alpha:LAYOUT_MODEL.thumbnailHoverAlpha, backgroundColor:LAYOUT_MODEL.thumbnailSelectedColor});
		}
	}

	function select(e) {
		var mode = '';
		if(vars.settings.type === 'strip' && vars.settings.defaultOn && vars.settings.dock === 'displace') {
			mode = 'thumbs';
		}
		if(!Scrolls.getScrolling()) {
			self.events.dispatchEvent(THUMB_CLICK, {
				assetId:Number(e.target.parent.index),
				type:'internal',
				path:vars.path,
				mode:mode,
				linkTarget:'_self'
			});
		}
	}

	function positionLoader(loader, parent) {
		loader.setX((parent.getWidth() - loader.getWidth()) * 0.5);
		loader.setY((parent.getHeight() - loader.getHeight()) * 0.5);
	}

	function closeOverlay(e) {
		self.events.dispatchEvent(NAVBAR_OVERLAY_BTN_CLICK, "thumbs");
	}

	function load() {
		if(!this.thumb) {
			var thumb = new Bitmap();
			thumb.addEventListener('load', thumbsStyle.thumbLoaded);
			thumb.setSrc(this.filename);
			thumb.setAlpha(0);
			this.loaded = true;
			this.addChild(thumb);
			this.thumb = thumb;
		}
	}

	function unload() {
		if(this.thumb) {
			this.loaded = false;
			this.removeChild(this.thumb);
			delete this.thumb;
		}
	}

	self.highlight = function(value) {
		var i = thumbArr.length;
		assetId = value !== undefined ? value : vars.assetId;
		while(i--) {
			if(i === assetId) {
				self.currentThumb = i + 1;
				thumbArr[i].hit.setBackgroundColor(LAYOUT_MODEL.thumbnailSelectedColor);
				Tween(thumbArr[i].hit, UPDATE_SPEED, {alpha:LAYOUT_MODEL.thumbnailHoverAlpha});
			} else {
				Tween(thumbArr[i].hit, UPDATE_SPEED, {alpha:0});
			}
		}
	};

	self.hide = function() {
		if(toggle.state) {
			Tween(self, UPDATE_SPEED, {alpha:0, onComplete:function() {
				self.setDisplay('none');
				self.updateSpeed = 0;
				toggle.state = false;
			}});
			if(vars.settings.type === 'strip') {
				dock.hide();
			}
		}
	};

	self.show = function() {
		if(!toggle.state) {
			self.setDisplay('block');
			self.setAlpha(0);
			Tween(self, UPDATE_SPEED, {alpha:1, onComplete:showComplete});
			if(vars.settings.type === 'strip') {
				dock.show();
			}
		}
	};

	function showComplete() {
		toggle.state = true;
		if(self.holdersBuilt) self.updateSpeed = UPDATE_SPEED;
	}

	self.toggle = function() {
		toggle.flip();
		if(vars.settings.type === 'strip') {
			dock.toggle();
		}
		if(toggle.state) {
			self.show();
		} else {
			self.hide();
		}
	};

	self.toggleState = function() {
		return toggle.state;
	};

	self.resize = function() {
		if(thumbsStyle) {
			thumbsStyle.resize();
		}
		if(closeHit) {
			closeHit.setWidth(layoutCalcs.mediaView.width());
			closeHit.setHeight(layoutCalcs.mediaView.height());
		}
		if(self.scroll) self.scroll.resize();
	};

	self.build();

	return self;
}

function ThumbsFill(vars) {

	var self = this,
		container = vars.container,
		mask = vars.mask,
		side = vars.side,
		size = vars.size,
		masonry = vars.masonry,
		grid = vars.grid,
		tile = vars.tile;

	tile.axis = 'y';
	tile.align = 'left';
	tile.wrap = true;

	function arrange() {
		tile.perpLength = layoutCalcs.mediaView.width() - (vars.horizontalMargin * 2);
		tile.layoutItems();
		items = mask.grid.children;
		var i = 0,
			l = items.length;
		for(; i < l; i++) {
			var position = tile.getPosition(i);
			if(i === items.length - 1) {
				items[i].lastItem = true;
			}
			Tween(items[i], vars.parent.updateSpeed, {x:position.x, y:position.y, onComplete:function(){
				Tween(this, UPDATE_SPEED, {alpha:1});
				if(this.lastItem && vars.parent.lazyLoader && vars.parent.holdersBuilt) vars.parent.lazyLoader.update();
			}});
		}
	}

	this.resize = function() {
		container.setTransition(0);
		container.setX(0);
		container.setY(0);
		container.setWidth(layoutCalcs.mediaView.width());
		container.setHeight(layoutCalcs.mediaView.height());
		mask.setWidth(container.getWidth());
		mask.setHeight(container.getHeight());
		arrange();
		var bounds = tile.getBounds();
		grid.setWidth(bounds.width);
		grid.setHeight(bounds.height);
		var align = new Align({hRange:layoutCalcs.mediaView.width(), vRange:layoutCalcs.mediaView.height(), width:bounds.width, height:bounds.height, hAlign:'center', vAlign:'top', hOffset:0, vOffset:vars.verticalMargin});
		grid.setX(align.getX());
	};

	this.thumbLoaded = function(e) {
		var thumbRatio = THUMB_SIZES[size].width / THUMB_SIZES[size].height;
		var ratio = this.getWidth() / this.getHeight();
		if(RETINA) {
			this.setWidth(this.getWidth() * 0.5);
			this.setHeight(this.getHeight() * 0.5);
		} else {
			this.setWidth(this.getWidth());
			this.setHeight(this.getHeight());
		}
		if(masonry) {
			this.setWidth(THUMB_SIZES[size].width);
			this.setHeight(THUMB_SIZES[size].width / ratio);
			this.parent.setWidth(this.getWidth());
			this.parent.setHeight(this.getHeight());
			this.parent.hit.setWidth(this.getWidth());
			this.parent.hit.setHeight(this.getHeight());
		} else {
			if(thumbRatio < ratio) {
				this.setWidth(THUMB_SIZES[size].width);
				this.setHeight(THUMB_SIZES[size].width / ratio);
			} else {
				this.setWidth(THUMB_SIZES[size].height * ratio);
				this.setHeight(THUMB_SIZES[size].height);
			}
			var align = new Align({hRange:this.parent.getWidth(), vRange:this.parent.getHeight(), width:this.getWidth(), height:this.getHeight(), hAlign:'center', vAlign:'center', hOffset:0, vOffset:0});
			this.setX(align.getX());
			this.setY(align.getY());
		}

		if(masonry) {
			tile.setSize(this.parent.index, this.getWidth(), this.getHeight());
		} else {
			tile.setSize(this.parent.index, this.parent.getWidth(), this.parent.getHeight());
		}
		vars.parent.resize();
		if(this.parent.loader) this.parent.removeChild(this.parent.loader);

		if(this.parent.playBtn) {
			this.parent.playBtn.setTransition(0);
			this.parent.playBtn.setX(Math.floor((this.getWidth() - 50) * 0.5) + this.getX());
			this.parent.playBtn.setY(Math.floor((this.getHeight() - 50) * 0.5) + this.getY());
		}

		Tween(this, UPDATE_SPEED, {alpha:1});
	};

}

function ThumbsStrip(vars) {

	var self = this,
		container = vars.container,
		mask = vars.mask,
		side = vars.side,
		size = vars.size,
		tile = vars.tile;

	tile.align = 'bottom';
	tile.wrap = true;

	function Const() {
		switch(side) {
			case 'top':
				tile.axis = 'x';
				tile.perpLength = THUMB_SIZES[size].height;
				container.setHeight(THUMB_SIZES[size].height);
				mask.setHeight(THUMB_SIZES[size].height);
				break;
			case 'right':
				tile.axis = 'y';
				tile.perpLength = THUMB_SIZES[size].width;
				container.setWidth(THUMB_SIZES[size].width);
				mask.setWidth(THUMB_SIZES[size].width);
				break;
			case 'bottom':
				tile.axis = 'x';
				tile.perpLength = THUMB_SIZES[size].height;
				container.setHeight(THUMB_SIZES[size].height);
				mask.setHeight(THUMB_SIZES[size].height);
				break;
			case 'left':
				tile.axis = 'y';
				tile.perpLength = THUMB_SIZES[size].width;
				container.setWidth(THUMB_SIZES[size].width);
				mask.setWidth(THUMB_SIZES[size].width);
				break;
		}
	}

	function arrange() {
		tile.layoutItems();
		items = mask.grid.children;
		var i = 0,
			l = items.length;
		for(; i < l; i++) {
			var position = tile.getPosition(i);
			items[i].setTransition(0);
			items[i].setAlpha(1);
			items[i].setX(position.x);
			items[i].setY(position.y);
			if(i === items.length - 1) {
				if(vars.parent.lazyLoader && vars.parent.holdersBuilt) vars.parent.lazyLoader.update();
			}
		}
	}

	this.resize = function() {
		if(side === 'top' || side === 'bottom') {
			container.setWidth(layoutCalcs.mediaView.width());
			mask.setWidth(layoutCalcs.mediaView.width());
		} else {
			mask.setHeight(layoutCalcs.mediaView.height());
			container.setHeight(layoutCalcs.mediaView.height());
		}
		arrange();
		var bounds = tile.getBounds();
		mask.grid.setWidth(bounds.width);
		mask.grid.setHeight(bounds.height);
		Docks.resize();
	};

	this.thumbLoaded = function() {
		var width = this.getWidth();
		var height = this.getHeight();
		var ratio = width / height;

		if(RETINA) {
			this.setWidth(width * 0.5);
			this.setHeight(height * 0.5);
		} else {
			this.setWidth(width);
			this.setHeight(height);
		}

		if(side === 'top' || side === 'bottom') {
			this.setWidth(Mth.floor(THUMB_SIZES[size].height * ratio));
			this.setHeight(THUMB_SIZES[size].height);
		} else {
			this.setWidth(THUMB_SIZES[size].width);
			this.setHeight(Mth.floor(THUMB_SIZES[size].width / ratio));
		}

		this.parent.setWidth(this.getWidth());
		this.parent.setHeight(this.getHeight());
		this.parent.hit.setWidth(this.getWidth());
		this.parent.hit.setHeight(this.getHeight());
		tile.setSize(this.parent.index, this.getWidth(), this.getHeight());
		vars.parent.resize();
		if(this.parent.loader) this.parent.removeChild(this.parent.loader);

		if(this.parent.playBtn) {
			this.parent.playBtn.setTransition(0);
			this.parent.playBtn.setX(Math.floor((this.getWidth() - 50) * 0.5) + this.getX());
			this.parent.playBtn.setY(Math.floor((this.setHeight() - 50) * 0.5) + this.getY());
		}

		Tween(this, UPDATE_SPEED, {alpha:1});
	};

	Const.call(this);
}


function CaptionView(vars) {

	var self = new TextField({events:vars.events});
	self.setAlpha(0);
	self.state = 'closed';
	self.setZIndex(2);

	self.updateSpeed = 0;

	var captionDefault;

	self.addEventListener(CHILD_ADDED, function(child) {
		self.updateStyle();
		self.updatePosition();
		self.style['-webkit-backface-visibility'] = 'hidden';
	});

	self.updateStyle = function(e) {
		if(LAYOUT_MODEL.captionDefault !== captionDefault) {
			captionDefault = LAYOUT_MODEL.captionDefault;
		}
		if(LAYOUT_MODEL.captionFont !== self.getFontFamily()) {
			self.setFontFamily(LAYOUT_MODEL.captionFont);
		}
		if(LAYOUT_MODEL.captionFontSize !== self.getFontSize()) {
			self.setFontSize(LAYOUT_MODEL.captionFontSize);
		}
		if(LAYOUT_MODEL.captionFontColor !== self.getFontColor()) {
			self.setFontColor(LAYOUT_MODEL.captionFontColor);
		}
		if(LAYOUT_MODEL.captionBackgroundRectColor !== self.getBackgroundColor()) {
			self.setBackgroundColor(hexToRgba(LAYOUT_MODEL.captionBackgroundRectColor, LAYOUT_MODEL.captionBackgroundAlpha));
		}
		self.setTextAlign('center');
		self.setPaddingTop(10);
	};

	self.updateContent = function(e) {
		self.setAlpha(0);
		var media = csvToArray(e.section.media);
		if(media.length > 0) {
			var mediaItem = MEDIA_MODEL[media[e.assetId]];
			if(mediaItem && mediaItem.type !== LINK) {
				self.setText(MEDIA_MODEL[media[e.assetId]].caption.replace(/\\n/g, '<br>'));
				layoutCalcs.captionView.size(self.getText());
			}
		}
		self.updateSpeed = 0;
		self.updateStyle(e);
		setTimeout(self.updatePosition, 50);
	};

	self.updatePosition = function(e) {
		var _x = layoutCalcs.captionView.x(),
			_y = layoutCalcs.captionView.y();
		self.setWidth(layoutCalcs.captionView.width);
		self.setHeight(layoutCalcs.captionView.height);
		if(self.updateSpeed === 0) {
			self.setTransition(0);
			self.setX(_x);
			self.setY(_y);
		} else {
			Tween(self, self.updateSpeed, {x:_x, y:_y});
		}
	};

	self.show = function() {
		self.setDisplay('block');
		self.updatePosition();
		Tween(self, UPDATE_SPEED, {alpha:1});
		self.state = 'open';
	};

	self.hide = function() {
		if(self.state === 'open') {
			Tween(self, UPDATE_SPEED, {alpha:0, onComplete:function() {
				self.setDisplay('none');
			}});
			self.state = 'closed';
		}
	};

	return self;
}

function InquiryView(vars) {

	var self = new Sprite(vars);

	var width = 573,
		height = 225,
		inquiry,
		title,
		line,
		info,
		preview,
		form,
		mediaItem,
		yourName,
		yourEmail,
		message,
		submit,
		closeHit,
		responseMessage = new Sprite();

	self.updateSpeed = 0;
	self.setBackgroundColor(hexToRgba(LAYOUT_MODEL.overlayColor, LAYOUT_MODEL.overlayAlpha));
	self.setZIndex(100);
	self.setAlpha(0);
	self.visible = false;

	function init() {
		closeHit = new Sprite();
		closeHit.setBackgroundColor('#cccccc');
		closeHit.setAlpha(0);
		closeHit.addEventListener(MOUSE_DOWN, closeOverlay);
		self.addChild(closeHit);

		inquiry = new Sprite();
		inquiry.setWidth(width);
		inquiry.setHeight(height);
		self.addChild(inquiry);

		title = new TextField();
		title.setText(SETTINGS_MODEL.inquiryTitle ? SETTINGS_MODEL.inquiryTitle : 'Inquire about this image');
		title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		title.setFontColor(LAYOUT_MODEL.thumbnailTitleFontColor);
		title.setFontSize(LAYOUT_MODEL.thumbnailTitleFontSize);
		inquiry.addChild(title);

		line = new Sprite();
		line.setBackgroundColor(hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35));
		line.setWidth(width);
		line.setHeight(1);
		line.setY(title.getHeight() + 10);
		inquiry.addChild(line);

		if(SETTINGS_MODEL.inquiryInfo) {
			info = new TextField();
			info.setText(SETTINGS_MODEL.inquiryInfo);
			info.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			info.setFontColor(LAYOUT_MODEL.thumbnailTitleFontColor);
			info.setX(2);
			info.setY(title.getHeight() + 20);
			info.setFontSize(13);
			inquiry.addChild(info);
		}

		preview = new Bitmap();
		preview.setAlpha(0);
		inquiry.addChild(preview);
		preview.addEventListener(LOAD, previewLoaded);

		form = new Sprite();
		form.setX(222);
		if(SETTINGS_MODEL.inquiryInfo) {
			form.setY(info.getY() + info.getHeight() + 10);
		} else {
			form.setY(40);
		}
		form.setWidth(width - form.getX());
		form.setHeight(height - form.getY());
		inquiry.addChild(form);

		yourName = buildFormInput("Your Name");
		yourName.setX(0);
		yourName.setY(0);
		form.addChild(yourName);

		yourEmail = buildFormInput('Your Email');
		yourEmail.setX(180);
		yourEmail.setY(0);
		form.addChild(yourEmail);

		message = buildFormInput("Message");
		message.setX(0);
		message.setY(50);
		form.addChild(message);

		submit = new Button();
		submit.setText('Submit');
		submit.setY(135);
		submit.setFontSize(12);
		submit.setFontColor(LAYOUT_MODEL.overlayColor);
		submit.setBackgroundColor(LAYOUT_MODEL.thumbnailTitleFontColor);
		submit.setPaddingTop(2);
		submit.setPaddingRight(14);
		submit.setPaddingBottom(2);
		submit.setPaddingLeft(14);
		form.addChild(submit);
		submit.setX(form.getWidth() - submit.getWidth());
		submit.addEventListener(FOCUS, onFocusSubmit);
		submit.addEventListener(BLUR, onBlurSubmit);
		submit.addEventListener(MOUSE_OVER, onFocusSubmit);
		submit.addEventListener(MOUSE_OUT, onBlurSubmit);
		if(TOUCH_DEVICE) {
			submit.addEventListener(TOUCH_END, onSubmit);
		} else {
			submit.addEventListener(CLICK, onSubmit);
		}

		responseMessage.setAlpha(0);
		responseMessage.setX(form.getX());
		if(SETTINGS_MODEL.inquiryInfo) {
			responseMessage.setY(info.getY() + info.getHeight() + 10);
		} else {
			responseMessage.setY(40);
		}
		responseMessage.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		responseMessage.setFontColor(LAYOUT_MODEL.thumbnailTitleFontColor);
		responseMessage.setFontSize(14);
		inquiry.addChild(responseMessage);

		self.updateStyle();
		self.updatePosition();
		self.show();

		self.events.addEventListener(INQUIRY_FORM_SENT, inquiryResponse);
	}

	function previewLoaded(e) {
		preview.setWidth(RETINA ? this.getWidth() * 0.5 : this.getWidth());
		preview.setHeight(RETINA ? this.getHeight() * 0.5 : this.getHeight());
		Tween(this, UPDATE_SPEED, {alpha:1});
	}

	function buildFormInput(label) {
		var holder = new Sprite();
		holder.setWidth(168);
		var title = new TextField();
		title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		title.setFontColor(hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.7));
		title.setFontSize(11);
		title.setText(label);
		holder.addChild(title);
		var input = label === 'Message' ? new TextArea() : new Input();
		input.setBackgroundColor(hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.1));
		input.setBorder('1px solid ' + hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.3));
		input.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		input.setFontColor(LAYOUT_MODEL.thumbnailTitleFontColor);
		input.setFontSize(14);
		input.setWidth(label === 'Message' ? (holder.getWidth() * 2) + 12 : 168);
		input.setHeight(label === 'Message' ? 55 : 20);
		input.setY(17);

		input.addEventListener(FOCUS, onFocus);
		input.addEventListener(BLUR, onBlur);

		holder.addChild(input);
		holder.id = label;
		holder.input = input;
		holder.title = title;
		return holder;
	}

	function onFocus(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.2), border:'1px solid ' + hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.7)});
	}

	function onBlur(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.1), border:'1px solid ' + hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.3)});
	}

	function onFocusSubmit(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.8)});
	}

	function onBlurSubmit(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 1)});
	}

	function onSubmit(e) {

		var str = '&shareItem=' + encodeURIComponent(getThumbQuery(1600, 1200, mediaItem.content)) +
			'&fromName=' + encodeURIComponent(yourName.input.getValue()) +
			'&fromEmail=' + encodeURIComponent(yourEmail.input.getValue()) +
			'&message=' + encodeURIComponent(message.input.getValue());

		str = str.replace(/\r\n/g,"<br>").replace(/\n\r/g,"<br>").replace(/\r/g,"<br>").replace(/\n/g,"<br>");

		self.events.dispatchEvent(INQUIRY_FORM_SUBMIT, str);
	}

	function inquiryResponse(e) {
		responseMessage.setText(e.response);
		Tween(form, UPDATE_SPEED, {alpha:0});
		Tween(responseMessage, UPDATE_SPEED, {alpha:1});
		self.addEventListener(MOUSE_DOWN, closeOverlay);
	}

	function closeOverlay(e) {
		if(e.target === self) self.removeEventListener(MOUSE_DOWN, closeOverlay);
		self.events.dispatchEvent(NAVBAR_OVERLAY_BTN_CLICK, "inquiry");
	}

	self.updateStyle = function(e) {
		if(LAYOUT_MODEL.thumbnailTitleFont !== title.getFontFamily()) {
			title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			if(SETTINGS_MODEL.inquiryInfo) info.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			yourName.title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			yourEmail.title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			message.title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			submit.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		}
		if(LAYOUT_MODEL.thumbnailTitleFontSize !== title.getFontSize()) {
			Tween(title, UPDATE_SPEED, {fontSize:LAYOUT_MODEL.thumbnailTitleFontSize, onComplete:function() {
				self.updatePosition(e);
			}});
		}
		if(LAYOUT_MODEL.thumbnailTitleFontColor !== title.getFontColor()) {
			Tween(title, UPDATE_SPEED, {fontColor:LAYOUT_MODEL.thumbnailTitleFontColor});
			if(SETTINGS_MODEL.inquiryInfo) Tween(info, UPDATE_SPEED, {fontColor:LAYOUT_MODEL.thumbnailTitleFontColor});
			Tween(line, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(yourName.title, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(yourEmail.title, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(message.title, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(submit, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.overlayColor, 0.35)});
			Tween(submit, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
		}
		if(hexToRgba(LAYOUT_MODEL.overlayColor, LAYOUT_MODEL.overlayAlpha) !== self.getBackgroundColor()) {
			Tween(self, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.overlayColor, LAYOUT_MODEL.overlayAlpha)});
		}
	};

	self.updatePosition = function(e) {
		self.setX(0);
		self.setY(0);
		width = layoutCalcs.mediaView.width() > 573 ? 573 : layoutCalcs.mediaView.width();
		height = layoutCalcs.mediaView.height() > 225 ? 225 : layoutCalcs.mediaView.height();

		line.setY(title.getHeight() + 10);

		preview.setX(2);
		if(SETTINGS_MODEL.inquiryInfo) {
			info.setY(title.getHeight() + 20);
			preview.setY(info.getY() + info.getHeight() + 10);
		} else {
			preview.setY(title.getHeight() + 20);
		}

		if(layoutCalcs.mediaView.width() > 573) {
			form.setX(222);
			if(SETTINGS_MODEL.inquiryInfo) {
				form.setY(info.getY() + info.getHeight() + 10);
			} else {
				form.setY(preview.getY());
			}
		} else {
			form.setX(2);
			form.setY(preview.getY() + preview.getHeight() + 10);
		}

		responseMessage.setX(form.getX());
		if(SETTINGS_MODEL.inquiryInfo) {
			responseMessage.setY(info.getY() + info.getHeight() + 10);
		} else {
			responseMessage.setY(40);
		}

		if(self.updateSpeed === 0) {
			self.setWidth(layoutCalcs.mediaView.width());
			self.setHeight(layoutCalcs.mediaView.height());
			inquiry.setX(((layoutCalcs.mediaView.width() - width) * 0.5));
			inquiry.setY(((layoutCalcs.mediaView.height() - height) * 0.5));
			inquiry.setWidth(width);
			inquiry.setHeight(height);
		} else {
			Tween(self, self.updateSpeed, {
				width:layoutCalcs.mediaView.width(),
				height:layoutCalcs.mediaView.height()
			});
			Tween(inquiry, self.updateSpeed, {
				x:(layoutCalcs.mediaView.width() - width) * 0.5,
				y:(layoutCalcs.mediaView.height() - height) * 0.5,
				width:width,
				height:height
			});
		}

		closeHit.setWidth(layoutCalcs.mediaView.width());
		closeHit.setHeight(layoutCalcs.mediaView.height());
		self.updateSpeed = UPDATE_SPEED;
	};

	self.show = function() {
		self.updateSpeed = 0;
		self.updatePosition();
		if(!self.visible) {
			self.visible = true;
			self.setDisplay('block');
			Tween(self, UPDATE_SPEED, {alpha:1});
			Tween(form, UPDATE_SPEED, {alpha:1});
			Tween(responseMessage, UPDATE_SPEED, {alpha:0});
		}
	};

	self.hide = function() {
		if(self.visible) {
			self.visible = false;
			Tween(self, UPDATE_SPEED, {alpha:0, onComplete:function(){
				this.setDisplay('none');
			}});
		}
	};

	self.updateInquiryInfo = function() {
		title.setText(SETTINGS_MODEL.inquiryTitle ? SETTINGS_MODEL.inquiryTitle : 'Inquire about this image');
		if(SETTINGS_MODEL.inquiryInfo) {
			info.setText(SETTINGS_MODEL.inquiryInfo);
		}
		self.updatePosition();
	};

	self.updateContent = function(e) {
		var mediaItems = csvToArray(e.section.media);
		mediaItem = getMediaById(mediaItems[e.assetId]);
		if(preview) {
			inquiry.removeChild(preview);
		}
		preview = new Bitmap();
		preview.setAlpha(0);
		preview.setX(2);
		if(SETTINGS_MODEL.inquiryInfo) {
			preview.setY(info.getY() + info.getHeight() + 10);
		} else {
			preview.setY(title.getHeight() + 20);
		}
		preview.setSrc(getThumbQuery(200, 165, mediaItem.content));
		inquiry.addChild(preview);
		preview.addEventListener(LOAD, previewLoaded);
	};

	self.addEventListener(CHILD_ADDED, function(child) {
		init.call(self);
	});

	return self;

}

function ShareView(vars) {

	var self = new Sprite(vars);

	var width = 573,
		height = 225,
		share,
		title,
		line,
		preview,
		form,
		mediaItem,
		yourName,
		friendsName,
		yourEmail,
		friendsEmail,
		message,
		submit,
		closeHit,
		responseMessage = new Sprite();

	self.updateSpeed = 0;
	self.setBackgroundColor(hexToRgba(LAYOUT_MODEL.overlayColor, LAYOUT_MODEL.overlayAlpha));
	self.setZIndex(100);
	self.setAlpha(0);
	self.visible = false;

	function init() {
		closeHit = new Sprite();
		closeHit.setBackgroundColor('#cccccc');
		closeHit.setAlpha(0);
		closeHit.addEventListener(MOUSE_DOWN, closeOverlay);
		self.addChild(closeHit);

		share = new Sprite();
		share.setWidth(width);
		share.setHeight(height);
		self.addChild(share);

		title = new TextField();
		title.setText("Email this image");
		title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		title.setFontColor(LAYOUT_MODEL.thumbnailTitleFontColor);
		title.setFontSize(LAYOUT_MODEL.thumbnailTitleFontSize);
		share.addChild(title);

		line = new Sprite();
		line.setBackgroundColor(hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35));
		line.setWidth(width);
		line.setHeight(1);
		line.setY(LAYOUT_MODEL.thumbnailTitleFontSize + 10);
		share.addChild(line);

		preview = new Bitmap();
		preview.setAlpha(0);
		share.addChild(preview);
		preview.addEventListener(LOAD, previewLoaded);

		form = new Sprite();
		form.setX(222);
		form.setY(title.getHeight() + 20);
		form.setWidth(width - form.getX());
		form.setHeight(height - form.getY());
		share.addChild(form);

		yourName = buildFormInput("Your Name");
		yourName.setX(0);
		yourName.setY(0);
		form.addChild(yourName);

		friendsName = buildFormInput("Friend's Name");
		friendsName.setX(180);
		friendsName.setY(0);
		form.addChild(friendsName);

		yourEmail = buildFormInput('Your Email');
		yourEmail.setX(0);
		yourEmail.setY(50);
		form.addChild(yourEmail);

		friendsEmail = buildFormInput("Friend's Email");
		friendsEmail.setX(180);
		friendsEmail.setY(50);
		form.addChild(friendsEmail);

		message = buildFormInput("Message");
		message.setX(0);
		message.setY(100);
		form.addChild(message);

		submit = new Button();
		submit.setText('Submit');
		submit.setY(185);
		submit.setFontSize(12);
		submit.setFontColor(LAYOUT_MODEL.overlayColor);
		submit.setBackgroundColor(LAYOUT_MODEL.thumbnailTitleFontColor);
		submit.setPaddingTop(2);
		submit.setPaddingRight(14);
		submit.setPaddingBottom(2);
		submit.setPaddingLeft(14);
		form.addChild(submit);
		submit.setX(form.getWidth() - submit.getWidth());
		submit.addEventListener(FOCUS, onFocusSubmit);
		submit.addEventListener(BLUR, onBlurSubmit);
		submit.addEventListener(MOUSE_OVER, onFocusSubmit);
		submit.addEventListener(MOUSE_OUT, onBlurSubmit);
		if(TOUCH_DEVICE) {
			submit.addEventListener(TOUCH_END, onSubmit);
		} else {
			submit.addEventListener(CLICK, onSubmit);
		}

		responseMessage.setAlpha(0);
		responseMessage.setX(form.getX());
		responseMessage.setY(40);
		responseMessage.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		responseMessage.setFontColor(LAYOUT_MODEL.thumbnailTitleFontColor);
		responseMessage.setFontSize(14);
		share.addChild(responseMessage);

		self.updateStyle();
		self.updatePosition();
		self.show();

		self.events.addEventListener(SHARE_FORM_SENT, shareResponse);

	}

	function previewLoaded(e) {
		preview.setWidth(RETINA ? this.getWidth() * 0.5 : this.getWidth());
		preview.setHeight(RETINA ? this.getHeight() * 0.5 : this.getHeight());
		Tween(this, UPDATE_SPEED, {alpha:1});
	}

	function buildFormInput(label) {
		var holder = new Sprite();
		holder.setWidth(168);
		var title = new TextField();
		title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		title.setFontColor(hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.7));
		title.setFontSize(11);
		title.setText(label);
		holder.addChild(title);
		var input = label === 'Message' ? new TextArea() : new Input();
		input.setBackgroundColor(hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.1));
		input.setBorder('1px solid ' + hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.3));
		input.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		input.setFontColor(LAYOUT_MODEL.thumbnailTitleFontColor);
		input.setFontSize(14);
		input.setHeight(label === 'Message' ? 55 : 20);
		input.setWidth(label === 'Message' ? (holder.getWidth() * 2) + 12 : 168);
		input.setY(17);

		input.addEventListener(FOCUS, onFocus);
		input.addEventListener(BLUR, onBlur);

		holder.addChild(input);
		holder.id = label;
		holder.input = input;
		return holder;
	}

	function onFocus(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.2), border:'1px solid ' + hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.7)});
	}

	function onBlur(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.1), border:'1px solid ' + hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.3)});
	}

	function onFocusSubmit(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.8)});
	}

	function onBlurSubmit(e) {
		Tween(this, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 1)});
	}

	function onSubmit(e) {

		var str = '&shareItem=' + encodeURIComponent(getThumbQuery(1600, 1200, mediaItem.content)) +
				'&toName=' + encodeURIComponent(friendsName.input.getValue()) +
				'&toEmail=' + encodeURIComponent(friendsEmail.input.getValue()) +
				'&fromName=' + encodeURIComponent(yourName.input.getValue()) +
				'&fromEmail=' + encodeURIComponent(yourEmail.input.getValue()) +
				'&message=' + encodeURIComponent(message.input.getValue());

		str = str.replace(/\r\n/g,"<br>").replace(/\n\r/g,"<br>").replace(/\r/g,"<br>").replace(/\n/g,"<br>");

		self.events.dispatchEvent(SHARE_FORM_SUBMIT, str);
	}

	function shareResponse(e) {
		responseMessage.setText(e.response);
		Tween(form, UPDATE_SPEED, {alpha:0});
		Tween(responseMessage, UPDATE_SPEED, {alpha:1});
		self.addEventListener(MOUSE_DOWN, closeOverlay);
	}

	function closeOverlay(e) {
		if(e.target === self) self.removeEventListener(MOUSE_DOWN, closeOverlay);
		self.events.dispatchEvent(NAVBAR_OVERLAY_BTN_CLICK, "email");
	}

	self.updateStyle = function(e) {
		if(LAYOUT_MODEL.thumbnailTitleFont !== title.getFontFamily()) {
			title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			yourName.title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			friendsName.title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			yourEmail.title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			friendsEmail.title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			message.title.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
			submit.setFontFamily(LAYOUT_MODEL.thumbnailTitleFont);
		}
		if(LAYOUT_MODEL.thumbnailTitleFontSize !== title.getFontSize()) {
			Tween(title, UPDATE_SPEED, {fontSize:LAYOUT_MODEL.thumbnailTitleFontSize, onComplete:function(){
				self.updatePosition(e);
			}});
		}
		if(LAYOUT_MODEL.thumbnailTitleFontColor !== title.getFontColor()) {
			Tween(title, UPDATE_SPEED, {fontColor:LAYOUT_MODEL.thumbnailTitleFontColor});
			Tween(line, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(yourName.title, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(friendsName.title, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(yourEmail.title, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(friendsEmail.title, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(message.title, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
			Tween(submit, UPDATE_SPEED, {fontColor:hexToRgba(LAYOUT_MODEL.overlayColor, 0.35)});
			Tween(submit, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.thumbnailTitleFontColor, 0.35)});
		}
		if(hexToRgba(LAYOUT_MODEL.overlayColor, LAYOUT_MODEL.overlayAlpha) !== self.getBackgroundColor()) {
			Tween(self, UPDATE_SPEED, {backgroundColor:hexToRgba(LAYOUT_MODEL.overlayColor, LAYOUT_MODEL.overlayAlpha)});
		}
	};

	self.updatePosition = function(e) {
		self.setX(0);
		self.setY(0);
		width = layoutCalcs.mediaView.width() > 573 ? 573 : layoutCalcs.mediaView.width();
		height = layoutCalcs.mediaView.height() > 225 ? 225 : layoutCalcs.mediaView.height();

		preview.setX(2);
		preview.setY(title.getHeight() + 20);
		line.setY(LAYOUT_MODEL.thumbnailTitleFontSize + 10);

		if(layoutCalcs.mediaView.width() > 573) {
			form.setX(222);
			form.setY(title.getHeight() + 20);
		} else {
			form.setX(2);
			form.setY(preview.getY() + preview.getHeight() + 10);
		}

		if(self.updateSpeed === 0) {
			self.setWidth(layoutCalcs.mediaView.width());
			self.setHeight(layoutCalcs.mediaView.height());
			share.setX(((layoutCalcs.mediaView.width() - width) * 0.5));
			share.setY(((layoutCalcs.mediaView.height() - height) * 0.5));
			share.setWidth(width);
			share.setHeight(height);
		} else {
			Tween(self, self.updateSpeed, {
				width:layoutCalcs.mediaView.width(),
				height:layoutCalcs.mediaView.height()
			});
			Tween(share, self.updateSpeed, {
				x:(layoutCalcs.mediaView.width() - width) * 0.5,
				y:(layoutCalcs.mediaView.height() - height) * 0.5,
				width:width,
				height:height
			});
		}
		closeHit.setWidth(layoutCalcs.mediaView.width());
		closeHit.setHeight(layoutCalcs.mediaView.height());
		self.updateSpeed = UPDATE_SPEED;
	};

	self.show = function() {
		self.updateSpeed = 0;
		self.updatePosition();
		if(!self.visible) {
			self.visible = true;
			self.setDisplay('block');
			Tween(self, UPDATE_SPEED, {alpha:1});
			Tween(form, UPDATE_SPEED, {alpha:1});
			Tween(responseMessage, UPDATE_SPEED, {alpha:0});
		}
	};

	self.hide = function(e) {
		if(self.visible) {
			self.visible = false;
			Tween(self, UPDATE_SPEED, {alpha:0, onComplete:function(){
				this.setDisplay('none');
			}});
		}
	};

	self.updateContent = function(e) {
		var mediaItems = csvToArray(e.section.media);
		mediaItem = getMediaById(mediaItems[e.assetId]);
		if(preview) {
			share.removeChild(preview);
		}
		preview = new Bitmap();
		preview.setAlpha(0);
		preview.setX(2);
		preview.setY(title.getHeight() + 20);
		preview.setSrc(getThumbQuery(200, 200, mediaItem.content));
		form.setY(title.getHeight() + 20);
		share.addChild(preview);
		preview.addEventListener(LOAD, previewLoaded);
	};

	self.addEventListener(CHILD_ADDED, function(child) {
		init.call(self);
	});

	return self;
}

function MenuController(vars) {

	var self = new ControllerProxy({parentView:vars.parentView, parentController:vars.parentController, events:vars.siteControllerEvents}),
		isInit = true,
		sectionsModelChangeTimer;
	self.updateSpeed = 0;

	self.menuView = new vars.MenuView(vars);
	self.menuView.setAlpha(0);
	self.parentView.addChild(self.menuView);

	self.menuView.menuText = new MenuText(vars);
	self.menuView.menuText.element.setAttribute('class', 'menuText');
	self.menuView.addContent(self.menuView.menuText);

	self.events.addEventListener(LAYOUT_MODEL_CHANGE, layoutModelChange);
	self.events.addEventListener(MEDIA_MODEL_CHANGE, mediaModelChange);
	self.events.addEventListener(SECTIONS_MODEL_CHANGE, sectionsModelChangeCheck);
	self.events.addEventListener(ORIENTATION_CHANGE, function(e) {
		self.menuView.menuText.rebuild(e);
	});
	self.events.addEventListener(RESIZE_END, self.menuView.menuText.updatePosition);
	self.events.addEventListener(RESIZE_END, self.menuView.updatePosition);
	self.events.addEventListener(LANDING_INIT, landingInit);
	self.events.addEventListener(SITE_URI_CHANGE, menuSiteUriChange);
	self.events.addEventListener(MENU_BUILT, function(){
		sectionsModelChangeTimer = setTimeout(function(){
			self.menuView.menuText.updatePosition();
			self.menuView.updatePosition();
		}, 0);
	});
	self.events.addEventListener(MENU_SIZE_CHANGE, self.menuView.updatePosition);

	function introComplete(e) {
		self.events.removeEventListener(INTRO_COMPLETE, introComplete);
		if(USER_AGENT === PAD) {
			setTimeout(self.menuView.closeDock, 2000);
		}
	}

	function landingInit() {
		self.events.removeEventListener(LANDING_INIT, landingInit);
		if(USER_AGENT === PAD || USER_AGENT === POD) {
			setTimeout(function() {
				self.menuView.closeDock();
			}, 2000);
		}
	}

	function menuSiteUriChange(e) {
		if(USER_AGENT === POD) self.events.addEventListener(MENU_CLICK, self.menuView.closeDock);
		if(USER_AGENT === POD || USER_AGENT === PAD) {
			if(LAYOUT_MODEL.introFile !== '' && !e.path) {
				self.events.addEventListener(INTRO_COMPLETE, introComplete);
			} else {
				setTimeout(self.menuView.closeDock, 2000);
			}
		}
		self.menuView.menuText.setActiveMenuItem(e);
	}

	function sectionsModelChangeCheck(e) {
		if(sectionsModelChangeTimer) {
			clearTimeout(sectionsModelChangeTimer);
		}
		sectionsModelChangeTimer = setTimeout(function(){
			if(e.action && e.action === 'sectionReorder') {
				/*dont rebuild menu*/
			} else if(e.action && e.action === "remove") {
				/*dont rebuild menu*/
			} else if(e.action && e.action === "addMediaToSection") {
				/*dont rebuild menu*/
			} else if(e.action && e.action === "addSelectionToSection") {
				/*dont rebuild menu*/
			} else {
				self.menuView.menuText.rebuild(e);
			}
		}, 0);
	}

	function layoutModelChange(e) {
		if(isInit || e && /menu|sitePaddingTop|sitePaddingRight|sitePaddingBottom|sitePaddingLeft/.test(e.id)) {
			self.menuView.updateStyle();
			self.menuView.menuText.rebuild();
			self.menuView.menuText.updatePosition();
			self.menuView.updatePosition();
			isInit = false;
		}
	}

	function mediaModelChange(e) {
		if(e.type === 'link' && e.filters !== 'Footer Links') {
			self.menuView.updateStyle();
			self.menuView.menuText.rebuild();
			self.menuView.menuText.updatePosition();
			self.menuView.updatePosition();
		}
	}

	return self;
}

function MenuView(vars) {

	var self = new ViewProxy({events:vars.events});
	self.setAlpha(0);
	self.updateSpeed = 0;
	self.setZIndex(3);
	self.setPointerEvents('none');
	/*self.overflow = 'hidden';*/

	var mask = new Sprite();
	mask.element.setAttribute('class', 'mask');
	mask.setBackgroundColor(hexToRgba('#FF0000', 0));
	mask.setWidth(layoutCalcs.menuView.width());
	mask.setOverflow('hidden');
	self.addChild(mask);

	self.mask = mask;

	var menuToggleBtn,
		menuToggleToggleState = false,
		menuButtonCheck;

	var dummyMask = {
		getWidth:function() {return stage.getWidth()},
		getHeight:function() {return stage.getHeight()}
	};

	self.addEventListener(CHILD_ADDED, function(child) {

	});

	self.dock = new Dock(self, dummyMask, {
		side:LAYOUT_MODEL.menuAlignHorizontal,
		align:LAYOUT_MODEL.menuAlignVertical,
		margin:layoutCalcs.menuView.x(),
		pOffset:layoutCalcs.menuView.y(),
		reveal:0
	});

	if(USER_AGENT === PAD || USER_AGENT === POD) {
		menuToggleBtn = new Bitmap();
		menuToggleBtn.setZIndex(50);
		menuToggleBtn.setWidth(20);
		menuToggleBtn.setHeight(16);
		menuToggleBtn.setPointerEvents('auto');
		if(RETINA) {
			menuToggleBtn.setSrc(REWRITE_BASE + ICONS + 'menu@2x.png?cache=2345');
		} else {
			menuToggleBtn.setSrc(REWRITE_BASE + ICONS + 'menu.png?cache=1235');
		}
		menuToggleBtn.setY(16);
		self.addChild(menuToggleBtn);
		menuToggleBtn.addEventListener(CLICK, toggleDock);

		self.events.addEventListener(TOUCH_NAVIGATION_END, checkMenuToggle);
		self.events.addEventListener(INTRO_COMPLETE, closeDockIntroCompete);
	}

	function closeDockIntroCompete() {
		setTimer(self.closeDock, 2000);
	}

	/*stage.addEventListener(MOUSE_MOVE, mouseOverDock);*/

	function checkMenuToggle(e) {
		if(!e.isMove && !self.dock.toggleState()) {
			toggleMenuToggleBtn();
		} else {
			self.closeDock();
		}
	}

	self.openDock = function() {
		self.events.dispatchEvent(MENU_DOCK_OPEN);
		self.dock.show();
		hideMenuToggleBtn();
		clearInterval(menuButtonCheck);
	};

	self.closeDock = function() {
		self.events.dispatchEvent(MENU_DOCK_CLOSE);
		self.dock.hide();
		menuIconCheckInterval();
	};

	function menuIconCheckInterval() {
		clearInterval(menuButtonCheck);
		menuButtonCheck = setInterval(function(){
			if(!self.dock.toggleState()) {
				showMenuToggleBtn();
			}
		}, 1000);
	}

	function toggleDock() {
		if(self.dock.toggleState()) {
			self.closeDock();
		} else {
			self.openDock();
		}
	}

	function toggleMenuToggleBtn() {
		if(menuToggleToggleState) {
			hideMenuToggleBtn();
		} else {
			showMenuToggleBtn();
		}
	}

	function showMenuToggleBtn() {
		menuToggleToggleState = true;
		Tween(menuToggleBtn, UPDATE_SPEED, {alpha:1});
	}

	function hideMenuToggleBtn() {
		menuToggleToggleState = false;
		Tween(menuToggleBtn, UPDATE_SPEED, {alpha:0});
	}

	function mouseOverDock(e) {
		if(self.hitTestPoint(e.clientX, e.clientY) && !self.dock.toggleState()) {
			self.openDock();
		} else if(!self.hitTestPoint(e.clientX, e.clientY) && self.dock.toggleState()) {
			self.closeDock();
		}
	}

	self.updateStyle = function(e) {
		if(USER_AGENT === POD && LAYOUT_MODEL.menuBgAlpha < 0.65) {
			Tween(self, self.updateSpeed, {backgroundColor: hexToRgba(LAYOUT_MODEL.menuBgColor, 0.65)});
		} else if(USER_AGENT === PAD && LAYOUT_MODEL.menuBgAlpha < 0.65) {
			Tween(self, self.updateSpeed, {backgroundColor: hexToRgba(LAYOUT_MODEL.menuBgColor, 0.65)});
		} else {
			Tween(self, self.updateSpeed, {backgroundColor: hexToRgba(LAYOUT_MODEL.menuBgColor, LAYOUT_MODEL.menuBgAlpha)});
		}
	};

	self.updatePosition = function(e) {

		var maskHeight = layoutCalcs.menuView.height() - layoutCalcs.menuText.y(self.menuText.getHeight()),
			maskY = layoutCalcs.menuText.y(self.menuText.getHeight());

		if(USER_AGENT === POD) {
			if(!layoutCalcs._footerHeight) {
				maskHeight = layoutCalcs.menuView.height() - layoutCalcs.menuText.y(self.menuText.getHeight());
			} else {
				maskHeight = layoutCalcs.menuView.height() - layoutCalcs.menuText.y(self.menuText.getHeight()) - layoutCalcs._footerHeight - (layoutCalcs.mobileFooterPadding * 2);
			}
		} else if(maskHeight > layoutCalcs.menuView.height()) {
			maskHeight = layoutCalcs.menuView.height();
			maskY = 0;
		}

		if(USER_AGENT === PAD || USER_AGENT === POD) {
			if(LAYOUT_MODEL.menuAlignHorizontal === 'right') {
				menuToggleBtn.setX(- 16 - menuToggleBtn.getWidth());
			} else {
				menuToggleBtn.setX(layoutCalcs.menuView.width() + 16);
			}
			menuToggleBtn.setY(16);
		}

		if(self.updateSpeed === 0) {
			self.setWidth(layoutCalcs.menuView.width());
			self.setHeight(layoutCalcs.menuView.height());

			mask.setWidth(layoutCalcs.menuView.width());
			mask.setHeight(maskHeight);
			mask.setY(maskY);
		} else {
			Tween(self, self.updateSpeed, {width:layoutCalcs.menuView.width()});
			Tween(self, self.updateSpeed, {height:layoutCalcs.menuView.height()});

			Tween(mask, self.updateSpeed, {width:layoutCalcs.menuView.width(), height:maskHeight, y:maskY});
		}

		if(self.getAlpha() === 0) {
			Tween(self, UPDATE_SPEED, {alpha:1});
		}

		self.updateSpeed = UPDATE_SPEED;

		self.dock.side = LAYOUT_MODEL.menuAlignHorizontal;
		self.dock.align = LAYOUT_MODEL.menuAlignVertical;
		self.dock.margin = layoutCalcs.menuView.x();
		self.dock.pOffset = layoutCalcs.menuView.y();
		self.dock.resize();

		setTimeout(function() {
			if(self.scroll) {
				self.scroll.resize();
			}
		}, 3000);
	};

	self.addContent = function(value) {
		self.mask.addChild(value);
		setTimeout(function() {
			createScrollbar();
		}, 2000);
	};

	function createScrollbar() {
		var scrollBarWidth = 8;
		var scrollBarOffsetX = LAYOUT_MODEL.menuScrollbarAlign === 'right' ? -scrollBarWidth : -percentToPixels(LAYOUT_MODEL.menuWidth, stage.getWidth());
		var types = USER_AGENT === POD ? ['wheel','touch'] : ['bar','wheel','touch'];
		self.scroll = new Scroll(self.menuText, mask, {
			types:types,
			axis:'y',
			align:'top',
			color:LAYOUT_MODEL.menuScrollbarColor,
			hover:LAYOUT_MODEL.menuScrollbarHover,
			side:LAYOUT_MODEL.menuScrollbarAlignment,
			width:scrollBarWidth,
			topPad:0,
			offsetX:scrollBarOffsetX,
			zIndex:500
		});
	}

	self.updateStyle();

	return self;
}

function MenuText(vars) {

	var self = new ViewProxy({events:vars.events});
	self.setZIndex(5);
	self.setPointerEvents('auto');

	var rootSections = csvToArray(SECTIONS_MODEL.ROOT_SECTION.media),
		subMenus = [];

	var menuBtns = {};
	var selectedBtn;
	var oldBtn;
	var menuAccordion;
	var path;
	var initTime = 700;

	self.updateSpeed = 0;

	function init() {
		self.setText('');
		self.children = [];
		menuAccordion = new Accordion(vars);
		menuAccordion.setAlpha(0);
		menuAccordion.gap = USER_AGENT === POD ? 0 : LAYOUT_MODEL.menuTextGap;
		if(USER_AGENT === POD || LAYOUT_MODEL.menuExpandAll) {
			menuAccordion.expand = 'all';
		} else {
			menuAccordion.expand = 'single';
		}
		menuAccordion.toggle = false;
		menuAccordion.wrap = false;
		menuAccordion.align = LAYOUT_MODEL.menuTextAlignHorizontal;
		menuAccordion.updateSpeed = self.updateSpeed;
		menuAccordion.animate = false;
		buildMenu(rootSections, false, menuAccordion, false);
		self.addChild(menuAccordion);
		self.setWidth(menuAccordion.getWidth());
		self.setHeight(menuAccordion.getHeight());
		menuAccordion.addEventListener(ACCORDION_SIZE_CHANGE, updateMenuSize);

		updateMenuSize();
		menuAccordion.layout();
		if(selectedBtn) {
			self.selectMenuItem();
		}

		setTimeout(function() {
			initLayout();
			alphaIn();
			self.events.dispatchEvent(MENU_BUILT);
			initTime = 0;
		}, initTime);
	}

	function initLayout() {
		layoutSubMenus();
		menuAccordion.layout();
	}

	function alphaIn() {
		setTimeout(function() {
			Tween(menuAccordion, 0.2, {alpha:1});
		}, 350);
	}

	function updateMenuSize(e) {
		if(e) {
			self.setWidth(e.width);
			self.setHeight(e.height);
		}
		self.events.dispatchEvent(MENU_SIZE_CHANGE);
	}

	self.updatePosition = function(e) {
		if(self.updateSpeed === 0) {
			self.setX(Mth.round(layoutCalcs.menuText.x(self.getWidth())));
		} else {
			Tween(self, self.updateSpeed, {x:Mth.round(layoutCalcs.menuText.x(self.getWidth()))});
		}
	};

	self.rebuild = function(e) {
		rootSections = csvToArray(SECTIONS_MODEL.ROOT_SECTION.media);
		self.updateSpeed = 0;
		init();
		menuAccordion.updateSpeed = 0;
		self.selectMenuItem();
	};

	self.setActiveMenuItem = function(e) {
		path = e.path;
		if(menuAccordion) menuAccordion.updateSpeed = UPDATE_SPEED;
		self.selectMenuItem();
	};

	self.selectMenuItem = function() {
		oldBtn = selectedBtn;
		if(path && (menuBtns[path] || menuBtns[removeDashes(path)])) {
			if(menuBtns[path]) {
				selectedBtn = menuBtns[path].label;
			} else {
				selectedBtn = menuBtns[removeDashes(path)].label;
			}
			selected.call(selectedBtn);
			selectedBtn.parentAccordion = selectedBtn.parent.parent.parent;
			if(selectedBtn.parentAccordion) {
				menuAccordion.select.call(selectedBtn.parentAccordion.label);

			}
		} else {
			selectedBtn = undefined;
		}
		if(oldBtn) {
			out.call(oldBtn);
			if(!selectedBtn && oldBtn.parentAccordion) {
				menuAccordion.select(oldBtn.parentAccordion.label);
			}
		}
	};

	function buildMenu(sections, isSub, parent, parentLabel) {

		parent.parentLabel = parentLabel;

		var i = 0,
			length = sections.length;

		for(;i < length; i++) {

			var label,
				content = undefined,
				container = new Sprite(),
				section = getSectionById(Number(sections[i]));

			if(isValidSection(section)) {

				label = buildButton(section, isSub, parentLabel);
				label.thumbs = section.thumb;
				if(USER_AGENT !== POD) label.bullet = buildBullet(isSub);

				if(isSub) {
					menuBtns[label.path] = container;
					parent.addChild(container);
				} else {
					menuBtns[label.path] = container;
					if(hasSubMenu(section)) {
						content = buildSubMenu(section, label);
						if(content) {
							container.parentAccordion = parent;
							container.parentAccordion.label = label;
							container.addChild(content);
						}
					}
					parent.addItem(container, label, content, false);
				}

				mouseEvents(label);
				container.label = label;
				container.addChild(label);
				if(USER_AGENT !== POD) container.addChild(label.bullet);
			}
		}

		return parent;
	}

	function buildSubMenu(section, label) {
		var content = buildMenu(csvToArray(section.media), true, buildSubMenuHolder(), label);
		var firstSub = getFirstValidSub(section);
		var firstMO = getMediaById(csvToArray(firstSub.media)[0]);
		if(firstMO.type === "link") {
			label.path = "";
			subMenus.push(content);
			return content;
		} else if(section.label === '%SPACER%') {
			label.path = false;
			label.thumbs = false;
			return content;
		} else if(!firstSub.label) {
			return false;
		} else if(label && label.label && content.children[0]) {
			label.path = label.label.getText() + '/' + firstSub.label;
			label.section = SECTIONS_MODEL[label.path];
			label.thumbs = firstSub.thumb;
			subMenus.push(content);
			return content;
		}
		return false;
	}

	function isValidSection(section) {
		if(USER_AGENT === POD && section.label === '%SPACER%') {
			return false;
		}
		return section.visible === 1;
	}

	function buildButton(section, isSub, parentLabel) {
		var label = buildLabel(section.label, isSub);

		var mediaItem = getMediaById(getFirstMediaId(section.media));

		if(mediaItem.type === 'link' && isSocialIcon(mediaItem)) {
			label = loadIcon(mediaItem);
		}

		if(USER_AGENT === POD) {
			label = buildPodLabel(label, isSub);
		}

		label.section = section;

		if(mediaItem && mediaItem.type === 'link') {
			label.type = 'external';
			label.path = mediaItem.content;
			label.linkTarget = mediaItem.linkTarget;
		} else {
			var path = isSub && parentLabel && parentLabel.label ? parentLabel.label.getText() + '/' + label.label.getText() : label.label.getText();
			label.type = 'internal';
			label.path = path;
			label.linkTarget = '_self';
		}

		return label;
	}

	function isSocialIcon(mediaItem) {
		return socialIcons[mediaItem.label.toLowerCase()];
	}

	function loadIcon(mediaItem) {
		var socialIcon = socialIcons[mediaItem.label.toLowerCase()];
		var icon = new Bitmap();
		icon.setSrc(REWRITE_BASE + 'dx/icon/' + socialIcon.filename);
		icon.setWidth(socialIcon.width);
		icon.setHeight(socialIcon.height);
		icon.style.padding = '2px 0';
		icon.socialLabel = mediaItem.label.toLowerCase();
		return icon;
	}

	function buildLabel(name, isSub) {
		var label = new Sprite();
		label.setFontFamily(LAYOUT_MODEL.menuFont);
		label.setFontSize(isSub ? LAYOUT_MODEL.menuSubFontSize : LAYOUT_MODEL.menuFontSize);
		label.setFontColor(LAYOUT_MODEL.menuFontColor);
		label.setTextWrap(false);
		label.setText(name === '%SPACER%' ? "&nbsp;" : name);
		label.label = label;
		return label;
	}

	function buildPodLabel(label, isSub) {
		var bg = new Sprite();
		bg.addChild(label);
		bg.label = label;
		bg.setWidth(layoutCalcs.menuView.width());
		bg.setHeight(60);
		bg.setBackgroundColor(hexToRgba(LAYOUT_MODEL.menuBgColor, LAYOUT_MODEL.menuBgAlpha));
		bg.setBorderBottom('1px solid ' + hexToRgba(LAYOUT_MODEL.menuFontColor, LAYOUT_MODEL.menuBgAlpha + 0.15));
		layoutCalcs.menuBullet.updateSize(isSub);
		label.setX(20);
		label.setY((bg.getHeight() - layoutCalcs.menuBullet.height) * 0.5);
		return bg;
	}

	function buildBullet(isSub) {
		var bullet = new Sprite();
		layoutCalcs.menuBullet.updateSize(isSub);
		bullet.setFontFamily(LAYOUT_MODEL.menuFont);
		bullet.setFontColor(LAYOUT_MODEL.menuFontColor);
		bullet.setFontSize(isSub ? LAYOUT_MODEL.menuSubFontSize : LAYOUT_MODEL.menuFontSize);
		bullet.setWidth(layoutCalcs.menuBullet.width);
		bullet.setText(menuBullet[LAYOUT_MODEL.menuBulletType]);
		if(isSub) {
			bullet.setX(Mth.round(-(bullet.getWidth() + 15) + LAYOUT_MODEL.menuSubBulletOffsetHorizontal));
			bullet.setY(0 + LAYOUT_MODEL.menuSubBulletOffsetVertical);
		} else {
			bullet.setX(Mth.round(-(bullet.getWidth() + 15) + LAYOUT_MODEL.menuBulletOffsetHorizontal));
			bullet.setY(0 + LAYOUT_MODEL.menuBulletOffsetVertical);
		}
		bullet.setAlpha(0);
		return bullet;
	}

	function layoutSubMenus() {
		var i = 0,
			length = subMenus.length;
		for(;i < length; i++) {

			var subMenu = subMenus[i];
			jLength = subMenu.children.length;

			for(j = 0; j < jLength; j++) {
				subMenu.tile.perpLength = subMenu.children[j].label.getWidth() > subMenu.tile.perpLength ? subMenu.children[j].label.getWidth() : subMenu.tile.perpLength;
				subMenu.tile.addItem(subMenu.children[j].label.getWidth(), subMenu.children[j].label.getHeight());
			}

			subMenu.tile.layoutItems();

			for(var k = 0; k < jLength; k++) {
				var position = subMenu.tile.getPosition(k);
				switch(LAYOUT_MODEL.menuTextAlignHorizontal) {
					case 'left':
						subMenu.children[k].setX(Mth.round(position.x + LAYOUT_MODEL.menuSubIndent));
						break;
					case 'center':
						subMenu.children[k].setX(Mth.round(position.x));
						break;
					case 'right':
						subMenu.children[k].setX(Mth.round(position.x));
						break;
				}
				if(USER_AGENT === POD) {
					subMenu.children[k].setX(position.x + 10);
					subMenu.children[k].setY(position.y);
				} else {
					subMenu.children[k].setY(position.y + LAYOUT_MODEL.menuSubGapLeading);
				}
			}

			var bounds = subMenu.tile.getBounds();
			subMenu.setWidth(subMenu.tile.perpLength + LAYOUT_MODEL.menuSubIndent);
			if(USER_AGENT === POD) {
				subMenu.setHeight(bounds.height);
			} else {
				subMenu.setHeight(bounds.height + LAYOUT_MODEL.menuSubGapLeading + LAYOUT_MODEL.menuSubGapTrailing);
			}

			switch(LAYOUT_MODEL.menuTextAlignHorizontal) {
				case 'left':
					subMenu.setX(0);
					break;
				case 'center':
					subMenu.setX(Mth.round((subMenu.parentLabel.getWidth() - subMenu.tile.perpLength) * 0.5));
					break;
				case 'right':
					subMenu.setX(Mth.round(subMenu.parentLabel.getWidth() - subMenu.tile.perpLength - LAYOUT_MODEL.menuSubIndent));
					break;
			}

			subMenu.setY(subMenu.parentLabel.getHeight());
		}
	}

	function mouseEvents(hit) {
		hit.addEventListener(MOUSE_OVER, over);
		hit.addEventListener(MOUSE_OUT, out);
		hit.addEventListener(CLICK, click);
	}

	function click(e) {
		var mode = USER_AGENT !== POD && this.thumbs.defaultOn ? 'thumbs' : '';
		menuAccordion.animate = true;
		var navEvent = {
			section:this.section,
			type:this.type,
			path:this.path,
			label:this.label || this.socialLabel || '',
			linkTarget:this.linkTarget,
			mode:mode
		};
		if(this.path !== "" || this.socialLabel) self.events.dispatchEvent(MENU_CLICK, navEvent);
	}

	function over(e) {
		if(this.getSrc()) {
			Tween(this, UPDATE_SPEED, {alpha:0.65});
		} else {
			if(this !== selectedBtn) {
				this.label.setTransition(0);
				Tween(this.label, UPDATE_SPEED, {fontColor:LAYOUT_MODEL.menuFontColorHover});
				if(this.bullet) {
					this.bullet.setTransition(0);
					var bulletX = -(this.bullet.getWidth() + 4);
					this.bullet.setFontColor(LAYOUT_MODEL.menuFontColorHover);
					Tween(this.bullet, UPDATE_SPEED, {alpha:1, x:bulletX + LAYOUT_MODEL.menuBulletOffsetHorizontal});
				}
			}
		}
	}

	function out(e) {
		if(this.getSrc()) {
			Tween(this, UPDATE_SPEED, {alpha:1});
		} else {
			if(this !== selectedBtn) {
				Tween(this.label, UPDATE_SPEED, {fontColor:LAYOUT_MODEL.menuFontColor});
				if(this.bullet) {
					var bulletX = -(this.bullet.getWidth() + 15);
					Tween(this.bullet, UPDATE_SPEED, {alpha:0, x:bulletX + LAYOUT_MODEL.menuBulletOffsetHorizontal, fontColor:LAYOUT_MODEL.menuFontColor});
				}
			}
		}
	}

	function selected(e) {
		this.label.setTransition(0);
		this.label.setFontColor(LAYOUT_MODEL.menuFontColorSelected);
		if(this.bullet) {
			this.bullet.setTransition(0);
			this.bullet.setFontColor(LAYOUT_MODEL.menuFontColorSelected);
			Tween(this.bullet, UPDATE_SPEED, {alpha:1, x:Mth.round(-(this.bullet.getWidth() + 4) + LAYOUT_MODEL.menuBulletOffsetHorizontal)});
		}
	}

	function getFirstMediaId(media) {
		return csvToArray(media)[0];
	}

	function hasSubMenu(section) {
		var media = csvToArray(section.media);
		var i; for(i in media) {
			if(isSection(media[i])) {
				return true;
			}
		}
		return false;
	}

	function buildSubMenuHolder() {
		var content = new Sprite();
		content.tile = new Tile();
		content.tile.gap = USER_AGENT === POD ? 0 : percentToPixels(LAYOUT_MODEL.menuSubTextGap, 100);
		content.tile.axis = 'y';
		content.tile.align = LAYOUT_MODEL.menuTextAlignHorizontal;
		content.tile.wrap = false;
		content.tile.perpLength = LAYOUT_MODEL.menuWidth;
		return content;
	}

	self.addEventListener(CHILD_ADDED, function(child) {

	});

	return self;
}

var menuBullet = {
	'':'',
	'none':'',
	'arrow':'&#x203A',
	'arrow2':'&#x00BB',
	'bullet':'&#x2022',
	'bullet2':'&#x00B7',
	'dash':'-'
};

function NavbarController(vars) {

	var self = new ControllerProxy({parentView:vars.parentView, parentController:vars.parentController, events:vars.siteControllerEvents});

	self.updateSpeed = 0;
	self.navbarView = new vars.NavbarView(vars);
	self.navbarView.setAlpha(0);
	self.parentView.addChild(self.navbarView);
	var initTime = 1000;

	self.events.addEventListener(NAVBAR_MODEL_CHANGE, self.navbarView.buildNavBtns);
	self.events.addEventListener(LAYOUT_MODEL_CHANGE, self.navbarView.updateStyle);
	self.events.addEventListener(SITE_URI_CHANGE, updateNavbarInfo);
	self.events.addEventListener(SECTIONS_MODEL_CHANGE, updateNavbarInfo);
	self.events.addEventListener(RESIZE_END, self.navbarView.updatePosition);

	function updateNavbarInfo(e) {
		setTimeout(function(){
			if(e.section) {
				self.navbarView.updateCount(e);
				self.navbarView.updateSpeed = UPDATE_SPEED;
			}
			initTime = 0;
		}, initTime);
	}

	return self;

}

function NavbarView(vars) {

	var self = new ViewProxy({events:vars.events}),
		tile;
	self.setAlpha(0);
	self.setZIndex(3);
	self.updateSpeed = 0;
	var childAdded = false,
		assetId = 0,
		sectionLength = 0,
		count;

	self.addEventListener(CHILD_ADDED, function(child) {
		childAdded = true;
	});

	self.updateStyle = function(e) {
		if(childAdded) {
			self.buildNavBtns();
		}
	};

	self.updatePosition = function(e) {
		if(self.updateSpeed === 0) {
			self.setX(layoutCalcs.navbarView.x(self.getWidth()));
			self.setY(layoutCalcs.navbarView.y(self.getHeight()));
		} else {
			Tween(self, self.updateSpeed, {x:layoutCalcs.navbarView.x(self.getWidth())});
			Tween(self, self.updateSpeed, {y:layoutCalcs.navbarView.y(self.getHeight())});
		}

	};

	self.hide = function() {
		Tween(self, UPDATE_SPEED, {alpha:0,onComplete:function(){
			self.setDisplay('none');
		}});
	};

	self.show = function() {
		self.setDisplay('block');;
		Tween(self, UPDATE_SPEED, {alpha:1});
	};

	self.updateCount = function(e) {
		sectionLength = e.section.media ? csvToArray(e.section.media).length : csvToArray(e.section).length;
		if(e.section.id === LANDING_MEDIA && childAdded) {
			self.hide();
		} else if(isMediaItem(e.section.media) && childAdded) {
			self.hide();
		} else if(e.assetId > -1 && childAdded) {
			assetId = e.assetId;
			if(sectionLength > 1) {
				self.show();
				self.buildNavBtns();
			} else {
				self.hide();
			}
		} else {
			if(sectionLength > 1) {
				self.show();
				self.buildNavBtns();
			} else {
				self.hide();
			}
		}
	};

	self.buildNavBtns = function() {
		/*self.setText('');
		self.children = [];*/
		self.parent.removeChildren(self);
		for(var i in NAVBAR_MODEL) {
			if(NAVBAR_MODEL.hasOwnProperty(i)) {

				if(NAVBAR_MODEL[i].type === 'fullscreen' && !isFullscreenCapable()) continue;
				var btn = new Sprite();
				self.addChild(btn);

				if(NAVBAR_MODEL[i].iconType === 'label' || NAVBAR_MODEL[i].type === 'count') {
					btn.label = buildLabel(NAVBAR_MODEL[i].name);
					btn.addChild(btn.label);
					btn.setWidth(btn.label.getWidth());
					btn.setHeight(LAYOUT_MODEL.navbarFontSize);
					btn.label.setX(0);
					btn.label.setY((LAYOUT_MODEL.navbarFontSize - btn.label.getHeight()) * 0.5);
				} else if(NAVBAR_MODEL[i].iconType === 'icon') {
					btn.icon = makeIcon(NAVBAR_MODEL[i].type);
					btn.addChild(btn.icon);
					btn.setWidth(btn.icon.getWidth());
					btn.setHeight(LAYOUT_MODEL.navbarFontSize);
					btn.icon.setX(0);
					btn.icon.setY((LAYOUT_MODEL.navbarFontSize - btn.icon.getHeight()) * 0.5);
				} else if(NAVBAR_MODEL[i].iconType === 'both') {
					btn.icon = makeIcon(NAVBAR_MODEL[i].type);
					btn.addChild(btn.icon);
					btn.label = buildLabel(NAVBAR_MODEL[i].name);
					btn.addChild(btn.label);

					if(NAVBAR_MODEL[i].type === 'next') {
						btn.label.setX(0);
						btn.label.setY((LAYOUT_MODEL.navbarFontSize - btn.label.getHeight()) * 0.5);
						btn.icon.setX(btn.label.getWidth() - LAYOUT_MODEL.navbarIconOffsetX + 3);
						btn.icon.setY(((LAYOUT_MODEL.navbarFontSize - btn.icon.getHeight()) * 0.5) + LAYOUT_MODEL.navbarIconOffsetY);
						btn.setWidth(btn.icon.getWidth() + btn.label.getWidth() - LAYOUT_MODEL.navbarIconOffsetX);
					} else {
						btn.icon.setX(0);
						btn.icon.setY(((LAYOUT_MODEL.navbarFontSize - btn.icon.getHeight()) * 0.5) + LAYOUT_MODEL.navbarIconOffsetY);
						btn.label.setX(btn.icon.getWidth() - LAYOUT_MODEL.navbarIconOffsetX + 3);
						btn.label.setY((LAYOUT_MODEL.navbarFontSize - btn.label.getHeight()) * 0.5);
						btn.setWidth(btn.icon.getWidth() + btn.label.getWidth() - LAYOUT_MODEL.navbarIconOffsetX + 3);
					}

					btn.setHeight(LAYOUT_MODEL.navbarFontSize);
				}

				var hit = new Sprite();
				hit.setWidth(btn.getWidth());
				hit.setHeight(btn.getHeight());
				hit.type = NAVBAR_MODEL[i].type;
				hit.tooltip = NAVBAR_MODEL[i].tooltip;
				if(btn.label) {
					hit.label = btn.label;
				}
				if(btn.icon) {
					hit.icon = btn.icon;
				}
				btn.addChild(hit);
				btn.hit = hit;

				if(NAVBAR_MODEL[i].type === 'count') {
					count = btn;
					count.label.setText(assetId + 1 + '&nbsp;of&nbsp;' + sectionLength);
					count.setWidth(count.label.getWidth());
					hit.setWidth(count.label.getWidth());
				} else {
					hit.addEventListener(MOUSE_OVER, over);
					hit.addEventListener(MOUSE_OUT, out);
					hit.addEventListener(CLICK, click);
				}
			}

			layoutBtns();
			self.updatePosition();
		}
	};

	function click(e) {
		if(this.type === 'fullscreen') {
			self.events.dispatchEvent(NAVBAR_FULLSCREEN);
		} else if(this.type === 'prev' || this.type === 'next') {
			self.events.dispatchEvent(NAVBAR_NAV_CLICK, this.type);
		} else {
			self.events.dispatchEvent(NAVBAR_OVERLAY_BTN_CLICK, this.type);
		}
	}

	function over(e) {
		if(this.label) {
			Tween(this.label, UPDATE_SPEED, {fontColor:LAYOUT_MODEL.navbarColorHover});
		}
		if(this.icon && this.icon.path && this.icon.path.getStroke() !== 'none') {
			Tween(this.icon.path, UPDATE_SPEED, {stroke:LAYOUT_MODEL.navbarColorHover});
		}
		if(this.icon && this.icon.path && this.icon.path.getFill() !== 'none') {
			Tween(this.icon.path, UPDATE_SPEED, {fill:LAYOUT_MODEL.navbarColorHover});
		}
		if(USER_AGENT !== POD && USER_AGENT !== PAD) vars.parentController.tooltip.show(e);
	}

	function out(e) {
		if(this.label) {
			Tween(this.label, UPDATE_SPEED, {fontColor:LAYOUT_MODEL.navbarColor});
		}
		if(this.icon && this.icon.path && this.icon.path.getStroke() !== 'none') {
			Tween(this.icon.path, UPDATE_SPEED, {stroke:LAYOUT_MODEL.navbarColor});
		}
		if(this.icon && this.icon.path && this.icon.path.getFill() !== 'none') {
			Tween(this.icon.path, UPDATE_SPEED, {fill:LAYOUT_MODEL.navbarColor});
		}
	}

	function buildLabel(name) {
		var label = new Button();
		label.setFontFamily(LAYOUT_MODEL.navbarFont);
		label.setFontSize(LAYOUT_MODEL.navbarFontSize);
		label.setFontColor(LAYOUT_MODEL.navbarColor);
		label.setBackgroundColor('transparent');
		label.setTextWrap(false);
		label.setText(name);
		label.setSelectable(false);
		return label;
	}

	function makeIcon(type) {
		var icon = new Svg();
		icon.setX(0);
		icon.setY(0);
		icon.setRotate(0);
		icon.path = new Path();
		switch(type) {
			case 'prev':
				icon.setWidth(8);
				icon.setHeight(15);
				icon.path.setD(svgPaths.navArrowLeft);
				icon.path.setStrokeWidth(3);
				icon.path.setFill('none');
				icon.path.setStroke(LAYOUT_MODEL.navbarColor);
				break;
			case 'next':
				icon.setWidth(8);
				icon.setHeight(15);
				icon.path.setD(svgPaths.navArrowRight);
				icon.path.setStrokeWidth(3);
				icon.path.setFill('none');
				icon.path.setStroke(LAYOUT_MODEL.navbarColor);
				break;
			case 'email':
				icon.setWidth(15);
				icon.setHeight(15);
				icon.path.setD(svgPaths.navEmailPixel);
				icon.path.setStrokeWidth(0);
				icon.path.setFill(LAYOUT_MODEL.navbarColor);
				icon.path.setStroke('none');
				break;
			case 'inquiry':
				icon.setWidth(14);
				icon.setHeight(9);
				icon.path.setD(svgPaths.navCart);
				icon.path.setStrokeWidth(0);
				icon.path.setFill(LAYOUT_MODEL.navbarColor);
				icon.path.setStroke('none');
				break;
			case 'fotomoto':
				icon.setWidth(14);
				icon.setHeight(9);
				icon.path.setD(svgPaths.navCart);
				icon.path.setStrokeWidth(0);
				icon.path.setFill(LAYOUT_MODEL.navbarColor);
				icon.path.setStroke('none');
				break;
			case 'thumbs':
				icon.setWidth(15);
				icon.setHeight(15);
				icon.path.setD(svgPaths.navThumbs);
				icon.path.setStrokeWidth(0);
				icon.path.setFill(LAYOUT_MODEL.navbarColor);
				icon.path.setStroke('none');
				break;
			case 'caption':
				icon.setWidth(15);
				icon.setHeight(15);
				icon.path.setD(svgPaths.navInfo);
				icon.path.setStrokeWidth(0);
				icon.path.setFill(LAYOUT_MODEL.navbarColor);
				icon.path.setStroke('none');
				break;
			case 'fullscreen':
				icon.setWidth(15);
				icon.setHeight(9);
				icon.path.setD(svgPaths.navFullscreen);
				icon.path.setStrokeWidth(0);
				icon.path.setFill(LAYOUT_MODEL.navbarColor);
				icon.path.setStroke('none');
				break;
		}
		icon.addChild(icon.path);
		return icon;
	}

	function layoutBtns() {

		var btns = self.children,
			i,
			length = btns.length;

		tile = new Tile();
		tile.gap = LAYOUT_MODEL.navbarGap + 2;
		tile.axis = 'x';
		tile.align = 'left';
		tile.wrap = true;
		if(tile.axis === 'x') {
			tile.perpLength = btns[0].getHeight();
		} else {
			tile.perpLength = btns[0].getWidth();
		}

		for(i = 0; i < length; i++) {
			if(tile.axis === 'x') {
				if(btns[i].getHeight() > tile.perpLength) {
					tile.perpLength = btns[i].getHeight();
				}
			} else {
				if(btns[i].getWidth() > tile.perpLength) {
					tile.perpLength = btns[i].getWidth();
				}
			}

			tile.addItem(btns[i].getWidth(), btns[i].getHeight());
		}

		tile.layoutItems();

		for(i = 0; i < length; i++) {
			var position = tile.getPosition(i);
			btns[i].setX(position.x);
			btns[i].setY(position.y);
		}

		var bounds = tile.getBounds();
		self.setWidth(bounds.width);
		self.setHeight(bounds.height);
	}

	return self;
}

function PasswordView(vars) {

	var self = new ViewProxy({events:vars.events}),
		passWidth = 296,
		passHeight = 47,
		inputField,
		password,
		background;

	self.updateSpeed = 0;

	self.addEventListener(CHILD_ADDED, function(child) {
		self.build();
		self.updatePosition();
	});

	self.setPassword = function (pass) {
		password = pass;
		inputField.setValue("PASSWORD");
	};

	self.updatePosition = function(e) {
		if(self.updateSpeed === 0) {
			self.setX((layoutCalcs.mediaView.width() - passWidth) * 0.5);
			self.setY((layoutCalcs.mediaView.height() - passHeight) * 0.5);
		} else {
			Tween(self, self.updateSpeed, {x:((layoutCalcs.mediaView.width() - passWidth) * 0.5), y:(layoutCalcs.mediaView.height() - passHeight) * 0.5});
		}
		self.updateSpeed = UPDATE_SPEED
	};

	self.build = function() {
		self.setAlpha(0);
		background = new Sprite();
		background.setBackgroundColor(LAYOUT_MODEL.overlayColor);
		background.setWidth(passWidth);
		background.setHeight(passHeight);

		inputField = new Input();
		inputField.setBackgroundColor(LAYOUT_MODEL.contactFieldRectColor);
		inputField.setFontFamily(LAYOUT_MODEL.contactFont);
		inputField.setFontColor(LAYOUT_MODEL.contactFontColor);
		inputField.setFontSize(14);
		inputField.setWidth(170);
		inputField.setHeight(23);
		inputField.setX(10);
		inputField.setY((passHeight - inputField.getHeight()) * 0.5);
		inputField.setValue("PASSWORD");
		inputField.addEventListener(FOCUS, onInputFieldFocus);
		inputField.addEventListener(BLUR, onInputFieldBlur);

		var submit = new Button();
		submit.setText('Submit');
		submit.setX(inputField.getX() + inputField.getWidth() + 10);
		submit.setY(inputField.getY());
		submit.setFontSize(12);
		submit.setFontFamily(LAYOUT_MODEL.contactFont);
		submit.setFontColor(LAYOUT_MODEL.contactFontColor);
		submit.setBackgroundColor(LAYOUT_MODEL.contactFieldRectColor);
		submit.setPaddingTop(5);
		submit.setPaddingRight(28);
		submit.setPaddingBottom(5);
		submit.setPaddingLeft(28);
		if(TOUCH_DEVICE) {
			submit.addEventListener(TOUCH_END, onSubmit);
		} else {
			stage.addEventListener(KEY_UP, onKeyUp);
			submit.addEventListener(CLICK, onSubmit);
		}

		self.addChild(background);
		self.addChild(inputField);
		self.addChild(submit);

		Tween(self, 0.35, {alpha:1});

	};

	self.kill = function () {
		stage.removeEventListener(KEY_UP, onKeyUp);
	};

	function onKeyUp(e) {
		if(e.keyCode === 13) {
			onSubmit(e);
		}
	}

	function onSubmit(e) {
		if(password !== inputField.getValue()) {
			inputField.blur();
		} else {
			self.events.dispatchEvent(AUTHENTICATED);
		}
	}

	function onInputFieldBlur(e) {
		if(inputField.getValue() === "") {
			inputField.setValue("PASSWORD");
		} else if(password !== inputField.getValue()) {
			inputField.setValue("INCORRECT PASSWORD");
		}
	}

	function onInputFieldFocus(e) {
		inputField.setValue("");
	}

	return self;
}

function IntroView(vars) {

	var self = new ViewProxy({events:vars.events}),
		duration = 1400,
		parentView = vars.parentView,
		background,
		image,
		updateSpeed = 0.35;

	self.updatePosition = function(e) {
		background.setWidth(stage.getWidth());
		background.setHeight(stage.getHeight());
		if(image) Tween(image, updateSpeed, {x:(stage.getWidth() - image.getWidth()) * 0.5 , y:(stage.getHeight() - image.getHeight()) * 0.5});
	};

	init();

	function init() {
		self.setZIndex(100);

		parentView.addChild(self);
		buildBackground();
		loadFile();
	}

	function buildBackground() {
		background = new Sprite();
		background.setBackgroundColor(LAYOUT_MODEL.siteBackgroundColor);
		background.setWidth(stage.getWidth());
		background.setHeight(stage.getHeight());
		self.addChild(background);

	}

	function loadFile() {
		if(LAYOUT_MODEL.introFile !== '') {
			image = new Bitmap();
			image.setAlpha(0);
			image.setSrc(REWRITE_BASE + MEDIA_ORIGINAL + LAYOUT_MODEL.introFile);
			image.addEventListener(LOAD, imageLoaded);
		} else {
			Tween(self, 0.7, {alpha:0, onComplete: function (){
				parentView.removeChild(self);
				self.events.dispatchEvent(INTRO_COMPLETE);
			}});
		}
	}

	function fade() {
		setTimeout(function() {
			Tween(image, 1, {alpha:0, onComplete:function() {
				Tween(self, 0.7, {alpha:0, onComplete: function (){
					parentView.removeChild(self);
					self.events.dispatchEvent(INTRO_COMPLETE);
				}});
			}});
		}, duration);
	}

	function imageLoaded(e) {
		/*show at 1/2 size if retina file provided*/
		if(LAYOUT_MODEL.introFile.toLowerCase().search('@2x') > -1) {
			image.setWidth(image.getWidth() * 0.5);
			image.setHeight(image.getHeight() * 0.5);
		}

		if(image.getWidth() > stage.getWidth() || image.getHeight() > stage.getHeight()) {
			var scale = new Scale();
			scale.setWidth(image.getWidth());
			scale.setHeight(image.getHeight());
			scale.setHRange(stage.getWidth());
			scale.setVRange(stage.getHeight());
			scale.setType('native');
			var imageWidth = Mth.round(scale.getWidth());
			var imageHeight = Mth.round(scale.getHeight());
			image.setWidth(imageWidth);
			image.setHeight(imageHeight);
		}

		image.setX((stage.getWidth() - image.getWidth()) * 0.5);
		image.setY((stage.getHeight() - image.getHeight()) * 0.5);
		self.addChild(image);
		setTimeout(function() {
			Tween(image, 1, {alpha:1, onComplete:fade});
		}, 1000);
	}

	return self;
}

 this.tween = Tween;
} window.dx = new DesignX();
