@font-face {
	font-family: 'Source Sans Pro ExtraLight';
	font-style: normal;
	font-weight: normal;
	src: url('/fonts/SourceSansProExtraLight.woff') format('woff'),
	url('/fonts/SourceSansProExtraLight.ttf') format('truetype');
}
@font-face {
	font-family: 'Raleway-Thin';
	font-style: normal;
	font-weight: normal;
	src: url('/fonts/Raleway-Thin.woff') format('woff'),
	url('/fonts/Raleway-Thin.ttf') format('truetype');
}
@font-face {
	font-family: 'Signika Negative Light';
	font-style: normal;
	font-weight: normal;
	src: url('/fonts/SignikaNegativeLight.woff') format('woff'),
	url('/fonts/SignikaNegativeLight.ttf') format('truetype');
}
@font-face {
	font-family: 'Varela';
	font-style: normal;
	font-weight: normal;
	src: url('/fonts/Varela.woff') format('woff'),
	url('/fonts/Varela.ttf') format('truetype');
}
@font-face {
	font-family: 'Questrial';
	font-style: normal;
	font-weight: normal;
	src: url('/fonts/Questrial.woff') format('woff'),
	url('/fonts/Questrial.ttf') format('truetype');
}
@font-face {
	font-family: 'pixel';
	font-style: normal;
	font-weight: normal;
	src: url('../../fonts/pixel.woff') format('woff'),
	url('../../fonts/pixel.ttf') format('truetype');
}

* {
	margin: 0;
	padding: 0;
	border:none;
}

html, body {
	width:      100%;
	height:     100%;
	overflow:   hidden;
}

body {
	text-rendering: 'optimizeLegibility';
	font-family: 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif;
}

cite, em, var, address {
	font-style: normal;
}

input:focus, select:focus, textarea:focus, button:focus {
	outline: none;
}

textarea {
	resize: none;
}

.image, .video {
	width: 80%;
	height: 80%;
}

#idSiteMeterHREF {
	position: absolute;
	height: 0;
	width: 0;
}

body > iframe {
	position: absolute;
	height: 0;
	width: 0;
}

.dx_page_text {
	white-space:pre-wrap;
}

.dx_page_text ul{
	padding-left: 25px;
}

body {
	background-color:#191919;
	color:#7A7A7A;
}
a {
	display:inline-block;
	color:#7A7A7A;
}
