var YaoApp = Ember.Application.create({
  LOG_TRANSITIONS: true
});

/****************
*   Router      *
*****************/
YaoApp.Router.map(function() {
  this.resource('portfolio', {path: "/portfolio"}, function() {
    this.route('wedding');
    this.route('people');
    this.route('places');
  });
  this.resource('info', {path: "/info"}, function() {
    this.route('about');
    this.route('contact');
  });
});

/****************
*   Routes      *
*****************/
YaoApp.ApplicationRoute = Em.Route.extend({
  model: function() {
      return this.store.all('application');
  }
});

YaoApp.IndexRoute = Ember.Route.extend({
  model: function() {
    return ['red', 'yellow', 'blue'];
  }
});

YaoApp.WeddingRoute = Em.Route.extend({
  model: function() {
      return this.store.all('wedding');
  }
});


YaoApp.InfoRoute = Ember.Route.extend({});

/****************
*  Controllers  *
*****************/
YaoApp.IndexController = Ember.Controller.extend({
  header: "Eric Yao",
  sidebar: {
    profolio: "PROFOLIO",
    about: "INFO"
  }
});


YaoApp.InfoController = Ember.Controller.extend({});

YaoApp.WeddingController = Em.ArrayController.extend({});



/****************
*Views Components*
*****************/
YaoApp.ApplicationView = Em.View.extend({
  classNames: ['wrapper']
});

YaoApp.IndexView = Em.View.extend({
  tagName: 'div',
  // classNames: [],
  didInsertElement: function() {
    
  }
});

YaoApp.PortfolioMenuBarView = Em.View.extend({
  tagName: 'li'
});

YaoApp.InfoMenuBarView = Em.View.extend({
  tagName: 'li'
});


/****************
*  Models       *
*****************/
YaoApp.ApplicationModel = DS.Model.extend({
  backgroundImage: DS.attr('string')
});


/********************
*   Data Store      *
*********************/
YaoApp.ApplicationAdapter = DS.FixtureAdapter.extend({});

YaoApp.Application.FIXTURES = [
  {
    id: 1,
    backgroundImage: "https://dl.dropboxusercontent.com/u/15863535/Italy2014-1.jpg"
  },
  {
    id: 2,
    backgroundImage: "https://dl.dropboxusercontent.com/u/15863535/Italy2014-2.jpg"
  },
  {
    id: 3,
    backgroundImage: "https://dl.dropboxusercontent.com/u/15863535/Italy2014-3.jpg"
  },
  {
    id: 4,
    backgroundImage: "https://dl.dropboxusercontent.com/u/15863535/Italy2014-4.jpg"
  }
];





























